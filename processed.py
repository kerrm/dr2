import os
from os.path import join,isfile,realpath,abspath,relpath
from collections import defaultdict,deque
import cPickle

import coadd
import util
import alias
import band

""" A module for the results of a pipeline run.

Use cases include assembling diagnostic plots and other analyses that
required "awareness" beyond a single observation.

The code is, like many other bits, based on a persistent object (the ProcDB)
which houses ProcObs objects.  The user can then query the DB for obs.
satisfying particular criteria and recover much of the information used to
process them."""

class ProcObs(object):
    """ Encapsulate a processed observation.
    """

    def __init__(self,obs,obsscr,obsplots,cal,calscr,calplots):
        self.obs = obs
        self.obsscr = obsscr
        self.obsplots = obsplots
        self.cal = cal
        self.calscr = calscr
        self.calplots = calplots

    def keys(self):
        """ Return keys for filing in database."""
        jname = alias.get_aliases(self.obs['name'].rstrip('_R'))[0]
        return jname,self.obs.get_combo(),self.obs.basename

    def __cmp__(self,other):
        mjd1 = self.obs.get_mjd()
        mjd2 = other.obs.get_mjd()
        if mjd1 < mjd2:
            return -1
        if mjd2 > mjd1:
            return 1
        return 0

    def get_toa(self,require_valid=True):
        """ Return the TOA file resulting from the obs. """
        if require_valid and (not self.obs.valid_toa()):
            return None
        profile = self.obsscr.get_target(self.obs)
        return profile + '.toa'

    def get_toa_string(self,localize=False,write=False,invint=False):
        """ Return a TOA string, incorporating the "pat" TOA and flags.
        
        localize -- if True, replace the profile name with basename
        write -- if True, write out the .toa file
        """
        obs = self.obs
        if invint:
            if self.obsscr.has_invariant_interval():
                toa_file = self.obsscr.get_invariant_interval(obs)+'.toa'
            else:
                return None
        else:
            toa_file = self.obsscr.get_target(obs)+'.toa'
        try:
            an = cPickle.load(file(toa_file+'.analysis'))
        except IOError:
            return
        # temporary code to handle old pickles
        if not an.has_pat_toa():
            toa = an.get_pat_toa() 
            cPickle.dump(an,file(toa_file+'.analysis','w'),protocol=2)
        # end temporary code
        toa = an.get_pat_toa(clobber=False)
        if toa is None:
            print '%s TOA is none'%(obs.basename)
            return
        if localize:
            toks = toa.split()
            profile = os.path.basename(toks[0])
            toa = os.path.basename(toks[0]) + toa.split(toks[0])[1]
        # add in data quality and delay flags
        delay_flags = ' '.join(('-%s 1'%f for f in obs.delay_flags.keys()))
        toa_flags = ' '.join(obs.get_toa_flags(add_info=True))
        analysis_flags = an.get_toa_flags()
        toa = '%s %s %s %s'%(toa,delay_flags,toa_flags,analysis_flags)
        if write:
            file(toa_file,'w').write(toa)
        return toa

    def get_profile(self):
        """ Return the profile (fully scrunched) resulting from the obs."""
        return self.obsscr.get_target(self.obs)

    def get_poln(self):
        """ Return the most scrunched polarization profile."""
        return self.obsscr.get_scrunched_poln(self.obs)

    def get_scrunched(self,T=False,F=False,P=True):
        return self.obsscr.get_scrunched(self.obs,T=T,F=F,P=P)
        
class ProcDB(object):
    """ Keep a record of processed observations.

    Primarily for use in generating output post facto, e.g. HTML.

    Sort results by pulsar name.
    """

    def __init__(self):
        self.jnames = defaultdict(dict)

    def __setstate__(self,state):
        """ Discard some erroneous data.
        
        With the move to allow invalid files to propagate through the
        pipeline, a few annoyances have crept in.  In particular, files with
        the obs mode set incorrectly yield _R names.  Therefore, apply a
        few quick fixes here.
        """
        jnames = state['jnames'].keys()
        for jname in jnames:
            if jname.endswith('_R'):
                state['jnames'].pop(jname)
                continue
            if alias.get_aliases(jname)[0] != jname:
                state['jnames'].pop(jname)
                continue
        self.__dict__.update(state)

    def get_jnames(self):
        """ Return all pulsar names in the database.
        """
        return sorted(self.jnames.keys())

    def select_jname(self,jname):
        """ Delete all entries without specified jname.
        
        Why would we do this?  Some early data have incorrect metadata and
        thus a one-to-one mapping of pulsars to database can be foiled.
        
        Calling this method in some outer loop helps ameliorate that!
        """
        if jname not in self.jnames.keys():
            print 'Disregarding select command as %s is not in database.'%(
                jname)
        for key in self.jnames.keys():
            if key != jname:
                self.jnames.pop(key)

    def add_procdb(self,other):
        """ Add contents of another procdb.

        Currently, only case supported is if the dbs have completely
        discrete entries, i.e. different pulsars.
        """
        for jname in other.jnames.keys():
            if jname in self.jnames.keys():
                raise KeyError('Duplicate entries not allowed!')
            self.jnames[jname] = other.jnames[jname]

    def add_analysis(self,analysis):
        """ Incorporate all observations from the analysis object."""
        if analysis.empty:
            return
        jnames = defaultdict(lambda: defaultdict(dict))
        jnames_combos = defaultdict(set)
        combos = set()
        for psr,cal in zip(analysis.psrobs,analysis.cals):
            procobs = ProcObs(
                psr,analysis._pscr,analysis.psr_plots,
                cal,analysis._cscr,analysis.cal_plots)
            jname,combo,obsname = procobs.keys()
            # sort procobs objects by jname and combo
            jnames[jname][combo][obsname] = procobs
            # keep unique combos for each pulsar name
            jnames_combos[jname].add(combo)

        # now, add these into the main database
        for jname in jnames.keys():
            src = jnames[jname]
            dst = self.jnames[jname]
            combos = jnames_combos[jname]
            # add combos to list
            try:
                dst['combos'].update(combos)
            except KeyError:
                dst['combos'] = combos
            # add obs for each combo
            for combo in combos:
                try:
                    dst[combo].update(src[combo])
                except KeyError:
                    dst[combo] = src[combo]
            dst['ephemeris'] = analysis.gather_ephemeris_string(jname)

    def flag_new_obs(self,db):
        """ Given a database, return an array of booleans set to True if
            the observation does not appear in this database.
        """
        basenames = deque()
        for jname in self.get_jnames():
            t = self.jnames[jname]
            for combo in t['combos']:
                basenames.extend(t[combo].keys())
        basenames = set(basenames)
        for obs in db.obs:
            obs.new = obs.basename not in basenames

    def get_obs(self,jname,combo=None,filter_valid=False,
        combo_is_regexp=False):
        """ Return all the observations for a given pulsar.
        
        If "combo" is specified, will return only observations matching.

        Combo should be an underscore-delimited string, typically of the
        format BAND_FRONTEND_BACKEND, e.g. 20CM_MULTI_DFB3.  If only part
        of a combo is specified, BAND_FRONTEND, all combos matching
        BAND_FRONTEND* will be returned.  Matching only done from the front.

        If combo_is_regexp==True, then combo matching will proceed via
        regularly expression.

        Optionally, filter out observations that are not valid.
        """
        rvals = deque()
        try:
            t = self.jnames[jname]
            if combo is None:
                for c in t['combos']:
                    rvals.extend(t[c].itervalues())
            else:
                if combo_is_regexp:
                    matches = util.regexp_filter(combo,t['combos'])
                    for c in matches:
                        rvals.extend(t[c].itervalues())
                else:
                    for c in t['combos']:
                        if c.startswith(combo):
                            rvals.extend(t[c].itervalues())
        except KeyError:
            pass
        if filter_valid:
            return filter(lambda p: p.obs.valid(), rvals)
        return list(rvals)

    def get_coadded(self,jname,combo='20CM',clobber=False):
        """ Return the filename of a co-added profile."""
        # TODO -- don't regenerate file if no new data (and !clobber)
        outpath = self.get_outpath(jname)
        output = coadd.new_coadd(self,jname,combo=combo,
            outpath=outpath,retain_freq=True,clobber=clobber)
        return output or ''

    def get_coadded_work(self,jname):
        """ Generate jobs to make coadded profiles.

            Do the basic ones for one -- 10cm and 20cm."""
        import mpi
        bands = ['10CM','20CM']
        work = [mpi.MPI_GenerateCoadd(self,jname,b) for b in bands]
        return work

    def get_residuals_work(self,jname,combo,target,clobber=False,
        strict_select=False):
        """ Generate a job to make a TOA residuals plot."""
        import mpi
        return mpi.MPI_GenerateTOAResidualsPlot(self,jname,combo,target,
            clobber=clobber,strict_select=strict_select)

    def get_combos(self,jname):
        """ Return available combos for a given pulsar."""
        try:
            return list(self.jnames[jname]['combos'])
        except KeyError:
            return []

    def get_ephemeris(self,jname):
        try:
            return self.jnames[jname]['ephemeris']
        except KeyError:
            return None

    def get_outpath(self,jname):
        """ TODO -- currently each observation COULD have a different path.
        In practice, they are all the same, but need to come up with a more
        coherent idea of "grouped output".
        """
        try:
            return self.get_obs(jname)[0].obsscr.outpath
        except IndexError:
            return None

    def delete_complement(self,jnames):
        """ Delete entries that are not in provided list."""
        for jname in self.get_jnames():
            if jname not in jnames:
                self.jnames.pop(jname)

    def get_by_basename(self,basename):
        """ Find a ProcObs by the observation basename."""
        for jname in self.get_jnames():
            t = self.jnames[jname]
            for combo in t['combos']:
                if basename in t[combo].keys():
                    return t[combo][basename]

    def get_eph_sel(self,jname,exclude_sql_flags=True,strict=True):
        """ Return the ephemeris and select file for the given pulsar.

        Note return type is two strings, each of which can be written out
        to a .par and .select file if desired.
        """
        procobs = self.get_obs(jname)
        if len(procobs) == 0:
            return

        # ephemeris
        eph = self.get_ephemeris(jname) # NB a string

        # flags
        delay_flags,toa_flags = get_toa_flags(procobs)

        # add in band jumps
        for band in [20,40,50]:
            s = 'JUMP -B %dCM'%band
            if s in eph:
                continue
            eph +='\n%s 0 0'%s

        # form "JUMP"s from delay flags and add to ephemeris
        # bit of a kluge -- we want to overwrite any existing JUMPs
        eph_lines = eph.split('\n')
        for k in sorted(delay_flags.keys()):
            flag = delay_flags[k]
            already_here = False
            for iline,line in enumerate(eph_lines):
                if k in line:
                    # overwrite existing flag
                    eph_lines[iline] = 'JUMP -%s 1 %.10f'%(k,flag)
                    already_here = True
                    break
            if not already_here:
                eph_lines.append('JUMP -%s 1 %.10f'%(k,flag))
        eph = '\n'.join(eph_lines)
        #eph +=  '\n'.join(('JUMP -%s 1 %.10f'%(
        #    k,delay_flags[k]) for k in sorted(delay_flags.keys())))

        # make a "select file" from the toa_flags
        select = '\n'.join(('%sLOGIC %s = %s REJECT'%('#' if 'ppta_sql' in f else '',f.split()[0],f.split()[1]) for f in sorted(toa_flags)))

        # exclude TOAs at high freqs and very short files
        select += '\n'.join(('', # get a newline
            'PROCESS freqpass 600 800',
            'PROCESS freqpass 1200 1500',
            'PROCESS freqpass 2800 3200',
        ))
        if strict:
            select += '\n'.join(('', # get a newline
                'LOGIC -length < 300 REJECT',
                'LOGIC -snr < 15 REJECT'
        ))

        # reject a few nonstandard receivers
        for rcvr in ['DRAO','MARS','13MM','GALILEO']:
            select += '\nPROCESS -fe %s REJECT'%rcvr

        return eph,select

    def get_toas(self,jname,combo=None,localize=False,invint=False):
        """ Return TOA strings for specified pulsar/combo."""
        procobs = sorted(self.get_obs(jname,combo=combo))
        kwargs = dict(localize=localize,invint=invint)
        tim = 'FORMAT 1\n'+'\n'.join(filter(None,
            (p.get_toa_string(**kwargs) for p in procobs)))
        return tim

    def get_toa_tarball(self,jname,invint=False,strict_select=True):
        """ Tarball up profiles, ephemeris, "select file", and TOAs.
        
        The profiles are defined as the "most scrunched" output product.

        The ephemeris, "select file", and toas are produced by calling
        gather_ephemeris, gather_select, and gather_toas.
        """

        outpath = self.get_outpath(jname)
        if outpath is None:
            # this handles case when there are NO observations for pulsar
            return
        outpath = realpath(abspath(outpath)) 

        # get and write out TOA strings -- do this always
        toas = self.get_toas(jname,localize=True)
        toafile = join(outpath,'%s.toa'%jname)
        file(toafile,'w').write(toas)

        if invint:
            invint_toas = self.get_toas(jname,localize=True,invint=invint)
            invint_toafile = join(outpath,'%s.invint.toa'%jname)
            file(invint_toafile,'w').write(invint_toas)

        # get ephemeris and select file
        try:
            eph,select = self.get_eph_sel(jname,strict=strict_select)
        except TypeError:
            # triggered if return value is None
            return

        # write out ephemeris and select file to outpath
        ephfile = join(outpath,'%s.par'%jname)
        selectfile = join(outpath,'%s.select'%jname)
        file(ephfile,'w').write(eph)
        file(selectfile,'w').write(select)

        # identify the profiles
        procobs = self.get_obs(jname)
        profiles = [realpath(abspath(p.get_profile())) for p in procobs]

        # finally, collect the files in a metafile
        files = profiles + [ephfile,selectfile,toafile]
        manifest = util.TemporaryFile(
            '\n'.join((relpath(f,outpath) for f in files)))

        output = '%s.tar.gz'%(jname)
        cwd = os.getcwd()
        # try/finally to ensure we always get back to correct working dir
        try:
            os.chdir(outpath)
            cmd = 'tar -cvzf %s --files-from=%s'%(output,str(manifest))
            util.run(cmd,echo=False)
        except Exception as e:
            print 'In get_toa_tarball encountered exception %s.'%e
        finally:
            os.chdir(cwd)

    def get_overlaps(self,procobs,min_overlap=0):
        """ Return all overlapping for the given Observation."""
        obs = procobs.obs
        combo = obs.get_combo(exclude_backend=True)
        all_procobs = self.get_obs(obs['name'],combo=combo)
        matches = deque()
        for p in all_procobs:
            if obs.overlap(p.obs) > min_overlap:
                matches.append(p)
        return matches


def get_toa_flags(procobs):
    """ Helpful free function to get the delay flags and TOA/data quality
    flags associated with a set of procobs.
    """
    delay_flags = dict()
    toa_flags = set()
    for obs in (x.obs for x in procobs):
        delay_flags.update(obs.delay_flags)
        toa_flags.update(obs.get_toa_flags(add_info=False))
    return delay_flags,toa_flags

    #jumps = ['JUMP -%s 1 %.10f'%(k,flags[k]) for k in sorted(flags.keys())]
    #return '\n'.join(jumps)
            
def do_dms(procdb,jnames,bands=['10cm','20cm'],output=None):
    """ Estimate in-band DMs for the observations belong to jnames."""

    results = dict()
    allowed_bands = map(band.Band,bands)

    for jname in jnames:
        results[jname] = dict()
        ephemeris = procdb.get_ephemeris(jname)
        if ephemeris is None:
            print 'No ephemeris for %s DM computation.'%(jname)
            continue
        ephemeris = util.TemporaryFile(ephemeris)
        all_procobs = procdb.get_obs(jname)
        tmp = util.TemporaryFile()
        for procobs in all_procobs:

            obs = procobs.obs
            myband = band.Band(obs)
            band_ok = False
            for b in allowed_bands:
                if myband == b:
                    band_ok = True
                    break
            if not band_ok:
                continue
            
            # use TOA to get template
            toa = procobs.get_toa()
            if toa is None:
                continue
            try:
                toa_analysis = cPickle.load(file(toa+'.analysis'))
                template = toa_analysis.template
            except IOError:
                print 'no template for %s/%s'%(jname,obs.basename)
                continue

            fname = procobs.get_scrunched(F=True)
            
            cmd = 'pat -j"T","p","fscrunch 8" -s %s -f tempo2 %s > %s'%(
                template,fname,str(tmp))
            rc,stdout,stderr = util.run(cmd,echo=False)
            cmd = 'tempo2 -f %s %s -nofit -fit dm -nits 2 | grep "DM (cm"'%(
                ephemeris,str(tmp))
            rc,stdout,stderr = util.run(cmd,echo=False)
            lines = [x for x in stdout.split('\n') if 'DM' in x]
            if (len(lines) != 1):
                continue
            toks = lines[0].split()
            dm,dme = float(toks[4]),float(toks[5])

            obs = procobs.obs
            results[jname][obs.basename] = [
                dm,dme,obs.get_mjd(),band.Band(obs).cm()]
    if output is not None:
        cPickle.dump(results,file(output,'w'),protocol=2)
    return results

def do_all_dms(procdb,clobber=False):
    jnames = procdb.get_jnames()
    for jname in jnames:
        print 'Working on %s.'%jname
        outpath = procdb.get_outpath(jname)
        output = '%s/%s_dms.pickle'%(outpath,jname)
        if (not clobber) and isfile(output):
            continue
        do_dms(procdb,[jname],output=output)
