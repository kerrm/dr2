"""
Parse a corrections file into the appropriate classes.

$Header: /nfs/slac/g/glast/ground/cvs/users/kerrm/tpipe/timing.py,v 1.14 2014/01/21 06:46:59 kerrm Exp $

author: Matthew Kerr <matthew.kerr@gmail.com>
"""

from collections import deque

from observation import recognized_keys
from corr import *

# NB order is important here because we don't do greedy parsing
separators = Criterion.operators + Action.operators

def is_binary_op(line):
    return find_separator(line) is not None

def is_keyword(tok):
    tok = tok.split(',')[0]
    return tok in recognized_keys

def tokenize(line,sep):
    return ''.join(line.split()).split(sep) 

def find_separator(line):
    seps = filter(lambda x: x in line,separators)
    return None if len(seps)==0 else seps[0]

def parse_line(line):

    # check for spectial functions
    if (line[0] == '?') or (line[0] == '!'):
        # instantiate functor
        toks = line.split()
        if len(toks) > 0:
            args = toks[1:]
        else:
            args = ()
        return eval(toks[0][1:])(*args)

    if line[0] == '@':
        return flag_data(line[1:])

    # primitive check or assignment
    sep = find_separator(line) 
    if sep is None:
        raise ValueError('Could not find binary operator in %s.'%line)

    toks = tokenize(line,sep)
    if sep in Criterion.operators:
        if is_keyword(toks[0]):
            if toks[1][0]=='(' and toks[1][-1]==')':
                # a regular expression
                if sep != '==':
                    raise ValueError(
                        'Can only use == with regular expressions.')
                return check_keyword_re(toks[0],toks[1][1:-1])
            return check_keyword(toks[0].split(','),toks[1].split(','),sep)
        elif toks[0]=='extension':
            if sep != '==':
                raise ValueError(
                    'Only check for equality of file extensions!')
            return check_extension(toks[-1])
        else:
            raise ValueError('Did not recognize %s as criterion.'%(toks[0]))
    if sep in Action.operators:
        if is_keyword(toks[0]):
            return change_keyword(toks[0],toks[1],sep)
        elif toks[0]=='delay':
            return fix_delay(toks[1],sep)
        elif toks[0]=='toa_delay':
            return fix_delay_toa(toks[1],sep)

    raise ValueError('Could not parse %s.'%line)

def parse_group(group,verbosity=1):
    description = group[0]
    objs = map(parse_line,group[1:])
    criteria = filter(lambda x: isinstance(x,Criterion),objs)
    actions = filter(lambda x: isinstance(x,Action),objs)

    # check for delay and add an additional criterion to exclude cals
    if len(filter(lambda x: isinstance(x,fix_delay),actions)):
        criteria.append(check_keyword('OBS_MODE','PSR'))

    # add verbosity to Action objects
    for action in actions:
        action.verbosity = verbosity

    return Correction(description,criteria,actions)

def parse_file(fname,verbosity=1):

    # first, parse file into individual corrections
    groups = deque()
    #f = lambda x: (len(x) > 0) and (x[0] != '#')
    #lines = filter(f,map(str.strip,file(fname).readlines()))
    lines = readlines(fname)
    for line in lines:
        if line[0] == '#':
            # comment
            continue
        if line[0] == '*':
            # start of a correction
            groups.append(deque())
            groups[-1].append(line)
            continue
        groups[-1].append(line)
    groups = map(list,groups)

    # now, parse each group into a Correction object
    return [parse_group(g,verbosity=verbosity) for g in groups]

