import matplotlib
matplotlib.use('Agg')

import os
import itertools
from os.path import join,isfile,abspath,expandvars,basename,isdir
import cPickle

import mpi
import marsfield
import html
import timing
import util
import processed
import ephemeris
import template
import alias
import offsets

CLOBBER_DELAYS = False

plist = 'pulsarlists/ppta_pulsars.asc'
jnames = sorted(map(alias.preferred_name,util.readlines(plist)))
#jnames = ['J1022+1001','J1939+2134']
#jnames = ['J1909-3744','J1939+2134']
#jnames = ['J1017-7156','J1744-1134','J2241-5236']
#jnames.extend(['J0711-6830','J1603-7202','J2129-5721','J1545-4550'])
#jnames = ['J1603-7202','J2129-5721','J0613-0200','J1600-3053']
#jnames.extend(['J1909-3744','J1713+0747','J1744-1134','J1022+1001','J1939+2134'])

outpath = '/u/ker14a/newt/ppta/test'
directories = marsfield.get_directories(['DFB'])
#jnames = ['HYDRA_N','HYDRA_S','HYDRA_O']

from mpi4py import MPI
comm = MPI.COMM_WORLD

def load_procdb(jname):
    tstart = MPI.Wtime()
    procdb_fname = join(outpath,jname,'procdb.pickle')
    #if CLOBBER_PROCDB or (not isfile(procdb_fname)):
    #    procdb = processed.ProcDB()
    #else:
    procdb = cPickle.load(file(procdb_fname))
    return MPI.Wtime()-tstart,procdb

if comm.rank == 0:

    tstart = MPI.Wtime()

    for jname in jnames:

        print 'Working on %s'%jname

        tprocdb,procdb = load_procdb(jname)

        delay_path = join(outpath,jname,'delays')
        if not os.path.isdir(delay_path):
            os.mkdir(delay_path)

        t1 = MPI.Wtime()

        # only do 10cm/20cm PDFB2-4 combos for now
        pairs = offsets.make_delays(jname,procdb,pairs_only=True,
            combo='[1,2]0CM_.*_PDFB[2-4]',combo_is_regexp=True,
            cals_only=False,cals_too=False)
        # get rid of DFB1
        pairs = [x for x in pairs if x[1]['backend'] != 'PDFB1']
        work = [mpi.MPI_Delays_Pickle(p[0],p[1],delay_path,
            clobber=CLOBBER_DELAYS,use_new=True) for p in pairs]

        t2 = MPI.Wtime()

        manager = mpi.MPI_Jobs(work,verbosity=1)
        manager.do_work(kill_threads=False)

        t3 = MPI.Wtime()

        #delays = [x.delay for x in manager.done]
        #cPickle.dump(delays,
        #    file(join(outpath,jname,'%s_delays.pickle'%jname),'w'),
        #    protocol=2)

        print '%s database elapsed time: %.2f minutes.'%(jname,(t2-t1)/60)
        print '%s total work wall time: %.2f minutes.'%(jname,(t3-t2)/60)

    manager.kill_threads()

else:
    worker = mpi.MPI_Worker()
    worker.do_work()

