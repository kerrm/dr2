# Phase offsets are not well defined during CASPSR commissioning.

# Flag all data prior to Nov 5, 2010 (MJD 55505).

* Apply caspsr_commissioning flag to CASPSR data taken before MJD 55505.
backend==CASPSR
mjd<55505
@caspsr_commissioning
