#!/usr/bin/python
import sys,os,datetime,re
import copy
try:
	sys.path.append(sys.path[0])
	import fits,psrfits
except ImportError:
	print "Could not find MJK's fits and psrfits modules."
	sys.exit(255)


def printhelp():
	print "%s [options] psrfits.file"%sys.argv[0]
	print ""
	print "Corrects the psrfits header start time using the latest"
	print "correction files."
	print "\nOptions\n=======\n"
	print "-h        Print this help"
	print ""
	print "-e [ext]  Output with this extention"
	print "-o [file] Output to this file"
	print "-m        Modify file in place"
	print ""
	print "-d [dir]  Load corrections from specified dir"
	print "-v        Verbose mode"
	print ""
	print "Note: -m option will only work if there is room in the"
	print "history table to add to it without resizing"
	sys.exit(0)

class correction:
	def __init__(self,firmware,beconfig,mjd,tbin,freq,bename):

		self.firmware=firmware
		self.mjd=mjd
		self.beconfig=beconfig
		self.tbin=tbin
		self.freq=freq
		self.bename=bename
		self.verbose=0
		self.prop_printed=0
		self.corrections=list()



	def parse(self, lines):
		if self.verbose and self.prop_printed==0:
			print "Properties read from file are:"
			print "MJD: '%s'\nBECONFIG: '%s'\nBENAME: '%s'\nTBIN: '%s'\nFIRMWARE: '%s'\nFREQ: '%s'"%(self.mjd,self.beconfig,self.bename,self.tbin,self.firmware,self.freq)
			self.prop_printed=1
		name="VOID"
		skip=1
		delay=0
		for line in lines:
			line = line.split("#")[0]
			elems=line.split()
			if len(elems) > 0 and elems[0]=="*":
				name=line.strip("* \t\n")
				delay=0
				skip=0
				continue
			if skip==0:
				if len(elems) > 2  and elems[1]=="~=":
					val=str(getattr(self, elems[0]))
					if re.match(elems[2],val)==None:
						# match failed
						skip=1
					continue
				if len(elems) > 2  and elems[1]=="!~=":
					val=str(getattr(self, elems[0]))
					if re.match(elems[2],val)!=None:
						# match failed
						skip=1
					continue
				if len(elems) > 2  and elems[1]=="<":
					val=float(getattr(self, elems[0]))
					if not val < float(elems[2]):
						# match failed
						skip=1
					continue
				if len(elems) > 2  and elems[1]==">":
					val=float(getattr(self, elems[0]))
					if not val > float(elems[2]):
						# match failed
						skip=1
					continue
				if len(elems) > 2  and elems[1]=="<=":
					val=float(getattr(self, elems[0]))
					if not val <= float(elems[2]):
						# match failed
						skip=1
					continue
				if len(elems) > 2  and elems[1]==">=":
					val=float(getattr(self, elems[0]))
					if not ival >= float(elems[2]):
						# match failed
						skip=1
					continue
				if len(elems) > 2 and elems[0] == "delay" and \
						(elems[1]=="=" or elems[1]=="+=" or elems[1]=="-="):
					conv=1
					val=float(elems[2])
					if len(elems) > 3:
						if elems[3]=="ms":
							conv=1e-3
						elif elems[3]=="us":
							conv=1e-6
						elif elems[3]=="ns":
							conv=1e-9
						else:
							conv=float(getattr(self, elems[3]))
					if self.verbose:
						print "*** Rule %s matches"%name

					if elems[1]=="=":
						self.corrections = list()
						delay=val*conv
						if self.verbose:
							print "*** NOTE: This rule replaces previous rules"
							print "*** delay set to %g s"%delay
					elif elems[1]=="+=":
						delay+=val*conv
						if self.verbose:
							print "*** delay incremented by %g s"%delay
					elif elems[1]=="-=":
						delay-=val*conv
						if self.verbose:
							print "*** delay decremented by %g s"%delay
						print ""
					c=dict()
					c['corr']=delay
					c['msg']=name
					c['val']=val
					c['conv']=conv
					self.corrections.append(c)
					delay=0
					continue
				if len(line.strip()) > 0:
					print "Warning: Cannot understand line in delay file:"
					print "'%s'"%line.strip()

# corrections
def getcorrection(mainhdr,freq,history,correctionfiles,verbose=0):
	# Parse some useful values...
	mjdobs=float(mainhdr.get("STT_IMJD").val)
	mjdobs+=float(mainhdr.get("STT_SMJD").val)/86400.0
	beconfig=mainhdr.get("BECONFIG").val.strip(" '")
	tbin = history.entries[0]['TBIN']
	bename = mainhdr.get('BACKEND').val.strip(" '")
	firmware = history.entries[0]['PROC_CMD'].strip()

	# Now see if we match any known delays

	corr = correction(firmware,beconfig,mjdobs,tbin,freq,bename)
	corr.verbose=verbose
	for file in correctionfiles:
		f = open(file)
		corr.parse(f.readlines())


	if verbose:
		for c in corr.corrections:
			name=c['msg']
			val=c['val']
			conv=c['conv']
			print "*** Applying correction '%s'"%name
			if conv != 1:
				print "*** Value = %g * %g s"%(val,conv)
			else:
				print "*** Value = %g s"%(val)



	return corr.corrections





#default options
modify=0
ext=None
confdirs=list()
confdir = sys.path[0]
infiles=list()
outfile=""
verbose=0
#Parse arguments
a=1
while a < len(sys.argv):
	arg=sys.argv[a]
	if arg.startswith("--"):
		if arg=="--output":
			a+=1
			outfile=sys.argv[a]
		if arg=="--modify":
			modify=1
		if arg=="--configdir":
			a+=1
			confdir = sys.argv[a]
	elif arg.startswith("-"):
		if arg.find("m") > 0:
			modify=1
		if arg.find("o") > 0:
			a+=1
			outfile=sys.argv[a]
		if arg.find("v") > 0:
			print "Verbose mode enabled"
			verbose=1
		if arg.find("e") > 0:
			a+=1
			ext=sys.argv[a]
		if arg.find("d") > 0:
			a+=1
			confdir = sys.argv[a]
		if arg.find("h") > 0:
			printhelp()
	else:
		infiles.append(arg)
	a+=1

if len(infiles) == 0:
	printhelp()

if len(infiles) > 1 and outfile != "":
	print "Can't operate on multiple files with -o option"
	printhelp()



cl=list()
conffiles=list()
dir=confdir
for f in os.listdir(dir):
	if f.endswith(".dlycfg"):
		if not os.path.basename(f) in cl:
			conffiles.append(dir+"/"+f)
			cl.append(f)
			
conffiles.sort()
for file in conffiles:
	print "Using config file:",file

if len(conffiles)==0:
	print "ERROR: No config files found!"
	sys.exit(1)


print ""


for infile in infiles:
	print "Reading from:",infile
	#work out outfile name if reqired
	if modify:
		outfile=infile
	elif ext!=None:
		instem=infile[:infile.find(".")]
		outfile=instem+"."+ext
	if outfile=="":
		print "No output file given, use -m, -e or -o"
		printhelp()



		# read in the fits header
	ifile=open(infile,"r")
	ifile.seek(0,0)
	mainhdr = (fits.readfitsheader(ifile))
	# Now look through the extensions for the history table.
	exthdr  = (fits.readfitsheader(ifile))
	while (exthdr.get("EXTNAME").val.strip() != "'HISTORY '"):
		ifile.seek(exthdr.getextsize(),1)
		exthdr=(fits.readfitsheader(ifile))

	# we now have the history table, so check if we have already fixed delays
	histhdr=exthdr
	history = psrfits.history(histhdr,ifile)
	FIXED=0
	for row in history.entries:
		if row['PROC_CMD'].startswith("dlyfix"):
			print "DELAYS ALREADY FIXED"
			print "on '%s'"%row['DATE_PRO']
			print "by '%s'"%row['PROC_CMD']
			FIXED=1

	# Now look for the subint table...
	exthdr  = (fits.readfitsheader(ifile))
	while (exthdr.get("EXTNAME").val.strip() != "'SUBINT  '"):
		ifile.seek(exthdr.getextsize(),1)
		exthdr=(fits.readfitsheader(ifile))
	# try and compute the centre freq from the first subint...
	subinthdr = exthdr
	bintab = fits.binarytable(subinthdr)
	subint = bintab.readrow(ifile)
	try:
		len(subint['DAT_FREQ'])
		fsum=0
		for f in subint['DAT_FREQ']:
			fsum+=f
		freq = fsum/float(len(subint['DAT_FREQ']))
	except TypeError:
		freq = subint['DAT_FREQ']

	# Compute the delay
	cur_correct=0
	cur_delay=float(mainhdr.get("STT_OFFS").val)
	corrs=getcorrection(mainhdr,freq,history,conffiles,verbose)

	corr=0
	for c in corrs:
		corr+=c['corr']

	new_delay = cur_delay - cur_correct + corr
	print "Correction is %g s,\n\t total delay is %s s"%(corr,new_delay)

	if FIXED==1:
		print "No correction made as already fixed!"
		sys.exit(1)


	# Add the history comment line:
	oldsize=len(history.output())
	for c in corrs:
		row=copy.deepcopy(history.entries[-1])
		msg=re.sub("\s\s*"," ",c['msg'])
		row['PROC_CMD']="dlyfix (%g) %s"%(c['corr'],msg)
		if len(row['PROC_CMD']) > 80:
			row['PROC_CMD']=row['PROC_CMD'][0:79]
		row['DATE_PRO']=str(datetime.datetime.utcnow())
		history.appendrow(row)
	
	newsize=len(history.output())
	print oldsize,newsize
	if modify:
		if newsize != oldsize:
			print "ERROR can't add to history table without changing file size"
			print "Cannot use --modify in this case"
			sys.exit(3)


	mainhdr.get("STT_OFFS").val=("%17.17f "%new_delay).rjust(18)

	#Write the main header:

	print "Writing to:",outfile

	if modify:
		ifile=ofile=open(infile,"r+")
	else:
		ofile=open(outfile,"w")
	ofile.seek(0,0)
	ofile.write(mainhdr.output())

	#Write the extention tables...
	#Move the the start of the ext tables in the input file..
	ifile.seek(0,0)
	dmy = fits.readfitsheader(ifile)
	exthdr = fits.readfitsheader(ifile)
	while exthdr != None:

		if exthdr.get("EXTNAME").val.strip() == "'HISTORY '":
			if modify:
				# we have to overwrite the header, so seek backwards
				ofile.seek(-len(history.hdr.output()),1)
			else:
				# we are not re-reading the history, so skip over
				ifile.seek(exthdr.getextsize(),1)
			ofile.write(history.output())
		else:
			toread=exthdr.getextsize()
			if modify:
				ifile.seek(toread,1)
			else:
				ofile.write(exthdr.output())
				while toread > 0:
					raw = ifile.read(2880)
					ofile.write(raw)
					toread -= 2880
		exthdr=fits.readfitsheader(ifile)


	ofile.close()
	ifile.close()
	print ""
