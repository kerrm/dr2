# Flag data with catastrophic digital artefacts.

# Last good observations for 10cm system appear to be on Feb 1, 2006.

* Apply wbc_eol flag to 10cm WBC data taken after MJD 53768.
backend==WBC
frequency>3000
mjd>53768
@wbc_eol
