from collections import deque

lines = file('ppta_flags.asc').readlines()[2:]
entries = deque()
for line in lines:
    toks = line.split()
    fname = toks[1]
    flag = toks[6]
    entries.append('%s %s'%(fname,flag))

file('brief_ppta_flags.asc','w').write('\n'.join(entries))
