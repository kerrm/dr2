"""
Routines for managing file system at Marsfield.

$Header: /nfs/slac/g/glast/ground/cvs/users/kerrm/tpipe/timing.py,v 1.14 2014/01/21 06:46:59 kerrm Exp $

author: Matthew Kerr <matthew.kerr@gmail.com>
"""
from glob import glob
from collections import deque
from os.path import expandvars,isfile,basename,join

import database
import flagging
import alias
import util

def read_list(fname,comment='#'):
    """ Read in a simple ASCII list and ignore empty/commented lines."""
    return filter(lambda l: len(l)>0 and (not l.startswith(comment)),
        map(str.strip,file(fname).readlines()))

def find_cals(backends,jname=None,blacklist=None,whitelist=None,
        regexp=None):
    """ Find all calibrators for a given backend/pulsar.

    The base set is the whitelist, if provided, otherwise all pulsar names
    found in the specified backend folder(s).  Names on the blacklist 
    supersede the whitelist.
    """
    if not hasattr(backends,'__iter__'):
        backends = [backends]
    if jname is None:
        # determine jnames by preference from whitelist, else from disk
        if whitelist is not None:
            jnames = set(read_list(whitelist))
        else:
            jnames = deque()
            for backend in backends:
                jnames.extend(map(lambda x: x.split('_')[0],
                    map(basename,glob(expandvars('%s/J*_R'%backend)))))
            jnames = set(jnames)
        # subtract any blacklist names
        if blacklist is not None:
            jnames = jnames-set(read_list(blacklist))
        jnames = list(jnames)
    else:
        # make sure jname has correct alias...should do above?
        jnames = alias.get_aliases(jname)

    # now read in the obs for each jname
    obs = deque()
    for backend in backends:
        tmp = deque()
        for jname in jnames:
            tmp.extend(glob(expandvars('%s/%s_R/*cf'%(backend,jname))))
        """
        # TEMP -- add in .orig files!
        tmp2 = deque()
        for fname in tmp:
            if isfile(fname+'.orig'):
                tmp2.append(fname+'.orig')
        tmp.extend(tmp2)
        # END TEMP
        """

        #if 'CASPSR' in backend:
        if False:
            # temporary kluge to prevent "no SK data from being ingested
            obs.extend((x for x in tmp if not ('p_sk' in x)))
        else:
            obs.extend(tmp)
            
    if regexp is not None:
        obs = util.regexp_filter(regexp,obs,basename=True)
    return list(obs)

def find_obs(backends,jname,regexp=None,fnames=None):
    """ Return .rf files matching the specified backends and pulsar name."""
    obs = deque()
    if not hasattr(backends,'__iter__'):
        backends = [backends]
    for backend in backends:
        ext = '.rf'
        if ('HYDRA' in jname):
            ext = '.cf'
        # kluge...
        #if 'FBT' in backend:
        #    ext = '.ar'

        tmp = deque()
        #if jname == 'HYDRA':
        if ('HYDRA' in jname):
            tmp.extend(glob(expandvars('%s/%s/*%s'%(backend,jname,ext))))
            #tmp.extend(glob(expandvars('%s/%s_N/*cf'%(backend,jname))))
            #tmp.extend(glob(expandvars('%s/%s_O/*cf'%(backend,jname))))
            #tmp.extend(glob(expandvars('%s/%s_S/*cf'%(backend,jname))))
        else:
            for aliased_name in alias.get_aliases(jname):
                #if 'FBT' in backend:
                #    name = aliased_name.lstrip('J')
                #else:
                name = aliased_name
                tmp.extend(glob(expandvars('%s/%s/*%s'%(backend,name,ext))))
        """
        # TEMP -- add in .orig files!
        tmp2 = deque()
        for fname in tmp:
            if isfile(fname+'.orig'):
                tmp2.append(fname+'.orig')
        tmp.extend(tmp2)
        # END TEMP
        """
        #if 'CASPSR' in backend:
        if False:
            # temporary kluge to prevent "no SK data from being ingested
            obs.extend((x for x in tmp if not ('p_sk' in x)))
        else:
            obs.extend(tmp)
    if regexp is not None:
        obs = util.regexp_filter(regexp,obs,basename=True)
    if fnames is not None:
        obs = util.fname_filter(obs,fnames)
    return list(obs)

def make_ppta_db(fname,search_function,clobber=False,update=True,
        delete=True,verbosity=0):

    newdb = clobber or (not isfile(fname))
    if newdb:
        db = database.Database(search_function(),checkpoint=fname)
        db.save(fname)
    else:
        db = database.Database(fname)

    db.dedup() # this temporary? kindof gross

    db.add_correction(util.local_file('corrections/fix_ppta_data.asc'))

    if update and (not newdb):
        db.update(search_function())
        db.save(fname)

    # only delete files from the copy in memory, not the copy saved to disk
    if delete:
        db.delete_complement(search_function())

    # these apply to both obs. and calibrators
    flagging.apply_global_flags(db,verbosity=verbosity)

    return db

def make_ppta_caldb(fname,backends,jname,clobber=False,update=True,
        regexp=None):
    search_function = lambda: find_cals(backends,jname,regexp=regexp)
    db = make_ppta_db(fname, search_function,clobber=clobber,update=update)
    return db

def make_ppta_obsdb(fname,backends,jname,clobber=False,update=True,
        regexp=None,fnames=None,verbosity=0):
    search_function = lambda: find_obs(
        backends,jname,regexp=regexp,fnames=fnames)
    db = make_ppta_db(fname,search_function,clobber=clobber,update=update,
        verbosity=verbosity)
    # putting the corrections here allows development on existing dbs
    db.add_correction(util.local_file('corrections/delays.asc'),
        verbosity=verbosity)
    db.add_correction(util.local_file('corrections/twobin_delays.asc'),
        verbosity=verbosity)
    flagging.apply_class_flags(
        db,verbosity=verbosity)
    flagging.apply_ppta_sql_flags(db)
    return db

def make_p574_caldb(fname,backends,clobber=False,update=True,regexp=None):
    search_func = lambda: find_cals(backends,whitelist=None,
        blacklist=util.local_file('pulsarlists/ppta_pulsars.asc'),
        regexp=regexp)
    db = make_ppta_db(fname,search_func,clobber,update=update)
    return db

def make_p574_obsdb(fname,backends,jname,clobber=False,update=True,
        regexp=None):
    search_func = lambda: find_obs(backends,jname,regexp=regexp)
    db = make_ppta_db(fname,search_func,clobber,update=update)
    db.add_correction(util.local_file('corrections/delays.asc'))
    db.add_correction(util.local_file('corrections/twobin_delays.asc'))
    flagging.apply_class_flags(db)
    return db
    
def make_pcmdb(fname,pcmdir=None,suppldir=None,clobber=False):
    """ Generate database from default pcm (MEM) calibrators."""
    if (not clobber) and isfile(fname):
        return database.Database(fname)
    if pcmdir is None:
        try:
            pcmdir = os.environ['PPTA_PCM']
        except KeyError:
            raise KeyError('Could not locate the pcm directory!')
    files = glob(join(pcmdir,'*avpcm'))
    if suppldir is not None:
        #files.extend(glob(join(suppldir,'*pcm')))
        files.extend(glob(join(suppldir,'*')))
    db = database.Database(files)
    db.add_correction(util.local_file('corrections/fix_ppta_data.asc'))
    db.save(fname)
    return db

def make_generic_db(fname,mydir,clobber=False):
    """ Generate generic database from files in a directory."""
    if (not clobber) and isfile(fname):
        return database.Database(fname)
    files = glob(join(mydir,'*'))
    db = database.Database(files)
    #db.add_correction(util.local_file('corrections/fix_ppta_data.asc'))
    db.save(fname)
    return db

def make_fluxdb(fname,fluxdir=None,suppldir=None,clobber=False):
    """ Generate database from default flux calibrators."""
    if (not clobber) and isfile(fname):
        return database.Database(fname)
    if fluxdir is None:
        try:
            fluxdir = os.environ['PPTA_FLUX']
        except KeyError:
            raise KeyError('Could not locate the fluxcal directory!')
    files = deque()
    # let supplementary directory take precendence
    if suppldir is not None:
        files.extend(glob(join(suppldir,'*fluxcal')))
    # take care of sub directories if present
    files.extend(glob(join(fluxdir,'*avfluxcal')))
    files.extend(glob(join(fluxdir,'10cm','*avfluxcal')))
    files.extend(glob(join(fluxdir,'20cm','*avfluxcal')))
    files.extend(glob(join(fluxdir,'50cm','*avfluxcal')))
    files.extend(glob(join(fluxdir,'other','*fluxcal')))
    # temporary kluge to remove bad cals
    bad_cals = ['apsr_20cm_10.avfluxcal','apsr_20cm_11.avfluxcal',
                'apsr_20cm_9.avfluxcal','apsr_HOH_1.avfluxcal']
    ok_files = deque()
    for f in files:
        ok = True
        for bc in bad_cals:
            if bc in f:
                ok = False
                break
        if ok:
            ok_files.append(f)
    files = ok_files
    # end temporary kluge
    db = database.Database(files)
    db.add_correction(util.local_file('corrections/fix_ppta_data.asc'))
    db.save(fname)
    return db

def make_file_db(fname,files):
    """ Function for use with MPI work."""
    return make_ppta_db(fname,lambda: files,True,False)

def get_directories(backends):
    ndfb = 10
    ncaspsr = 6
    d = dict(
        CPSR2=['$CPA'],
        NEW_CPSR2=['/DATA/HYDRA_2/ker14a/new_cpa'],
        WBC=['$WBCORR_1','$WBCORR_2'],
        DFB=['$DFB_%d'%(x+1) for x in xrange(ndfb)],
        CASPSR= ['$CASPSR_%d'%(x+1) for x in xrange(ncaspsr)],
        APSR = ['$APSR_%d'%(x+1) for x in xrange(3)],
        AFB = ['/DATA/NEWTON_2/ker14a/PKSFBT']

    )
    rvals = []
    for backend in backends:
        if backend in d.keys():
            rvals.extend(d[backend])
        else:
            print 'Unrecognized backend %s.'%backend
    return rvals

def get_pcm_obs_db(fname,backends,clobber=False):
    """ Whitelist a set of observations to use for pcm calibrations.
    These tend to be J0437-4715 observations, but not exclusively."""

    regexp = '.130411.*' # J0437-4715 long track, April 11, 2013
    obsdb = make_ppta_obsdb(fname,backends,'J0437-4715',clobber=clobber,
        update=False,regexp=regexp)

    return obsdb

def get_pcm_cal_db(fname,backends,clobber=False):
    """ Whitelist a set of observations to use for pcm calibrations.
    These tend to be J0437-4715 observations, but not exclusively."""

    regexp = '.130411.*' # J0437-4715 long track, April 11, 2013
    caldb = make_ppta_caldb(fname,backends,'J0437-4715',clobber=clobber,
        update=False,regexp=regexp)

    return caldb
    
"""
from jobs import MPI_Work
class MPI_Database(MPI_Work):

    def __init__(self,files):
        self.files = files
        self._success = False
        self._id = hash(self)
        self._db = mkstemp()[1] # file name

    def can_schedule(self):
        return True

    def __call__(self):
        make_file_db(self._db,self.files)
        self._success = True
"""
