from mpi4py import MPI
_mpicomm = MPI.COMM_WORLD
_mpistatus = MPI.Status()
WORKTAG = 0
STOPTAG = 1
SLEEPTAG = 2

import numpy as np
import cPickle

from collections import deque
import traceback
import os
from os.path import join,isdir,isfile,basename
from time import sleep

import util
import html
from profiles import ProfileAnalysis
import offsets
import coadd

class MPI_Work(object):
    """ Encapsulate a task to be executed via an MPI worker thread.
    
    This parallels (hah) the schemes above, with a specialized class that
    knows the particular routines to be run.  Moreover, with the
    dependency information, the server thread can assign work in a manner
    that guarantees dependencies will be satisified without repeating work.

    To avoid excessive I/O checks, dependencies are in terms of other
    objects, so the check should be done once and only once, by the
    MPI_Work object that "owns" the relevant target.
    """

    def __init__(self,target,dependencies=[],clobber=False):
        """ target -- a single file whose existence indicates success
            dependencies -- optional list of MPI_Work objects that should
                successfully complete for work to proceed
        """
        self.target = target
        self.dependencies = dependencies
        self._pending = True
        self._success = not (clobber or not self._check_complete())
        self._id = hash(self)
        self.clobber = clobber
        self._wtime0 = 0.
        self._wtime1 = 0.

    def _check_complete(self):
        return (self.target is not None) and isfile(self.target)
        
    def update(self,other):
        """ Sync instances after MPI communication."""
        self._success = other._success
        self._pending = other._pending
        self._wtime0 = other._wtime0
        self._wtime1 = other._wtime1

    def complete(self):
        return self._success

    def failed(self):
        return not (self._pending or self._success)

    def set_failed(self):
        self._pending = False
        self._success = False

    def can_schedule(self,verbosity=0):
        """ Return True if job can be scheduled (completed dependencies.)
        """
        for dep in self.dependencies:
            if not dep.complete():
                if verbosity > 0:
                    print str(self)
                    print '... is waiting on %s.'%(str(dep))
                return False
        return True

    def cannot_schedule(self):
        """ Return True if job can never be scheduled (failed dependencies.)
        """
        for dep in self.dependencies:
            if dep.failed():
                return True
        return False

    def __call__(self):
        """ Start clock, carry out work, stop clock."""
        self._wtime0 = MPI.Wtime()
        try:
            self._work()
        except Exception as e:
            print '\033[91m'+'*** Worker encountered an exception:'
            print traceback.format_exc()
            print '\033[0m'+'Marking job as failed and carrying on.'
            self.set_failed()
        self._wtime1 = MPI.Wtime()

    def _work(self):
        """ Virtual method to be implemented by specific child classes."""
        raise NotImplementedError

    def __str__(self):
        """ Virtual method to be implemented by specific child classes."""
        raise NotImplementedError

    def __eq__(self,other):
        """ Rudimentary check that two objects represent the same work."""
        c1 = type(other)==type(self)
        c2 = self.target==other.target 
        return c1 and c2

    def real_work(self):
        return True

    def get_suggested_work(self):
        if hasattr(self,'suggested_work'):
            return self.suggested_work
        return None

class MPI_Work_NoOutput(MPI_Work):
    """ A special subclass for work without per-job output."""

    def __init__(self,dependencies=[],clobber=False):
        """ target -- a single file whose existence indicates success
            dependencies -- optional list of MPI_Work objects that should
                successfully complete for work to proceed
        """
        super(MPI_Work_NoOutput,self).__init__(
            None,dependencies=dependencies,clobber=clobber)

    def _check_complete(self):
        return False

class MPI_Link(MPI_Work):
    """ Soft link files."""

    def __init__(self,srcs,dsts,clobber=False):
        super(MPI_Link,self).__init__(dsts[0],clobber=clobber)
        self.srcs = srcs
        self.dsts = dsts

    def _work(self):
        for src,dst in zip(self.srcs,self.dsts):
            if not os.path.islink(dst):
                os.symlink(src,dst)
            elif self.clobber:
                os.remove(dst)
                os.symlink(src,dst)

class MPI_GeneratePsrsh(MPI_Work):
    """ no dependencies, target is .psrsh script"""
    def __init__(self,ops,scrunches,dependencies=[],clobber=False,
            label=None):
        """ ops a ScrunchDepOps object"""
        self.ops = ops
        self.scrunches = scrunches
        label_string = ('' if label is None else '.%s'%label)+'.psrsh'
        target = ops.obs.get_stem(scrunches.outpath)+label_string
        super(MPI_GeneratePsrsh,self).__init__(
            target,dependencies=dependencies,clobber=clobber)

    def _work(self):
        if self._success:
            return
        psrsh = self.ops.get_psrsh(self.scrunches)
        psrsh.write(self.target)
        self._pending = False
        self._success = True

    def __str__(self):
        return 'Generate %s.'%(basename(self.target))

class MPI_ExecutePsrsh(MPI_Work):
    """ depends on .psrsh, target is profile/calibrator etc."""

    def __init__(self,ops,scrunches,dependencies,clobber=False,label=None):
        label_string = ('' if label is None else '.%s'%label)+'.psrsh'
        self.script = ops.obs.get_stem(scrunches.outpath)+label_string
        target = scrunches.get_target(ops.obs)
        self.datamb = ops.obs.datamb
        super(MPI_ExecutePsrsh,self).__init__(
            target,dependencies=dependencies,clobber=clobber)
        
    def _work(self):
        if self._success:
            return
        # disable logging -- relatively worthless right now
        #cmd = 'psrsh -n -v %s &> %s'%(self.script,self.script+'.log')
        cmd = 'psrsh -n -v %s'%(self.script)
        rc,stderr,stdout = util.run(cmd,echo=False)
        self._pending = False
        self._success = isfile(self.target)

    def __str__(self):
        return 'Execute %s.'%(basename(self.script))

class MPI_CombinedPsrsh(MPI_Work):
    """ Generate and execute psrsh in single job."""

    def __init__(self,ops,scrunches,dependencies,clobber=False,label=None):
        label_string = ('' if label is None else '.%s'%label)+'.psrsh'
        self.script = ops.obs.get_stem(scrunches.outpath)+label_string
        target = scrunches.get_target(ops.obs)
        self.ops = ops
        self.scrunches = scrunches
        self.datamb = ops.obs.datamb
        self._expected_failure = False
        super(MPI_CombinedPsrsh,self).__init__(
            target,dependencies=dependencies,clobber=clobber)

    def _check_complete(self):
        return isfile(self.target) and isfile(self.script)
        
    def _work(self):
        if self._success:
            return
        psrsh = self.ops.get_psrsh(self.scrunches)
        psrsh.write(self.script)
        # write out any ancillary information
        if hasattr(self.ops.obs,'tmp_ancillary'):
            self.ops.obs.set_ancillary(
                self.ops.obs.tmp_ancillary,self.scrunches.outpath)
        cmd = 'psrsh -n -v %s &> %s'%(self.script,self.script+'.log')
        rc,stderr,stdout = util.run(cmd,echo=False)
        self._pending = False
        self._success = isfile(self.target)
        if not self._success:
            # for some flagged files, psrsh will always fail
            # need to signal job as failed so dependencies are removed from
            # the queue, but record in log if it's an ACTUAL failure
            for line in psrsh:
                if 'test 0' in line:
                    self._expected_failure = True
                    break
            msg = 'Failed! (Expected)\n' if self._expected_failure else 'Failed!\n'
            # make sure log includes some indicator of problem
            # sometimes it seems an empty log is all we get
            file(self.script+'.log','a').write(msg)

    def __str__(self):
        base = 'Generate & execute %s.'%(basename(self.script))
        if self._expected_failure:
            return base + ' (FAIL expected.)'
        return base

class MPI_GeneratePlot(MPI_Work):
    """ Manage generation of a variety of plots.
    
    Only allow one plot to be generated per instance.  This helps keeps us
    with the 1 target per job paradigm used elsewhere.
    """

    def __init__(self,plot,ops,scrunches,dependencies=[],clobber=False):
        target,self.action = plot.get_mpi(ops.obs,scrunches)
        super(MPI_GeneratePlot,self).__init__(
            target,dependencies=dependencies,clobber=clobber)

    def _work(self):
        if self._success:
            return
        #rc,stderr,stdout = run(self.actions,echo=False)
        rc,stderr,stdout = self.action(echo=False)
        self._pending = False
        self._success = isfile(self.target)
        if not self._success:
            print self.action.cmd
            print stderr
            print stdout

    def __str__(self):
        return 'Generate %s.'%(basename(self.target))

class MPI_GenerateCoadd(MPI_Work):
    """ Manage co-addition of a profile.
    """

    def __init__(self,procdb,jname,combo,dependencies=[],clobber=False):
        self.procdb = procdb
        self.jname = jname
        self.combo = combo
        self.outpath = procdb.get_outpath(jname)
        target = coadd.new_coadd(procdb,jname,combo=combo,
            outpath=self.outpath,retain_freq=True,name_only=True)
        super(MPI_GenerateCoadd,self).__init__(
            target,dependencies=dependencies,clobber=clobber)

    def _work(self):
        if self.target is None:
            self.set_failed()
            return
        if self._success:
            return
        coadd.new_coadd(self.procdb,self.jname,combo=self.combo,
            outpath=self.outpath,retain_freq=True)
        self._pending = False
        self._success = isfile(self.target)

    def __str__(self):
        if self.target is not None:
            return 'Generate %s.'%(basename(self.target))
        return 'Generate coadd (unknown).'

class MPI_GenerateTOAResidualsPlot(MPI_Work):
    """ Manage co-addition of a profile.
    """

    def __init__(self,procdb,jname,combo,target,clobber=False,
        strict_select=False):
        """ NB -- NOT copy over large procdb object over MPI! """
        super(MPI_GenerateTOAResidualsPlot,self).__init__(
            target,clobber=clobber)
        self._do_init(procdb,jname,combo,strict_select)

    def _do_init(self,procdb,jname,combo,strict_select=False):
        self.eph,self.sel,self.toas = None,None,None
        try:
            self.eph,sel = procdb.get_eph_sel(jname,strict=strict_select)
            self.sel = '\n'.join(
                filter(lambda l: 'sql' not in l,sel.split('\n')))
        except TypeError as e:
            return
        self.toas = procdb.get_toas(jname,combo)

    def _work(self):
        if self._success:
            return
        if (self.eph is None) or (self.sel is None) or (self.toas is None):
            self.set_failed()
            return
        # write in-memory strings to files
        par = util.TemporaryFile(self.eph)
        sel = util.TemporaryFile(self.sel)
        tim = util.TemporaryFile(self.toas)
        html.residuals_plot(self.target,str(par),str(tim),select=str(sel))
        self._pending = False
        self._success = isfile(self.target)

    def __str__(self):
        return 'Generate TOA residuals %s.'%(self.target)

class MPI_GenerateTOAs(MPI_Work):

    def __init__(self,ops,scrunches,dependencies=[],clobber=False,
            plot=True,use_invint=False,override_template=None,pat_args=''):
        if not use_invint:
            self.profile = scrunches.get_target(ops.obs)
        else:
            self.profile = scrunches.get_invariant_interval(ops.obs)
        #self.flags = ops.toa_flags()
        if override_template is not None:
            self.template = override_template
        else:
            self.template = ops.template
        self.plot = plot
        self.obs = ops.obs
        self.pat_args = pat_args
        target = self.profile+'.toa.analysis'
        super(MPI_GenerateTOAs,self).__init__(target,
            dependencies=dependencies,clobber=clobber)

    def _work(self):
        if self._success:
            return
        if not isfile(self.profile):
            self.set_failed()
            return
        pa = ProfileAnalysis(self.obs,self.profile,self.template.fname,
            pat_args=self.pat_args)
        cPickle.dump(pa,file(self.target,'w'),protocol=2)
        if self.plot:
            outplot = self.target.replace('analysis','residuals.png')
            pa.residuals_plot(outplot)
        self._pending = False
        self._success = isfile(self.target)

    def __str__(self):
        return 'Generate %s.'%(basename(self.target))

class MPI_Delays(MPI_Work_NoOutput):
    """ Compute the delay between two backends through cross-correlation.

    This version instantiates a Delay object and returns it through the
    MPI.
    """

    def __init__(self,obs1,obs2):
        self.obs1 = obs1
        self.obs2 = obs2
        self.delay = None
        super(MPI_Delays,self).__init__(dependencies=[],clobber=True)

    def _work(self):
        if self._success:
            return
        try:
            self.delay = offsets.Delay(self.obs1,self.obs2)
        except Exception as e:
            print e
            self._success = False
        self._pending = False
        self._success = True

    def update(self,other):
        """ Transfer delay object too."""
        super(MPI_Delays,self).update(other)
        self.delay = other.delay

    def __str__(self):
        return 'Generate delay %s / %s.'%(self.obs1.basename,self.obs2.basename)

class MPI_Delays_Pickle(MPI_Work):
    """ Compute the delay between two backends through cross-correlation.

    This version instantiates a Delay object and pickles it.
    """

    def __init__(self,obs1,obs2,outpath,clobber=False,use_new=True):
        self.obs1 = obs1
        self.obs2 = obs2
        self.delay = None
        self.use_new = use_new
        fnames = sorted([obs1.basename,obs2.basename])
        fnames = [x.rstrip('.rf').strip('.cf') for x in fnames]
        target = join(outpath,'%s_%s_delay.pickle'%(fnames[0],fnames[1]))
        super(MPI_Delays_Pickle,self).__init__(target,
            dependencies=[],clobber=clobber)

    def _work(self):
        if self._success:
            return
        self._success = False
        try:
            delay = offsets.Delay(self.obs1,self.obs2,use_new=self.use_new)
            cPickle.dump(delay,file(self.target,'w'),protocol=2)
            self._success = True
        except Exception as e:
            print e
        self._pending = False

    def __str__(self):
        return 'Generate %s.'%(basename(self.target))

class MPI_Sleep(MPI_Work):

    def __init__(self,time=2):
        self.time = time
        self._success = False
        self._id = hash(self)

    def _work(self):
        sleep(self.time)

    def can_schedule(self):
        return True

    def __str__(self):
        return 'sleep for %s seconds.'%(self.time)

    def real_work(self):
        return False

class MPI_Jobs(object):
    """ Manage a collection of Work.
    
    Not sure how concurrency will be at play here.  Keep a queue of jobs
    ready to go, jobs submitted, and jobs returned."""

    def __init__(self,work=[],verbosity=0):

        self.verbosity = verbosity
        self.init_work(work)

    def init_work(self,work):
        self.done = deque()
        self.todo = deque()
        self.fail = deque()
        self.in_progress = dict()
        self.add_work(work)

    def get_work(self,old_work=None):
        work = None

        # check to see if we are finished
        if len(self.todo)==0:
            return None

        # check queue to see if there is a related job we should do
        # I worry this may be quite slow, need to profile
        if old_work is not None:
            suggested_work = old_work.get_suggested_work()
            if suggested_work is not None:
                # try to find work in queue
                swid = suggested_work._id
                for i,job in enumerate(self.todo):
                    if swid==job._id:
                        self.todo.rotate(-i)
                        work = self.todo.popleft()
                        self.todo.rotate(i)
                        self.in_progress[work._id] = work
                        return work

        # try to submit job on top of queue
        top = self.todo[0]
        if top.can_schedule():
            work = self.todo.popleft()

        # otherwise, find next available job
        else:
            for i,job in enumerate(self.todo):
                if job.can_schedule():
                    break
            if job.can_schedule():
                self.todo.rotate(-i)
                work = self.todo.popleft()
                self.todo.rotate(i)

        # if no acceptable job, submit a sleep job
        if work is None:
            # remove any jobs that have failed from front of queue
            for i in xrange(len(self.todo)):
                top = self.todo[0]
                if top.cannot_schedule():
                    top.set_failed()
                    self.fail.append(self.todo.popleft())
                    if self.verbosity > 1:
                        print 'Removing %s due to failed dependencies.'%str(top)
                else:
                    break
            work = MPI_Sleep()
        else:
            if self.verbosity > 1:
                print 'Submitting "%s"... %d remaining.'%(
                    str(work),len(self.todo))
            self.in_progress[work._id] = work
        return work

    def add_work(self,work):
        if work is None:
            return
        if not hasattr(work,'__len__'):
            work = [work]
        for w in work:
            if w.complete():
                self.done.append(w)
            else:
                self.todo.append(w)

    def add_finished(self,work,node):
        if not work.real_work():
            return # ignore Sleep instances
        if self.verbosity > 0:
            finished = len(self.done)+1
            remaining = len(self.todo)+len(self.in_progress)
            total = finished+remaining
            success = work.complete()
            places = len('%d'%total)
            fmt = '%' + str(places) + 'd'
            s1 = fmt%finished
            s2 = fmt%total
            if success:
                s = '  OK'
                print '\033[92m',
            else:
                s = 'FAIL'
                print '\033[91m',
            print '%2d: %s / %s: %s: "%s"\033[0m'%(
                node,s1,s2,s,str(work))
        # update existing Work instance and put it in done queue
        ework = self.in_progress.pop(work._id)
        ework.update(work)
        self.done.append(ework)

    def do_work(self,work=None,max_threads=None,kill_threads=False,timeout=600):

        if work is not None:
            self.add_work(work)

        comm,status = _mpicomm,_mpistatus
        twork = MPI.Wtime()

        # check for single-threaded case
        if comm.size == 1:
            self._do_work_single_thread()
            return

        # initialize threads
        threads_in_use = 0
        threads_to_use = comm.size-1
        if max_threads is not None:
            threads_to_use = min(max_threads,threads_to_use)
        for i in xrange(1,threads_to_use+1):
            work = self.get_work()
            if work is not None:
                print 'Starting up rank %d.'%(i)
                comm.send(work,dest=i,tag=WORKTAG)
                threads_in_use += 1

        while(True):

            if threads_in_use == 0:
                break

            # get old work first -- this way we can chain jobs efficiently
            old_work = comm.recv(
                source=MPI.ANY_SOURCE,tag=MPI.ANY_TAG,
                status=status)
            old_node = status.Get_source()
            self.add_finished(old_work,old_node) # sync job objects

            new_work = self.get_work(old_work)

            # check to see if we are finished
            if new_work is None:
                # put thread to sleep, decrease thread counter
                comm.send(None,dest=old_node,tag=SLEEPTAG)
                threads_in_use -= 1

            else:
                comm.send(new_work,dest=old_node,tag=WORKTAG)

            """
            # check to see if we are in a hung state
            if new_work.real_work():
                twork = MPI.Wtime()
            else:
                # TODO -- would be better to poll the threads to see if
                # any are actually doing work
                if (MPI.Wtime()-twork) > timeout:
                    print 'Execution timed out after %d s.  Aborting.'%(
                        timeout)
                    comm.Abort() # big guns...
            """

            sleep(0.01)

        if kill_threads:
            self.kill_threads()

    def _do_work_single_thread(self):

        while(True): 
            work = self.get_work()
            if work is None:
                break
            else:
                work()
                self.add_finished(work,1)
                continue

    def kill_threads(self):
        for i in xrange(1,_mpicomm.size):
            _mpicomm.send(None,dest=i,tag=STOPTAG)

    def sleep_threads(self):
        for i in xrange(1,_mpicomm.size):
            _mpicomm.send(None,dest=i,tag=SLEEPTAG)

class MPI_Worker(object):
    """ Use a non-blocking outer loop to periodically poll messages, and a
    blocking inner loop to do work.  This keeps CPU from spinning
    pointlessly during serial computations.
    """

    def __init__(self,sleep_interval=1):
        self.sleep_interval = sleep_interval

    def do_work(self,timeout=60):
        """ Use a non-blocking outer loop to periodically poll messages, 
        and a blocking inner loop to do work.  This keeps CPU from spinning
        pointlessly during serial computations.

        Exceptions are caught within Work classes so there should be no
        worry of a thread dying unless a signal propagates outside the
        Python exception chain.
        """
        comm,status = _mpicomm,_mpistatus
        while (True):
            if comm.Iprobe(source=0,tag=MPI.ANY_TAG,status=status):
                while (True):
                    work = comm.recv(source=0,tag=MPI.ANY_TAG,status=status)
                    if status.Get_tag()==STOPTAG:
                        return
                    if status.Get_tag()==SLEEPTAG:
                        break

                    # This has built-in timing and exception checking.
                    work()

                    comm.send(work,dest=0)
            else:
                sleep(self.sleep_interval)

def thread_stats(mpi_jobs):
    """ Determine overall scaling of wall time to processing time."""
    nthread = max(1,_mpicomm.size-1)
    wtime = 0
    start = np.inf
    stop = -np.inf
    for job in mpi_jobs.done:
        if job._wtime0 == 0 or job._wtime1 == 0:
            # catch jobs that aren't initialized
            continue
        start = min(start,job._wtime0)
        stop = max(stop,job._wtime1)
        wtime += (job._wtime1-job._wtime0)
    print 'Processing elapsed time: %.2f minutes'%((stop-start)/60)
    print 'Total wall time on-job: %.2f minutes (mean=%.2f)'%(
        wtime/60,wtime/60/nthread)

def throughput_stats(mpi_jobs,do_obs=True,output=None):
    """ Examine "execute psrsh" jobs and determine the I/O stats."""
    time,size = 1,0 # be safe with divide
    start = np.inf
    stop = -np.inf
    jobs = []
    allowed_classes = [MPI_ExecutePsrsh,MPI_CombinedPsrsh]
    for c in allowed_classes:
        jobs.extend(filter(lambda job: isinstance(job,c),mpi_jobs.done))
    for job in jobs:
        if job._wtime0 == 0 or job._wtime1 == 0:
            # catch jobs that aren't initialized
            continue
        is_obs = '.rf' in job.script
        if (do_obs and is_obs) or ((not do_obs) and (not is_obs)):
            start = min(start,job._wtime0)
            stop = max(stop,job._wtime1)
            time += (job._wtime1-job._wtime0)
            size += job.datamb
    total_time = max(1,stop-start) # safe divide
    print 'Total processed: %.2f MB'%(size)
    print 'Mean throughput per job: %.2f MB/s'%(size/time)
    print 'Mean throughput        : %.2f MB/s'%(size/total_time)

def throughput_plot(mpi_jobs,output=None):
    sizes = []
    times = []
    for job in mpi_jobs.done:
        if isinstance(job,MPI_ExecutePsrsh) or isinstance(job,MPI_CombinedPsrsh):
            times.append(job._wtime1-job._wtime0)
            sizes.append(job.datamb)

    import matplotlib
    matplotlib.use("Agg")
    import pylab as pl
    pl.figure(2); pl.clf()
    throughputs = [float(s)/max(1,t) for s,t in zip(sizes,times)]
    #pl.plot(sizes,times,ls=' ',marker='o')
    pl.semilogx(sizes,throughputs,ls=' ',marker='o')
    pl.xlabel('Size (MiB)')
    #pl.ylabel('Execution Time (s)')
    pl.ylabel('Throughput (MiB/s)')
    if output is not None:
        pl.savefig(output)
