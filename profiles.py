"""
Module collecting code for manipulating profiles.

$Header: /nfs/slac/g/glast/ground/cvs/users/kerrm/tpipe/timing.py,v 1.14 2014/01/21 06:46:59 kerrm Exp $

author: Matthew Kerr <matthew.kerr@gmail.com>
"""

import numpy as np
from scipy.optimize import brentq,leastsq
from scipy.stats import shapiro
import cPickle
import os
from os.path import join,isdir,basename
import pyfits
from collections import deque

import psrchive

import util
import rfi
from plotting import plot_profile_residuals

ARG = (2j*np.pi)*np.arange(1,2048)

def std_mad(profile,get_median=False):
    """ Return std from median absolute deviation."""
    m = np.median(profile)
    s = 1.4826*np.median(np.abs(profile-m))
    if get_median:
        return s,m
    return s

def check_normality(profile,thresh=1e-4):
    n = len(profile)
    try:
        avals = check_normality._cache[n]
        w,p = shapiro(profile,reta=False,a=avals)
    except AttributeError:
        check_normality._cache = dict()
        w,p,avals = shapiro(profile,reta=True)
        check_normality._cache[n] = avals
    except KeyError:
        w,p,avals = shapiro(profile,reta=True)
        check_normality._cache[n] = avals
    return p > thresh

def sanity_check(profile):
    """ A variety of important checks."""
    # check for power of 2
    power_of_2 = (len(profile)&(len(profile)-1)) == 0
    return power_of_2

def average_profile(profile,navg):
    rvals = profile.copy()
    for i in xrange(navg):
        rvals = 0.5*(rvals[0::2]+rvals[1::2])
    return rvals

def reconcile_profiles(p1,p2):
    """ Handle case when profiles have different binning schemes.

    Average the longer of the two profiles down to the common length.
    """
    if len(p2) > len(p1):
        navg = int(round(np.log2(len(p2)/len(p1))))
        p2 = average_profile(p2,navg)
    elif len(p1) > len(p2):
        navg = int(round(np.log2(len(p1)/len(p2))))
        p1 = average_profile(p1,navg)
    return p1,p2

def convolve(profile1,profile2):
    """ Return the convolution of two profiles."""
    p1,p2 = profile1,profile2
    if len(p1) != len(p2):
        raise ValueError('Profiles must have same dimension.')
    conv = np.fft.ifft(np.fft.fft(p1)*np.fft.fft(p2)).real
    return 1./len(p1)*np.fft.fftshift(conv)

def rotated(profile,delta=0.5):
    """ Produce a copy of profile matrix p shifted in phase by delta."""
    delta = delta % 1
    f = np.fft.fft(profile,axis=-1) 
    n = f.shape[-1]
    arg = np.fft.fftfreq(n)*(n*np.pi*2.j*delta)
    return np.real(np.fft.ifft(np.exp(arg)*f,axis=-1))

def differentiated(profile):
    nbin = len(profile)
    F = np.fft.fft(profile)
    arg = (2j*np.pi)*np.fft.fftfreq(nbin)*nbin
    d = np.fft.ifft(F*arg)
    #return np.real(d)/nbin
    return np.real(d)

def median_baseline(profile,err,passes=10,threshold=1.5,cal=True):
    """ Estimate baseline using median and radiometer noise level.

    This is generally outperformed by iterative baseline.
    """
    if cal:
        profile = np.sort(profile)
        mask = np.asarray([True]*(len(profile)/2)+[False]*(len(profile)/2))
    else:
        mask = np.asarray([True]*len(profile))
        #profile = np.sort(profile)
        #t = int(round(0.1*len(profile)))
        #mask = np.asarray([True]*t+[False]*(len(profile)-t))
    for i in xrange(passes):
        m = np.median(profile[mask])
        b = np.mean(profile[mask])
        mask = np.abs((profile-m)/(err*b)) < threshold
    return np.mean(profile[mask])

def iterative_baseline(profile,err=None,nseg=32,get_dev=False):
    """ Identify baseline by greedily adding up pieces.

    Start with a small section (default 1/32) with the smallest overall mean
    as the initial baseline.  By selecting with mean rather than median, an
    RFI clean section should be selected.

    Then, choose the next segment closest in mean, and compare the mean with
    and without the new segment.  If it exceeds a threshold, stop iteration.

    err is the fractional error, dmean/mean.
    """
    profile = np.asarray(profile,dtype=float)

    if check_normality(profile):
        baseline = np.mean(profile)
        if get_dev:
            return baseline,np.std(profile)
        return baseline

    window = len(profile)/nseg

    slices = [profile[i*window:(i+1)*window] for i in xrange(nseg)]
    means = np.asarray([np.mean(s) for s in slices])
    a = np.argsort(means)
    means = means[a]

    if err is not None:
        sigma = means[0]*err # should be very close to true radiometer error
    else:
        sigma = 1. # assume profile already normalized
    old_baseline = means[0]
    
    for i in xrange(1,len(profile)/window):
        namps = (i+1)*window
        baseline = np.mean(means[:i+1])
        baseline_err = sigma/namps**0.5
        if (i > 1) and (abs((baseline-old_baseline)/baseline_err) > 0.5):
            break
        old_baseline = baseline
    if get_dev:
        # it shoud be the case that "i" is always one more than we want now
        dev = np.mean(np.asarray([np.std(s) for s in slices])[a][:i])
        return old_baseline,dev
    return old_baseline

def iterative_baseline_noerr(profile,nseg=32,thresh=0.55,get_dev=False):
    """ Identify baseline by greedily adding up pieces.

    Start with a small section (default 1/32) with the smallest overall mean
    as the initial baseline.  By selecting with mean rather than median, an
    RFI clean section should be selected.

    Then, choose the next segment closest in mean, and compare the mean with
    and without the new segment.  If it exceeds a threshold, stop iteration.

    This is identical to the other implementation save we use the sample
    variance to compute the significance.
    """

    profile = np.asarray(profile,dtype=float)

    if check_normality(profile):
        baseline = np.mean(profile)
        if get_dev:
            return baseline,np.std(profile)
        return baseline

    window = len(profile)/nseg

    slices = [profile[i*window:(i+1)*window] for i in xrange(nseg)]
    means = np.asarray([np.mean(s,dtype=float) for s in slices])
    squares = np.asarray([np.mean(s**2,dtype=float) for s in slices])
    a = np.argsort(means)
    means = means[a]
    squares = squares[a]

    old_baseline = means[0]
    baseline_err = (squares[0]-old_baseline**2)**0.5
    
    for i in xrange(1,len(profile)/window):
        namps = (i+1)*window
        baseline = np.mean(means[:i+1],dtype=float)
        db = (baseline-old_baseline)/baseline_err*namps**0.5
        if (i > 1) and (abs(db) > thresh):
            break
        old_baseline = baseline
        baseline_err = (np.mean(squares[:i+1])-baseline**2)**0.5
    if get_dev:
        return old_baseline,baseline_err
    return old_baseline

def cal_baseline(profile,duty_cycle=0.5,get_residuals=True):
    """ Identify transition points and return the change points, baselines,
    and deviations for the ON/OFF sections."""

    # identify the ON portion by sorting the intensities and taking the
    # mean of the relevant portions, in most cases the central 40% of the
    # top and bottom halves of the intensities
    d = profile[1:]-profile[:-1]
    t = np.sort(profile)
    b_off = np.mean(
        t[int(0.05*len(profile)):int((duty_cycle-0.05)*len(profile))])
    b_on = np.mean(
        t[int((duty_cycle+0.05)*len(profile)):int(0.95*len(profile))])

    # Now make a rough template based on these estimated levels and use it
    # to make a refined estimate of the phases at which the profile changes
    # from ON to OFF; the "cps" below are the indices of the change points
    template = np.empty(len(profile))
    template[:] = b_off
    template[:int(round(duty_cycle*len(profile)))] = b_off + b_on
    resid = np.empty(len(profile))
    for i in xrange(len(profile)):
        resid[i] = np.sum((profile-np.roll(template,i))**2)
    cp1 = np.argmin(resid)
    cp2 = cp1 + int(round(duty_cycle*len(profile)))
    if cp2 > len(profile):
        cp2 -= len(profile)
    if abs(float(abs(cp1-cp2))/len(profile)-duty_cycle) > 0.01:
        raise ValueError('Change points do not match stated duty cycle.')

    # Now use these change points to estimate the profile statistics; a
    # small buffer is adopted to allow for transients around the change
    # points.  Note that if cp1 > cp2, the ON portion of the cal wraps
    # around 0/1.  The below computations assume this isn't so, but if it
    # is, we just swap the ON and OFF masks.
    on_mask = np.asarray([False]*len(profile))
    off_mask = on_mask.copy()
    buff = 0.02 * len(profile)
    swap_masks = False
    if cp1 > cp2:
        swap_masks = True # ON portion phase wraps
        cp1,cp2 = cp2,cp1
    on_mask[cp1+buff:cp2-buff] = True
    off_mask[cp2+buff:] = True
    off_mask[:cp1-buff] = True
    if swap_masks:
        on_mask,off_mask = off_mask,on_mask
    s_on,b_on = std_mad(profile[on_mask],get_median=True)
    s_off,b_off = std_mad(profile[off_mask],get_median=True)
    if not get_residuals:
        return b_off,b_on,s_off,s_on

    # Return the residuals using the exact change points and no buffers.
    residuals = profile.copy()
    on_mask[:] = False
    on_mask[cp1:cp2] = True
    if swap_masks:
        on_mask = ~on_mask 
    residuals[on_mask] = (profile[on_mask]-b_on)/s_on
    residuals[~on_mask] = (profile[~on_mask]-b_off)/s_off
    return residuals,b_off,b_on,s_off,s_on

def fit_template(profile,template,frac_err=None):
    """ Return baseline amplitude and scale to match template to profile.

        This assumes that profile and template are phase aligned.
    """

    p,t = profile,template
    if len(p) != len(t):
        # interpolate template onto same domain as profile
        pdom = np.linspace(0,1,len(p)+1)[:-1]
        tdom = np.linspace(0,1,len(t)+1)[:-1]
        t = np.interp(pdom,tdom,t)

    # estimate baseline and amplitude from profile
    N = len(p)
    T = np.sum(t)
    P = np.sum(p)
    tp = np.sum(t*p)
    t2 = np.sum(t*t)
    ahat = (N*tp-T*P)/(N*t2-T*T) # scale estimator
    bhat = (1./N)*(P-ahat*T) # baseline estimator
    m = ahat*T + bhat

    # scale results to noise
    if frac_err is not None:
        err = bhat*frac_err
    else:
        offpulse_mask = t < 0.01*t.max()
        err = np.std((p-m)[offpulse_mask])

    # compute flux
    flux = np.average(ahat*t)
    ahat_err = err*t2**-0.5
    fluxerr = ahat_err/ahat * flux

    #snr = np.sum(((p-bhat)/err)**2)**0.5
    # compute snr as "sigma"
    snr = ahat/ahat_err

    # scale profiles to unit variance
    sp = (p-bhat)/err # scaled to unit variance
    st = t*(ahat/err)
    chi2 = np.sum((st-sp)**2)/len(sp)

    return ahat,bhat,sp,st,snr,chi2,flux,fluxerr

def fit_template_full(profile,template,get_resids=False,do_scan=False):
    """ Return phase, baseline and scale to match template to profile.
    """

    def scan():
        chi2 = np.empty_like(template)
        amps = np.empty_like(template)
        m2 = np.sum(template**2)
        t2 = np.sum(profile**2)
        for i in xrange(len(template)):
            m = np.roll(template,i)
            sm = np.sum(m*profile)
            amps[i] = sm/m2
            chi2[i] = t2 - sm**2/m2
        a = np.argmin(chi2)
        return float(a)/len(template),amps[a]

    p,t = profile,template
    if len(p) != len(t):
        # interpolate template onto same domain as profile
        pdom = np.linspace(0,1,len(p)+1)[:-1]
        tdom = np.linspace(0,1,len(t)+1)[:-1]
        t = np.interp(pdom,tdom,t)

    nbin = len(p)
    fp = np.fft.fft(profile)
    ft = np.fft.fft(template)
    # set template mean to 0
    #fp[0] = 0
    #t_mean = ft[0]
    ft[0] = 0
    #shift_arg = np.fft.fftfreq(nbin)[1:]*(2j*np.pi*nbin)
    shift_arg = np.fft.fftfreq(nbin)*(2j*np.pi*nbin)

    def chi(params):
        phase,scale,baseline = params
        resid = fp-np.exp(shift_arg*phase)*scale*ft
        resid[0] -= baseline
        return np.abs(resid)

    p0 = [0,1,0]
    if do_scan:
        loc,amp = scan()
        p0 = [-loc,amp,0]

    phase,scale,baseline = leastsq(chi,p0)[0]
    stuff = leastsq(chi,[phase,scale,baseline],
        full_output=1)
    phase,scale,baseline = stuff[0]
    cov_x = stuff[1]
    st = np.fft.ifft(np.exp(shift_arg*phase)*scale*ft).real + baseline/len(profile)
    resid = p-st
    # NB because in Fourier space need to adjust by nbins as well as chi^2
    #cov_x *= (np.sum(resid**2)/(nbin-3))*nbin
    try:
        cov_x *= nbin
    except TypeError:
        # bad fit
        pass
    if not get_resids:
        return phase,scale,baseline,cov_x
    else:
        return phase,scale,baseline,cov_x,resid


def phase_gradient(profiles,times,max_dphi=0.1,steps=200):
    """ Take clean profiles with equal white noise levels and search for
        a phase gradient across the profiles, e.g. as from an incorrect
        folding frequency.
        
        The metric is simply the S/N of the rotated profiles.

        This doesn't seem to work very well.
    """
    pos_chi2 = np.empty(steps)
    neg_chi2 = np.empty(steps)
    nprof = len(profiles)
    tscale = (np.asarray(times,dtype=float)-times[0])/(times[-1]-times[0])
    for i in xrange(steps):
        pos = np.zeros_like(profiles[0])
        neg = np.zeros_like(profiles[0])
        # compute shifted, summed profile
        for j in xrange(nprof):
            dphi = max_dphi*tscale[j]*float(i)/steps
            pos += rotated(profiles[j],delta=dphi)
            neg += rotated(profiles[j],delta=-dphi)
        pos_chi2[i] = np.inner(pos,pos)/nprof # norm to unit variance
        neg_chi2[i] = np.inner(neg,neg)/nprof
    if pos_chi2.max() > neg_chi2.max():
        best_shift = float(np.argmax(pos_chi2))/steps*max_dphi
        best_sig = pos_chi2.max()-pos_chi2[0]
    else:
        best_shift = float(np.argmax(neg_chi2))/steps*max_dphi
        best_sig = neg_chi2.max()-neg_chi2[0]
    freq_offset = best_shift / (times[-1]-times[0])
    return freq_offset,best_sig

class ProfileAnalysis(object):

    def __init__(self,obs,profile,template,pat_args=None):
        self.obs = obs
        self.profile = profile
        self.template = template
        self.pat_args = '' if (pat_args is None) else pat_args

        # run pat to make a "standard" TOA
        self._pat_toa = self.make_pat_toa()

        # run pat to get phase consistent with TOA analysis
        cmd = 'pat %s -s %s -R %s'%(self.pat_args,template,profile)
        rc,stdout,stderr = util.run(cmd,echo=False)
        try:
            phase_shift = float(
                stdout.split('<-turns')[0].strip().split()[-2])
        except Exception as e:
            phase_shift = 0.

        # load up profile and template
        a_templ = psrchive.Archive_load(template)
        a_profl = psrchive.Archive_load(profile)
        templ_amps = a_templ[0].get_Profile(0,0).get_amps()
        profl_amps = a_profl[0].get_Profile(0,0).get_amps()
        profl_freq = a_profl[0].get_Profile(0,0).get_centre_frequency()

        # rotate so peak for ease of display
        disp_shift = 0.5+float(np.argmax(profl_amps))/len(profl_amps)

        # rotate profile and template
        p = self.rotated_profile = rotated(profl_amps,disp_shift)
        t = rotated(templ_amps,disp_shift-phase_shift)

        # compute noise level
        nsamp = rfi.ppta_set_radiometer(obs)
        if nsamp == 0.:
            # some problem -- ignore zapping
            bw = abs(a_profl.get_bandwidth()*1e6)
            tobs = a_profl[0].get_duration()/a_profl.get_nbin()
            nsamp = 2*bw*tobs

        frac_err = nsamp**-0.5

        ahat,bhat,sp,st,snr,chi2,flux,fluxerr = fit_template(p,t,frac_err)
        # with baseline and rotated profile, and produce an absolute flux
        # plot at a later time
        self.baseline = bhat

        self.scaled_profile = sp
        self.scaled_template = st

        self.nsamp = nsamp
        self.snr = snr
        self.chi2 = chi2
        self.flux = flux
        self.fluxerr = fluxerr
        self.scale = a_profl.get_scale()
        self.mjd = obs.get_mjd()
        self.combo = obs.get_combo()
        self.phase_shift = phase_shift
        self.centre_frequency = profl_freq

        cmd = 'pdv -I %s'%profile # run pdv for comparison
        rc,stdout,stderr = util.run(cmd,echo=False)
        try:
            self.pdv_flux,self.pdv_fluxerr = stdout.split('\t')[3].split()[:2]
        except Exception as e:
            print 'Could not compute "pdv" style flux.'
            print e
            self.pdv_flux,self.pdf_fluxerr = None,None

    def residuals_plot(self,output=None):
        plot_profile_residuals(self.scaled_profile,self.scaled_template,
            self.snr,self.chi2,label=self.obs.basename,output=output)

    def get_toa_flags(self):
        return '-snr %.2f -gof %.2f'%(self.snr,self.chi2)

    def get_profile(self,nbin=None,residuals=False):
        """ Return baseline-subtracted, rotated profile."""
        try:
            prof = self.rotated_profile - self.baseline
        except:
            prof = self.scaled_profile
        if residuals:
            prof = self.scaled_profile-self.scaled_template
        if nbin is not None:
            while nbin > len(prof):
                new_prof = np.empty(len(prof)*2,dtype=prof.dtype)
                new_prof[::2] = prof
                new_prof[1::2] = prof
                prof = 0.5*new_prof
            while nbin < len(prof):
                prof = 0.5*(prof[::2] + prof[1::2])
            return prof

    def get_pat_toa(self,clobber=False):
        if clobber:
            self._pat_toa = self.make_pat_toa()
        try:
            return self._pat_toa
        except AttributeError:
            print 'Did not already have a pat toa! %s/%s'%(
                self.obs['name'],self.obs.basename)
            self._pat_toa = self.make_pat_toa()
        return self._pat_toa

    def has_pat_toa(self):
        return hasattr(self,'_pat_toa')

    def make_pat_toa(self):
        """ Return a tempo2-format TOA string with some flags.

        This is called on instantiation and cached, so needn't necessarily
        be called again.
        
        These flags are those that "can't change", e.g. the template the 
        TOA was generated with, the observation length, the combo.  However,
        leave things like the delay flags, data quality flags, etc. for
        later, so that they can be updated on the fly.
        """
        # generate TOA
        cmd = 'pat %s -f tempo2 -s %s %s'%(
            self.pat_args,self.template,self.profile)
        rc,stdout,stderr = util.run(cmd,echo=False)
        if rc != 0:
            print 'Failed to generate TOA.'
            return
        tmp = stdout.strip().split('\n')
        if (len(tmp) != 2):
            print 'TOA invalid format: ',tmp
            return
        toa = tmp[1] # strip out FORMAT 1

        # add in "static" flags
        obs = self.obs
        combo = obs.get_combo()
        toks = combo.split('_')
        if len(toks)==3:
            band,rcvr,backend = toks
        else:
            band = toks[0]
            backend = toks[-1]
            rcvr = '_'.join(toks[1:-1])
        toa = ' %s %s'%(toa,' '.join((
            '-f %s_%s'%(rcvr,backend),
            '-g %s_%s'%(band,backend),
            '-h %s_%s_%s'%(band,rcvr,backend),
            '-fe %s'%(rcvr),
            '-be %s'%(backend),
            '-B %s'%(band),
            '-length %.2f'%(obs.tobs), # for backwards compatibility
            '-tobs %.2f'%(obs.tobs),
            '-bw %.2f'%(abs(obs['bandwidth'])),
            '-nchan %d'%(obs['nchan']),
            '-pta PPTA',
            '-tmplt %s'%(basename(self.template)),
        )))
        return toa

def gain_analysis(procobs):
    """ Analyze the gain drifts over an observation.""" 

    # require obs has a TOA/ProfileAnalysis
    toa = procobs.get_toa()
    if toa is None:
        return

    try:
        prof = cPickle.load(file(toa+'.analysis'))
        toa = file(toa).readlines()[0]
    except IOError:
        return

    # TODO -- quality checks

    # load up subint/poln data
    profile = procobs.obsscr.get_scrunched_poln_time(procobs.obs)

    # construct time series of baselines
    a = psrchive.Archive_load(profile)
    a.fscrunch()
    nsub = len(a)

    # although we don't necessarily know the "denominator" for the weights,
    # under assumption that bandpass is levelled, the normalization will
    # cancel in the ratio of baselines
    baselines = np.empty([4,nsub,2])
    for i in xrange(4):
        for j in xrange(nsub):
            prof = a[j].get_Profile(i,0).get_amps()
            bhat,dev = iterative_baseline_noerr(prof,get_dev=True)
            baselines[i,j] = bhat/a[j].get_weight(0),dev
    mjd = np.empty(nsub)
    for j in xrange(nsub):
        mjd[j] = a[j].get_start_time().in_days()
    return baselines,mjd

def plot_baselines(procobs,outplot=None,fignum=2):
    
    import pylab as pl
    pl.figure(fignum); pl.clf()
    ax = pl.axes([0.10,0.12,0.84,0.83])

    baselines,mjd = gain_analysis(procobs)
    dt = 24*60*(mjd-mjd[0])
    dt += 24*60*0.5*np.mean(mjd[1:]-mjd[:-1])

    c1,c2,c3,c4 = baselines[...,0]
    ax.plot(dt,c1-c1[0],label='AA',color='gray',lw=2,marker='o')
    ax.plot(dt,c2-c2[0],label='BB',color='gray',lw=2,marker='^',ls='--')
    ax.plot(dt,c3-c3[0],label='AB',color='salmon',marker='o',lw=1)
    ax.plot(dt,c4-c4[0],label='BA',color='salmon',marker='^',lw=1,ls='--')

    ax.set_xlabel('Elapsed Time (min.)')
    ax.set_ylabel('Baseline Drift')
    pl.legend(loc='upper left',ncol=2,numpoints=1)
    if outplot is not None:
        pl.savefig(outplot)

def compare_profiles(profiles1,profiles2,make_plots=False,relative=True,
    vmin=None,vmax=None):
    """ Given a set of profiles, use tempo2 to compute the phase offset,
    then find scale and amplitude that minimize residuals.  Return the
    residuals.

    NB -- this is what I used to make the invariant interval plots with the
    simultaneous DFB3/DFB4 observations.  There is similar code in the
    analyses/dr2paper/j0437_analysis module.
    """
    assert(len(profiles1)==len(profiles2))

    mjds = deque()
    residuals = deque()
    profs1 = deque()
    profs2 = deque()
    shifts = deque()

    for p1,p2 in zip(profiles1,profiles2):

        # run pat to get phase consistent with TOA analysis
        cmd = 'pat -s %s -R %s'%(p1,p2)
        rc,stdout,stderr = util.run(cmd,echo=False)
        try:
            phase_shift = float(
                stdout.split('<-turns')[0].strip().split()[-2])
        except Exception as e:
            phase_shift = 0.
        # sigh
        phase_shift = -phase_shift

        # load up profile and template
        a1_profl = psrchive.Archive_load(p1)
        a2_profl = psrchive.Archive_load(p2)
        if a1_profl.get_nbin() > a2_profl.get_nbin():
            a1_profl.bscrunch_to_nbin(a2_profl.get_nbin())
        if a2_profl.get_nbin() > a1_profl.get_nbin():
            a2_profl.bscrunch_to_nbin(a1_profl.get_nbin())
        a1_amps = a1_profl[0].get_Profile(0,0).get_amps().copy()
        a2_amps = a2_profl[0].get_Profile(0,0).get_amps().copy()
        assert(len(a1_amps)==len(a2_amps))

        # rotate so peak for ease of display
        disp_shift = 0.5+float(np.argmax(a2_amps))/len(a2_amps)

        # rotate profile and template
        p = rotated(a1_amps,disp_shift)
        t = rotated(a2_amps,disp_shift-phase_shift)

        # some problem -- ignore zapping
        bw = abs(a1_profl.get_bandwidth()*1e6)
        tobs = a1_profl[0].get_duration()/a1_profl.get_nbin()
        nsamp = 2*bw*tobs

        frac_err = nsamp**-0.5

        ahat,bhat,sp,st,snr,chi2,flux,fluxerr = fit_template(p,t,frac_err)

        if relative:
            residuals.append((p-bhat)/ahat/t - 1)
        else:
            residuals.append((p-bhat)/ahat - t)
        #residuals.append((p/ahat-bhat)/t - 1)
        mjds.append(a1_profl[0].get_epoch().in_days())
        shifts.append(phase_shift)
        #profiles.append(p)
        profs1.append((p-bhat)/ahat)
        profs2.append(t)

    nbins = np.asarray([len(x) for x in residuals])
    nbin = util.most_common(nbins)

    def fix_bins(a,bin_goal):
        if len(a) == bin_goal:
            return a
        if len(a) > bin_goal:
            fac = int(round(np.log2(len(a) / bin_goal)))
            for i in xrange(fac):
                a = 0.5*(a[::2]+a[1::2])
            try:
                assert(len(a)==bin_goal)
            except AssertionError as e:
                print bin_goal,len(a)
                raise e
        tmp = np.empty(bin_goal)
        fac = bin_goal/len(a)
        for i in xrange(fac):
            tmp[i::fac] = a[:]
        assert(len(tmp)==bin_goal)
        return tmp
        
    residuals = np.asarray([fix_bins(x,nbin) for x in residuals])
    profs1 = np.asarray([fix_bins(x,nbin) for x in profs1])
    profs2 = np.asarray([fix_bins(x,nbin) for x in profs2])
    a = np.argsort(mjds)
    mjds = np.asarray(mjds)[a]
    residuals = residuals[a]
    profs1 = profs1[a]
    profs2 = profs2[a]
    shifts = np.asarray(shifts)[a]

    """
    mask = mjds < 57000
    mjds = mjds[mask]
    residuals = residuals[mask]
    profs1 = profs1[mask]
    profs2 = profs2[mask]
    """


    nepoch = 3000
    grid_epochs = np.linspace(mjds.min(),mjds.max(),nepoch)
    indices = np.searchsorted(mjds,grid_epochs)
    output = residuals[indices]

    if make_plots:
        import pylab as pl
        pl.figure(1); pl.clf()
        vmin = vmin or -0.04
        vmax = vmax or 0.04
        pl.imshow(output.transpose(),interpolation='nearest',aspect='auto',
            extent=[mjds[0],mjds[-1],0,1],vmin=vmin,vmax=vmax)
        pl.xlabel('MJD')
        pl.ylabel('Phase')

        pl.figure(2); pl.clf()
        dom = np.linspace(0,1,nbin+1)[:-1]
        pl.plot(dom,np.median(residuals,axis=0),color='k')
        pl.axhline(0,ls='--',color='k')
        pl.axis([0.0,1.0,-0.04,0.04])
        pl.xlabel('Phase')
        pl.ylabel('Relative Difference')
        ax = pl.twinx()
        ax.plot(dom,np.median(profs1,axis=0),color='salmon',ls='-')
        ax.set_ylabel('Profile Amplitude')
        ax.axis([0.0,1.0,-ax.axis()[3],ax.axis()[3]])


    return mjds,residuals,profs1,profs2,shifts,output

