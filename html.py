"""
Generate HTML output containing pipeline/data diagnostic information.

$Header: /nfs/slac/g/glast/ground/cvs/users/kerrm/tpipe/timing.py,v 1.14 2014/01/21 06:46:59 kerrm Exp $

author: Matthew Kerr <matthew.kerr@gmail.com>
"""
import numpy as np
from collections import deque
import os
from os.path import basename,join,abspath,relpath,realpath,isfile
import cPickle

import band
import util
import plotting
import processed

# thoughts for improving the grim situation of flexible output
# want a general "overview" class that's capable of handling a variety of
# band/plot combos.  So stick with the "row" picture, but generalize to
# allow each row to generate its own sub table.  Each child class will be
# made unique by specifying a Layout and a set of ComboPlot objects that
# slot into the pieces.  With a bonus column for jname etc.

# layout needs to support "overflow", e.g. if we have 20 combos but want
# to restrict to a 4x4 flow; or use CSS to reflow

# then finally, have the child pages remain more or less the same, but
# generate two versions, a "complete" and a "recent" which are interlinked

# throughout, the mapping should be procobs <--> plots, to allow e.g. 
# residuals to be made with pretty much anything

# these are default dimensions for pav and psrplot pngs
BASE_W=850
BASE_H=680

# scale to these dimensions
SCALE_W=int(0.450*BASE_W)
SCALE_H=int(0.450*BASE_H)

# crop by these factors
CROP_TOP=int(0.145*SCALE_H)
CROP_BOT=int(0.210*SCALE_H)
CROP_LFT=int(0.065*SCALE_W)
CROP_RGT=int(0.140*SCALE_W)

def residuals_plot(outplot,parfile,timfile,select=None,
    snr_cut=3,chi2_cut=1000):
    """ Collate TOAs in output table and plot."""
    if timfile is None:
        'No tim file!  Returning...'
        return
    plotting.plot_toa_residuals(parfile,str(timfile),outplot=outplot,
        select=select,snr_cut=snr_cut,chi2_cut=chi2_cut)

def snr_plot(outplot,timfile):
    plotting.plot_toa_sn_spectrum(str(temp_toa),
        outplot=outplot.replace('residuals','spectrum'))

def coadd_plot(outplot,coadded_profile):
    """ Make a plot of the coadded profile."""
    if not isfile(coadded_profile):
        return
    cmd = plotting.StokesPlot().get_cmd(coadded_profile,outplot)
    cmd(echo=True,verbosity=1)

def carpet_plot(outplot,coadded_profile):
    if not isfile(coadded_profile):
        return
    plotting.plot_coadd_residual(coadded_profile,outplot=outplot)

def get_dm_etc(eph):
    """ Return DM, period, etc. from an ephemeris string."""
    keys = ['DM','F0','F1']
    if eph is None:
        return [0.]*len(keys)
    d = dict()
    for key in keys:
        d[key] = 0.
    for line in eph.split('\n'):
        for key in d.keys():
            if line.startswith(key+' '):
                d[key] = float(line.split()[1])
    #p0 = 0. if d['F0']==0. else 1./d['F0']
    return [d[k] for k in keys]

class HTMLSegment(object):

    def __init__(self,outpath=None):
        self.files = set()
        self.outpath = None if (outpath is None) else realpath(outpath)

    def _link_target(self,fname):
        if fname is None:
            return '' 
        #if not os.path.isfile(fname):
            #return ''
        fname = realpath(fname)
        self.files.add(fname)
        if self.outpath is not None:
            output = relpath(fname,self.outpath)
        else:
            output = basename(fname)
        output = output.replace(':','%3A') # escape colons!
        return output

    def _link_img(self,img,thumb=None,double=False,valid=True,nocrop=False):
        if not valid:
            return ''
        img = self._link_target(img)
        thumb = img if thumb is None else self._link_target(thumb)
        vp = 'viewport'
        if double:
            vp += '2'
        if nocrop:
            vp += '_nocrop'
        return '<div class="%s"><a href="%s"><img src="%s"></a></div>'%(vp,img,thumb)

    def _link_file(self,fname,label=''):
        return '<a href=\'%s\'>%s</a>'%(self._link_target(fname),label)

    def _header(self,code,level=2):
        return '<h%d>%</h%d>'%(level,code,level)

    def _font(self,code,color='black',size=None):
        size = size or ''
        return '<font color="%s" size="%s">%s</font>'%(color,size,code)

    def tarball(self,outpath,additional_files=[],fname='web.tar.gz'):
        """ Write out a tarball with a local version of HTML structure."""

        # use temporary file to store all filenames -- too long for cmdline
        outpath = realpath(abspath(outpath))
        cwd = os.getcwd()
        try:
            os.chdir(outpath)
            files = map(lambda f: relpath(f,outpath),
                list(self.files) + map(realpath,additional_files))
            tmp = util.TemporaryFile('\n'.join(files))
            cmd = 'tar -cvzf %s --files-from=%s'%(fname,tmp)
            util.run(cmd,echo=False)
        except Exception as e:
            print 'Encountered exception in generating tarball.'
            print e
        finally:
            os.chdir(cwd)

class TableRows(HTMLSegment):
    """ Emit HTML for the rows of a pipeline table."""

    def __init__(self,analysis,outpath=None):
        super(TableRows,self).__init__(outpath)
        self.analysis = analysis
        self.psrpath = analysis._pscr.outpath
        self.calpath = analysis._cscr.outpath
        self.label = ''
        if analysis._pscr.label is not None:
            self.label = '.%s'%analysis._pscr.label
        #self.outpath = outpath
        self.rows = self.get_rows()

    def __len__(self):
        return len(self.rows)

    def get_rows(self):
        obs,cals = self.analysis.psrobs,self.analysis.cals
        obs,cals = (list(x) for x in zip(*sorted(
            zip(obs,cals),key=lambda p: p[0].get_mjd(),reverse=True)))
        return map(self.get_row,obs,cals)

    def get_row(self,obs,cal):
        html = deque(['<tr>'])
        html.append(self._text(obs,cal))
        top,bot = self._psr_plots(obs,html,valid=cal.valid())
        plots = self._cal_plots(cal.diode,html,valid=cal.valid())
        plots.extend(self._misc_plots(obs,html,valid=cal.valid()))
        q1,q2 = (top,bot) if (len(top)<=len(bot)) else (bot,top)
        for i in xrange(len(plots)):
            q1.append(plots[i])
            q1,q2 = q2,q1
        html.extend(q1)
        html.append('</tr>\n<tr>')
        html.extend(q2)
        html.append('</tr>')
        return '\n'.join(html)

    def _sub_table(self,obs):
        """ Make a summary table for observation characteristics."""
        sub_table = []
        try:
            sub_table.append(['mjd','%.2f'%(round(obs.get_mjd()))])
        except Exception:
            sub_table.append(['mjd','unknown'])
        try:
            sub_table.append(['tobs','%ds'%(round(obs.tobs))])
        except Exception:
            sub_table.append(['tobs','unknown'])
        try:
            sub_table.append(['nchan', str(obs['nchan'])])
        except Exception:
            sub_table.append(['nchan','unknown'])
        try:
            sub_table.append(['nbin', str(obs['nbin'])])
        except Exception:
            sub_table.append(['nbin','unknown'])
        try:
            sub_table.append(['freq', '%d MHz'%(round(obs['frequency']))])
        except Exception:
            sub_table.append(['freq','unknown'])
        try:
            sub_table.append(['bw', '%d MHz'%(round(obs['bandwidth']))])
        except Exception:
            sub_table.append(['bw','unknown'])
        sub_table = ['<td>%s</td><td>%s</td>'%(x,y) for x,y in sub_table]
        sub_table = ['<tr>%s</tr>'%x for x in sub_table]
        sub_table = '\n<table cellspacing=0>%s\n</table>'%(
            '\n'.join(sub_table))
        return '<div class="black_border">%s</div>\n'%sub_table

    def _text(self,obs,cal):
        name = '%s'%(obs['name'])
        rcvr = '%s / %s'%(obs['receiver'],str(band.Band(obs)))
        back = '%s'%(obs['backend'])
        proj = '%s'%(obs['projid'])

        sub_table = self._sub_table(obs)
        if cal.valid():
            cal_script = join(self.calpath,cal.diode.basename+'.psrsh')
            calh = self._link_file(cal_script,cal.diode.basename)
        else:
            calh = self._font('NO CAL!','red')
        psr_script = obs.get_stem(self.psrpath)+self.label+'.psrsh'
        psrh = self._link_file(psr_script,obs.basename)
        pcm = 'pcm: None' if (cal.pcm is None) else 'pcm: %s'%cal.pcm.basename
        flux = 'flux: None' if (cal.fluxcal is None) else 'flux: %s'%cal.fluxcal.basename
        #t = '<br>'.join([rcvr,back,proj,psrh,calh,tobs,bw,nchan,nbin,pcm])
        t = '<br>'.join([name,rcvr,back,proj,psrh,calh,pcm,flux,sub_table])

        # if there are any data quality flags, use a red border for the cell
        if len(obs.data_flags) > 0:
            t = '<div class="red_border">%s</div>'%(t)

        # check for a frequency drift; if so use a green border (g = glitch)
        # this doesn't work very well...
        #d = obs.get_ancillary(self.psrpath)
        #if 'dfreq_val' in d.keys():
            #if ((abs(d['dfreq_val']) > 2e-6) and (d['dfreq_sig'] > 1500)):
                #t = '<div class="green_border">%s</div>'%(t)
            
        return '<td rowspan=2>%s</td>'%(t)

    def _psr_plots(self,obs,html,valid=True):
        """ Make one or more double-sized plots, then stagger those remaining.
        """
        plots = self.analysis.psr_plots
        top,bot = deque(),deque()

        # first element
        # NB that we need to hard code TOA residual... oh well
        profile = self.analysis._pscr.get_target(obs)
        img = profile + '.toa.residuals.png'
        html.append('<td colspan=2 rowspan=2>%s</td>'%(
            self._link_img(img,double=True,nocrop=True,valid=valid)))
        if len(plots)<1:
            return top,bot

        # second element
        img = plots[0].get_target(obs,self.analysis._pscr)
        html.append('<td colspan=2 rowspan=2>%s</td>'%(
            self._link_img(img,double=True,valid=valid)))
        if len(plots)<2:
            return top,bot

        # now put the odd strides on the top row
        for plot in plots[1::2]:
            img = plot.get_target(obs,self.analysis._pscr)
            top.append('<td>%s</td>'%(self._link_img(img,valid=valid)))
        if len(plots)<3:
            return top,bot

        #html.append('</tr><tr>' + '<td></td>'*0)
        for plot in plots[2::2]:
            img = plot.get_target(obs,self.analysis._pscr)
            bot.append('<td>%s</td>'%(self._link_img(img,valid=valid)))
        return top,bot

    def _cal_plots(self,cal,html,valid=True):
        calplots = deque()
        for plot in self.analysis.cal_plots:
            img = plot.get_target(cal,self.analysis._cscr)
            #img = join(self.calpath,basename(img))
            calplots.append('<td>%s</td>'%(self._link_img(img,valid=valid)))
        return calplots

    def _misc_plots(self,obs,html,valid=True):
        """ Any other miscellaneous plots."""
        # subint zapping
        mplots = deque()
        img = join(
            self.analysis._pscr.outpath,obs.basename+'.subint_zapping.png')
        mplots.append('<td>%s</td>'%(self._link_img(img,valid=valid,nocrop=False)))
        return mplots

    def to_html(self,fname=None,title=None,forematter=None):
        forematter = forematter or ''
        body = '%s<table cellspacing=0 border=0>\n%s\n</table>'%(
            forematter,'\n'.join(self.rows))
        html = html_template(body,title,style=css)
        if fname is None:
            return html
        else:
            file(fname,'w').write(html)

class P574_TableRows(TableRows):
    """ A class for stringing together observations for P574 data.
        
    These should all have a common band and arranged by time.

    If specified, exclude observations more than max_lapse days before
    the final observation of the set.
    """

    def __init__(self,analysis,combo,max_lapse=None,outpath=None):
        self.is_band = len(combo.split('_'))==1
        self.combo = combo
        self.max_lapse = max_lapse
        super(P574_TableRows,self).__init__(analysis,outpath)
        self.rows,self.toas = self.rows # klugey, ah well

    def _get_matches(self,obs):
        """ Return combo or band according to class."""
        combos = [ob.get_combo() for ob in obs]
        if self.is_band:
            return map(lambda c: c.split('_')[0],combos)
        return combos

    def get_rows(self):
        lines = deque()
        toas = deque()
        obs = deque()
        cals = deque()

        # first, select correct combo
        combos = self._get_matches(self.analysis.psrobs)
        for ob,ca,combo in zip(
            self.analysis.psrobs,self.analysis.cals,combos):
            if combo == self.combo:
                obs.append(ob)
                cals.append(ca)

        # now, sort by MJD
        obs,cals = (list(x) for x in zip(*sorted(
            zip(obs,cals),key=lambda p: p[0].get_mjd(),reverse=True)))

        # finally, filter observations by time if necessary
        if self.max_lapse is not None:
            tmax = obs[0].get_mjd() # already reverse chrono sorted
            for i in xrange(len(obs)):
                if (tmax-obs[i].get_mjd()) > self.max_lapse:
                    break
            obs = obs[:i]
            cals = cals[:i]

        for ob,cal,com in zip(obs,cals,combos):
            lines.append(self.get_row(ob,cal))
            if ob.valid_toa():
                profile = self.analysis._pscr.get_target(ob)
                toas.append(profile + '.toa')
        return lines,toas

class P574_OverviewTable(HTMLSegment):
    """ A class to display summary plots for each pulsar.
    
    Each analysis is added and cached with its HTML elements in memory.  
    This allows a final, interlinked output to be generated at conclusion.

    This object keeps track of a list of pulsars, which are represented by
    P574_TableRows objects.

    P574_OverviewTable
        self.tables
        -- "J0835-4510"
            -- "20CM_MULTI_DFB1" --> P574_TableRows
            -- "20CM_HOH_DFB2" --> P574_TableRows
        -- "J0030+0451"
        ...
    """

    def __init__(self,outpath,max_lapse=None):
        self.max_lapse = max_lapse
        super(P574_OverviewTable,self).__init__(outpath)
        self.tables = dict()
        self.band_only = False

    def _filter_combo(self,combo):
        """ Determine whether to include results for a band/receiver combo.
        """
        return True

    def _add_combos(self,jname,analysis,outpath):
        """ Set up dictionary entry for a pulsar.  Clobbers old entry."""
        if jname in self.tables:
            print 'Clobbering old entry for %s.'%jname
        self.tables[jname] = dict()
        combos = filter(self._filter_combo,
            analysis.get_combos(band_sort=True))
        if outpath is None:
            outpath = analysis._pscr.outpath # NB -- cal plots location?
        self.tables[jname]['outpath'] = outpath
        self.tables[jname]['combos'] = combos
        bands = sorted(set((x.split('_')[0] for x in combos)))
        self.tables[jname]['bands'] = bands
        if analysis.ephdb is not None:
            ephemeris = analysis.ephdb(jname)
            self.tables[jname]['ephemeris'] = analysis.ephdb(jname)

    def _add_table(self,jname,analysis,combo):
        """ Make an entry for band/combo and return (if more than one) the
            primary table.
        """
        outpath = self.tables[jname]['outpath']
        table = P574_TableRows(analysis,combo,outpath=outpath,
            max_lapse=self.max_lapse)
        self.tables[jname][combo] = table
        self.files = self.files.union(table.files)
        return table

    def _residuals_plot(self,jname,combo):
        """ Return appropriate filename for TOA residuals plot."""
        label = '%s_%s'%(jname,combo)
        outpath = self.tables[jname]['outpath']
        return join(outpath,'%s_residuals.png'%(label))

    def residuals_plots(self,jname,analysis,combo):
        outplot = self._residuals_plot(jname,combo)
        if self.tables[jname]['ephemeris'] is not None:
            temp_par = util.TemporaryFile()
            analysis.gather_ephemeris(jname,output=str(temp_par))

            table = self.tables[jname][combo]
            residuals_plot(outplot,str(temp_par),table)

            try:
                table = self.tables[jname][combo+'_recent']
                outplot = outplot.replace('residuals','residuals_recent')
                residuals_plot(outplot,str(temp_par),table)
            except KeyError:
                pass

    def add_entry(self,jname,analysis,plot_toas=True,outpath=None):
        """ Add entries for a specific pulsar using obs in analysis.

            Under each jname, each receiver/backend combo is stored 
            separately via the "tables" class member.
        """
        
        if analysis.empty:
            return

        self._add_combos(jname,analysis,outpath)
        combos = self._get_combos(jname)
        bands = self.tables[jname]['bands']
        outpath = self.tables[jname]['outpath']

        # combos handled separately -- merge with parent?
        for combo in combos:
            table = self._add_table(jname,analysis,combo)

            # if "band" type, don't make residuals plots for combos
            if self.band_only and (combo not in bands):
                continue

            # if toas available, make a tempo2 plot
            if plot_toas:
                self.residuals_plots(jname,analysis,combo)

    def get_rows(self):
        """ Generate HTML for overview page."""
        rows = deque()
        jnames = self.tables.keys()
        for jname in jnames:
            combos = self.tables[jname]['combos']
            outpath = self.tables[jname]['outpath']

            current_band = combos[0].split('_')[0]

            html = deque(['<tr><td>%s<hr></td></tr>'%(
                self._font(jname,size=5,color='green'))])
            html.append('\n<tr><td>%s<hr></td></tr>'%(
                self._font(current_band,size=5,color='green')))
            html.append('\n<tr>\n')
            for combo in combos:
                new_band = combo.split('_')[0]
                if current_band != new_band:
                    current_band = new_band
                    html.append('\n</tr>\n<tr><td>%s<hr></td></tr>\n<tr>\n'%(
                        self._font(current_band,size=5,color='green')))
                label = '%s_%s'%(jname,combo)
                output = join(outpath,'%s.html'%label)
                nobs = len(table.rows)

                if nobs == 0:
                    continue
                html.append('<td>')
                html.append('%s %d'%(self._link_file(output,combo),nobs))

                outplot = self._residuals_plot(jname,combo)
                if isfile(outplot):
                    ihtml = self._link_img(outplot,double=False,nocrop=True)
                    html.append('<br>%s'%(ihtml))

                html.append('<td>')

            html.append('\n</tr>\n')
            rows.append(''.join(html))
        return rows

    def get_body(self,rows):
        """ Generate the actual body from lesser elements."""
        body = '<table id="overview_table">\n%s\n</table>'%('\n'.join(rows))
        return body

    def _get_landing_link_prefs(self):
        return ['20CM_MULTI_PDFB3'],'combos'

    def _get_combos(self,jname):
        return self.tables[jname]['combos']

    def _get_tables(self,jname,get_all=False):
        """ Return Table objects, labels, and HTML outputs."""
        keys = self._get_combos(jname)
        outpath = self.tables[jname]['outpath']
        htmls = [join(outpath,'%s_%s.html'%(jname,key)) for key in keys]
        tables = [self.tables[jname][key] for key in keys]
        return tables,keys,htmls

    def _landing_link(self,jname):
        """ Return the link for an overview sub-page for a given pulsar."""
        preferences,key = self._get_landing_link_prefs()
        tables,combos,htmls = self._get_tables(jname)
        for preference in preferences:
            for combo,html in zip(combos,htmls):
                if preference==combo:
                    return html
        return htmls[0]

    def _common_block_jname(self,jname,fname):
        """ Generate the HTML for a common block of a pulsar-specific page.

        This method generates only the code linking amongst other pulsars
        and the overview page, allowing child classes to perform more
        specific linking amongst band/combos.
        """

        jnames = sorted(self.tables.keys())
        tables,combos,htmls = self._get_tables(jname)
        dummy = tables[0]
        line1 = self._font(
            dummy._link_file(self._landing_link(jname),label=jname),
            color='black',size=5)+'<br>'
        curr_idx = jnames.index(jname)
        next_psr = jnames[(curr_idx+1)%len(jnames)]
        prev_psr = jnames[(curr_idx-1)%len(jnames)]
        over_link= dummy._link_file(
            join(self.outpath,fname),label='Overview')
        next_link = dummy._link_file(self._landing_link(next_psr),
            label='Next (%s)'%next_psr)
        prev_link = dummy._link_file(self._landing_link(prev_psr),
            label='Prev (%s)'%prev_psr)
        line2 = self._font(
            '\n'.join([over_link,next_link,prev_link]),
            color='black',size=5)+'<br>'
        return line1,line2

    def _common_block(self,jname,fname='index.html'):
        """ Generate the HTML for a common block of a pulsar-specific page.
        """

        line1,line2 = self._common_block_jname(jname,fname)
        tables,combos,htmls = self._get_tables(jname)
        pulsar_links = deque()
        toa_plot_links = deque()
        for table,combo,output in zip(tables,combos,htmls):
            nobs = len(table.rows)
            if nobs == 0:
                continue
            pulsar_links.append(
                table._link_file(output,label='%s (%d)'%(combo,nobs)))
            outplot = self._residuals_plot(jname,combo)
            if isfile(outplot):
                toa_plot_links.append(table._link_img(
                    outplot,double=True,nocrop=True) + '<br>')

        line3 = self._font('\n'.join(pulsar_links),color='black',size=5)
        line4 = '\n'.join(toa_plot_links)
        return line1+line2+line3+'<br>'+line4+'<br'

    def _link_tables(self,fname='index.html'):
        """ Go through stored tables, interlink them, and write them out.
        
        Anything that should be common to all pages should be written out
        in the "common block" methods.  Thus, e.g., TOA residuals plots
        could/should go there.  While items restricts to a particular combo
        (e.g. a summary plot) should go here.
        """

        jnames = sorted(self.tables.keys())
        for jname in jnames:
            cb = self._common_block(jname,fname=fname)
            tables,combos,htmls = self._get_tables(jname)
            for table,combo,output in zip(tables,combos,htmls):
                if len(table.rows) == 0:
                    continue
                self._link_target(output) # ensure we have all html
                outplot = self._residuals_plot(jname,combo)
                if isfile(outplot):
                    link = table._link_img(
                        outplot,double=True,nocrop=True)
                    stuff = cb + '<br>\n%s'%link
                else:
                    stuff = cb
                title = '%s %s'%(jname,combo)
                table.to_html(fname=output,title=title,forematter=stuff)

    def write(self,title='P574 Pulsars',fname='index.html'):
        self._link_tables(fname=fname)
        rows = self.get_rows()
        body = self.get_body(rows)
        html = html_template(body,title,style=css)
        output = join(self.outpath,fname)
        self.files.add(output)
        file(output,'w').write(html)

    def tarball(self,fname='overview.tar.gz'):
        super(P574_OverviewTable,self).tarball(self.outpath,fname=fname)

class P574_SessionTable(P574_OverviewTable):
    """ A class to display summary plots for every observation in a sess.
    """

    def add_entry(self,jname,analysis,plot_toas=False):
        super(P574_SessionTable,self).add_entry(
            jname,analysis,plot_toas=plot_toas,outpath=self.outpath)

    def get_rows(self):
        return None

    def get_body(self,rows):
        jnames = sorted(self.tables.keys())
        body = deque()
        for jname in jnames:
            combos = self.tables[jname]['combos']
            for c in combos:
                table = self.tables[jname][c]
                body.append(table.to_html(fname=None))
        return '\n'.join(body)

    def write(self,title='P574 Session',fname='session.html'):
        super(P574_SessionTable,self).write(title=title,fname=fname)
        jnames = sorted(self.tables.keys())

    def tarball(self,fname='session.tar.gz'):
        additional_files = [join(self.outpath,'session.html')]
        super(P574_OverviewTable,self).tarball(
            self.outpath,additional_files=additional_files,fname=fname)

class PPTA_OverviewTable(P574_OverviewTable):
    """ A class for display summary plots for every pulsar.
    
    Each analysis is added and cached with its HTML elements in memory.  
    This allows a final, interlinked output to be generated at conclusion.
    """

    def _filter_combo(self,combo):
        if combo.split('_')[0] in ['10CM','20CM','40CM','50CM']:
            return True
        return False

    def write(self,title='PPTA Pulsars',fname='index.html'):
        super(PPTA_OverviewTable,self).write(title=title,fname=fname)

class P574_BandOverviewTable(P574_OverviewTable):
    """ A class to display summary plots for each pulsar.

    This version is grouped by band by adding an extra layer to the
    hierarchy.
    """

    def __init__(self,outpath,max_lapse=None):
        super(P574_BandOverviewTable,self).__init__(outpath,max_lapse)
        self.band_only = True

    def get_rows(self):
        """ Generate HTML for overview page."""

        rows = deque()
        jnames = sorted(self.tables.keys())
        for jname in jnames:
            combos = self.tables[jname]['combos']
            bands = self.tables[jname]['bands']
            outpath = self.tables[jname]['outpath']

            html = deque(['<tr><td>%s<hr></td></tr>'%(
                self._font(jname,size=5,color='green'))])
            for band in bands:
                label = '%s_%s'%(jname,band)
                output = join(outpath,'%s.html'%label)
                nobs = len(self.tables[jname][band].rows)
                if nobs == 0:
                    continue

                html.append('<td>')
                html.append('%s %d'%(self._link_file(output,band),nobs))

                outplot = self._residuals_plot(jname,band)
                if isfile(outplot):
                    ihtml = self._link_img(outplot,double=False,nocrop=True)
                    html.append('<br>%s'%(ihtml))

                html.append('<td>')

            html.append('\n</tr>\n')
            rows.append(''.join(html))
        return rows

    def _common_block(self,jname,fname='index.html'):
        """ Generate the HTML for a common block of a pulsar-specific page.

        This version is sorted by band and combo.
        """

        line1,line2 = self._common_block_jname(jname,fname)
        tables,combos,htmls = self._get_tables(jname)
        pulsar_links = deque()
        for table,combo,output in zip(tables,combos,htmls):
            nobs = len(table.rows)
            if nobs == 0:
                continue
            is_band = len(combo.split('_')) == 1
            pulsar_links.append('</ul>' if is_band else '<li>')
            pulsar_links.append(
                table._link_file(output,label='%s (%d)'%(combo,nobs)))
            if is_band:
                pulsar_links.append('<ul>')
        pulsar_links.popleft() # remove first </ul>
        pulsar_links.append('</ul>') # add one to close last list

        line3 = self._font('\n'.join(pulsar_links),color='black',size=5)
        return line1+line2+line3+'<br>'

    def _get_landing_link_prefs(self):
        return ['20CM'],'bands'

    def _get_combos(self,jname):
        return sorted(
            self.tables[jname]['bands']+self.tables[jname]['combos'])

    def tarball(self,fname='band_overview.tar.gz'):
        super(P574_BandOverviewTable,self).tarball(fname=fname)

class P574_BandHybridOverviewTable(P574_BandOverviewTable):
    """ A class to display summary plots for each pulsar.

    This version is grouped by band by adding an extra layer to the
    hierarchy.

    It also maintains two tables for each band/combo -- one with all obs.,
    one with obs within max_lapse of the last obs. in the band/combo.
    """

    def __init__(self,outpath,max_lapse=90):
        super(P574_BandOverviewTable,self).__init__(outpath,max_lapse)
        self.band_only = True

    def _add_table(self,jname,analysis,combo):
        """ Make an entry for band/combo and return (if more than one) the
            primary table.
        """
        outpath = self.tables[jname]['outpath']
        table = P574_TableRows(analysis,combo,outpath=outpath,
            max_lapse=None)
        table_recent = P574_TableRows(analysis,combo,outpath=outpath,
            max_lapse=self.max_lapse)
        self.tables[jname][combo] = table
        self.tables[jname][combo+'_recent'] = table_recent
        self.files = self.files.union(table.files)
        self.files = self.files.union(table_recent.files)
        return table

    def _get_tables(self,jname,get_all=False):
        """ Return Table objects, labels, and HTML outputs."""
        if not get_all:
            return super(P574_BandHybridOverviewTable,self)._get_tables(
                jname)
        keys = self._get_combos(jname)
        t = self.tables[jname]
        outpath = t['outpath']
        htmls = [(join(outpath,'%s_%s.html'%(jname,key)),join(outpath,'%s_%s_recent.html'%(jname,key))) for key in keys]
        tables = [(t[key],t[key+'_recent']) for key in keys]
        return tables,keys,htmls

    def get_rows(self):
        """ Generate HTML for overview page."""

        rows = deque()
        jnames = sorted(self.tables.keys())
        for jname in jnames:
            tables,combos,htmls = self._get_tables(jname,get_all=True)

            html = deque(['<tr><td>%s<hr></td></tr>'%(
                self._font(jname,size=5,color='green'))])
            for table,combo,output in zip(tables,combos,htmls):
                if len(combo.split('_')) > 1:
                    continue # only bands
                table,table_recent = table
                output,output_recent = output
                nobs = len(table.rows)
                nobs_recent = len(table_recent.rows)
                if nobs == 0:
                    continue

                html.append('<td>')
                html.append('%s %s '%(combo,self._link_file(output_recent,'Last (%d)'%nobs_recent)))
                html.append(self._link_file(output,'All (%d)'%(nobs)))

                outplot = self._residuals_plot(jname,combo)
                if isfile(outplot):
                    ihtml = self._link_img(outplot,double=False,nocrop=True)
                    html.append('<br>%s'%(ihtml))

                html.append('<td>')

            html.append('\n</tr>\n')
            rows.append(''.join(html))
        return rows

    def _common_block(self,jname,fname='index.html'):
        """ Generate the HTML for a common block of a pulsar-specific page.

        This version is sorted by band and combo.
        """

        line1,line2 = self._common_block_jname(jname,fname)
        tables,combos,htmls = self._get_tables(jname,get_all=True)
        pulsar_links = deque()
        for table,combo,output in zip(tables,combos,htmls):
            table,table_recent = table
            output,output_recent = output
            nobs = len(table.rows)
            nobs_recent = len(table_recent.rows)
            if nobs == 0:
                continue
            is_band = len(combo.split('_')) == 1
            pulsar_links.append('</ul>' if is_band else '<li>')
            pulsar_links.append('%s %s'%(combo,table_recent._link_file(output_recent,label='Last (%d)'%nobs_recent)))
            pulsar_links.append(
                table._link_file(output,label=' All (%d)'%nobs))
            if is_band:
                pulsar_links.append('<ul>')
        pulsar_links.popleft() # remove first </ul>
        pulsar_links.append('</ul>') # add one to close last list

        line3 = self._font('\n'.join(pulsar_links),color='black',size=3)
        return line1+line2+line3+'<br>'

    def _landing_link(self,jname):
        """ Return the link for an overview sub-page for a given pulsar."""
        preferences,key = self._get_landing_link_prefs()
        tables,combos,htmls = self._get_tables(jname,get_all=True)
        for preference in preferences:
            for combo,html in zip(combos,htmls):
                if preference==combo:
                    return html[1]
        return htmls[0][1]

    def _link_tables(self,fname='index.html'):
        """ Go through stored tables, interlink them, and write them out.
        
        Anything that should be common to all pages should be written out
        in the "common block" methods.  Thus, e.g., TOA residuals plots
        could/should go there.  While items restricts to a particular combo
        (e.g. a summary plot) should go here.
        """

        jnames = sorted(self.tables.keys())
        for jname in jnames:
            cb = self._common_block(jname,fname=fname)
            tables,combos,htmls = self._get_tables(jname,get_all=True)
            for table,combo,output in zip(tables,combos,htmls):
                table,table_recent = table
                output,output_recent = output
                if len(table.rows) == 0:
                    continue
                self._link_target(output) # ensure we have all html
                self._link_target(output_recent) # ensure we have all html

                title = '%s %s (All)'%(jname,combo)
                link = deque([cb])
                # link in plots
                outplot = self._residuals_plot(jname,combo)
                if isfile(outplot):
                    link.append('<br>\n<table><tr><td>')
                    link.append(table._link_img(
                        outplot,double=True,nocrop=True))
                    outplot = outplot.replace('residuals','spectrum')
                    if isfile(outplot):
                        link.append('</td><td>')
                        link.append(table._link_img(
                            outplot,double=True,nocrop=True))
                    link.append('</td></tr></table>')
                link = '\n'.join(link)
                table.to_html(fname=output,title=title,forematter=link)

                title = '%s %s (Last)'%(jname,combo)
                table,output = table_recent,output_recent
                link = deque([cb])
                # link in plots
                outplot = self._residuals_plot(jname,combo).replace(
                    'residuals','residuals_recent')
                if isfile(outplot):
                    link.append('<br>\n<table><tr><td>')
                    link.append(table._link_img(
                        outplot,double=True,nocrop=True))
                    outplot = outplot.replace('residuals','spectrum')
                    if isfile(outplot):
                        link.append('</td><td>')
                        link.append(table._link_img(
                            outplot,double=True,nocrop=True))
                    link.append('</td></tr></table>')
                link = '\n'.join(link)
                table.to_html(fname=output,title=title,forematter=link)

class PPTA_BandHybridOverviewTable(P574_BandHybridOverviewTable):
    """ A class to display summary plots for each pulsar.

    This version is grouped by band by adding an extra layer to the
    hierarchy.

    It also maintains two tables for each band/combo -- one with all obs.,
    one with obs within max_lapse of the last obs. in the band/combo.
    """

    def _filter_combo(self,combo):
        if combo.split('_')[0] in ['10CM','20CM','40CM','50CM']:
            return True
        return False

    def write(self,title='PPTA Pipeline Overview',fname='index.html'):
        super(PPTA_BandHybridOverviewTable,self).write(
            title=title,fname=fname)

    def get_rows(self):
        """ Generate HTML for overview page."""

        rows = deque()
        jnames = sorted(self.tables.keys())
        for jname in jnames:
            tables,combos,htmls = self._get_tables(jname,get_all=True)

            html = deque(['<tr><td>%s<hr></td></tr>'%(
                self._font(jname,size=5,color='green'))])
            for table,combo,output in zip(tables,combos,htmls):
                if len(combo.split('_')) > 1:
                    continue # only bands
                table,table_recent = table
                output,output_recent = output
                nobs = len(table.rows)
                nobs_recent = len(table_recent.rows)
                if nobs == 0:
                    continue

                html.append('<td>')
                html.append('%s %s '%(combo,self._link_file(output_recent,'Last (%d)'%nobs_recent)))
                html.append(self._link_file(output,'All (%d)'%(nobs)))

                outplot = self._residuals_plot(jname,combo)
                if isfile(outplot):
                    ihtml = self._link_img(outplot,double=False,nocrop=True)
                    html.append('<br>%s'%(ihtml))

                html.append('<td>')

            # kluge to add another row of plots
            html.append('\n</tr>\n<tr>')
            for table,combo,output in zip(tables,combos,htmls):
                if len(combo.split('_')) > 1:
                    continue # only bands
                table,table_recent = table
                nobs = len(table.rows)
                if nobs == 0:
                    continue

                html.append('<td>')
                outplot = self._residuals_plot(jname,combo).replace(
                    'residuals','spectrum')
                if isfile(outplot):
                    ihtml = self._link_img(outplot,double=False,nocrop=True)
                    html.append('<br>%s'%(ihtml))

                html.append('<td>')

            html.append('\n</tr>\n')
            rows.append(''.join(html))
        return rows

# Note to self -- all the code above is old and deprecated!

class TableRows2(HTMLSegment):
    """ Emit HTML for the rows of a pipeline table."""

    def __init__(self,procobs,outpath=None):
        """ A set of ProcObs objects."""
        super(TableRows2,self).__init__(outpath)
        self.procobs = procobs # a list
        self.rows = self.get_rows()
        self._cache_toas = None

    def __len__(self):
        return len(self.rows)

    def get_rows(self):
        return map(self.get_row,sorted(self.procobs,reverse=True))

    def get_row(self,procob):
        #valid = procob.cal.valid()
        valid = True # hard code to make obs plots appear
        html = deque(['<tr>'])
        html.append(self._text(procob))
        top,bot = self._psr_plots(procob,html,valid=valid)
        plots = self._cal_plots(procob,html,valid=valid)
        plots.extend(self._misc_plots(procob,html,valid=valid))
        q1,q2 = (top,bot) if (len(top)<=len(bot)) else (bot,top)
        for i in xrange(len(plots)):
            q1.append(plots[i])
            q1,q2 = q2,q1
        html.extend(q1)
        html.append('</tr>\n<tr>')
        html.extend(q2)
        html.append('</tr>')
        return '\n'.join(html)

    def get_toas(self):
        """ Produce a temporary .tim file.
        
        There is arguably a better way to do this...
        """
        if self._cache_toas is not None:
            return self._cache_toas
        self._cache_toas = processed.get_toas(self.procobs)
        return self._cache_toas

    def _sub_table(self,obs):
        """ Make a summary table for observation characteristics."""
        sub_table = []
        try:
            sub_table.append(['mjd','%.2f'%(round(obs.get_mjd()))])
        except Exception:
            sub_table.append(['mjd','unknown'])
        try:
            sub_table.append(['tobs','%ds'%(round(obs.tobs))])
        except Exception:
            sub_table.append(['tobs','unknown'])
        try:
            sub_table.append(['nchan', str(obs['nchan'])])
        except Exception:
            sub_table.append(['nchan','unknown'])
        try:
            sub_table.append(['nbin', str(obs['nbin'])])
        except Exception:
            sub_table.append(['nbin','unknown'])
        try:
            sub_table.append(['freq', '%d MHz'%(round(obs['frequency']))])
        except Exception:
            sub_table.append(['freq','unknown'])
        try:
            sub_table.append(['bw', '%d MHz'%(round(obs['bandwidth']))])
        except Exception:
            sub_table.append(['bw','unknown'])
        sub_table = ['<td>%s</td><td>%s</td>'%(x,y) for x,y in sub_table]
        sub_table = ['<tr>%s</tr>'%x for x in sub_table]
        sub_table = '\n<table cellspacing=0>%s\n</table>'%(
            '\n'.join(sub_table))
        return '<div class="black_border">%s</div>\n'%sub_table

    def _text(self,procob):
        obs,cal = procob.obs,procob.cal
        obsscr = procob.obsscr
        obspath = procob.obsscr.outpath
        calpath = procob.calscr.outpath
        name = '%s'%(obs['name'])
        rcvr = '%s / %s'%(obs['receiver'],str(band.Band(obs)))
        back = '%s'%(obs['backend'])
        proj = '%s'%(obs['projid'])

        sub_table = self._sub_table(obs)
        if cal.valid():
            cal_script = join(calpath,cal.diode.basename+'.psrsh')
            calh = self._link_file(cal_script,cal.diode.basename)
        else:
            if 'HYDRA' in name:
                calh = ''
            else:
                calh = self._font('NO CAL!','red')
        label_string = '' if obsscr.label is None else '.%s'%obsscr.label
        psr_script = obs.get_stem(obspath)+label_string+'.psrsh'
        psrh = self._link_file(psr_script,obs.basename)
        pcm = 'pcm: None' if (cal.pcm is None) else \
              'pcm: %s'%cal.pcm.basename
        flux = 'flux: None' if (cal.fluxcal is None) else \
               'flux: %s'%cal.fluxcal.basename
        t = '<br>'.join([name,rcvr,back,proj,psrh,calh,pcm,flux,sub_table])

        # if there are any data quality flags, use red border for the cell
        df = obs.data_flags
        if len(df) > 0:
            if ('HYDRA' in name):
                if len(df)>1 or df[0].flag()!='no_cal':
                    t = '<div class="red_border">%s</div>'%(t)
            else:
                t = '<div class="red_border">%s</div>'%(t)

        return '<td rowspan=2>%s</td>'%(t)

    def _psr_plots(self,procob,html,valid=True):
        """ Make one or more double-sized plots, then stagger those 
            remaining.
        """
        obs = procob.obs
        obsscr = procob.obsscr
        plots = procob.obsplots
        top,bot = deque(),deque()

        # first element
        # NB that we need to hard code TOA residual... oh well
        if not ('HYDRA' in obs['name']):
            profile = obsscr.get_target(obs)
            img = profile + '.toa.residuals.png'
            html.append('<td colspan=2 rowspan=2>%s</td>'%(
                self._link_img(img,double=True,nocrop=True,valid=valid)))
        else:
            html.append('<td colspan=2 rowspan=2></td>')
        if len(plots)<1:
            return top,bot

        # second element
        img = plots[0].get_target(obs,obsscr)
        html.append('<td colspan=2 rowspan=2>%s</td>'%(
            self._link_img(img,double=True,valid=valid)))
        if len(plots)<2:
            return top,bot

        # now put the odd strides on the top row
        for plot in plots[1::2]:
            img = plot.get_target(obs,obsscr)
            top.append('<td>%s</td>'%(self._link_img(img,valid=valid)))
        if len(plots)<3:
            return top,bot

        #html.append('</tr><tr>' + '<td></td>'*0)
        for plot in plots[2::2]:
            img = plot.get_target(obs,obsscr)
            bot.append('<td>%s</td>'%(self._link_img(img,valid=valid)))
        return top,bot

    def _cal_plots(self,procob,html,valid=True):
        cal = procob.cal.diode
        calscr = procob.calscr
        cplots = deque()
        for plot in procob.calplots:
            img = plot.get_target(cal,calscr)
            cplots.append('<td>%s</td>'%(self._link_img(img,valid=valid)))
        return cplots

    def _misc_plots(self,procob,html,valid=True):
        """ Any other miscellaneous plots."""
        # subint zapping
        mplots = deque()
        img = join(procob.obsscr.outpath,
            procob.obs.basename+'.subint_zapping.png')
        mplots.append(
            '<td>%s</td>'%(self._link_img(img,valid=valid,nocrop=False)))
        return mplots

    def to_html(self,fname=None,title=None,forematter=None,maxrows=None):
        forematter = forematter or ''
        rows = self.rows[:maxrows]
        body = '%s<table cellspacing=0 border=0>\n%s\n</table>'%(
            forematter,'\n'.join(rows))
        html = html_template(body,title,style=css)
        if fname is None:
            return html
        else:
            file(fname,'w').write(html)

class P574_OverviewTable2(HTMLSegment):
    """ A class to display summary plots for each pulsar.

    This version makes use of the "processing" db to allow more granular
    production of output.  (And perhaps dynamic output.)
    
    The user supplies a processing db as well as a list of pulsar names,
    and optionally any limits on what subsets of the data to display,
    e.g. recent data, only some bands, ...
    """

    def __init__(self,procdb,outpath,max_lapse=None,strict_select=False):
        self.max_lapse = max_lapse
        self.procdb = procdb
        super(P574_OverviewTable2,self).__init__(outpath)
        self.tables = dict()
        self.band_only = False
        self.strict_select = strict_select

    def _filter_combo(self,combo):
        """ Determine whether to include results for a band/receiver combo.
        """
        #return True
        return ( (combo.startswith(('10CM','20CM','40CM','50CM'))) and 
                 ('DRAO' not in combo) )

    def _add_combos(self,jname):
        """ Generate a table for each set of combos."""
        self.tables[jname] = dict()
        combos = filter(self._filter_combo,self.procdb.get_combos(jname))
        self.tables[jname]['combos'] = combos
        bands = sorted(set((x.split('_')[0] for x in combos)))
        self.tables[jname]['bands'] = bands
        try:
            eph,sel = self.procdb.get_eph_sel(
                jname,strict=self.strict_select)
            # temp -- remove "sql" flags from select file
            sel = '\n'.join(
                filter(lambda l: 'sql' not in l,sel.split('\n')))
        except TypeError as e:
            eph = sel = None
        # do we actually need to do this ahead of time, or otf will work?
        self.tables[jname]['ephemeris'] = eph
        self.tables[jname]['select'] = sel
        # going to work?  who knows
        self.tables[jname]['outpath'] = join(self.outpath,jname)

    def _add_table(self,jname,combo):
        """ Populate the combo entries with Table objects."""
        procobs = self.procdb.get_obs(jname,combo=combo)
        table = TableRows2(procobs,outpath=self.tables[jname]['outpath'])
        self.tables[jname][combo] = table
        self.files = self.files.union(table.files)
        return table

    def _get_plot_name(self,jname,combo,name='residuals'):
        """ Return appropriate filename for TOA residuals plot."""
        label = '%s_%s'%(jname,combo)
        outpath = self.tables[jname]['outpath']
        return join(outpath,'%s_%s.png'%(label,name))

    def residuals_plots(self,jname,combo,work_queue=None,clobber=False):
        outplot = self._get_plot_name(jname,combo,name='residuals')
        print 'in residuals_plots for %s/%s'%(jname,combo)
        if (not clobber) and isfile(outplot):
            return
        if work_queue is not None:
            print 'using work queue'
            work = self.procdb.get_residuals_work(
                jname,combo,outplot,clobber=True,
                strict_select=self.strict_select) 
            work_queue.append(work)
            return
        t = self.tables[jname]
        if t['ephemeris'] is not None:
            # check for TOAs
            toas = self.procdb.get_toas(jname,combo)
            if toas is None:
                return
            # write in-memory strings to files
            par = util.TemporaryFile(t['ephemeris'])
            sel = util.TemporaryFile(t['select'])
            tim = util.TemporaryFile(toas)
            residuals_plot(outplot,str(par),str(tim),select=str(sel))

    def coadd_plots(self,jname,combo,clobber=False):
        outplot = self._get_plot_name(jname,combo,name='coadded')
        if (not clobber) and isfile(outplot):
            return
        # changed below -- clobber plot implies clobber coadded file
        # could consider something more sophisticated as generating the
        # coadded file is rather expensive
        coadded = self.procdb.get_coadded(jname,combo,clobber=clobber)
        coadd_plot(outplot,coadded)
        if combo.startswith('20CM'):
            outplot = self._get_plot_name(jname,combo,name='carpet')
            carpet_plot(outplot,coadded)

    def get_good_jnames(self):
        jnames = []
        not_jnames = []
        for jname in self.tables.keys():
            if len(self.tables[jname]['bands']) > 0:
                if jname.startswith('J'):
                    jnames.append(jname)
                else:
                    not_jnames.append(jname)
        return sorted(jnames) + sorted(not_jnames)

    def add_entry(self,jname,plot_toas=True,plot_coadd=True,
        work_queue=None,clobber_plots=False):
        """ Add entries for a specific pulsar using obs in analysis.

            Under each jname, each receiver/backend combo is stored 
            separately via the "tables" class member.
        """
        
        self._add_combos(jname)

        combos = self._get_combos(jname)
        bands = self.tables[jname]['bands']

        # combos handled separately -- merge with parent?
        for combo in combos:
            table = self._add_table(jname,combo)

            # if "band" type, don't make residuals plots for combos
            if self.band_only and (combo not in bands):
                continue

            # if toas available, make a tempo2 plot
            if plot_toas:
                print 'Making TOA residuals plot for %s/%s.'%(jname,combo)
                self.residuals_plots(jname,combo,work_queue=work_queue,
                    clobber=clobber_plots)
            if plot_coadd:
                print 'Making coadd plot for %s/%s.'%(jname,combo)
                self.coadd_plots(jname,combo,clobber=clobber_plots)

    def add_entries(self,jnames=None,plot_toas=True,plot_coadd=True,
        manager=None,clobber_plots=False):
        """ Convenience method to process all jnames in the ProcDB."""
        work_queue = None if (manager is None) else deque()

        if jnames is None:
            jnames = self.procdb.get_jnames()
        for jname in jnames:
            print 'Adding %s to HTML output.'%jname
            self.add_entry(jname,plot_toas=plot_toas,plot_coadd=plot_coadd,
                work_queue=work_queue,clobber_plots=clobber_plots)

        if manager is not None:
            manager.do_work(work_queue)

    def get_rows(self):
        """ Generate HTML for overview page."""
        rows = deque()
        jnames = self.tables.keys()
        for jname in jnames:
            combos = sorted(self.tables[jname]['combos'])
            outpath = self.tables[jname]['outpath']

            current_band = combos[0].split('_')[0]

            html = deque(['<tr><td>%s<hr></td></tr>'%(
                self._font(jname,size=5,color='green'))])
            html.append('\n<tr><td>%s<hr></td></tr>'%(
                self._font(current_band,size=5,color='green')))
            html.append('\n<tr>\n')
            for combo in combos:
                new_band = combo.split('_')[0]
                if current_band != new_band:
                    current_band = new_band
                    html.append('\n</tr>\n<tr><td>%s<hr></td></tr>\n<tr>\n'%(
                        self._font(current_band,size=5,color='green')))

                nobs = len(self.tables[jname][combo])
                if nobs < 1:
                    continue

                label = '%s_%s'%(jname,combo)
                output = join(outpath,'%s.html'%label)
                html.append('<td>')
                html.append('%s %d'%(self._link_file(output,combo),nobs))

                outplot = self._residuals_plot(jname,combo)
                if isfile(outplot):
                    ihtml = self._link_img(outplot,double=False,nocrop=True)
                    html.append('<br>%s'%(ihtml))

                html.append('<td>')

            html.append('\n</tr>\n')
            rows.append(''.join(html))
        return rows

    def get_body(self,rows):
        """ Generate the actual body from lesser elements."""
        body = '<table id="overview_table">\n%s\n</table>'%(
            '\n'.join(rows))
        return body

    def _get_landing_link_prefs(self):
        return ['20CM_MULTI_PDFB3'],'combos'

    def _get_combos(self,jname):
        return self.tables[jname]['combos']

    def _get_tables(self,jname,get_all=False):
        """ Return Table objects, labels, and HTML outputs."""
        keys = self._get_combos(jname)
        outpath = self.tables[jname]['outpath']
        htmls = [join(outpath,'%s_%s.html'%(jname,key)) for key in keys]
        tables = [self.tables[jname][key] for key in keys]
        return tables,keys,htmls

    def _landing_link(self,jname):
        """ Return the link for an overview sub-page for a given pulsar."""
        preferences,key = self._get_landing_link_prefs()
        tables,combos,htmls = self._get_tables(jname)
        for preference in preferences:
            for combo,html in zip(combos,htmls):
                if preference==combo:
                    return html
        return htmls[0]

    def _common_block_jname(self,jname,fname):
        """ Generate the HTML for a common block of a pulsar-specific page.

        This method generates only the code linking amongst other pulsars
        and the overview page, allowing child classes to perform more
        specific linking amongst band/combos.
        """

        #jnames = sorted(self.tables.keys())
        jnames = self.get_good_jnames()
        tables,combos,htmls = self._get_tables(jname)
        dummy = tables[0]
        line1 = self._font(
            dummy._link_file(self._landing_link(jname),label=jname),
            color='black',size=5)+'<br>'
        curr_idx = jnames.index(jname)
        next_psr = jnames[(curr_idx+1)%len(jnames)]
        prev_psr = jnames[(curr_idx-1)%len(jnames)]
        over_link= dummy._link_file(
            join(self.outpath,fname),label='Overview')
        next_link = dummy._link_file(self._landing_link(next_psr),
            label='Next (%s)'%next_psr)
        prev_link = dummy._link_file(self._landing_link(prev_psr),
            label='Prev (%s)'%prev_psr)
        line2 = self._font(
            '\n'.join([over_link,next_link,prev_link]),
            color='black',size=5)+'<br>'
        return line1,line2

    def _common_block(self,jname,fname='index.html'):
        """ Generate the HTML for a common block of a pulsar-specific page.
        """

        line1,line2 = self._common_block_jname(jname,fname)
        tables,combos,htmls = self._get_tables(jname)
        pulsar_links = deque()
        toa_plot_links = deque()
        for table,combo,output in zip(tables,combos,htmls):
            nobs = len(table.rows)
            if nobs == 0:
                continue
            pulsar_links.append(
                table._link_file(output,label='%s (%d)'%(combo,nobs)))
            outplot = self._residuals_plot(jname,combo)
            if isfile(outplot):
                toa_plot_links.append(table._link_img(
                    outplot,double=True,nocrop=True) + '<br>')

        line3 = self._font('\n'.join(pulsar_links),color='black',size=5)
        line4 = '\n'.join(toa_plot_links)
        return line1+line2+line3+'<br>'+line4+'<br'

    def _link_tables(self,fname='index.html',maxrows=10):
        """ Go through stored tables, interlink them, and write them out.
        
        Anything that should be common to all pages should be written out
        in the "common block" methods.  Thus, e.g., TOA residuals plots
        could/should go there.  While items restricted to a particular 
        combo (e.g. a summary plot) should go here.

        In this version, we automatically write out two versions of the
        web pages, one with a maximum number of entries, one with all
        of the entries.
        """

        #jnames = sorted(self.tables.keys())
        jnames = self.get_good_jnames()
        for jname in jnames:
            try:
                cb = self._common_block(jname,fname=fname)
                tables,combos,htmls = self._get_tables(jname)
                for table,combo,output in zip(tables,combos,htmls):
                    if len(table.rows) == 0:
                        continue
                    outplot = self._get_plot_name(jname,combo,'residuals')
                    if isfile(outplot):
                        link = table._link_img(
                            outplot,double=True,nocrop=True)
                        stuff = cb + '<br>\n%s'%link
                    else:
                        stuff = cb
                    title = '%s %s'%(jname,combo)
                    table.to_html(fname=output,title=title,
                        forematter=stuff,maxrows=maxrows)
                    self._link_target(output) # ensure we have all html
                    output = output.replace('.html','_full.html')
                    table.to_html(fname=output,title=title,
                        forematter=stuff,maxrows=None)
                    self._link_target(output) # ensure we have all html
            except IndexError as e:
                import traceback
                print 'Pulsar %s futzed out with %s.'%(jname,e)
                traceback.print_exc()

    def write(self,title='P574 Pulsars',fname='index.html'):
        good_jnames = self.get_good_jnames()
        all_jnames = self.tables.keys()
        print 'These pulsars have no HTML data:'
        for jname in set(all_jnames).difference(good_jnames):
            print jname
        self._link_tables(fname=fname)
        rows = self.get_rows()
        body = self.get_body(rows)
        html = html_template(body,title,style=css)
        output = join(self.outpath,fname)
        self.files.add(output)
        file(output,'w').write(html)

    def tarball(self,fname='overview.tar.gz'):
        super(P574_OverviewTable,self).tarball(self.outpath,fname=fname)

class P574_BandOverviewTable2(P574_OverviewTable2):
    """ A class to display summary plots for each pulsar.

    This version is grouped by band by adding an extra layer to the
    hierarchy.
    """

    def __init__(self,procdb,outpath,max_lapse=None,strict_select=False):
        super(P574_BandOverviewTable2,self).__init__(
            procdb,outpath,max_lapse,strict_select)
        self.band_only = True

    def get_rows(self):
        """ Generate HTML for overview page."""

        rows = deque()
        jnames = sorted(self.tables.keys())
        for jname in jnames:
            combos = self.tables[jname]['combos']
            #bands = self.tables[jname]['bands']
            bands = ['20CM','10CM','40CM','50CM','70CM']
            outpath = self.tables[jname]['outpath']

            dm,f0,f1 = get_dm_etc(self.tables[jname]['ephemeris'])
            p0 = 0. if f0==0. else 1./f0
            edot = -1e45*f0*f1*(2*np.pi)**2
            if edot > 0:
                edot_exp = int(np.log10(edot))
                edot_int = int(round(edot/10**(edot_exp)))
            else:
                edot_exp = edot_int = 0
            s1 = self._font(jname,size=5,color='green')
            s2 = '%d ms'%(round(p0*1000))
            s3 = '%de%d erg/s'%(edot_int,edot_exp)
            s4 = '%d'%round(dm)
            # this is an ad hoc "get" to see if 20CM carpet plot exists
            sub_plot = self._get_plot_name(jname,'20CM','carpet')
            sub_plot_ihtml = self._link_img(
                sub_plot,double=False,nocrop=True)
            sub_table = '<div class="black_border"><table style="vertical-align:top"><tr><td>%s</td></tr><tr><td>P</td><td>%s</td><td>Edot</td><td>%s</td></tr><tr><td>DM</td><td>%s</td></tr></table></div>'%(s1,s2,s3,s4)
            html = deque(['<tr style="vertical-align:top"><td>%s<br>%s</td>'%(sub_table,sub_plot_ihtml)])
            #html = deque(['<tr><td valign="top">%s<br>%s<br>%s</td>'%(
                #self._font(jname,size=5,color='green'),
                #'P: %d ms'%(round(p0*1000)),
                #'DM: %d'%round(dm))])

            for band in bands:
                label = '%s_%s'%(jname,band)
                output = join(outpath,'%s.html'%label)
                try:
                    nobs = len(self.tables[jname][band])
                except KeyError:
                    nobs = 0
                #if nobs == 0:
                #    continue

                html.append('<td>')
                html.append('%s %d'%(self._link_file(output,band),nobs))

                outplot = self._get_plot_name(jname,band,'residuals')
                ihtml = self._link_img(outplot,double=False,nocrop=True)
                html.append('<br>%s'%(ihtml))

                outplot = self._get_plot_name(jname,band,'coadded')
                ihtml = self._link_img(outplot,double=False,nocrop=False)
                html.append('<br>%s'%(ihtml))

                html.append('<td>')

            html.append('\n</tr>\n')
            rows.append(''.join(html))
        return rows

    def _common_block(self,jname,fname='index.html'):
        """ Generate the HTML for a common block of a pulsar-specific page.

        This version is sorted by band and combo and includes full and
        abbreviated versions.
        """

        line1,line2 = self._common_block_jname(jname,fname)
        tables,combos,htmls = self._get_tables(jname)
        pulsar_links = deque()
        for table,combo,output in zip(tables,combos,htmls):
            nobs = len(table.rows)
            if nobs == 0:
                continue
            is_band = len(combo.split('_')) == 1
            pulsar_links.append('</ul>' if is_band else '<li>')
            pulsar_links.append(
                table._link_file(output,label='%s (%d)'%(combo,nobs)))
            output = output.replace('.html','_full.html')
            pulsar_links.append(' ')
            pulsar_links.append(
                table._link_file(output,label='(full)'))
            if is_band:
                pulsar_links.append('<ul>')
        pulsar_links.popleft() # remove first </ul>
        pulsar_links.append('</ul>') # add one to close last list

        line3 = self._font('\n'.join(pulsar_links),color='black',size=3)
        return line1+line2+line3+'<br>'

    def _get_landing_link_prefs(self):
        return ['20CM'],'bands'

    def _get_combos(self,jname):
        return sorted(
            self.tables[jname]['bands']+self.tables[jname]['combos'])

    def tarball(self,fname='band_overview.tar.gz'):
        super(P574_BandOverviewTable2,self).tarball(fname=fname)

class PPTA_BandOverviewTable2(P574_BandOverviewTable2):
    """ This version skips the coadd/carpet plots.
    """

    def __init__(self,procdb,outpath,max_lapse=None,strict_select=True):
        super(PPTA_BandOverviewTable2,self).__init__(
            procdb,outpath,max_lapse,strict_select)

    def get_rows(self):
        """ Generate HTML for overview page."""

        rows = deque()
        jnames = sorted(self.tables.keys())
        for jname in jnames:
            combos = self.tables[jname]['combos']
            #bands = self.tables[jname]['bands']
            bands = ['10CM','20CM','40CM','50CM']
            outpath = self.tables[jname]['outpath']

            dm,f0,f1 = get_dm_etc(self.tables[jname]['ephemeris'])
            p0 = 0. if f0==0. else 1./f0
            s1 = self._font(jname,size=5,color='green')
            s2 = '%.2f ms'%(p0*1000)
            s3 = '%.2f'%dm
            sub_table = '<div class="black_border"><table style="vertical-align:top"><tr><td>%s</td></tr><tr><td>P</td><td>%s</td></tr><tr><td>DM</td><td>%s</td></tr></table></div>'%(s1,s2,s3)
            html = deque(['<tr style="vertical-align:top"><td>%s</td>'%(sub_table)])

            for band in bands:
                label = '%s_%s'%(jname,band)
                output = join(outpath,'%s.html'%label)
                try:
                    nobs = len(self.tables[jname][band])
                except KeyError:
                    nobs = 0

                html.append('<td>')
                html.append('%s %d'%(self._link_file(output,band),nobs))

                outplot = self._get_plot_name(jname,band,'residuals')
                ihtml = self._link_img(outplot,double=False,nocrop=True)
                html.append('<br>%s'%(ihtml))

                html.append('<td>')

            html.append('\n</tr>\n')
            rows.append(''.join(html))
        return rows

    def add_entry(self,jname,plot_toas=True,plot_coadd=False,**kwargs):
        super(PPTA_BandOverviewTable2,self).add_entry(
            jname,plot_toas=True,plot_coadd=False,**kwargs)

    def add_entries(self,jnames=None,plot_toas=True,plot_coadd=False,
        strict_select=True,**kwargs):
        super(PPTA_BandOverviewTable2,self).add_entries(
            jnames=jnames,plot_toas=True,plot_coadd=False,**kwargs)

    def write(self,title='PPTA Pulsars',fname='index.html'):
        super(PPTA_BandOverviewTable2,self).write(title=title,fname=fname)

class PPTA_QuickLook(TableRows2):

    def __init__(self,procdb,tspan=14,outpath=None,exclude_nosk=True):
        # sorry about the double negative...
        procobs = deque()
        for jname in procdb.get_jnames():
            procobs.extend(procdb.get_obs(jname))
        tmax = 0
        for procob in procobs:
            if procob.obs.get_mjd() > tmax:
                tmax = procob.obs.get_mjd()
        tmin = tmax - tspan
        procobs = [x for x in procobs if x.obs.get_mjd() > tmin]
        if exclude_nosk:
            procobs = [x for x in procobs if not x.obs['nosk']]
        #else:
            #procobs = [x for x in procobs if x.obs['sk']]
        procobs = sorted(procobs,key=lambda x:x.obs.get_mjd())[::-1]
        super(PPTA_QuickLook,self).__init__(procobs,outpath=outpath)

def html_template(body,title=None,style=None):
    title = title or ''
    style = style or ''
    return """
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <title>%s</title>
    <style>%s</style>
</head>
<body>
%s
</body>
</html>
"""%(title,style,body)

css_classes = deque()

css_classes.append(
"""
.viewport {
width: %dpx;
height: %dpx;
overflow: hidden;
}
.viewport img {
width: %dpx;
height: %dpx;
margin-top: -%dpx;
margin-left: -%dpx;
}
"""%(SCALE_W-CROP_RGT,SCALE_H-CROP_BOT,SCALE_W,SCALE_H,CROP_TOP,CROP_LFT))

css_classes.append(
"""
.viewport_nocrop {
width: %dpx;
height: %dpx;
overflow: hidden;
}
.viewport_nocrop img {
width: %dpx;
height: %dpx;
margin-top: -0px;
margin-left: -0px;
}
"""%(SCALE_W-CROP_RGT,SCALE_H-CROP_BOT,SCALE_W-CROP_RGT,SCALE_H-CROP_RGT))

css_classes.append(
"""
.viewport2 {
width: %dpx;
height: %dpx;
overflow: hidden;
}
.viewport2 img {
width: %dpx;
height: %dpx;
margin-top: -%dpx;
margin-left: -%dpx;
}
"""%(2*(SCALE_W-CROP_RGT),2*(SCALE_H-CROP_BOT),2*SCALE_W,2*SCALE_H,2*CROP_TOP,2*CROP_LFT))

css_classes.append(
"""
.viewport2_nocrop {
width: %dpx;
height: %dpx;
overflow: hidden;
}
.viewport2_nocrop img {
width: %dpx;
height: %dpx;
margin-top: 0px;
margin-left: 0dpx;
}
"""%(2*(SCALE_W-CROP_RGT),2*(SCALE_H-CROP_BOT),2*(SCALE_W-CROP_RGT),2*(SCALE_H-CROP_RGT)))

css_classes.append(
"""
.red_border {
border: 3px solid red;
border-radius:10px;
}
""")

css_classes.append(
"""
.green_border {
border: 3px solid green;
border-radius:10px;
}
""")

css_classes.append(
"""
.black_border {
border: 1px solid black;
}
""")

css_classes.append(
"""
#overview_table {display: block; }
#overview_table td {display: inline-block; }
""")

css =  ''.join(css_classes)
