"""
Routines for excising RFI from fold-mode data.

author: Matthew Kerr <matthew.kerr@gmail.com>
"""

"""
Implementation notes:

(1) radiometer noise limit gives white noise level in each profile
(2) the signal (pulsar/cal) is approximately the same in each subint, so the
    difference of two subints is approximately white with summed variance

Then, RFI in a subint can be flagged simply by exceeding that white noise
level.

Since we are not calibrating, must be careful to correct differential gain
at an approximate level to get the white noise level right.

WBCORR (wideband correlator) appears to have noise well in excess (~2.4) of
what one would expect, even allowing for 2 bits.  There were some issues
with correlation between bins ("even odd") problem, and the levels aren't
dynamically set.  So this isn't too surprising, just have to make a special
case of the data.  See e.g.
    -- w070420_002323.rf

Some data -- only identified in PDFB1 so far -- have problems in the
correlator that look like RFI in only AA or BB.  These are currently
treated just like RFI, but might merit a special flag.  See e.g.
    -- a060814_162521.rf

"""

# TODO -- one way to gauge how much the RFI affects the timing is to look
# at the proper-DM time series -- then, something like

# (0.5*(p1+p2)**5-N)/N**0.5 is the "sigma" for the RFI + pulsar
# (0.5*(p1-p2)**5-N)/N**0.5 is the "sigma" for the RFI

# so comparing these two things would give some idea of the RFI's strength
# relative to the pulse profile

# e.g., in s130126_092043.rf, the zerodm method throws out every subint;
# there is appreciable RFI, but it's also J0437!  one gets numbers like
# 117 "sigma" for the RFI
# 30569 "sigma" for the RFI + pulsar
# clearly the TOAs should be OK ( though this should be checked! )

# one drawback to this method is that it's slow to dedisperse the data

# so this would lead to an approach in which we can get the "cleanest" data
# by using the zerodm to find RFI, while doing a more practical "clean
# enough" filter for most applications; the former method is also what we'd
# want to use in the beautiful bin-masking future

# TODO -- need to incorporate some type of scaling to account for scintill.
# -- perhaps this will help out zerodm too, and is certainly necessary for
# nonzero dm

import numpy as np
from collections import deque
import os
from os.path import join

import psrchive

import observation,zapping,jobs,profiles
from util import shrink_png

def dmp_window(a):
    """ Compute the number of window bins to approximate DM smearing."""
    nbin = a.get_nbin()
    dm = a.get_dispersion_measure()
    p = a[0].get_folding_period()
    bw = abs(a.get_bandwidth())*1e-3
    freq = a.get_centre_frequency()*1e-3
    try:
        fhi,flo = freq+0.5*bw,freq-0.5*bw
        phase_delay = 4.15e-3*dm/p*(flo**-2-fhi**-2) # seconds
        nwin = int(round(nbin*phase_delay))
        return max(2,nwin + nwin%2)
    except ZeroDivisionError:
        # handle case for bad files with no frequency info
        return 2

def navg(prof,n):
    """ Poor man's DM smearing."""
    try:
        rvals = np.zeros_like(prof)
        for i in xrange(n):
            rvals += np.roll(prof,i)
        return rvals/n
    except TypeError:
        # this happens when the DM smearing window is in error
        return prof

class Subint(object):
    """ Little wrapper class to help keep track of state."""

    def __init__(self,amplitudes,valid=True,dmwindow=None,nsamp=0.):
        self.amps = amplitudes
        self.valid = valid
        self.dmwindow = dmwindow
        self.nsamp = nsamp

    def __len__(self):
        return len(self.amps)

    def scaled_difference(self,other):
        """ Return the difference and sum of two profiles.
        
        The purpose of the scale is to account for changes in pulsar 
        brightness from scintillation and is of use when comparing 
        dedispersed profiles.
        
        The input profiles are assumed to have equal variance.  The
        resulting D and S series are renormalized to have the same variance.
        The scale is chozen to minimize chi^2 in the scaled difference.

        NB -- this does NOT do well if one subint is badly RFI contaminated.
        """
        if hasattr(other,'amps'):
            p1,p2 = self.amps,other.amps
        else:
            p1,p2 = self.amps, other
        b = (np.sum(p1**2)-np.sum(p2**2))/np.sum(p1*p2)
        scale = 0.5*((b**2+4)**0.5-b)
        renorm = 1./(1+scale**2)**0.5
        S = renorm * (p1*scale + p2)
        D = renorm * (p1*scale - p2)
        return D,S

    def difference(self,other):
        """ Simply return the sum and difference of two profiles, scaled to
            maintain variance.
        """
        if hasattr(other,'amps'):
            p1,p2 = self.amps,other.amps
        else:
            p1,p2 = self.amps, other
        S = (p1+p2)/2**0.5
        D = (p1-p2)/2**0.5
        return D,S
    
def get_subints(obs,zaps=None,zerodm=True):
    """ Return the normalized, baselined Stokes I profile for each subint.
    """
    a = psrchive.Archive_load(obs.fname)
    dmwindow = dmp_window(a)
    cal = obs.is_cal()
    if a.get_state() != 'Coherence':
        print 'Warning!!!  Expected PSRFITS in Coherence state.'
    if zaps is not None:
        for zap in zaps:
            a.execute(zap)
    try:
        if zerodm:
            a.dededisperse() # just in case
        else:
            a.dedisperse()
    except RuntimeError:
        print 'problem file'
        print obs.fname
        # not sure if PSRCHIVE will be consistent after this?
        # in any case, hope it's dedispersed!
        pass
    weights = a.get_weights()
    wdata = np.transpose(a.get_data(),(3,1,0,2))# nbin x npol x nsub x nchan
    wdata *= weights
    # flatten bandpass
    scale = wdata.sum(axis=0) # avg. over bins; shape = npol x nsub x nchan
    scale[scale > 0] = 1./scale[scale > 0]
    fscrunch = np.sum(wdata*scale,axis=-1)
    fscrunch = np.transpose(fscrunch,(2,1,0)) # subint x npol x bins

    subints = deque()
    for i in xrange(len(a)):
        subint = a[i]
        bw_fac = float(np.sum(weights[i]>0))/len(weights[i])
        bw = abs(subint.get_bandwidth()*bw_fac)*1e6
        #if i==0:
        #    print 'Bandwidth: %.1f MHz'%(bw*1e-6)
        #if bw_fac==0:
            #print 'Warning! All channels zapped in subint %d.'%i
        tobs = subint.get_duration()/subint.get_nbin()
        err = (bw*tobs)**-0.5 if (bw*tobs) >0 else 0.
        #return fscrunch,err

        aa = fscrunch[i,0]
        #aa_baseline = median_baseline(aa,err,cal=cal)
        aa_baseline = profiles.iterative_baseline(aa,err)

        try:
            bb = fscrunch[i,1]
            #bb_baseline = median_baseline(bb,err,cal=cal)
            bb_baseline = profiles.iterative_baseline(bb,err)
        except IndexError:
            bb = 0
            bb_baseline = 0

        err = err*(aa_baseline**2+bb_baseline**2)**0.5
        if err > 0:
            scale = 1./err
            amps = (aa+bb-(aa_baseline+bb_baseline))*scale
            subint = Subint(amps,True,dmwindow=dmwindow,nsamp=2*bw*tobs)
        else:
            subint = Subint(np.zeros_like(fscrunch[i,0]),False)
        subints.append(subint)

    return subints

def get_nsamps(obs,zaps=None,zerodm=True):
    """ A fast version of get_subints where we only care about getting the
    radiometer noise level..
    """
    a = psrchive.Archive_load(obs.fname)
    if zaps is not None:
        for zap in zaps:
            a.execute(zap)
    weights = a.get_weights()

    nsamps = np.zeros(len(a))
    for i in xrange(len(a)):
        subint = a[i]
        bw_fac = float(np.sum(weights[i]>0))/len(weights[i])
        bw = abs(subint.get_bandwidth()*bw_fac)*1e6
        tobs = subint.get_duration()/subint.get_nbin()
        nsamps[i] = 2*bw*tobs

    return nsamps

def get_stats(profs):
    """ Take difference of normalized subints, and compute the chi^2
        statistic.

        (Unimplemented) support for clipping out high outliers from chi^2.
    """

    nprof = len(profs)
    nbin = len(profs[0])

    stats = np.zeros([nprof,nprof])
    clipped = np.zeros([nprof,nprof],dtype=int)
    besti = bestj = 0
    best_stat = np.inf
    for i in xrange(nprof):
        for j in xrange(i+1,nprof):
            diff2 = 0.5*(profs[i].amps-profs[j].amps)**2
            mask = diff2 < 9
            #ts = np.sum(diff2[mask])
            ts = np.sum(diff2)
            if ts < best_stat:
                best_stat = ts
                besti = i
                bestj = j
            stats[i,j] = stats[j,i] = ts
            clipped[i,j] = clipped[j,i] = mask.sum()
        stats[i,i] = np.inf
    return stats

def get_stats2(profs):
    """ Compute the ratio of chi^2 for the sum and difference of subints.
    """

    nprof = len(profs)
    nbin = len(profs[0])

    stats = np.zeros([nprof,nprof])
    for i in xrange(nprof):
        for j in xrange(i+1,nprof):
            # compute the difference ("RFI") and sum ("RFI + pulsar")
            #D,S = profs[i].scaled_difference(profs[j])
            D,S = profs[i].difference(profs[j])
            ts = np.sum(S**2) / np.sum(D**2)
            stats[i,j] = stats[j,i] = ts
        stats[i,i] = -np.inf
    return stats

def get_clean_subints(obs,zaps=None,thresh=None,include_best=False,
    max_freq=3500,zerodm=True,allow_smearing=True):
    """ Return a mask for which subints should be zapped.
    
    If requested, return the cleanest two subints regardless of whether they
    are absolutely clean.

    Observations taken at a frequency above max_freq are automatically
    clean.
    """

    profs = get_subints(obs,zaps=zaps,zerodm=zerodm)
    nprof = len(profs)
    if obs.is_cal():
        # always keep at least 2 cal subints
        include_best = True
    if thresh is None:
        thresh = 1.6
        if 'WBCORR' in obs['backend']:
            thresh = thresh * (3.5/1.4)
    thresh = len(profs[0])*thresh
    stats = get_stats(profs)
    m1 = np.asarray([np.any(stats[i,:] < thresh) for i in xrange(nprof)])
    mask = m1 & np.asarray([p.valid for p in profs])
    if obs['frequency'] > max_freq:
        mask = np.asarray([True]*len(profs))
    if allow_smearing:
        # go through the profiles and see if the RFI is sttil significant 
        # after it has been smeared by dispersion
        for i in xrange(len(profs)):
            if mask[i]:
                continue
            # find the "best" profile for this subint
            best = np.argmin(stats[i,:])
            p = (profs[i].amps-profs[best].amps)/2**0.5 # ~unit variance
            t = navg(p,profs[i].dmwindow)
            if np.abs(t).max() < 1: # max S/N less than noise level
                mask[i] = True 
                #print 'Accepting subint %d for DM smearing.'%i
    if include_best:
        idx = np.argmin(stats)
        mask[int(idx/nprof)] = True
        mask[idx%nprof] = True
    return mask,profs,stats

def get_clean_subints2(obs,zaps=None,thresh=None,include_best=False,
    max_freq=3500,zerodm=False):
    """ Return a mask for which subints should be zapped.
    
    If requested, return the cleanest two subints regardless of whether they
    are absolutely clean.

    In this approach, do not zerodm, but instead estimate the impact of the
    RFI on the actual pulse profile.  If it is modest, keep the subint.

    "Modest" is determined from the ratio of the S/N of the pulsar + RFI to
    the S/N of the RFI alone.

    Observations taken at a frequency above max_freq are automatically
    clean.
    """

    profs = get_subints(obs,zaps=zaps,zerodm=zerodm)
    nprof = len(profs)
    if obs.is_cal():
        # always keep at least 2 cal subints
        include_best = True
    if thresh is None:
        thresh = 10 # require signal ~ 10 times RFI
    stats = get_stats2(profs)
    mask = np.asarray([profs[i].valid and np.any(stats[i,:] > thresh) for i in xrange(nprof)])
    if obs['frequency'] > max_freq:
        mask = np.asarray([True]*len(profs))
    if include_best:
        idx = np.argmin(stats)
        mask[int(idx/nprof)] = True
        mask[idx%nprof] = True
    return mask,profs,stats

def get_clean_subints3(obs,zaps=None,thresh=3.1):

    # get clean subints from zero dm -- get at least two cleanest
    mask_zerodm,profs_zerodm,stats_zerodm = get_clean_subints(
        obs,zaps=zaps,include_best=True)
    subint_order = np.argsort(stats_zerodm.min(axis=1))

    # get dedispersed profiles
    profs_dedisp = get_subints(obs,zaps=zaps,zerodm=False)

    if obs.is_cal():
        return profs_zerodm,profs_dedisp,stats_zerodm,mask_zerodm,mask_zerodm.copy()
        
    # make an initial guess at dedispersed profile
    def form_working_profile(mask):
        p = np.zeros_like(profs_dedisp[0].amps)
        for i in xrange(len(mask)):
            if mask[i]:
                p += profs_dedisp[i].amps
        p *= 1./mask.sum()**0.5
        p -= profiles.iterative_baseline(p)
        return p

    # set working_profile to unit variance (probably)
    wp = form_working_profile(mask_zerodm)

    # now, go through remaining profiles and try to estimate RFI
    final_mask = mask_zerodm.copy()
    import pylab as pl
    pl.figure(1); pl.clf()
    for i in xrange(len(final_mask)):
        idx = subint_order[i]
        if final_mask[idx]:
            continue
        # "D" contains the estimate of the RFI
        p = profs_dedisp[idx]
        D,S = p.scaled_difference(wp)
        D -= np.median(D)

        # our metric for RFI contamination is the area in "chi" units
        area_rfi = np.sum(np.abs(D))-(2./np.pi)**0.5*len(D)

        # for comparison, get the area of the wp in "chi" for a single sub
        area_pulse = (np.sum(np.abs(wp))-(2./np.pi)**0.5*len(wp))/final_mask.sum()**0.5
        #area_pulse = np.sum(wp)/final_mask.sum()**0.5
        #print area_rfi,area_pulse,np.sum(wp)/final_mask.sum()**0.5,area_pulse/area_rfi,np.std(D),np.std(wp[:512])
        pl.plot(D+i*20)
        pl.plot(wp+i*20)

        # if the subint is clean enough, accept it and add to working prof
        if area_pulse/area_rfi > thresh:
            #print 'Accepting subint %d for S/N.'%idx
            final_mask[idx] = True
            wp = form_working_profile(final_mask)

    return profs_zerodm,profs_dedisp,stats_zerodm,mask_zerodm,final_mask


def plot_subint_stack(mask,profs,stats,title=None,output=None,fignum=3,
        profs_dedisp=None,mask_zerodm=None):
    import pylab as pl
    from plotting import set_rcParams
    set_rcParams()
    pl.close(fignum);pl.figure(fignum,(8.5,6.8)); pl.clf()
    ax = pl.gca()
    # figure out scale from best profile
    nprof,nbin = len(profs),len(profs[0])
    a = np.argmin(stats)
    x = a/nprof
    y = a-x*nprof
    scale = 8
    dom = np.arange(len(profs[0].amps))
    mask_dedisp = mask
    mask_zerodm = mask_zerodm if (mask_zerodm is not None) else mask
    for iprof,prof in enumerate(profs):
        best_match = np.argmin(stats[iprof])
        color = 'grey' if mask_zerodm[iprof] else 'red'
        offs = iprof*scale
        amps = (prof.amps-profs[best_match].amps)/2**0.5 # back to unit variance
        ax.plot(dom,offs+amps,color=color)
        if profs_dedisp is not None:
            if iprof==0:
                ax.axvline(nbin,color='salmon',lw=2)
            color = 'grey' if mask_dedisp[iprof] else 'red'
            ax.plot(dom+nbin,offs+profs_dedisp[iprof].amps,color=color)
    xmax = nbin if profs_dedisp is None else 2*nbin
    ax.axis([0,xmax,-scale,nprof*scale])
    ax.set_xlabel('Bin')
    ax.set_ylabel('Normalized Amplitude')
    if title is not None:
        ax.set_title(title)
    if output is not None:
        pl.subplots_adjust(left=0.07,top=0.85,right=0.92,bottom=0.078)
        shrink_png(fignum,output)

def p574_default_zap(obs,outplot=None,dry=False):

    psrsh = zapping.ppta_default_zap(obs)
    if not obs.valid():
        return psrsh
    # remove any lawn mowing
    psrsh = jobs.psrsh_segment(filter(lambda l: 'mow' not in l,psrsh))
    zaps = filter(lambda l: l[0] != '#',psrsh)
    #mask,profs,stats = results = get_clean_subints(obs,zaps=zaps)
    profs_zerodm,profs_dedisp,stats_zerodm,mask_zerodm,final_mask = \
        get_clean_subints3(obs,zaps=zaps)
    bad_subints = np.ravel(np.argwhere(~final_mask))
    if dry:
        zap_cmds = map(lambda idx: '#zap subint %d'%idx,bad_subints)
    else:
        zap_cmds = map(lambda idx: 'zap subint %d'%idx,bad_subints)
    psrsh.add('# Zapping the following subints due to impulsive RFI:')
    psrsh.add(*zap_cmds)
    psrsh.add('# Method 1: %d / %d subints zapped.'%(
        np.sum(~mask_zerodm),len(mask_zerodm)))
    psrsh.add('# Method 2: %d / %d subints zapped.'%(
        np.sum(~final_mask),len(final_mask)))
    if outplot is not None:
        results = (final_mask,profs_zerodm,stats_zerodm)
        plot_subint_stack(*results,title=obs.basename,output=outplot,
            profs_dedisp=profs_dedisp,mask_zerodm=mask_zerodm)

    # save some useful ancillary information
    nsamp = 0
    for i in xrange(len(profs_dedisp)):
        if (not dry) or final_mask[i]:
            nsamp += profs_dedisp[i].nsamp
    d = dict(nsamp=nsamp)
    """
    if not obs.is_cal():
        # check for frequency drift -- assume file is contiguous
        profs = np.asarray([x.amps for x in profs_dedisp])
        times = np.arange(obs.nsub)*(obs.tobs/obs.nsub)
        if final_mask.sum() < 2:
            d['dfreq_val'] = 0
            d['dfreq_sig'] = 0
        else:
            dfreq,sig = phase_gradient(profs[final_mask],times[final_mask])
            d['dfreq_val'] = dfreq
            d['dfreq_sig'] = sig
    """
    obs.tmp_ancillary = d # klugey
    return psrsh

def ppta_set_radiometer(obs):
    """ Correct for frequency zapping and get radiometer noise."""

    psrsh = zapping.ppta_default_zap(obs)
    # remove any lawn mowing
    psrsh = jobs.psrsh_segment(filter(lambda l: 'mow' not in l,psrsh))
    zaps = filter(lambda l: l[0] != '#',psrsh)
    nsamps = get_nsamps(obs,zaps=zaps)

    # save some useful ancillary information
    nsamp = np.sum(nsamps)
    d = dict(nsamp=nsamp)
    obs.tmp_ancillary = d # klugey
    return nsamp

def scaled_data(fname):
    import pyfits
    f = pyfits.open(fname)
    d = f['subint'].data.field('data') # nsub x npol x nbin x nchan
    sp = f['subint'].data.field('dat_scl') # nsub x (nchan x npol)
    op = f['subint'].data.field('dat_offs') # nsub x (nchan x npol)
    nsub,npol,nbin,nchan = d.shape

    # reshape scales and weights to a sensible thing
    s = np.empty([nsub,npol,nchan])
    o = np.empty([nsub,npol,nchan])
    for i in xrange(npol):
        print sp[:,i*nchan:(i+1)*nchan].shape
        s[:,i,:] = sp[:,i*nchan:(i+1)*nchan]
        o[:,i,:] = op[:,i*nchan:(i+1)*nchan]

    # transpose d for multiplication
    d = np.transpose(d,(2,0,1,3)) # nbin x nsub x npol x nchan
    d = d*s + o
    d = np.transpose(d,(1,2,3,0))

    # this is not consistent in shape (?) with what psrchive loads...
    return d

def get_test_files():
    test_data = 'rfi/data/J0134-2937'
    files = [
    # 0 -- sub 4 should probably be filtered
    join(test_data,'s131214_102712.rf'),
    # 1 -- good but for subint 11; however, could pass it
    join(test_data,'s130325_013456.rf'),
    # 2 -- clean, scintillating -- all should pass
    join(test_data,'s120319_011934.rf'),
    # 3 -- no obvious signal, should zap at least last two subs
    join(test_data,'s110502_014219.rf'),
    # 4 -- subints 8,10, 13 very bad; rest should pass
    join(test_data,'s100519_020112.rf'),
    # 5 --badly contaminated, but with a few salvageable subints
    join(test_data,'r080319_035345.rf'),
    # 6 long file with modest contamination -- should filter 3 to 4 subints
    join(test_data,'r071116_102952.rf'),
    # 7 -- clean cals
    join(test_data,'../../../data','s130125_091845.cf'),
    # 8
    join(test_data,'../../../data','t130125_091845.cf'),
    # 9 --partial cal -- should filter subints 0,4,5
    join(test_data,'../../../data','p130125_091856.cf'),
    # 10 --clean, scintillating pulsar -- should pass all subints
    join(test_data,'w060824_171428.rf'),
    # 11 --strong RFI, but stronger pulsar -- should pass ~all subints
    join(test_data,'../../../data','s130126_092043.rf'),
    # internal RFI, ideally flag entire files...
    join(test_data,'a060814_162521.rf'),
    join(test_data,'a060814_170213.rf'),
    # relatively clean, but with bin-to-bin correlation
    join(test_data,'w070420_002323.rf'),
    # a clean file, should pass all
    join(test_data,'../../../data','t130125_092233.rf'),
    # a reasonably clean file with some spiky RFI
    join(test_data,'../J1651-4246','s140419_150117.rf'),
    # Vela with drifting phase
    join(test_data,'../J0835-4510','s140420_053525.rf'),
    # one bad subint on borderline -- should prolly filter
    join(test_data,'../J0543+2329','s140420_044902.rf'),
    # another with one subint that should prolly get zapped
    join(test_data,'../J1328-4357','s140419_095812.rf'),
    # another with one subint that should prolly get zapped
    join(test_data,'../J1741-3927','s140403_212048.rf'),
    # another with one subint that should prolly get zapped
    join(test_data,'../J1823-3106','s140419_220652.rf'),
    # pulsar with glitch, (much) fainter than Vela 
    join(test_data,'../J1614-5048','s140419_131646.rf'),
    # intermittent pulsar
    join(test_data,'../J0034-0721','s140419_233743.rf'),
    ]
    return files

def test_obs(fname):
        import database
        db = database.Database_Base()
        db.add_obs(observation.Observation(fname))
        db.add_correction('corrections/fix_ppta_data.asc')
        obs = db.obs[0]
        psrsh = zapping.ppta_default_zap(obs)
        psrsh = jobs.psrsh_segment(filter(lambda l: 'mow' not in l,psrsh))
        zaps = filter(lambda l: l[0] != '#',psrsh)
        p1,p2,stats_zerodm,mask_zerodm,final_mask = t = get_clean_subints3(
            obs,zaps=zaps)
        print 'Method 1: %d / %d subints zapped.'%(np.sum(~mask_zerodm),len(mask_zerodm))
        print 'Method 2: %d / %d subints zapped.'%(np.sum(~final_mask),len(final_mask))
        results = (final_mask,p1,stats_zerodm)
        if not os.path.isdir('/tmp/rfiplots'):
            os.mkdir('/tmp/rfiplots')
        output = '/tmp/rfiplots/%s_zaps.png'%obs.basename
        plot_subint_stack(*results,title=obs.basename,output=output,profs_dedisp=p2)
        return (obs,) + t

def test_suite(fnames=None):
    if fnames is None:
        fnames = get_test_files()

    for fname in fnames:
        print 'Working on %s.'%fname
        test_obs(fname)

# TODO -- handle files with only one subint

if __name__== '__main__':
    fname = get_test_files()[-1]
    obs,p1,p2,stats_zerodm,mask_zerodm,final_mask = test_obs(fname)
