"""
Manage analysis of TOAs, e.g. comparisons amongst pipelines.

$Header: /nfs/slac/g/glast/ground/cvs/users/kerrm/tpipe/timing.py,v 1.14 2014/01/21 06:46:59 kerrm Exp $

author: Matthew Kerr <matthew.kerr@gmail.com>
"""

import numpy as np
from os.path import basename
from collections import deque

from external.parfiles import get_toa_strings,StringFloat,get_bats_etc

from util import readlines

class TOA(object):
    """ Encapsulate information that can be gleaned solely from .tim."""

    def __init__(self,tim_string,bat=None):
        toks = tim_string.split()
        self.fname = toks[0]
        self.profile = basename(self.fname).split('.')[0]
        self.sat = np.float128(toks[2])
        self._parse_flags(toks)
        if '-B' in toks:
            self.band = toks[toks.index('-B')+1]
        else:
            self.band = None
        self.bat = bat

    def get_band(self):
        if self.band is not None:
            return self.band
        if '-f' in self.flags:
            idx = self.flags.index('-f')
            c = self.vals[idx]
            if ('MULTI' in c) or ('H-OH' in c):
                return '20CM'
            if '10CM' in c:
                return '10CM'
            if '50CM' in c:
                return '50CM'

    def _parse_flags(self,toks):
        flags = deque()
        vals = deque()
        counter = 0
        while counter < len(toks):
            if toks[counter][0] == '-':
                flags.append(toks[counter])
                vals.append(toks[counter+1])
                counter += 2
            else:
                counter += 1
        self.flags = list(flags)
        self.vals = list(vals)

    def __eq__(self,other):
        """ Return true if TOAs represent same observation."""
        return self.profile == other.profile

class TOAs(object):

    def __init__(self,toas):
        self.toas = toas
        self.cache = [t.profile for t in toas]
        self.band_cache = [t.get_band() for t in toas]

    def __len__(self):
        return len(self.toas)

    def __getitem__(self,idx):
        return self.toas[idx]

    def band_match(self,band):
        toas = [self.toas[i] for i in xrange(len(self)) if self.band_cache[i]==band]
        return TOAs(toas)

    def find_match(self,toa):
        profile = toa.profile
        try:
            idx = self.cache.index(profile)
            return self.toas[idx],idx
        except ValueError:
            pass
        return None,None

def compare_toas(tim1,tim2):
    """ Perform a residuals analysis on two sets of TOAs.
    
    It is assumed that at least one (the first) of the two files contains
    some of the "standard" DR2 flags to allow in band identification, etc.

    TOAs present in one file but not the other are racked to 0 residual
    with appropriate symbols.
    """
    t1 = map(str.strip,get_toa_strings(tim1))
    t2 = map(str.strip,get_toa_strings(tim2))
    return t1,t2

if __name__ == '__main__':
    par = '/tmp/J1909-3744.pcm.par'
    tim1 = '/tmp/J1909-3744.pcm.toa'
    tim2 = '/tmp/J1909-3744_ppta.tim'
    t1,t2 = compare_toas(tim1,tim2)
    
    bats1,err1,phas1 = get_bats_etc(par,tim1)
    bats2,err1,phas2 = get_bats_etc(par,tim2)
    toas1 = TOAs(map(TOA,t1,bats1))
    toas2 = TOAs(map(TOA,t2,bats2))

    import pylab as pl
    pl.figure(1); pl.clf()
    bands = ['20CM']#,'10CM','50CM']
    colors = ['lightgreen','blue','salmon']

    for i in xrange(len(bands)):
        bats = deque()
        resids = deque()
        outcome = deque()
        subset1 = toas1.band_match(bands[i])
        subset2 = toas2.band_match(bands[i])
        subset2_included = np.asarray([False]*len(subset2))
        for toa in subset1:
            bats.append(float(toa.bat))
            match,idx = subset2.find_match(toa)
            if match is None:
                outcome.append(1)
                resids.append(0)
            else:
                outcome.append(0)
                resids.append(86400*(toa.bat-match.bat))
                subset2_included[idx] = True
                print toa.bat,match.bat
        remaining = np.ravel(np.argwhere(~subset2_included))
        for idx in remaining:
            toa = subset2[idx]
            bats.append(toa.bat)
            resids.append(0)
            outcome.append(2)

        a = np.argsort(bats)
        x = np.asarray(bats)[a]
        y = np.asarray(resids)[a]
        r = np.asarray(outcome)[a]
        m0 = (r==0) & (np.abs(y)<5e-4)
        pl.plot(x[m0],y[m0],color=colors[i],marker='o',ls=' ')
        m1 = (r==1) & (np.abs(y)<5e-4)
        pl.plot(x[m1],y[m1],color=colors[i],marker='s',ls=' ')
        m2 = (r==2) & (np.abs(y)<5e-4)
        pl.plot(x[m2],y[m2],color=colors[i],marker='^',ls=' ')

    #pl.axis([53000,57000,-1e-3,1e-3])

