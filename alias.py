"""
Provide aliases for pulsar names when searching disks.

$Header: /nfs/slac/g/glast/ground/cvs/users/kerrm/tpipe/timing.py,v 1.14 2014/01/21 06:46:59 kerrm Exp $

author: Matthew Kerr <matthew.kerr@gmail.com>
"""
# Format -- key == correct name; value == [correct name, aliases]

aliases = {
'J0101-6422' : ['J0101-6422','J0100-6424'],
'J0627+0706' : ['J0627+0706','J0627+0705'],
'J0820-3826' : ['J0820-3826','J0821-3824'],
'J1017-7156' : ['J1017-7156','J1018-7154'],
'J1028-5819' : ['J1028-5819','J1028-5820'],
'J1055-6028' : ['J1055-6028','J1055-6032'],
'J1227-6208' : ['J1227-6208','J1226-6202'],
'J1446-4701' : ['J1446-4701','J1447-4659'],
'J1514-4946' : ['J1514-4946','J1514-4945'],
'J1545-4550' : ['J1545-4550','J1546-4552'],
'J1658-5324' : ['J1658-5324','J1658-5317'],
'J1702-4306' : ['J1702-4306','J1702-4305'],
'J1824-2452A': ['J1824-2452A','J1824-2452'],
'J1832-0836' : ['J1832-0836','J1832-0837'],
'J1902-5105' : ['J1902-5105','J1902-5109'],
'J2236-5527' : ['J2236-5527','J2236-5526'],
}

# put in entries for calibrators
tocal = lambda key: key+'_R'

for key in aliases.keys():
    aliases[tocal(key)] = map(tocal,aliases[key])

def make_reverse_lookup():
    reverse_lookup = dict()
    for key in aliases.keys():
        for val in aliases[key]:
            reverse_lookup[val] = key
    return reverse_lookup

reverse_lookup = make_reverse_lookup()

def get_aliases(name):
    """ Accept any recognized version of a source name and return all
        aliases.  The preferred (typically Jname) version is return first.
    """
    
    try:
        preferred = reverse_lookup[name]
        return aliases[preferred]
    except KeyError:
        return [name]

# build cache for speedy application of corrections
preferred_names = dict()

def preferred_name(name):
    try:
        return preferred_names[name]
    except KeyError:
        preferred_names[name] = get_aliases(name)[0]
    return preferred_names[name]
