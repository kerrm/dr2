import numpy as np
import pylab as pl

import os
import glob
from collections import deque

import make_dsets
import clkcorr
import plotting

def plot_events():
    ls = '-'
    steps = [54601,54656,54663,54766,54779,54843,54852,54880,54968,55019,55179,55189,55221,55238,55382,55705,56153]
    steps += [56421,56620,56742,56784,57370]
    for step in steps:
        pl.axvline(step,color='red',ls='--')

def plot_offsets(mjds,offs,yrange=1e-6,rack_outliers=False,fignum=1,
        clear=True):
    mjds = np.asarray(mjds)
    offs = np.asarray(offs)
    m = np.median(offs)
    if rack_outliers:
        offs = offs.copy()
        offs[offs > (m + 0.99*yrange)] = m + 0.99*yrange
        offs[offs < (m - 0.99*yrange)] = m - 0.99*yrange
    pl.figure(fignum); 
    if clear:
        pl.clf()
    pl.plot(mjds,(offs)*1e9,marker='.',ls='-')
    pl.axis([np.min(mjds),np.max(mjds),m-yrange*1e9,m+yrange*1e9])
    pl.xlabel('Modified Julian Day')
    pl.ylabel('GPS-PKS (ns)')

def plot_toas(timfile):
    toas = make_dsets.parse_toas(timfile)
    mjds = np.asarray([x.mjd for x in toas])
    yval = np.zeros_like(mjds)
    pl.plot(mjds,yval,ls=' ',marker='.',color='k')

def plot_all_toas():
    import glob
    timfiles = glob.glob(os.path.join(make_dsets.combined,'*.toa'))
    for timfile in timfiles:
        plot_toas(timfile)

def plot_pks2gps(fname=None):
    if fname is None:
        fname = os.path.join(os.environ['TEMPO2'],'clock','pks2gps.clk')
    mjds,offs = np.loadtxt(fname,comments='#').transpose()
    mjds = mjds[:-1]
    offs = offs[:-1]
    pl.plot(mjds,offs*1e9,marker='.')

def make_dr2_paper_plot():
    """ Make a plot showing GPS-PKS over the dr2 span, with colours/symbols
    indicating the sampling points during observing sessions as inferred
    by the present of TOAs in the dataset.
    """

    timfiles = glob.glob(os.path.join(make_dsets.combined,'*.toa'))
    rvals = deque()
    for timfile in timfiles:
        toas = make_dsets.parse_toas(timfile)
        mjds = np.asarray([x.mjd for x in toas])
        rvals.append(mjds)
    toas = np.sort(np.concatenate(rvals))

    mjds,offs = clkcorr.get_pks_to_gps()

    # clip to DR2 range
    mask = mjds < 57388
    mjds = mjds[mask]
    offs = offs[mask]

    # construct a mask of the affected points
    mask = np.zeros(len(mjds),dtype=bool)
    indices = np.searchsorted(mjds,toas)
    mask[np.clip(indices-1,0,len(mask)-1)] = True
    mask[np.clip(indices+1,0,len(mask)-1)] = True

    offs_clips = 1e6*np.clip(offs,-5e-5,5e-5)

    colors = plotting.get_tableau20()
    from plotting import get_tableau20,set_rcParams
    set_rcParams(bigticks=True,ticklabelsize='large')
    pl.rcParams['text.usetex'] = True
    #pl.close(fignum); pl.figure(fignum,(5.75,5))
    pl.figure(2,(5.75,5)); pl.clf()
    ax = pl.axes([0.16,0.13,0.775,0.82])
    ax.plot(mjds,offs_clips,marker='.',ls='-',color=colors[1],
        rasterized=True)
    ax.plot(mjds[mask],offs_clips[mask],marker='.',ls=' ',color=colors[0],
        rasterized=True)
    ax.set_xlabel('MJD')
    ax.set_ylabel('PKS-GPS ($\mu$s)')
    ax.axis([50500,57500,-55,55])
    pl.savefig('/tmp/clock_p1.pdf')

    pl.figure(3,(5.75,5)); pl.clf()
    ax = pl.axes([0.16,0.13,0.775,0.82])
    ax.plot(mjds,offs_clips,marker='.',ls='-',color=colors[1],
        rasterized=True)
    ax.plot(mjds[mask],offs_clips[mask],marker='.',ls=' ',color=colors[0],
        rasterized=True)
    ax.set_xlabel('MJD')
    ax.set_ylabel('PKS-GPS ($\mu$s)')
    ax.axis([52900,57500,-8,2])
    pl.savefig('/tmp/clock_p2.pdf')

    pl.figure(4,(5.75,5)); pl.clf()
    ax = pl.axes([0.16,0.13,0.775,0.82])
    ax.plot(mjds-56950,offs_clips,marker='.',ls='-',color=colors[1],
        rasterized=True)
    ax.plot(mjds[mask]-56950,
        offs_clips[mask],marker='.',ls=' ',color=colors[0],rasterized=True)
    ax.set_xlabel('MJD-56950')
    ax.set_ylabel('PKS-GPS ($\mu$s)')
    ax.axis([56923-56950,56981-56950,-0.1,0.07])
    pl.savefig('/tmp/clock_p3.pdf')

    return mjds,offs,mask
