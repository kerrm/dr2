"""
Collection of classes and functions for producing tempo2 clock
correction files from the Parkes observatory clock logs.

Based on C++ code that can be found on the Marsfield system:

/pulsar/psr/dev/cvshome/pulsar/soft_atnf/atas/clkcorr

The raw logs are available on the Parkes system:
/nfs/online/timing

Copies of the logs covering the dr2 span are provided with the release.

Re-write is necessary both for transparency and to more accurately handle
sharp features in the observatory-GPS frame correction.
"""

"""
Notes on the observatory clock, from Russell Edwards 24 Sept 2004:

MJD 50844 (Jan 31, 1998) - 52311.17 (Feb 6, 2002):
    GPS to observatory tie via Mark IV clock vs Totally Accurate Clock (GPS)
MJD 52311.17 - present
    GPS to observatory tia via Mark VI clock vs GPS East
"""

"""
Notes for producing the tempo2 format files from the logs:

The typical format for a log line:

5140 08:39:58 01-Jan-2007 134 54100 77849 6.847333E-07 1.014267E-06 4.830000E-06 1.810000E-06 2.000000E+00 2.000000E+00 7.635333E-07 9.890000E-06 1.600000E-07

The key values are in columns 5, 6, and 7 (counting from 1), which are the
integer MJD, the additional seconds component, and the offset (PKS-GPS) in
seconds.

From R. Edwards readParkesGPS.h:

 24 // NOTE (2): Automatically applies corrections to compensate for changes
 25 // in the configuration of the monitoring system. These consist of 
 26 // discrete jumps at the following times:
 27 //
 28 //   -92 ns   @ MJD > 51835  (18 Oct 2000)
 29 //   +40 ns   @ MJD > 52241  (28 Nov 2001)
 30 //
 31 //  These are compensated by adding the offset to all earlier data
 32 //  (since the reconfigurations are believed to bring the system closer
 33 //  to the true value of GPS-PKS).
 34 //  Note that while this should have been compensated for in the old
 35 //  scheme (see time_fix.c), the second fix was apparently NOT present
 36 //  in time.dat as of August 2004. It is suggested that the corrected
 37 //  time.dat was accidentally overwritten by an uncorrected one.

MTK's notes: By examining the TAC2 and common view logs together, it is
clear that some TAC2 points are simply in error.  This often occurs when
there is a step in the maser, but also randomly.  I have commented these
out of the log copies provided with the data release.  Likewise, some CV
points are in error by comparison with TAC2.
"""

import numpy as np
from collections import deque
import glob

def parse_log(fname,scrunch_ytol=None,scrunch_xtol=None,clipval=None,
    verbose=False):

    mjds = deque()
    offs = deque()
    last_mjd = -np.inf

    lines = map(str.strip,file(fname).readlines())

    for line in lines:
        if len(line) == 0:
            continue
        if line[0] == '#':
            continue
        toks = line.split()
        #if len(toks) != 15:
            #continue

        try:
            mjd = int(toks[4]) + float(toks[5])/86400
        except ValueError:
            if 'repeated' in line:
                continue

        # these lines seem to happen when the clock is reset
        if int(toks[4]) == 0:
            if verbose:
                print 'Problem line:'
                print line
            continue

        # ensure sorting!
        if (mjd < last_mjd):
            if verbose:
                print 'Problem MJD decreasing! (last = %.5f, current = %.5f)'%(last_mjd,mjd)
                print line
            continue
        else:
            last_mjd = mjd
            

        # sign switch from PKS-GPS to GPS-PKS
        try:
            offset = -float(toks[6])
        except ValueError:
            # ignore some issues in GPS logs
            if 'repeated' in line:
                continue

        # Apply corrections for re-configurations of the monitoring system.
        if mjd < 51835:
            offset -= 92e-9
        if mjd < 52241:
            offset += 40e-9

        mjds.append(mjd)
        offs.append(offset)

    mjds = np.asarray(mjds)
    offs = np.asarray(offs)

    if clipval is not None:
        mask = np.abs(offs) < clipval
        mjds = mjds[mask]
        offs = offs[mask]

    if (scrunch_ytol is not None) and (scrunch_xtol is not None):
        return scrunch(mjds,offs,ytol=scrunch_ytol,xtol=scrunch_xtol)
    return np.asarray(mjds),np.asarray(offs)

    """ Note from source:
    // NOTE:
    // Actually there is an integer number of seconds omitted from
    // this offset! However, there is also an equal number of seconds
    // omitted from the utc-gps offset, so they will cancel later
    """

def scrunch(mjds,offs,ytol=1e-8,xtol=1./24):
    """ Produce a new time series combining points within xtol provided the
    peak-to-peak variation is not greater than ytol.  If ytol is exceeded,
    a new segment is produced.

    After a time jump (missing data, reset system, etc.), we want to jump
    directly to the new value rather than linearly interpolating to the
    middle of the next segment.  Therefore, after a time jump, if there is
    a scrunch, copy that value to the edge of the bin (first sample).
    """

    mjds = np.asarray(mjds)
    offs = np.asarray(offs)

    nmjds = deque()
    noffs = deque()

    last_idx = 0
    current_ymin = np.inf
    current_ymax = -np.inf
    yexcursion = False

    for i in xrange(len(mjds)):
        m = mjds[i]
        o = offs[i]

        # check y tol
        if o < current_ymin:
            current_ymin = o
        if o > current_ymax:
            current_ymax = o
        if abs(current_ymax - current_ymin) > ytol:
            nmjds.append(np.mean(mjds[last_idx:i]))
            noffs.append(np.mean(offs[last_idx:i]))
            current_ymin = current_ymax = o
            # insert a sample of the previous one was an average
            if (i-last_idx) > 1:
                nmjds.append(mjds[i-1])
                noffs.append(offs[i-1])
            last_idx = i
            yexcursion = True
            continue

        # if we exceeded tolerance along the y direction (i.e. skipped),
        # keep at least one sample at high time resolution
        if yexcursion:
            nmjds.append(np.mean(mjds[last_idx:i]))
            noffs.append(np.mean(offs[last_idx:i]))
            current_ymin = current_ymax = o
            last_idx = i
            yexcursion = False
            continue

            
        # check x tol
        if (m - mjds[last_idx]) > xtol:
            nmjds.append(np.mean(mjds[last_idx:i]))
            noffs.append(np.mean(offs[last_idx:i]))
            # check to see if this is also a y skip (e.g. after a restart)
            #if abs(current_ymax - current_ymin) > ytol:
            #    if (i-last_idx) > 1:
            #        nmjds.append(mjds[i-1])
            #        noffs.append(offs[i-1])
            #    yexcursion = True
            current_ymin = current_ymax = o
            last_idx = i
            continue

    return np.asarray(nmjds),np.asarray(noffs)

def remove_blips(mjds,offs,tol=1e-8):
    """ Remove single outlying points."""
    mjds = np.asarray(mjds)
    offs = np.asarray(offs)
    mask = np.empty(len(mjds),dtype=bool)
    b1 = np.abs(mask[1:-1]-mask[:-2]) > 10*tol
    b2 = np.abs(mask[2:]-mask[:-2]) < 3*tol
    mask[1:-1] = ~(b1 & b2)
    mask[0] = True
    mask[-1] = True
    return mask

def parse_logs(fnames,**kwargs):
    fnames = sorted(fnames)
    mjds = deque()
    offsets = deque()
    for fname in fnames:
        m,o = parse_log(fname,**kwargs)
        mjds.append(m)
        offsets.append(o)
    return np.concatenate(mjds),np.concatenate(offsets)

def get_gps(noscrunch=False,clipval=1e-4,verbose=False):
    fnames = glob.glob('logs/gps/gps.*')
    if noscrunch:
        return parse_logs(fnames,clipval=clipval,verbose=verbose)
    else:
        return parse_logs(fnames,scrunch_ytol=500e-9,scrunch_xtol=24./24,
            clipval=clipval,verbose=verbose)

def get_pkclk00(noscrunch=False,clipval=1e-4,verbose=False):
    fnames = glob.glob('logs/pkclk00*')
    if noscrunch:
        return parse_logs(fnames,clipval=clipval,verbose=verbose)
    else:
        return parse_logs(fnames,scrunch_ytol=30e-9,scrunch_xtol=2./24,
            clipval=clipval,verbose=verbose)

def get_pkclk01(noscrunch=False,clipval=1e-4):
    fnames = glob.glob('logs/pkclk01/pkclk01*')
    if noscrunch:
        return parse_logs(fnames,clipval=clipval)
    else:
        return parse_logs(fnames,scrunch_ytol=15e-8,scrunch_xtol=1./24,
            clipval=clipval)

def get_difference(mjds0,offs0,mjds1,offs1):
    """ Compute the daily difference between two realizations. """

    # first, average the time series down to daily values
    tmin = int(max(mjds0.min(),mjds1.min()))
    tmax = int(min(mjds0.max(),mjds1.max()))
    bins = np.arange(tmin,tmax+1)
    h0 = np.histogram(mjds0,bins=bins)
    h1 = np.histogram(mjds0,bins=bins,weights=offs0)
    avg_0 = h1[0]/h0[0]

    b1 = np.arange(int(mjds1.min()),int(mjds1.max())+1)
    h0 = np.histogram(mjds1,bins=bins)
    h1 = np.histogram(mjds1,bins=bins,weights=offs1)
    avg_1 = h1[0]/h0[0]

    return bins,avg_0,avg_1


def get_common_view(noscrunch=False):
    """ Read in common view logs and, using the NMI AUS to GPS conversion,
    create PKS to GPS through AUS.
    """

    # the NMI logs store AUS-GPS
    def parse_nmi(fname):
        mjds = deque()
        offs = deque()
        for line in file(fname).readlines():
            if (line[0] == '#') or (line[0] == '!'):
                continue
            try:
                toks = line.split()
                if len(toks)==3:
                    m = int(toks[1])
                    o = int(toks[2])
                    mjds.append(m)
                    offs.append(o)
            except:
                continue
        return mjds,offs

    def read_nmi():
        nmi_fnames = glob.glob('logs/nmi/*.gps')
        mjds = deque()
        offs = deque()
        for fname in nmi_fnames:
            m,o = parse_nmi(fname)
            mjds.extend(m)
            offs.extend(o)
        a = np.argsort(mjds)
        return np.asarray(mjds)[a],np.asarray(offs)[a]

    nmi_mjds,nmi_offs = read_nmi()

    # the CV logs are in ATNF-AUS; so CV + NMI = ATNF-GPS
    cvmjds,cvoffs = np.loadtxt('logs/common_view/daily_reports/daily.log',
        usecols=[2,3]).transpose()
    counter = 0
    for nmjd,noff in zip(nmi_mjds,nmi_offs):
        for cvmjd in cvmjds[counter:]:
            if cvmjd > nmjd:
                break
            cvoffs[counter] += noff
            counter += 1

    cvoffs = (-1e-9)*cvoffs

    if not noscrunch:
        scrunch_ytol=15e-8
        scrunch_xtol=1./24,
        return scrunch(cvmjds,cvoffs,ytol=scrunch_ytol,xtol=scrunch_xtol)

    return cvmjds,cvoffs

def splice_in_common_view(mjds_pkclk00,offs_pkclk00,mjds_cv,offs_cv):
    """ Splice in common view data during two week interval when pkclk00
    monitoring was down.
    """
    t1 = (56994 + 13350./86400)
    t2 = (57010 + 7950./86400)
    m1 = (((mjds_pkclk00 > (t1-10)) & (mjds_pkclk00 < t1)) |
          ((mjds_pkclk00 > t2) & (mjds_pkclk00 < (t2+10))))
    m2 = (((mjds_cv > (t1-10)) & (mjds_cv < t1)) |
          ((mjds_cv > t2) & (mjds_cv < (t2+10))))
    offset = np.mean(offs_pkclk00[m1])-np.mean(offs_cv[m2])
    m_pkclk00_0 = mjds_pkclk00 <= t1
    m_pkclk00_1 = mjds_pkclk00 >= t2
    m_cv = (mjds_cv > t1) & (mjds_cv < t2)
    mjds = np.concatenate((
        mjds_pkclk00[m_pkclk00_0],
        mjds_cv[m_cv],
        mjds_pkclk00[m_pkclk00_1]
    ))
    offs = np.concatenate((
        offs_pkclk00[m_pkclk00_0],
        offs_cv[m_cv] + offset,
        offs_pkclk00[m_pkclk00_1]
    ))
    return mjds,offs

def write_pks2gps(mjds,offs):
    
    lines = '\n'.join(('%.5f %.10f'%(m,o) for m,o in zip(mjds,offs)))
    f = file('pks2gps.clk','w')
    f.write('# UTC(PKS) UTC(GPS)\n')
    f.write(lines)
    f.write('\n99999 %.10f\n'%(offs[-1]))
    f.close()

def parse_cirt_gps(fname):
    lines = map(str.strip,file(fname).readlines())

    mjds = deque()
    offs = deque()
    found_gps = False
    read_one = False
    for line in lines:
        toks = line.split()
        if found_gps:
            try:
                month,date,mjd,off = toks[:4]
                month = str(month)
                date = int(date)
                mjd = int(mjd)
                off = float(off)*1e-9
                mjds.append(mjd)
                offs.append(off)
                read_one = True
            except Exception:
                if not read_one:
                    continue
                # check for blank lines
                if len(toks) == 0:
                    continue
                # check for early "units" line
                if (len(toks) > 2) and (toks[2] == '(ns)'):
                    continue
                # finish parsing
                break
        else:
            if 'MJD' in toks:
                for tok in toks:
                    if tok.startswith('C0'):
                        found_gps = True
                        break

    return mjds,offs

def get_gpsutc():
    fnames = sorted(glob.glob('logs/cirt/cirt.*'))
    mjds = deque()
    offs = deque()
    for fname in fnames:
        m,o = parse_cirt_gps(fname)
        mjds.extend(m)
        offs.extend(o)
    return mjds,offs

def write_gps2utc(mjds,offs):
    
    lines = '\n'.join(('%.5f %.11f'%(m,o) for m,o in zip(mjds,offs)))
    f = file('gps2utc.clk','w')
    f.write('# UTC(GPS) UTC\n')
    f.write(lines)
    f.write('\n99999 %.11f\n'%(offs[-1]))
    f.close()

def get_pks_to_gps():
    mjds_old,offs_old = get_gps()
    mask_old = (mjds_old > 50844) & (mjds_old < 52311.17)
    mjds,offs = get_pkclk00()
    mask = mjds > 52311.17
    mjds = np.append(mjds_old[mask_old],mjds[mask])
    offs = np.append(offs_old[mask_old],offs[mask])
    return mjds,offs

if __name__ == '__main__':
    print 'Updating pks2gps...'
    mjds,offs = get_pks_to_gps()
    write_pks2gps(mjds,offs)
    print 'Updating gps2utc...'
    print 'TODO'
        

# thoughts -- define "GTI" like intervals where clock isn't changing too
# quickly, and have a function to check that TOAs lie within those GTI

# NOTES to self
# the pks2gps realization is clearly bad ~56376, which affects at least a
# few observations; see if they have been flagged; ditto 56442 (this
# is a bona fide step change); also 56552; 56153
