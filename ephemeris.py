"""
A quick and dirty bit of code to handle locating and managing ephemerides.

author: Matthew Kerr <matthew.kerr@gmail.com>
"""
from glob import glob
import os
from os.path import join,basename,abspath
import re
from jobs import psrsh_segment

import alias

letter_jnames = ['J0737-3039','J1807-2459','J1910-5959','J1701-3006']

jname_re = re.compile(
    r'J[0-2][0-9][0-5][0-9][-,+][0-8][0-9][0-5]?[0-9]?[A-Z]?')

jname_strict_re = re.compile(
    r'J[0-2][0-9][0-5][0-9][-,+][0-8][0-9][0-5]?[0-9]?[A-Z]?.par')

jname_plus_re = re.compile(
    r'J[0-2][0-9][0-5][0-9][-,+][0-8][0-9][0-5]?[0-9]?[A-Z]?.*par')

def jname_match(fname,strict=True):
    """Check if the basename of a file is "jname.par"."""
    if strict:
        return jname_strict_re.match(basename(fname)) is not None
    return jname_plus_re.match(basename(fname)) is not None

def jname_from_fname(fname):
    """ Attempt to determine jname based on filename.
    """
    t = basename(fname)
    match = jname_re.match(t)
    if match is None:
        return None
    jname = t[match.start():match.end()]
    if (len(jname) > 10) and jname[:10] not in letter_jnames:
        jname = jname[:10]
    return alias.preferred_name(jname)

class Ephemeris(object):
    """ Manage a tempo2-style ephemeris.
    """

    def __init__(self,name,fname):
        self.name = name
        self.fname = abspath(fname)
        self._cache = dict()

    def __str__(self):
        return basename(self.fname)

    def get_key(self,key='DM'):
        try:
            return self._cache[key]
        except KeyError:
            self._cache[key] = None
            lines = file(self.fname).readlines()
            for line in lines:
                toks = line.split()
                if (len(toks) > 1) and (toks[0]==key):
                    self._cache[key] = toks[1]
        return self._cache[key]

    def get_dm(self,obs):
        """ Return ephemeris DM for now, but could be time dep."""
        return self.get_key('DM')

    def get_rm(self,obs):
        """ Return ephemeris RM for now, but could be time dep."""
        return self.get_key('RM')

    def get_dm_rm_psrsh(self,obs):
        """ Update DM and RM, but not ephemeris."""
        psrsh = psrsh_segment()
        dm = self.get_dm(obs)
        if dm is not None:
            psrsh.add('edit dm=%s'%dm)
        rm = self.get_rm(obs)
        if rm is not None:
            psrsh.add('edit rm=%s'%rm)
        return psrsh

    def get_eph_psrsh(self,obs,strict=True,fudge=10):
        """ Return psrsh for installing an ephemeris to align data subints.

        If strict, require that the observation lie within the bounds of
        validity of the ephemeris.
        """
        if strict:
            mjd = obs.get_mjd()
            start,stop = self.get_bounds()
            # if start/stop are None, the the logic below will eval to false
            try:
                valid = ((mjd+fudge) > start) and ((mjd-fudge) < stop)
            except TypeError:
                # handle case when mjd is None for bad obs.
                valid = False
            # but to be sure:
            if start is None:
                valid = False
            if not valid:
                return psrsh_segment(
                    ['# data lie outside validity of ephemeris',
                     '# install par %s'%self.fname])
        return psrsh_segment(['install par %s'%self.fname])

    def get_update_psrsh(self):
        psrsh = self.get_dm_rm_psrsh()
        psrsh += self.get_eph_psrsh()
        return psrsh
        
    def get_bounds(self,fudge=10):
        try:
            start = float(self.get_key('START'))
            stop = float(self.get_key('FINISH'))
            return start-fudge,stop+fudge
        except:
            return None,None
        

class EphemerisDatabase(object):
    """ Base implementation -- do not instantiate directly."""

    def __init__(self,ephs):
        self.ephs = dict()
        for eph in ephs:
            if eph.name in self.ephs:
                raise ValueError(
                    'Multiple ephemerides for %s found!'%eph.name)
            self.ephs[eph.name] = eph

    def __call__(self,obs):
        if type(obs)==type(''):
            # assume is a jname
            name = obs
        else:
            name = obs['name']
        if name in self.ephs:
            return self.ephs[name]
        return None

class GeneralEphemerisDatabase(EphemerisDatabase):
    """ Read in ephemerides in specified directory.

    Filenames must be of the form JNAME*.par, e.g. J0835-4510_new.par.

    Additionally, a supplementary directory specifying "override"
    ephemerides may be specified.  This is for times the user may wish to
    begin with a basic ephemeris, e.g. from psrcat, and then for some
    pulsars replace it with an "upgraded" ephemeris based on the pipeline
    output with the initial ephemeris.
    """

    def __init__(self,ephdir,suppldir=None):

        ephs = filter(jname_match,glob(join(ephdir,'*.par')))
        jnames = map(jname_from_fname,ephs)

        if suppldir is not None:
            suppl_ephs = filter(jname_match,glob(join(suppldir,'*.par')))
            ephs.extend(suppl_ephs)
            jnames.extend(map(jname_from_fname,suppl_ephs))

        if len(set(jnames)) != len(jnames):
            # handle multiple ephemerides by taking the last to appear
            # thus supplementary ephemerides override default ones
            unique_ephs = dict()
            for eph,jname in zip(ephs,jnames):
                multiple_ephs = jname in unique_ephs.keys()
                unique_ephs[jname] = eph
                if multiple_ephs:
                    print 'Found multiple ephemerides for %s; using %s.'%(
                        jname,unique_ephs[jname])
            jnames = unique_ephs.keys()
            ephs = [unique_ephs[k] for k in unique_ephs.keys()]
            
        ephemerides = map(Ephemeris,jnames,ephs)
        super(GeneralEphemerisDatabase,self).__init__(ephemerides)

class PPTA_EphemerisDatabase(GeneralEphemerisDatabase):

    def __init__(self,ephdir=None,suppldir=None):

        if ephdir is None:
            try:
                ephdir = os.environ['PPTA_EPHEMERIDES']
            except KeyError:
                raise ValueError('Could not locate an ephemeris directory!')

        super(PPTA_EphemerisDatabase,self).__init__(
            ephdir,suppldir=suppldir)

class P574_EphemerisDatabase(EphemerisDatabase):

    def __init__(self,ephdir,suppldir=None):
        ephs = filter(lambda x: jname_match(x,strict=False),
            glob(join(ephdir,'*.par')))
        jnames = map(jname_from_fname,ephs)

        if suppldir is not None:
            suppl_ephs = filter(jname_match,glob(join(suppldir,'*.par')))
            ephs.extend(suppl_ephs)
            jnames.extend(map(jname_from_fname,suppl_ephs))

        if len(set(jnames)) != len(jnames):
            raise ValueError('Multiple ephemerides for same pulsar!')

        ephemerides = map(Ephemeris,jnames,ephs)
        super(P574_EphemerisDatabase,self).__init__(ephemerides)

def ppta_default():
    try:
        return PPTA_EphemerisDatabase()
    except ValueError:
        print 'Warning! No ephemerides to install.'
