""" Module provides support for flagging data.

Currently supports two paradigms:

(1) a list of specific observations with a common classification
-- e.g., observations affected by catastrophic RFI
-- observations with unidentifiable configuration problems (wrong LO?)

(2) a set of observations satisfying particular constraints
-- e.g., commissioning data for a backend
-- data affected by a particular problem (e.g. temporary backend bug)

See notes for DataQualityFlag for more detailed usage.
"""

""" INSTRUCTIONS TO ADD A NEW FLAG:

To add a new flag, create the appropriate child class of DataQualityFlag.

It should provide a brief description as well as unique flags for both the
data and, if necessary, the TOA associated with it.

If the data shouldn't be processed at all, inherit "can_process" and have it
return False.  This is done automatically by inheriting
DataQualityFlag_NoProcess.

Finally, to allow objects to be built from a text interface, add the
class/flag pair to the flag_map dictionary.
"""

import util
from collections import deque

# TODO -- note similarities to blacklist.py, combine (all flagging)

class PPTA_SQL_Flags(object):
    """ Read in the flags from the PPTA database output, and flag TOAs.
    
    Although this could be worked in to the general DataQualityFlag
    framework, I expect this to be a temporary class, so find as ad hoc.
    """

    def __init__(self):
        # parse the flags
        lines = util.readlines(
            util.local_file('corrections/brief_ppta_flags.asc'))
        self.flags = dict()
        for line in lines:
            fname,flag = line.split()
            self.flags[fname] = int(flag)

    def flag_obs(self,obs):
        if obs.basename in self.flags.keys():
            flag = '-ppta_sql %d'%(self.flags[obs.basename])
            obs.toa_flags.append(flag)

    def flag_database(self,db):
        for obs in db.obs:
            # avoid function call overhead
            if obs.basename in self.flags.keys():
                flag = '-ppta_sql %d'%(self.flags[obs.basename])
                obs.add_toa_flag(flag)

def apply_ppta_sql_flags(db):
    t = PPTA_SQL_Flags()
    t.flag_database(db)
            

class DataQualityFlag(object):
    """ Implement a hierarchy of data quality flagging.

    To provide a description of a particular feature, inherit this class.

    This class and all children should provide a succinct description of
    the feature/issue via the self._description member.

    If the data are completely useless, inherit can_process and have it
    return False.

    If the data are usable in some aspects (e.g. profile is fine) but are
    not good for timing, the child class should have a _toa_flag member
    which will be attached to TOAs from data with the flag.
    """
    
    def init(self):
        self._description = 'Unknown flag.'
        self._flag = 'unknown'

    def __init__(self,comment=None):
        self.comment = comment
        self.init()

    def __str__(self):
        s = self._description
        if self.comment is not None:
            s = s + '\n' + self.comment
        return s

    def __eq__(self,other):
        return self._flag == other._flag

    def flag(self):
        return self._flag

    def toa_flag(self):
        try:
            return self._toa_flag
        except AttributeError:
            return None

    def can_process(self):
        return True

class DataQualityFlag_NoProcess(DataQualityFlag):

    def can_process(self):
        return False

class CorruptFile(DataQualityFlag_NoProcess):
    """ Files that cannot be opened as FITS are excluded by the pipeline
    There is a subset of files that are valid FITS but invalid PSRFITS,
    and these can be flagged to avoid spammy error messages.
    """

    def init(self):
        self._description = 'Corrupt file.'
        self._flag = 'corrupt_file'

class NoData(DataQualityFlag_NoProcess):

    def init(self):
        self._description = 'Observation has no valid data.'
        self._flag = 'no_data'

class DuplicatedData(DataQualityFlag_NoProcess):
    """ Occasionally, files are written out twice with subtle differences.

    Ignore the "bad" copies.
    """

    def init(self):
        self._description = 'Observation is a duplicate -- ignore.'
        self._flag = 'duplicate_data'

class NoCal(DataQualityFlag):

    def init(self):
        self._description = 'Observation has no acceptable calibrator.'
        self._flag = 'no_cal'
        self._toa_flag = '-nocal 1'

class BadCal(DataQualityFlag_NoProcess):
    """ This class will flag bad calibrators.

    They will not be processed, but the raw data will persist.  Thus,
    if there are other acceptable cals (e.g. in P574) those will be
    applied.  Otherwise, the processed observations will receive NoCal
    flags.
    """

    def init(self):
        self._description = 'Observation has corrupted calibrator.'
        self._flag = 'bad_cal'
        # this shouldn't be applied since only POLNCAL obs flagged
        self._toa_flag = '-badcal 1'

class NoDelay(DataQualityFlag):

    def init(self):
        self._description = 'Could not determine a delay correction.'
        self._flag = 'no_dly'
        self._toa_flag = '-no_rnm_delay 1'

class BlackListed(DataQualityFlag_NoProcess):

    def init(self):
        self._description = 'Observation on blacklist.'
        self._flag = 'blacklisted'

class RFI(DataQualityFlag):

    def init(self):
        self._description = 'Observation affected by RFI.'
        self._flag = 'rfi'

class PoorProfile(DataQualityFlag):
    """ For various reasons (RFI, poor cal, poor correlator state) do not
    use this profile for careful work -- polarimetry, coadded profiles,
    etc."""

    def init(self):
        self._description = 'Observation has poor (unrepresentative) profile.'
        self._flag = 'poor_profile'
        self._toa_flag = '-poor_profile 1'

class VaryingProfile(DataQualityFlag):
    """ For astrophysical reasons, do not use this profile for timing due
    to intrinsic pulse profile variation.
    """

    def init(self):
        self._description = 'Pulsar has unusual pulse profile during observation.'
        self._flag = 'varying_profile'
        self._toa_flag = '-varying_profile 1'

class BadFlux(DataQualityFlag):
    """ Do not use this observation for absolute flux density measurements.
    """

    def init(self):
        self._description = 'Observation has unreliable flux calibration.'
        self._flag = 'bad_flux'
        self._toa_flag = '-bad_flux 1'

class LowSN(DataQualityFlag):
    """ Explicitly flagged non-detections.

        These are for rare cases where a flat cut (S/N > 10 e.g.) doesn't
        cull the TOA but there's no evidence of a pulse profile.
        """

    def init(self):
        self._description = 'Observation has low S/N (no detection).'
        self._flag = 'lowsn'
        self._toa_flag = '-lowsn 1'

class CatastrophicRFI(DataQualityFlag_NoProcess):
    
    def init(self):
        self._description = 'Entire observation irrecoverably, deleteriously, and horrifically affected by RFI.'
        self._flag = 'catastrophic_rfi'
        # generally, these observations shouldn't be processed, but if
        # flagged after initial processing, may persist for a while
        # therefore, have a TOA flag too to keep the TOAs out!
        self._toa_flag = '-catastrophic_rfi 1'

class BadConfig(DataQualityFlag_NoProcess):
    
    def init(self):
        self._description = 'Observation has irrecoverably bad setup (e.g. incorrect LO setup).'
        self._flag = 'bad_config'
        self._toa_flag = '-bad_config 1'

class CorrelatorProblem(DataQualityFlag_NoProcess):
    """ Specific to observations with obvious profile / folding problems.
    """
    
    def init(self):
        self._description = 'Observation has irrecoverably bad setup (e.g. incorrect LO setup).'
        self._flag = 'corr_prob'
        self._toa_flag = '-corr_prob 1'

class UnusualConfig(DataQualityFlag):
    
    def init(self):
        self._description = 'Observation has unusual setup, e.g. a nonstandard correlator mode.'
        self._flag = 'unusual_config'
        self._toa_flag = '-unusual_config 1'

class NoTiming(DataQualityFlag):

    def init(self):
        self._description = 'Profile OK but timing unreliable.'
        self._flag = 'notiming'
        self._toa_flag = '-notiming 1'

class PKS_CLK_Prob(NoTiming):

    def init(self):
        self._description = 'Profile OK but PKS CLK unreliable.'
        self._flag = 'pks_clk_prob'
        self._toa_flag = '-pks_clk_prob 1'

class SolarWind(NoTiming):
    """ Observations performed at small sun angle with large solar delay.
    """

    def init(self):
        self._description = 'Observation taken at small sun angle.'
        self._flag = 'solar_wind'
        self._toa_flag = '-solar_wind 1'

class WBC_PhaseLock(DataQualityFlag):

    def init(self):
        self._description = 'Obs. plagued by WBC phase lock loss.'
        self._flag = 'wbc_phase_lock'
        self._toa_flag = '-wbc_phase_lock 1'

class DFB1_PhaseLock(DataQualityFlag):

    def init(self):
        self._description = 'Obs. plagued by PDFB1 phase lock loss -- see PPTA timeline.'
        self._flag = 'pdfb1_phase_lock'
        self._toa_flag = '-pdfb1_phase_lock 1'

class DFB3_PhaseLock(DataQualityFlag):

    def init(self):
        self._description = 'Obs. plagued by PDFB3 5MHz phase lock loss -- see PPTA timeline.'
        self._flag = 'pdfb3_phase_lock'
        self._toa_flag = '-pdfb3_phase_lock 1'

class DFB4_PhaseLock(DataQualityFlag):

    def init(self):
        self._description = 'Obs. plagued by PDFB4 5MHz phase lock loss -- see PPTA timeline.'
        self._flag = 'pdfb4_phase_lock'
        self._toa_flag = '-pdfb4_phase_lock 1'

class DFB4_ResetBug(DataQualityFlag):

    def init(self):
        self._description = 'Obs. plagued by PDFB4 "reset bug" -- see timeline.'
        self._flag = 'pdfb4_reset_bug'
        self._toa_flag = '-pdfb4_reset_bug 1'

class WBC_Commissioning(DataQualityFlag):
    """ Early WBC data have a variety of phase offsets.  There is no
    official date for its end, but I have adopted 2004 Jan 1 (MJD 53005) as
    a notional boundary.
    """

    def init(self):
        self._description = 'Early WBC observations unreliable for timing.'
        self._flag = 'wbc_commissioning'
        self._toa_flag = '-wbc_commissioning 1'

class WBC_EndOfLife(DataQualityFlag):
    """ Late WBC data have a digital artefacts.  These appear to set in
    largely after the observations on Feb 1, 2006.  They primarily affect
    the 10cm data, while H-OH (20cm) data appear OK.
    """

    def init(self):
        self._description = 'Early WBC observations unreliable for timing.'
        self._flag = 'wbc_endoflife'
        self._toa_flag = '-wbc_endoflife 1'

class DFB2_Commissioning(DataQualityFlag):
    """ Early DFB2 data have a variety of phase offsets during first months.

    Flag all data before June 19, 2007 (MJD 54270).
    """

    def init(self):
        self._description = 'Early PDFB2 observations unreliable for timing.'
        self._flag = 'pdfb2_commissioning'
        self._toa_flag = '-pdfb2_commissioning 1'

class DFB3_Commissioning(DataQualityFlag):
    """ Early DFB3 data have a variety of phase offsets during first months.

    Flag all data before July 30, 2008 (MJD 54677).
    """

    def init(self):
        self._description = 'Early PDFB3 observations unreliable for timing.'
        self._flag = 'pdfb3_commissioning'
        self._toa_flag = '-pdfb3_commissioning 1'

class DFB4_Commissioning(DataQualityFlag):
    """ Early DFB4 data have a variety of phase offsets during first months.

    Flag all data before Sept 22, 2008 (MJD 54731).
    """

    def init(self):
        self._description = 'Early PDFB4 observations unreliable for timing.'
        self._flag = 'pdfb4_commissioning'
        self._toa_flag = '-pdfb4_commissioning 1'

class APSR_Commissioning(DataQualityFlag):
    """ Early APSR data have a variety of phase offsets during first months.

    Flag all data before Feb 2, 2009 (MJD 55076).
    """

    def init(self):
        self._description = 'Early APSR observations unreliable for timing.'
        self._flag = 'apsr_commissioning'
        self._toa_flag = '-apsr_commissioning 1'

class CASPSR_Commissioning(DataQualityFlag):
    """ Early CASPSR data have a variety of phase offsets during ealry obs.

    Flag all data before Nov 5, 2010 (MJD 55505).
    """

    def init(self):
        self._description = 'Early CASPSR observations unreliable for timing.'
        self._flag = 'caspsr_commissioning'
        self._toa_flag = '-caspsr_commissioning 1'

class DFB1_2007_Offset(DataQualityFlag):
    """ Flag the 20cm observations that are offset by a fixed amount (per
    given pulsar) between July and November, 2007.
    """

    def init(self):
        self._description = 'Late-2007 20CM DFB1 observations with phase offset.'
        self._flag = 'dfb1_2007_offset'
        self._toa_flag = '-dfb1_2007_offset 1'

flag_map = dict(
    no_data = NoData,
    dup_data = DuplicatedData,
    no_cal = NoCal,
    no_dly = NoDelay,
    blacklisted = BlackListed,
    pdfb2_commissioning = DFB2_Commissioning,
    pdfb3_commissioning = DFB3_Commissioning,
    pdfb4_commissioning = DFB4_Commissioning,
    caspsr_commissioning = CASPSR_Commissioning,
    apsr_commissioning = APSR_Commissioning,
    wbc_eol = WBC_EndOfLife,
    wbc_phase_lock = WBC_PhaseLock,
    pdfb1_phase_lock = DFB1_PhaseLock,
    pdfb3_phase_lock = DFB3_PhaseLock,
    pdfb4_phase_lock = DFB4_PhaseLock,
    pdfb4_reset_bug = DFB4_ResetBug,
    catastrophic_rfi = CatastrophicRFI,
    bad_config = BadConfig,
    unusual_config = UnusualConfig,
    notiming = NoTiming,
    dfb1_2007_offset = DFB1_2007_Offset,
    bad_calibrator = BadCal,
    corrupt_file = CorruptFile,
    pks_clk_prob = PKS_CLK_Prob,
    corr_prob = CorrelatorProblem,
    poln_rfi = PoorProfile,
    bad_flux = BadFlux,
    poor_prof = PoorProfile,
    lowsn = LowSN,
    prof_var = VaryingProfile,
    solar_wind = SolarWind,
)

def flag_factory(flag,comment=None):
    try:
        constructor = flag_map[flag]
        return constructor(comment=comment)
    except:
        return DataQualityFlag(comment=comment)

        
class DataFlags(object):
    """ Read in a set of files to be flagged and the appropriate flag.

    Apply the flags to observations in a database.

    There are two acceptable formats for files, one listing particular
    observations (e.g. those affected by RFI), the other a class of 
    observations that can be selected following the same criteria as the
    metadata corrections.  This file will be read by 
    parse_corrections/parse_file.

    (1) -- a list of observations
    # -- indicates a commented line, will be ignored
    @ -- indicates the flag to apply
            one per file, and should be first uncommented line
    Remainder: file names for flagged observations

    (2) -- a class of observations
    # -- indicates a commented line, will be ignored
    * -- indicates beginning of correction
            for now, one per file, should be first uncommented line
    Remainder of file specifies corrections.
    """

    def __init__(self,fname):
        lines = util.readlines(fname)
        self.flags = dict()
        self.corrs = None
        if lines[0][0] == '*':
            # interpret as a correction-style flag
            self.corrs = fname
            return
        if lines[0][0] != '@':
            print 'Warning!  File "%s" begins with no specific flag.  Applying default "blacklist" flag.  Affected files will not be processed.'%fname
            current_flag = 'blacklisted'
        else:
            current_flag = lines[0][1:]
        self.flags[current_flag] = deque()

        for line in lines[1:]:
            if line[0] == '@':
                current_flag = line[1:]
                if current_flag not in self.flags.keys():
                    self.flags[current_flag] = deque()
            else:
                self.flags[current_flag].append(line)
        for key in self.flags.keys():
            self.flags[key] = list(self.flags[key])

    def flag_database(self,db,verbosity=1):
        if self.corrs is not None:
            self.db.add_correction(self.corr)
            return
        flags = self.flags.keys()
        for flag in flags:
            flagged_obs = self.flags[flag]
            for obs in filter(lambda x: x.basename in flagged_obs,db.obs):
                obs.add_data_flag(flag)
                if verbosity > 0:
                    print '\033[91m',
                    print '%s flag applied to %s.'%(flag,obs.basename)
                    print '\033[0m',

# TODO -- really need to factor these lists

def apply_global_flags(db,verbosity=0): 
    """ Flag list to apply against all observations."""
    files = map(util.local_file,(
                'obslists/wbc_phase_lock.asc',
                'obslists/dfb1_phase_lock.asc',
                'obslists/dfb3_phase_lock.asc',
                'obslists/dfb4_phase_lock.asc',
                'obslists/dfb4_phase_lock_p574.asc',
                'obslists/catastrophic_rfi.asc',
                'obslists/poln_rfi.asc',
                'obslists/poor_prof.asc',
                'obslists/poor_prof_ppta.asc',
                'obslists/prof_var.asc',
                'obslists/cpsr2_profile_shifts.asc',
                'obslists/cpsr2_aliasing.asc',
                'obslists/lowsn.asc',
                'obslists/solar_wind.asc',
                'obslists/ppta_catastrophic_rfi.asc',
                'obslists/dfb2_artefact.asc',
                'obslists/bad_config.asc',
                'obslists/bad_config_p574.asc',
                'obslists/bad_fluxes_p574.asc',
                'obslists/bad_config_p789.asc',
                'obslists/bad_config_other.asc',
                'obslists/bad_config_vela.asc',
                'obslists/bad_config_hydra.asc',
                'obslists/unusual_config.asc',
                'obslists/j0437_dfb4_50cm_badconfig.asc',
                'obslists/dfb1_2007_offset.asc',
                'obslists/bad_calibrator.asc',
                'obslists/bad_calibrator_p574.asc',
                'obslists/bad_calibrator_p789.asc',
                'obslists/apsr_duplicated_data.asc',
                'obslists/cpsr2_freq_smearing.asc',
                'obslists/notiming.asc',
                'obslists/notiming_p574.asc',
                'obslists/corrupt_file.asc',
                'obslists/band_shift_40cm_test.asc',
                'obslists/dfb3_50cm.asc',
                'obslists/p737_offset_sept_2015.asc',
                ))
    for f in files:
        df = DataFlags(f)
        df.flag_database(db,verbosity=verbosity)

def apply_class_flags(db,verbosity=1):
    """ Apply lists based on correction-type files rather than observation
    lists.  E.g., commissioning data, configuration errors affecting data
    taken over timespans, etc.
    
    NB -- in principle this can be done with DataFlags, but keep them
    separate for now.  I need a better way to dispose of these things."""
    files = map(util.local_file,(
                'corrections/dfb4_reset_bug.asc',
                'corrections/wbc_commissioning.asc',
                'corrections/wbc_endoflife.asc',
                'corrections/dfb2_commissioning.asc',
                'corrections/dfb3_commissioning.asc',
                'corrections/dfb4_commissioning.asc',
                'corrections/apsr_commissioning.asc',
                'corrections/caspsr_commissioning.asc',
                'corrections/pks_clock_dec_2009.asc',
                ))
    for f in files:
        db.add_correction(f,verbosity=verbosity)

