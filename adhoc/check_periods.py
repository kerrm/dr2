"""
Check the folding periods with which data were acquired against the
ephemeris specified.

This has a couple of applications.  E.g., early data on precision-timing
MSPs may be taken with a less-than-adequate ephemeris.  These profiles
should not be used in precision work.

For young pulsars, TOAs following a glitch may likewise be biased.  It's
unavoidable that these should be used in timing, but they should be
disregarded for applications like studies of profile variation.
"""

import numpy as np
import psrchive

def check_folding_periods(fname,ephemeris):
    """ Compare the folding periods predicted by the provided ephemeris
    with those stored in the psrchive archive specified by fname.  Because
    predictors are updated when installing a new ephemeris, this archive
    should be the original, raw data if possible.

    Returns the implied phase smearing for each sub-integration.
    """
    a = psrchive.Archive_load(fname)
    epochs = [subint.get_epoch() for subint in a]
    orig_pred = a.get_predictor()
    folding_freqs = np.asarray([orig_pred.frequency(epoch) for epoch in epochs])
    a.set_ephemeris(ephemeris)
    new_pred = a.get_predictor()
    updated_freqs = np.asarray([new_pred.frequency(epoch) for epoch in epochs])
    freq_shifts = folding_freqs - updated_freqs
    phase_shifts = np.asarray([freq_shifts[i]*a[i].get_duration() for i in xrange(len(freq_shifts))])
    return phase_shifts
