#!/usr/bin/python
""" Check the digitiser statistics for poor levels.

Generally this will only work for DFB data which record the proper
metadata.
"""

import os
import numpy as np
import pyfits

def check_digitiser_statistics(fname):
    f = pyfits.open(fname)
    try:
        stats = f['dig_stat'].data.field('data')
        rms_aa = np.ravel(stats[...,0,-1])
        rms_bb = np.ravel(stats[...,1,-1])
        mean_aa = np.ravel(stats[...,0,-2])
        mean_bb = np.ravel(stats[...,1,-2])
    except Exception:
        return 0,0
    f.close()
    return np.mean(rms_aa),np.mean(rms_bb)

def s20(s):
    return s+' '*(20-len(s)) 

def s14(s):
    return s+' '*(14-len(s))

def check_files(fnames):
    # desire rms near 10, but don't sound the alarm for values between
    # 8 and 15
    UL = 15
    LL = 8
    green = '\033[92m'
    red = '\033[91m'
    black = '\033[0m'
    for fname in fnames:
        rms_aa,rms_bb = check_digitiser_statistics(fname)
        ok_aa = (rms_aa > LL) and (rms_aa < UL)
        ok_bb = (rms_bb > LL) and (rms_bb < UL)
        print '%s '%(s20(os.path.basename(fname))),
        print green if ok_aa else red,
        print s14('rms AA=%02.2f'%rms_aa),
        print green if ok_bb else red,
        print s14('rms BB=%02.2f'%rms_bb),
        print black

if __name__ == '__main__':
    import sys
    fnames = sys.argv[1:]
    check_files(fnames)
