""" "Fan out" out a TOA file back to less channelized data and produce a
set of channelized TOAs based on the S/N of the single-channel TOAs."""

# can get template from the TOA analysis objects, which should exist
# can use the processed database to find appropriate file and issue
# scrunch commands

import numpy as np
import cPickle
from collections import deque
import os

import psrchive
import make_dsets
import util
import profiles

FIXED_NCHAN = 32

def get_nchan(sn,nchan_max=32,adaptive=True):
    if not adaptive:
        return min(nchan_max,FIXED_NCHAN)
    nchan = 1
    if sn > 30:
        nchan = 2
    if sn > 50:
        nchan = 4
    if sn > 70:
        nchan = 8
    if sn > 100:
        nchan = 16
    if sn > 150:
        nchan = 32
    return min(nchan_max,nchan)

def fanout(timfile,procdb,nchan_max=8,remove_profiles=True,adaptive=True,
    calculate_new_sn=True,verbosity=0,require_valid=True):
    toa_strings = deque()
    toas = make_dsets.parse_toas(timfile) 
    success = 0
    for itoa,toa in enumerate(toas):
        # first, get the corresponding ProcObs
        basename = toa.basename()
        procobs = procdb.get_by_basename(basename)
        local_nchan_max = nchan_max
        if procobs.obs.get_combo() == '40CM_1050CM_CASPSR':
            local_nchan_max *= 2
        if verbosity > 0:
            print 'Working on %d/%d = %s'%(itoa+1,len(toas),basename)
        # now find the single TOA analysis object
        try:
            toa_fname = procobs.get_toa(require_valid=require_valid)+'.analysis'
        except TypeError as e:
            if verbosity > 0:
                print e
            continue 
        try:
            toa_an = cPickle.load(file(toa_fname))
        except IOError as e:
            if verbosity > 0:
                print e
            continue
        template = toa_an.template
        sn = toa_an.snr
        chi2 = toa_an.chi2
        data = procobs.obsscr.get_scrunched_3d(procobs.obs)

        # compute number of channels
        nchan = get_nchan(sn,nchan_max=local_nchan_max,adaptive=adaptive)

        # make temp file in working directory with correct # of channels
        newext = 'dzTf%dp'%nchan
        cmd = 'pam -Tp --setnchn %d -u. -e %s %s'%(nchan,newext,data)
        rc,stdout,stderr = util.run(cmd,echo=verbosity > 1)
        tdata = os.path.splitext(os.path.basename(data))[0] + '.%s'%newext
        if not os.path.isfile(tdata):
            # try again, in case the file is already somewhat scrunched
            a = psrchive.Archive_load(data)
            nchan = min(nchan,a.get_nchan())
            cmd = 'pam -Tp --setnchn %d -u. -e %s %s'%(nchan,newext,data)
            rc,stdout,stderr = util.run(cmd,echo=verbosity > 1)
            if not os.path.isfile(tdata):
                print 'Failed to generate profile for %s.'%basename
                continue
        tdata = os.path.splitext(os.path.basename(data))[0] + '.%s'%newext
        cmd = 'pat -f tempo2 -s %s  %s'%(template,tdata)
        rc,stdout,stderr = util.run(cmd,echo=verbosity > 1)

        if calculate_new_sn:
            chi2,sn,freqs = multi_channel_sn_analysis(
                procobs.obs,template,tdata)
            sn[np.isnan(sn)] == 0
            chi2[np.isnan(sn)] == 0
        else:
            chi2 = [chi2*nchan]
            sn = [sn*nchan]

        if remove_profiles:
            cmd = 'rm %s'%tdata
        util.run(cmd,echo=verbosity > 1)

        if rc != 0:
            print 'Failed to generate TOA for %s.'%basename
            continue
        tmp = stdout.strip().split('\n')
        if (len(tmp) < 2):
            print 'TOA invalid format: ',tmp
            continue


        # append the flags from the original TOA
        flags = toa.get_flag_string()
        for x in tmp[1:]: # strip out FORMAT 1
            # replace S/N and gof
            toks = flags.split(' ')
            freq = float(x.split()[1])
            idx = np.argwhere(np.abs(freqs-freq)<0.01)[0][0]
            for itok,tok in enumerate(toks):
                if tok == '-snr':
                    toks[itok+1] = '%.2f'%(sn[idx])
                if tok == '-gof':
                    toks[itok+1] = '%.2f'%(chi2[idx])
            myflags = ' '.join(toks)
            s = '%s %s'%(x,myflags)
            toa_strings.append(s)

        success += 1

    print 'Finished with %d/%d successful.'%(success,len(toas))
    output = os.path.basename(timfile) + '.mchan'
    file(output,'w').write('FORMAT 1\n%s'%('\n'.join(toa_strings)))

def multi_channel_sn_analysis(obs,template,profile):
    """ An ad hoc method to get S/N for each channel."""

    # load up profile and template
    a_profl = psrchive.Archive_load(profile)
    a_profl.tscrunch()
    a_profl.pscrunch()
    nchan = a_profl.get_nchan()
    nbin = a_profl.get_nbin()
    profl_amps = np.empty([nchan,nbin])
    freqs = np.empty(nchan)
    for i in xrange(nchan):
        profl_amps[i] = a_profl[0].get_Profile(0,i).get_amps()
        freqs[i] = a_profl[0].get_Profile(0,i).get_centre_frequency()

    a_templ = psrchive.Archive_load(template)
    templ_amps = a_templ[0].get_Profile(0,0).get_amps()

    # run pat to get phase consistent with TOA analysis
    phase_shifts = np.zeros(nchan)
    phase_mask = np.asarray([False]*nchan)
    cmd = 'pat -s %s -R %s'%(template,profile)
    rc,stdout,stderr = util.run(cmd,echo=False)
    lines = stdout.split('\n')
    for line in lines:
        try:
            toks = line.strip().split()
            if len(toks) != 9:
                continue
            chan = int(toks[2])
            phase_shifts[chan] = float(toks[3])
            phase_mask[chan] = True
            #phase_shifts[chan] = float(
            #    lines[i].split('<-turns')[0].strip().split()[-2])
        except Exception as e:
            print 'Warning: Error estimating phase shifts.'
            print line
            pass

    weights = a_profl.get_weights()[0]
    weights *= a_profl[0].get_duration()/obs.nsub/a_profl.get_nbin()
    weights *=  abs(a_profl.get_bandwidth())/obs['nchan']*1e6
    weights *= 2
    frac_err = np.where(weights > 0,weights**-0.5,0)
    # compute noise level -- NB only approximate
    #nsamp = rfi.ppta_set_radiometer(obs)/nchan
    #if nsamp == 0.:
        # some problem -- ignore zapping
    #bw = abs(a_profl.get_bandwidth()*1e6)
    #tobs = a_profl[0].get_duration()/a_profl.get_nbin()
    #nsamp = 2*bw*tobs
    #frac_err = nsamp**-0.5
    frac_err = [None]*nchan

    chi2 = np.zeros(nchan)
    snr = np.zeros(nchan)

    for i in xrange(nchan):
        # rotate profile and template
        if phase_mask[i]:
            p = profl_amps[i]
            t = profiles.rotated(templ_amps,-phase_shifts[i])
            ahat,bhat,sp,st,snr[i],chi2[i],flux,fluxerr = profiles.fit_template(p,t,frac_err[i])

    return chi2,snr,freqs
