# Combine various TOA datasets following some reasonably 
# sensible prescription.

import numpy as np

import os
import glob
from collections import deque

INCLUDE_CPSR2 = True
PREFER_CPSR2_20CM = False
PREFER_WBCORR_20CM = False
USE_WBCORR_20CM = False
NO_CPSR2_50CM = False
NO_CPSR2_20CM = False
ONLY_1020 = False
ONLY_10 = False
USE_RAW = True
J0437_ALL_INVINT = True
J0437_NO_INVINT = False

GOF_CUT = 30
#GOF_CUT = np.inf

# TESTING_CONFIGURATIONS
PREFER_CASPSR_40CM = False
PREFER_CASPSR_20CM = False
USE_WBCORR = True

RPATH = '/DATA/NEWTON_1/pulsar/ker14a/ppta/test'
LPATH = '/home/ker14a/research/analyses/dr2paper'

dsets = ['datasets/dfb1','datasets/dfb2','datasets/dfb3','datasets/dfb4']
if INCLUDE_CPSR2:
    dsets.append('datasets/cpsr2')
    combined = 'datasets/combined'
else:
    combined = 'datasets/all_dfb'

if NO_CPSR2_50CM:
    combined = 'datasets/combined_nocpsr2_50cm'

if USE_RAW:
    dsets = ['datasets/raw']
    combined = 'datasets/combined_raw'

dsets = [os.path.join(LPATH,dset) for dset in dsets]
combined = os.path.join(LPATH,combined)

try:
    if not os.path.isdir(combined):
        os.mkdir(combined)
except OSError:
    pass

def update_raw(jnames = None):
    """ Copy over raw TOA files from newton and process them into
    directories.
    """

    def do_apply_flags(jname,par,tim,sel):
        """ Run tempo2 in order to apply the SELECT file to the data."""
        cmd = 'tempo2 -f %s %s -select %s -nofit -writetim'%(par,tim,sel)
        code,stdout,stderr = util.run(cmd,echo=False)
        os.system('mv out.tim %s.tim'%jname)

    cwd = os.getcwd()
    outpath = os.path.join(os.path.abspath(cwd),dsets[0])
    try:
        os.mkdir('/tmp/staging')
    except Exception:
        pass
    os.chdir('/tmp/staging')
    cmd = 'rsync -avz venice:%s/J*/J*.toa .'%RPATH; os.system(cmd)
    cmd = 'rsync -avz venice:%s/J*/J*.par .'%RPATH; os.system(cmd)
    cmd = 'rsync -avz venice:%s/J*/J*.select .'%RPATH; os.system(cmd)
    # TODO -- remove invint, pcm files...
    if jnames is None:
        jnames = [x.split('.par')[0] for x in glob.glob('J*.par')]
    for jname in jnames:
        par = '%s.par'%jname
        toa = '%s.toa'%jname
        sel = '%s.select'%jname
        cmd = 'tempo2 -f %s %s -select %s -nofit -writetim'%(par,toa,sel)
        #code,stdout,stderr = util.run(cmd,echo=False)
        os.system(cmd)
        os.system('mv out.tim %s/%s'%(outpath,toa))
        # append invariant interval TOAs for J0437-4715
        if jname == 'J0437-4715':
            toa = '%s.invint.toa'%jname
            lines = file(sel).readlines()
            for iline,line in enumerate(lines):
                if 'snr' in line:
                    lines[iline] = '#' + line
            file(sel+'.tmp','w').write(''.join(lines))
            cmd = 'tempo2 -f %s %s -select %s -nofit -writetim'%(par,toa,sel+'.tmp')
            os.system(cmd)
            toa = '%s.toa'%jname
            os.system('cat out.tim >> %s/%s'%(outpath,toa))
    os.chdir(cwd)

def update_mwl(snr_min=12):
    """ Copy over MWL TOA files from newton and apply a S/N cut."""

    cwd = os.getcwd()
    outpath = os.path.join(os.path.abspath(cwd),dsets[0])
    try:
        os.mkdir('/tmp/staging')
    except Exception:
        pass
    os.chdir('/tmp/staging')
    cmd = 'rsync -avz venice:%s/mwl/J*.mchan .'%RPATH; os.system(cmd)
    # TODO -- remove invint, pcm files...
    toas = sorted(glob.glob('J*.mchan'))
    jnames = [x.split('_')[0] for x in toas]
    outpath = os.path.join(combined,'..','combined_mwl')
    for toa,jname in zip(toas,jnames):
        toas = parse_toas(toa)
        toas = filter(lambda t: t.snr() > snr_min,toas)
        output_fname = os.path.join(outpath,'%s_mchan.toa'%(jname))
        write_toas(toas,output_fname)
    os.chdir(cwd)

def get_jnames():
    jnames = set()
    for dset in dsets:
        for j in glob.glob(os.path.join(dset,'J*')):
            t = os.path.split(j)[-1]
            if '_' in t:
                jnames.add(t.split('_')[0])
            else:
                jnames.add(t.split('.')[0])
    return sorted(jnames)

def get_toas(jname):
    toas = []
    for dset in dsets:
        try:
            toas.append(glob.glob(os.path.join(dset,'%s*'%jname))[0])
        except IndexError:
            continue
    return toas

def id_toa_line(line,get_comments=False):
    """ Determine if the line from a .tim file corresponds to an 
        (uncommented) TOA.
        
        Primarily, check if the third column is consistent with an MJD.
    """
    try:
        is_comment = (line[0]=='#' or line[0]=='C')
    except IndexError:
        return False
    if is_comment and (not get_comments):
        return False
    if 'FORMAT' in line:
        return False
    if 'MODE' in line:
        return False
    toks = line.split()
    if is_comment:
        toks = toks[1:]
    if len(toks) < 3:
        return False
    mjd = toks[2].split('.')
    if len(mjd)==2 and len(mjd[0])==5:
        return True
    return False

class TOA(object):

    def __init__(self,line):
        self.line = line
        toks = line.strip().split()
        if (toks[0] == 'C') or (toks[0] == '#'):
            self.is_comment = True
            self.line = self.line.lstrip(toks[0]).lstrip()
            toks = toks[1:]
        else:
            self.is_comment = False
        self.profile = toks[0]
        self.freq = float(toks[1])
        self.mjd = float(toks[2])
        self.err = float(toks[3])
        self.flags = dict()
        for i in xrange((len(toks)-5)/2):
            self.flags[toks[5+i*2]] = toks[5+i*2+1]
        # NB kluge -- add a TOA flag for this!
        self.is_invint = self.profile.endswith('.dzTFi')
        # NB kluge -- handle no SK pipeline
        self.nosk = self.profile.startswith('p_sk')

    def __str__(self):
        if self.is_comment:
            return 'C '+self.line
        return self.line

    def basename(self):
        return self.profile.split('.rf')[0] + '.rf'

    def get_flag_string(self):
        return ' '.join(self.line.strip().split()[5:])

    def overlap(self,other,get_val=False):
        """ Determine if and to what extent observations overlap.
        
        get_val = True -- > return overlap normalized to longest obs.
        Otherwise, simply return True or False

        NB, if one of the observations is flagged (commented), we return
        False by default.  This helps with assembling datasets and having
        flagged TOAs go along for the ride.
        """
        if self.is_comment or other.is_comment:
            return False
        if self.flags['-B'] != other.flags['-B']:
            return False
        dt = abs(self.mjd - other.mjd)
        # skip careful calculation if obviously not overlapping
        if dt > 1:
            return False
        # NB tobs in seconds
        t00 = self.mjd
        t01 = t00 + float(self.flags['-tobs'])/86400
        t10 = other.mjd
        t11 = t10 + float(other.flags['-tobs'])/86400
        t0 = max(t00,t10)
        t1 = min(t01,t11)
        #tobs = max(float(self.flags['-tobs']),float(other.flags['-tobs']))
        #return (dt*86400) < tobs
        if not get_val:
            return t0 < t1
        else:
            dt = max(0,t1-t0)
            longest = max(t01-t00,t11-t10)
            return dt/longest

    def add_overlap_flag(self):
        """ Add True if overlapping another TOA (and less desirable)."""
        self.add_flag('-overlap','1')

    def get_overlap_flag(self):
        try:
            return bool(int(self.flags['-overlap']))
        except KeyError:
            return False
        return False

    def simultaneous(self,other,max_offset=5):
        """ Determine if two observations start and stop within max_offset
        seconds of each other.  (In practice, they will actually be
        simultaneous, but pat can sometimes move the TOAs by a couple of
        seconds...)
        """

        if self.flags['-B'] != other.flags['-B']:
            return False
        dt = abs(self.mjd - other.mjd)
        # skip careful calculation if obviously not overlapping
        if dt > 1:
            return False
        # NB tobs in seconds
        dstart = 86400*abs(self.mjd-other.mjd)
        dtobs = abs(self.tobs()-other.tobs())
        return (dstart < max_offset) and (dtobs < max_offset)

    def add_flag(self,flag,val,clobber=False):
        if flag in self.flags.keys():
            if not clobber:
                return
            oldval = self.flags[flag]
            oldstr = '%s %s'%(flag,oldval)
            idx = self.line.find(oldstr)
            if idx < 0:
                raise ValueError('Could not replace old value.')
            self.line = self.line.replace(oldstr,'%s %s'%(flag,val))
        else:
            self.line = '%s %s %s\n'%(self.line.rstrip(),flag,val)
        self.flags[flag] = val

    def tobs(self):
        return float(self.flags['-tobs'])

    def snr(self):
        try:
            return float(self.flags['-snr'])
        except:
            return -1

    def band(self):
        return self.flags['-B']

    def backend(self):
        return self.flags['-be']

    def frontend(self):
        return self.flags['-fe']

    def combo(self):
        return self.flags['-h']

    def group(self):
        return self.flags['-g']

    def invint(self):
        return self.is_invint

    def projid(self):
        return self.flags['-projid']

    def gof(self):
        return float(self.flags['-gof'])

    def add_white_noise_flag(self,ignore_unknown=False):
        """ Strategy -- link up CPSR2m/n, PDFB3/4."""
        if '-q' in self.flags.keys():
            return
        be = self.backend()
        band = self.band()
        if 'DFB' in be:
            val = 'PDFB_%s'%(band)
            if be == 'PDFB1':
                if (self.mjd < 53670) and (band=='20CM'):
                    val = 'PDFB1_1433'
                elif self.mjd < 53881:
                    val = 'PDFB1_early_%s'%(band)
                else:
                    val = 'PDFB1_%s'%(band)
            else:
                val = 'PDFB_%s'%(band)
        elif 'CPSR2' in be:
            val = 'CPSR2_%s'%(band)
        elif 'WBCORR' in be:
            val = 'WBCORR_%s'%(band)
        elif 'CASPSR' in be:
            val = 'CASPSR_%s'%(band)
        elif 'FPTM' in be:
            val = 'FPTM_%s'%(band)
        else:
            if not ignore_unknown:
                raise ValueError('Did not recognize backend==%s.'%be)
            else:
                val = self.flags['-g']
        self.add_flag('-q',val,clobber=False)

        # merge PDFB3/PDFB4 noise parameters, CPSR2m/CPSRn
        if 'DFB3' in be:
            if band == '20CM':
                val = '20CM_PDFB4'
            else:
                val = '%s_PDFB3'%(band)
        elif 'CPSR2' in be:
            val = '%s_CPSR2'%(band)
        else:
            val = self.flags['-g']
        self.add_flag('-v',val,clobber=False)

    def add_55319_flag(self,change_group=False):
        """ Need to basically add a flag for every configuration that was
        present, but generally this is only a single configuration for a 
        given pulsar, so it suffices to have a band jump for DFB3 and DFB4.

        TODO -- for pulsars like 0437, MAKE SURE every configuration is
        JUMPed.

        Ultimately, a better strategy is to measure the backend delays
        globally.
        """
        if self.mjd < 55319:
            return
        try:
            val = self.flags['-g']
            for band in ['10','20','40']:
                if val == '%sCM_PDFB4'%band:
                    self.add_flag('-j','%sCM_55319_PDFB4'%band)
                    if change_group:
                        old = '-g %sCM_PDFB4'%band
                        new = '-g %sCM_PDFB4a'%band
                        self.line = self.line.replace(old,new)
                elif val == '%sCM_PDFB3'%band:
                    self.add_flag('-j','%sCM_55319_PDFB3'%band)
                    if change_group:
                        old = '-g %sCM_PDFB3'%band
                        new = '-g %sCM_PDFB3a'%band
                        self.line = self.line.replace(old,new)
                    break
        except KeyError:
            return

    def add_dfb1_flag(self):
        """
        """
        #if self.mjd < 53843:
        #if self.mjd < 53740:
        if self.backend() != 'PDFB1':
            return
        # flag for 1433 MHz region
        if (self.band() == '20CM') and (self.mjd < 53670):
            self.add_flag('-j','20CM_PDFB1_1433_JUMP')
            return
        if self.mjd < 53881.5:
            return
        if self.band() == '10CM':
            self.add_flag('-j','10CM_PDFB1_JUMP')
            return
        if self.band() == '20CM':
            self.add_flag('-j','20CM_PDFB1_JUMP')

    def add_cpsr2_flag(self):
        """ Add flag for the span of MB data between the two H-OH sessions.
        """
        if (self.mjd < 53900) and  ('CPSR2' in self.backend()) and (self.frontend()=='MULTI'):
            if self.backend() == 'CPSR2m':
                self.add_flag('-j', 'CPSR2m_JUMP')
            else:
                self.add_flag('-j', 'CPSR2n_JUMP')

    def add_orig_flag(self):
        if 'orig' in self.profile:
            self.add_flag('-orig', '1')

    def add_wbc_delay(self):
        """ Adjust delay for beconfig wbb256_256_128_3p_c and 
        wbb256_256_256_3p_c, which notionally starts at March 1, 2005, 
        to begin from first occurrence."""
        if self.backend() != 'WBCORR':
            return
        be = self.flags['-beconfig']
        if (be == 'wbb256_256_256_3p_c') or (be == 'wbb256_256_128_3p_c'):
            self.add_flag('-wbb_c_config','1',clobber=False)
            



def parse_toas(timfile,get_comments=False):
    func = lambda x: id_toa_line(x,get_comments=get_comments)
    return map(TOA,filter(func,file(timfile).readlines()))

def flag_white_noise_toas(fname):
    """ Stand alone flagger."""
    toas = parse_toas(fname)
    map(TOA.add_white_noise_flag,toas)
    # sort TOAs by time
    toas.sort(key=lambda x: x.mjd)
    output = ''.join((x.line for x in toas))
    file(fname,'w').write('%s\n%s'%('FORMAT 1',output))

def flag_55319(fname):
    """ Stand alone flagger."""
    toas = parse_toas(fname)
    map(TOA.add_55319_flag,toas)
    # sort TOAs by time
    toas.sort(key=lambda x: x.mjd)
    output = ''.join((x.line for x in toas))
    file(fname,'w').write('%s\n%s'%('FORMAT 1',output))

def min_flags(toas,flag='-g',mincount=0):
    """ Print the value of a flag for each TOA, in order of descending
    frequency.  If mincount specified, remove those TOAs with a flag
    occurring fewer than mincount times.
    """

    def s20(s):
        return s + ' '*(20-len(s))

    if not flag.startswith('-'):
        flag = '-' + flag
    flags = [x.flags[flag] for x in toas]
    unique = list(set(flags))
    counts = [flags.count(x) for x in unique]
    indices = sorted(range(len(counts)),key=lambda x: counts[x])[::-1]
    for i in indices:
        print s20(unique[i]), counts[i]
    if mincount > 0:
        valid_flags = set(
            [unique[i] for i in indices if counts[i] >= mincount])
        toas = [x for x in toas if x.flags[flag] in valid_flags]
    return toas

def write_toas(toas,fname):
    output = ''.join((str(x) for x in toas))
    print 'Writing out to %s.'%fname
    file(fname,'w').write('%s\n%s'%('FORMAT 1',output))
    

def combine_toas(jname,remove_overlaps=True,mjd_max=None,tobs_min=0,
        snr_min=12,output_path=None):
    timfiles = get_toas(jname)
    toas = deque()
    for tf in timfiles:
        toas.extend(parse_toas(tf,get_comments=True))

    # filter out orig TOAs
    toas = filter(lambda x: 'orig' not in x.profile, toas)

    # remove any TOAs prior to Feb 2004, MJD 53040 precisely
    toas = filter(lambda x: x.mjd > 53040, toas)

    if mjd_max is not None:
        toas = filter(lambda x: x.mjd < mjd_max, toas)

    # remove any TOAs with band set to 3CM, UNKNOWN, etc. *sigh*
    valid_bands = set(('10CM','20CM','40CM','50CM'))
    toas = filter(lambda x: x.band() in valid_bands, toas)

    # remove any TOAs from observations that are too short
    toas = filter(lambda x: x.tobs() >= tobs_min, toas)

    # remove any low S/N TOAs
    toas = filter(lambda x: (x.snr() < 0) or (x.snr() >= snr_min), toas)

    # filter out invariant interval TOAs for non-multibeam
    if jname == 'J0437-4715':
        tmp = deque()
        for toa in toas:
            if J0437_NO_INVINT:
                if not toa.invint():
                    tmp.append(toa)
                continue
            if J0437_ALL_INVINT or (toa.frontend() == 'MULTI'):
                if not toa.invint():
                    continue
            else:
                if toa.invint():
                    continue
            tmp.append(toa)
        toas = tmp

        # filter out P737 TOAs
        tmp = deque()
        for toa in toas:
            if toa.projid() == 'P737':
                    continue
            tmp.append(toa)
        toas = tmp

    selected_toas = deque()
    remaining_toas = deque()

    if PREFER_CPSR2_20CM:
        backends = ['CPSR2m','CPSR2n','CPSR2']
        for toa in toas:
            if (toa.backend() in backends) and (toa.band() == '20CM'):
                selected_toas.append(toa)
            else:
                remaining_toas.append(toa)
        toas = remaining_toas
        remaining_toas = deque()

    if PREFER_WBCORR_20CM:
        for toa in toas:
            if (toa.backend() == 'WBCORR') and (toa.band() == '20CM'):
                selected_toas.append(toa)
            else:
                remaining_toas.append(toa)
        toas = remaining_toas
        remaining_toas = deque()

    if PREFER_CASPSR_20CM:
        for toa in toas:
            if (toa.backend() == 'CASPSR') and (toa.band() == '20CM'):
                if toa.nosk:
                    continue
                selected_toas.append(toa)
            else:
                remaining_toas.append(toa)
        toas = remaining_toas
        remaining_toas = deque()

    if PREFER_CASPSR_40CM:
        for toa in toas:
            if (toa.backend() == 'CASPSR') and (toa.band() == '40CM'):
                if toa.nosk:
                    continue
                selected_toas.append(toa)
            else:
                remaining_toas.append(toa)
        toas = remaining_toas
        remaining_toas = deque()

    """
    selected_toas = deque()
    for toa in toas:
        if toa.flags['-be'] == 'PDFB4':
            selected_toas.append(toa)
        else:
            remaining_toas.append(toa)
    toas = remaining_toas
    remaining_toas = deque()
    """

    # select DFB4 data -- notionally dates > 54752, in practice accept all
    # select DFB3 data in chinks of DFB4 data and overlap, 54690-->54752
    # select DFB2 data -- notionally, dates between 54276 and 54642,
    # select DFB1 data -- notionally, dates before 54276
    for backend in ['PDFB4','PDFB3','PDFB2','PDFB1']:
        for toa in toas:
            if toa.backend() == backend:
                overlap = False
                for t in selected_toas:
                    if toa.overlap(t):
                        overlap = True
                        break
                if overlap:
                    toa.add_overlap_flag()
                if not (overlap and remove_overlaps):
                    selected_toas.append(toa)
            else:
                remaining_toas.append(toa)
        toas = remaining_toas
        remaining_toas = deque()

    # remove any DFB3 data after failure
    selected_toas = filter(lambda x: not (
        x.backend()=='PDFB3' and x.mjd > 56780),selected_toas)

    # remove some early 20cm DFB3 data
    selected_toas = filter(lambda x: not (
        x.backend()=='PDFB3' and x.mjd < 54749 and x.band() == '20CM'),
        selected_toas)

    # add in CASPSR 40cm TOAs
    if not PREFER_CASPSR_40CM:
        for toa in toas:
            if (toa.backend() == 'CASPSR') and (toa.band() == '40CM'):
                if toa.nosk:
                    continue
                overlap = False
                for t in selected_toas:
                    if toa.overlap(t):
                        overlap = True
                        break
                if overlap:
                    toa.add_overlap_flag()
                if not (overlap and remove_overlaps):
                    selected_toas.append(toa)
            else:
                remaining_toas.append(toa)
        toas = remaining_toas
        remaining_toas = deque()

    # filter out DFB4 H-OH 20cm TOAs (early) and 40cm TOAs
    # filter out DFB3 10cm and 50cm TOAs, and H-OH 20cm TOAs
    # filter out DFB2 H-OH 20CM TOAs
    # filter out DFB1 50cm TOAs
    tmp = deque()
    for toa in selected_toas:
        if (toa.band() == '20CM') and (toa.frontend() != 'MULTI'):
            # allow new H-OH TOAs
            if (toa.backend() == 'PDFB4'):
                if (toa.mjd < 57417):
                    continue
            elif (toa.backend() != 'PDFB1'):
                continue
        elif toa.backend() == 'PDFB3':
            if toa.band() == '50CM' or toa.band() == '10CM':
                continue
        elif toa.backend() == 'PDFB4' and toa.band() == '40CM':
            continue
        elif toa.backend() == 'PDFB1' and toa.band() == '50CM':
            continue
        tmp.append(toa)
    selected_toas = tmp

    # finally, add WBC TOAs
    # NB -- have swapped with CPSR2 for now, since we aren't using 20cm WBC
    # and the 10CM CPSR2 TOAs shouldn't have precedence for 0437
    for toa in toas:
        # don't include 20cm WBCORR -- there aren't enough to be worth it
        #if (toa.backend() == 'WBCORR') and (toa.band() == '10CM'):
        if (toa.backend() == 'WBCORR'):
            if not USE_WBCORR:
                continue
            if (not USE_WBCORR_20CM) and (toa.band() == '20CM'):
                continue
            # TEMPORARY -- these will be flagged ultimately, for now
            # get rid of them here
            if (toa.band() == '10CM') and (toa.mjd > 53767):
                continue
            overlap = False
            for t in selected_toas:
                if toa.overlap(t):
                    overlap = True
                    break
            if overlap:
                toa.add_overlap_flag()
            if not (overlap and remove_overlaps):
                selected_toas.append(toa)
        else:
            remaining_toas.append(toa)


    # now do CPSR2 -- handle specially since there are two observing bands
    # in one "20CM" band
    cpsr2_toas = deque()
    backends = ['CPSR2m','CPSR2n','CPSR2']
    for toa in toas:
        if toa.flags['-be'] in backends:
            overlap = False
            for t in selected_toas:
                if toa.overlap(t):
                    overlap = True
                    break
            if overlap:
                toa.add_overlap_flag()
            if not (overlap and remove_overlaps):
                cpsr2_toas.append(toa)
        else:
            remaining_toas.append(toa)
    toas = remaining_toas
    remaining_toas = deque()

    # always filter out 40cm
    cpsr2_toas = filter(lambda x: x.band() != '40CM',cpsr2_toas)
    # if not J0437-4715, filter out 10CM
    if jname != 'J0437-4715':
        cpsr2_toas = filter(lambda x: x.band() != '10CM',cpsr2_toas)
    if NO_CPSR2_50CM:
        cpsr2_toas = filter(lambda x: x.band() != '50CM',cpsr2_toas)
    if NO_CPSR2_20CM:
        cpsr2_toas = filter(lambda x: x.band() != '20CM',cpsr2_toas)
    # TODO -- filter CPSR2 TOAs after certain date?
    selected_toas.extend(cpsr2_toas)

    # if required, filter out 40 and 50 cm TOAs
    if ONLY_1020:
        selected_toas = filter(
            lambda x: x.band() == '20CM' or x.band() == '10CM',
            selected_toas)

    # if required, filter out all but 10cm
    if ONLY_10:
        selected_toas = filter(lambda x: x.band() == '10CM',selected_toas)

    # apply goodness-of-fit cut
    # hopefully TEMPORARY
    if not (jname == 'J0437-4715'):
        selected_toas = [x for x in selected_toas if x.gof() < GOF_CUT]
            
    # sort TOAs by time
    final_toas = list(selected_toas)
    final_toas.sort(key=lambda x: x.mjd)
    
    # add in additional flags
    for toa in final_toas:
        toa.add_white_noise_flag()
        toa.add_55319_flag()
        toa.add_dfb1_flag()
        toa.add_orig_flag()
        toa.add_cpsr2_flag()
        #toa.add_wbc_delay()

    label = '%s_comb'%jname
    if not USE_WBCORR:
        label += '_nowbcorr'
    if PREFER_CASPSR_20CM:
        label += '_caspsr20cm'
    if PREFER_CASPSR_40CM:
        label += '_caspsr40cm'
    if ONLY_10:
        label += '_10'
    output_fname = os.path.join(output_path or combined,'%s.toa'%label)
    write_toas(final_toas,output_fname)

    return final_toas

def make_overlap(jname,h1='20CM_MULTI_PDFB3',h2='20CM_MULTI_PDFB4',
    mjd0=None,mjd1=None,output=None,select_invint=False,select_band=None,
    override_tims=None,min_overlap=None,use_invint_as_combo=False,
    require_simultaneous=False):
    """ Make an output .tim file containing only overlapping observations
    for the specified combos.  Optionally cut to a date range.
    """
    if override_tims is None:
        timfiles = get_toas(jname)
    else:
        timfiles = override_tims
        if not type(timfiles)==type([]):
            timfiles = [timfiles]

    toas = deque()
    for tf in timfiles:
        toas.extend(parse_toas(tf))

    if (mjd0 is not None):
        toas = filter(lambda x: x.mjd > mjd0, toas)

    if (mjd1 is not None):
        toas = filter(lambda x: x.mjd < mjd1, toas)

    if select_invint:
        toas = filter(lambda x: x.invint(), toas)

    if select_band is not None:
        toas = filter(lambda x: x.band()==select_band, toas)

    # now, limit to only the two combos
    if not use_invint_as_combo:
        h1_toas = filter(lambda x: x.combo() == h1,toas)
        h2_toas = filter(lambda x: x.combo() == h2,toas)
    else:
        h1_toas = filter(lambda x: x.invint(),toas)
        h2_toas = filter(lambda x: not x.invint(),toas)

    toas = deque()
    counter = 0
    for h1toa in h1_toas:
        for h2toa in h2_toas:
            ol = h1toa.overlap(h2toa,get_val=True)
            if (ol > 0):
            #if h1toa.overlap(h2toa):
                if use_invint_as_combo and h1toa.combo() != h2toa.combo():
                    # skip those TOAs that are simultaneous but with
                    # different backends
                    continue
                if require_simultaneous:
                    if h1toa.simultaneous(h2toa):
                        print '%s %s simultaneous'%(
                            h1toa.profile,h2toa.profile)
                        counter += 1
                        toas.append(h1toa)
                        toas.append(h2toa)
                        break
                    else:
                        continue
                if (min_overlap is None) or (ol > min_overlap):
                    toas.append(h1toa)
                    toas.append(h2toa)
                    break
    print '%d simultaneous TOAs.'%(counter)

    toas = list(toas)
    toas.sort(key=lambda x: x.mjd)
    if output is not None:
        outstring = ''.join((x.line for x in toas))
        print 'Writing out to %s.'%output
        file(output,'w').write('%s\n%s'%('FORMAT 1',outstring))

    return toas

def do_all(remove_overlaps=True):
    for jname in get_jnames():
        combine_toas(jname,remove_overlaps=remove_overlaps)

def do_dr2(no_max=False,remove_overlaps=True,shannon15_cuts=False,
    outpath=None):
    """ Use DR2 default settings."""
    mjd_max = 100000 if no_max else 57419
    if shannon15_cuts:
        # NB uses DE421 ephemeris
        # Dec 1, 2014
        mjd_max = 56992
    for jname in get_jnames():
        my_mjd_max = mjd_max
        #if jname == 'J1643-1224':
        #    my_mjd_max = 57087
        combine_toas(jname,remove_overlaps=remove_overlaps,
            mjd_max=my_mjd_max,tobs_min=490,snr_min=15,output_path=outpath)

def adhoc_0437():
    toas = parse_toas('cr/J0437-4715_comb.toa')
    import util
    profiles = util.readlines('/home/ker14a/research/dr2/obslists/unusual_config.asc')
    ok_toas = deque()
    for toa in toas:
        bad_toa = False
        for profile in profiles:
            if toa.profile.startswith(profile):
                bad_toa = True
                break
        if not bad_toa:
            ok_toas.append(toa)
    write_toas(ok_toas,'/home/ker14a/research/analyses/dr2paper/j0437_residuals_analysis/j0437/j0437.toa')

def make_plots(jnames=None):
    if not os.path.isdir('/tmp/plots'):
        os.mkdir('/tmp/plots')
    if jnames is None:
        jnames = get_jnames()
    for jname in jnames:
        par = 'ephemerides/%s.par'%jname
        toa = '%s/%s_comb.toa'%(combined,jname)
        print par,toa
        cmd = 'tempo2 -gr plk -f %s %s -yplot 2 -grdev /tmp/plots/%s.png/png'%(par,toa,jname)
        os.system(cmd)
