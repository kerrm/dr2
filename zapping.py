"""
Module handling zapping data, primarily for RFI excision.

$Header: /nfs/slac/g/glast/ground/cvs/users/kerrm/tpipe/timing.py,v 1.14 2014/01/21 06:46:59 kerrm Exp $

author: Matthew Kerr <matthew.kerr@gmail.com>
"""
import numpy as np
import psrchive
from band import Band,Band_10cm,Band_20cm,Band_50cm,Band_40cm
from jobs import psrsh_segment
import util
import profiles

def check_cal_subints(cal,tol=0.05,zaps=None):
    """ Perform a quick check to see if the cal is on in every subint.
    
    This stems from the problem of DFB3 controlling the cal and observations    with other backends starting nonsimultaneously.
    
    The check here is a simple one, requiring the maximum of each cal to be
    within 95% of the global maximum.
    
    We also support the option for applying some other zaps, e.g.
    narrowband RFI, bad subints, etc. so that these don't affect our
    calculation here."""

    psrsh = psrsh_segment()
    if (cal is None) or not cal.valid():
        return psrsh
    a = psrchive.Archive_load(cal.fname)
    a.pscrunch()
    if zaps is not None:
        for zap in zaps:
            if (zap[0] == '#') or ('load' in zap) or ('mow' in zap):
                continue
            result = a.execute(str(zap))
            if result != '':
                print 'Failed in executing %s.'%zap
    a.fscrunch()
    profs = [i.get_Profile(0,0).get_amps() for i in a]

    # these sub-ints are manually zapped or zeroed for some reason
    already_zapped = np.asarray([p[0] == 0 for p in profs])

    stats = [profiles.cal_baseline(p,get_residuals=False) for p in profs]
    off_baselines = np.asarray([x[0] for x in stats])
    on_baselines = np.asarray([x[1] for x in stats])
    height = on_baselines - off_baselines

    # NB -- we need at least three subints for median calculation to work
    # well when one or more of the subints is not at full intensity; so for
    # cases with only two subints, use the max rather than the median

    # handle cases like P789 where there are only 3 subints, and the first
    # and last are "off", so the median ends up at an off level

    height_mask = ~already_zapped
    if sum(height_mask) == 0:
        return psrsh

    if cal['backend'] == 'CASPSR':
        # exclude first and last subint from median computation as they
        # almost always suffer from cal transients
        # must handle the case where there are only two subints!
        if sum(height_mask) > 2:
            height_mask[0] = False
            height_mask[-1] = False

    # if there are enough sub-ints, take the median
    if sum(height_mask) > 2:
        typical_height = np.median(height[height_mask])
    # otherwise take max and hope for no RFI contamination
    else:
        typical_height = np.max(height[height_mask])
    tol *= typical_height

    c = (np.abs(height-typical_height) > tol) | np.isnan(height)
    zap_subints = np.ravel(np.argwhere( c & (~already_zapped) ))

    if len(zap_subints) > 0:
        # check to make sure we leave at least one sub-int!
        if (len(zap_subints) + sum(already_zapped)) == len(height):
            keep = np.nanargmin(np.abs(height-typical_height))
            if not np.isnan(keep):
                zap_subints = zap_subints[zap_subints != keep]
        psrsh.comment('# Zapping subints due to low level.')
        psrsh.add(*('zap subint %d'%zi for zi in zap_subints))
    return psrsh

def zap_final_subint(obs,psrsh=None):
    """ For some backends, the weighting of the last sub-int isn't correct
    and it substantially increases the baseline noise.  Zap those cases.
    """
    if psrsh is None:
        psrsh = psrsh_segment()
    if obs.is_cal():
        return psrsh
    if 'CPSR2' in obs['backend']:
        if obs.nsub > 1:
            psrsh.comment('# Zap final sub-int in CPSR2 observations.')
            psrsh.add('zap subint %d'%(obs.nsub-1))
    return psrsh

def ppta_10cm_zap(obs):
    psrsh = psrsh_segment(['zap edge 0.05'])
    mjd = obs.get_mjd()
    if (mjd >= 57200) and (mjd <= 57250):
        psrsh.comment(
            'Zap Optus 4G signals 2.67-2.69 GHz from late June 2015.')
        psrsh.add('zap freq 2670:2690')
    """
    if mjd >= 57417.4:
        psrsh.comment(
            'Zap Telstra 4G signals 2.63-2.67 GHz from Jan 30 2016.')
        psrsh.add('zap freq 2630:2670')
    """
    if obs.is_cal():
        psrsh.add('zap mow')
    return psrsh

def ppta_20cm_zap(obs):
    cal = obs.is_cal()
    psrsh = psrsh_segment()
    #nchan = 50 if cal else int(round(float(obs['nchan'])/8))
    nchan = int(round(float(obs['nchan'])/8))
    # handle H-OH observations -- NB will have to be tuned for final
    # configurations
    if (obs['frontend'] == 'H-OH'):
        if (abs(obs['bandwidth']) == 1024):
            psrsh.comment('Zap mobile phone band.')
            psrsh.add('zap freq 1805:1820')
            psrsh.add('zap freq <1142')
            psrsh.add('zap freq >1960')
        if (abs(obs['bandwidth']) >= 512):
            psrsh.comment('Zap narrowband RFI.')
            psrsh.add('zap freq 1550:1550.5')
            psrsh.add('zap freq 1556.5:1559')
            psrsh.add('zap freq 1617.5:1626.75')
            psrsh.add('zap freq 1634.25:1634.75')
            psrsh.add('zap freq 1650.5:1651')
    if obs['backend'] == 'CASPSR':
        psrsh.comment('CASPSR 20cm bandpass zaps.')
        psrsh.add('zap freq <1210','zap freq >1521')
    if 'CPSR2' in obs['backend']:
        # zap 10% for CPSR2 -- the aliasing is really bad at one edge of
        # the band, and the bandpass shape together with the median zapping
        # can induce some profile variations by zapping different #s of
        # channels.  Excising deeply mitigates this somewhat.
        psrsh.add(
            'zap edge 0.10','zap median window=%d'%nchan,'zap median')
    else:
        psrsh.add(
            'zap edge 0.05','zap median window=%d'%nchan,'zap median')
    if cal:
        psrsh.add('zap mow')
    return psrsh

def ppta_50cm_zap(obs):

    backend = obs['backend']
    cal = obs.is_cal()
    nchan = int(round(float(obs['nchan'])/10))
    if backend=='PDFB1':
        # handle case of incorrect DFB1 bandpass
        eff_channels = 64./abs(obs['bandwidth'])*obs['nchan']
        nchan = int(round(eff_channels/10))
    mjd = obs.get_mjd()
    psrsh = psrsh_segment()
    DFB_BACKENDS = ['WBCORR','PDFB1','PDFB2','PDFB3','PDFB4']

    def _get_dfb_zaps(mjd):
        """ Return the sub-bands to zap."""
        psrsh = psrsh_segment()
        if backend == 'PDFB1':
            # MK added -- some PDFB1 50cm obs taken with 256 MHz of bw
            # NB that the file centre frequency doesn't agree with the RF
            # centre frequency due to the bw mismatch
            lo = 685-32
            hi = 685+32
            psrsh.add('#Remove extra bandpass.')
            psrsh.add('zap freq <%d'%lo,'zap freq >%d'%hi)

        # TV stations gradually coming on
        # Dick email 20 March 2011
        # from fix.C:
        #   ABC: 652-659, MJD > 52852
        #   Prime: 659-666, MJD > 54313
        #   SBS: 666-673, MJD > 53310
        #   WIN: 680-687, MJD > 54282
        #   Sthn Cross: 687-694, MJD > 54313

        # MTK note -- it appears SBS actually begins transmission by at
        # least MJD 55302, so date changed below

        # default zaps < MJD 52852 (Aug 1 2003) and > 55039 (July 27 2009)
        if (mjd < 52852) or (mjd > 55039):
            return psrsh
        # otherwise, filter a time-dependent series of channels
        if mjd > 54313:
            # Aug 1, 2007
            zaps = [[652,673],[680,694]]
        else:
            zaps = [[652,659]]
            if (mjd > 53301):
                zaps.append([666,673])
            if (mjd > 54282):
                zaps.append([680,687])
        psrsh.comment('# Time-dependent TV zaps.')
        psrsh.add(*('zap freq %d:%d'%(x[0],x[1]) for x in zaps))
        return psrsh

    # this prescription is decided to be unwarranted
    #if (backend == 'APSR') and (mjd > 55039):
        #psrsh.comment(
            #'APSR 732 MHz: Zap DTV channels from MJD 55039 onwards.')
        #psrsh.add('zap freq 716:724','zap freq 736:744')

    # do a special set of narrowband zaps for APSR, where the crazy
    # bandpass wreaks havoc on the median zapping
    # these are identified in frequency and time from looking at data
    if (backend == 'APSR') and (abs(obs.get_centre_frequency()-732) < 2):
        # a very bright line that persists through the end of Sept 2013 
        if (mjd < 56565):
            psrsh.comment('# Zap one bright RFI lines in 50cm band.')
            psrsh.add('zap chan 7')
        # five bright lines that seem to disappear around mid-2012
        if (mjd < 56079):
            psrsh.comment('# Zap five bright RFI lines in 50cm band.')
            psrsh.add('zap chan 11,42,53,84,95')

    if (backend in DFB_BACKENDS) or (backend=='CPSR2') or (backend=='APSR'):
        # CPSR2 and DFB2 have same bw/freq, do same thing
        psrsh += _get_dfb_zaps(mjd)
        psrsh.add(
            'zap edge 0.05','zap median window=%d'%nchan,'zap median')
    elif (backend == 'CASPSR'):
        # start to handle new mobile phone RFI from Alectown tower
        # band shifted to 724 MHz and other issues
        # NB date should be tuned
        if (mjd > 57150):
            if (obs['freq'] == 720):
                    psrsh.comment('# CASPSR 40cm/724 MHz/4G bandpass zaps.')
                    psrsh.add('zap freq 400:700','zap freq 753:900')
            else:
                    psrsh.comment('# CASPSR 40cm/732 MHz/4G bandpass zaps.')
                    psrsh.add('zap freq 400:705','zap freq 753:900')
        else:
            # Dick email 16 Dec 2012
            psrsh.comment('# CASPSR 40cm bandpass zaps.')
            psrsh.add('zap freq 400:705','zap freq 758:900')
            # add in narrowband zaps
            if (mjd < 55957):
                psrsh.comment('# Zap narrowband 40cm RFI before Feb 2012.')
                #psrsh.add('zap chan 142')
                # a new trial set of narrowband zaps
                psrsh.add('zap chan 89,92,109,116,135-137,142-144')
    else:
        #raise ValueError('Did not recognize backend %s.'%backend)
        print 'WARNING: Did not recognize backend %s.'%backend
    if cal:
        psrsh.append('zap mow')
    return psrsh
                
def ppta_default_band_zaps(obs):
    band = Band(obs)
    if band == Band_10cm():
        return ppta_10cm_zap(obs)
    elif band == Band_20cm():
        return ppta_20cm_zap(obs)
    elif (band == Band_50cm()) or (band == Band_40cm()):
        return ppta_50cm_zap(obs)
    else:
        return psrsh_segment(['# Unrecognized band; no default channel zaps.'])

def ppta_default_zap(obs):
    return zap_final_subint(obs,psrsh=ppta_default_band_zaps(obs))

def parse_zaps(fname):
    lines = util.readlines(fname)
    if lines[0] == '@manual':
        zaps = parse_manual_zaps(lines[1:])
    else:
        zaps = parse_satellite_zaps(lines[1:])
    return lines[0][1:],zaps

def parse_manual_zaps(lines):
    """ Return psrsh lines given a file of zaps."""
    zaps = dict()
    for line in lines:
        # only subints specified -- backwards compatibility
        toks = line.split()
        basename = toks[0]
        subints,chans,chan_subints = None,None,None
        try:
            toks = toks[1:]
            ids = [x for x in toks if x.startswith('-')]

            # old default case -- format = "basename sub1,sub2,..."
            if len(ids) == 0:
                subints = toks[0]

            # handle case format = "basename -z chan1,chan2,..."
            elif len(ids) == 1 and ids[0] == '-z':
                chans = toks[1]

            # handle format = "basename -z chan1... -w sub1" and
            #                 "basename -w sub1... -z chan1..."
            #                 "basename -s sub1... -z chan1..."
            #                 "basename -s sub1... -w sub2... -z chan1..."
            # etc.
            else:
                # toks at this stage should have a structure like
                # [id] data [id] data [id] data... so simply step through

                for i in xrange(len(ids)):
                    if ids[i]=='-w':
                        subints = toks[i*2+1]
                    elif ids[i]=='-z':
                        chans = toks[i*2+1]
                    elif ids[i]=='-s':
                        chan_subints = toks[i*2+1]

        except Exception as e:
            print 'Could not parse entry for %s.'%basename

        zaps[basename] = [subints,chans,chan_subints]

    return zaps

def parse_satellite_zaps(lines):
    """ Return zaps for a Galilo/Compass etc. file."""

    zaps = dict()

    for line in lines:
        chan_subints = full_subints = None
        toks = line.split(' ')
        basename = toks[0]
        # zap the appropriate channels for the full observation
        if len(toks) == 1:
            zaps[basename] = [None,None]
        else:
            if toks[1] == '-s':
                chan_subints = toks[2]
            elif toks[1] == '-w':
                full_subints = toks[2]
            if len(toks) > 3:
                if toks[3] == '-s':
                    chan_subints = toks[4]
                elif toks[3] == '-w':
                    full_subints = toks[4]
        zaps[basename] = [chan_subints,full_subints]

    return zaps

def manual_zaps(obs,zaps):
    subs,chans,chan_subints = zaps
    if chan_subints is not None:
        obs.command('\n'.join((
                  'zap subint set %s'%chan_subints,
                  'zap chan %s'%(chans),
                  'zap subint set')))
        print 'manually zapping %s %s for sub-ints %s'%(
            obs.basename,chans,chan_subints) # TMP
    elif chans is not None:
        obs.comment('Zap the following manually-specified channels:')
        obs.command('zap chan %s'%(chans))
        print 'manually zapping %s %s for all sub-ints'%(
            obs.basename,chans) # TMP
    if subs is not None:
        obs.comment('Zap the following manually-specified subints:')
        obs.command('zap subint %s'%(subs))
        print 'manually zapping sub-ints %s %s'%(obs.basename,subs) # TMP

def satellite_zaps(obs,zaps,freq_zaps):
    """ General function for performing a set of frequency zaps in the
    "satellite style", viz. where we might want to zap the same frequencies
    for a subset of the subints, for all of the subints, and we might want
    to do full zapping for bad subints.
    """
    chan_subints,full_subints = zaps
    if chan_subints is not None:
        obs.command('\n'.join((
                  'zap subint set %s'%chan_subints,
                  freq_zaps,
                  'zap subint set')))
    else:
        obs.command(freq_zaps)
    if full_subints is not None:
        obs.command('zap subint %s'%full_subints)

def galileo_zaps(obs,zaps):
    """ Apply a zap for the brightest Galileo bands.
    
    Carrier is at 1278.75 MHz, sub-carriers offset by 10.23 MHz, code freq.
    is 5.11 MHz.

    In practice, these definitions are not quite broad enough for young
    pulsars, so this definition should really only be used for PPTA, etc.
    See galileo_full_zaps for occurrence in young pulsars.
    """
    obs.comment('Zaps for Galileo passing through main beam')
    freq_zaps = '\n'.join((
        'zap freq 1265.965:1271.075',
        'zap freq 1276.195:1281.305',
        'zap freq 1286.425:1291.535',
    ))
    satellite_zaps(obs,zaps,freq_zaps)

def galileo_full_zaps(obs,zaps):
    """ Apply a zap for the brightest Galileo bands.
    
    Carrier is at 1278.75 MHz, sub-carriers offset by 10.23 MHz, code freq.
    is 5.11 MHz.

    This version is for when the RFI is bright enough that the sidebands
    are important too.  (Shows up as seven peaks in bandpass, with the
    the centre triple dominant.)

    It's safest just to zap the entire bracket when it's this bad.

    """
    obs.comment(
        'Zaps for Galileo passing through main beam; zap entire range.')
    freq_zaps = 'zap freq 1254:1304'
    satellite_zaps(obs,zaps,freq_zaps)

def compass_zaps(obs,zaps):
    """ Apply a zap for the brightest Compass/Beidou bands.
    
    Carrier is at 1268.52 MHz, code frequency is 10.23 MHz.

    Effective width of primary lobe is about 16 MHz, also seems slightly
    offset.
    """
    obs.comment('Zaps for Compass passing through main beam')
    freq_zaps = 'zap freq 1261:1277'
    satellite_zaps(obs,zaps,freq_zaps)

def compass_full_zaps(obs,zaps):
    """ Apply a zap for the brightest Compass/Beidou bands.
    
    Carrier is at 1268.52 MHz, code frequency is 10.23 MHz.

    Effective width of primary lobe is about 16 MHz, also seems slightly
    offset.

    This zap additional handles the two main sidelobes.
    """
    obs.comment('Zaps for Compass passing through main beam')
    freq_zaps = '\n'.join((
        'zap freq 1260:1278',
        'zap freq 1280:1286',
        'zap freq 1252:1256',
    ))
    satellite_zaps(obs,zaps,freq_zaps)
    
def midweek_fiveband_zaps(obs,zaps):
    """ Apply channel zaps for the form of mid-week with five bright bands.

        We additionally allow for full subint zapping, a la the satellites.
        If chan_subints are not specified, the channels are zapped for the
        entire observation.
    """

    obs.comment('Zaps for "five-band" mid-week RFI.')
    freq_zaps = '\n'.join((
        'zap freq 1250:1264',
        'zap freq 1293:1306',
        'zap freq 1372:1383',
        'zap freq 1413:1426',
        'zap freq 1453:1462',
        'zap freq 1492:1497',
    ))
    satellite_zaps(obs,zaps,freq_zaps)

def midweek_twoband_zaps(obs,zaps):
    """ Apply channel zaps for the form of mid-week with two bright bands.

    This would typically occur when the mid-week is fainter -- so apply
    a somewhat narrower cut and only zap out two of the five bands.

    We additionally allow for full subint zapping, a la the satellites.
    If chan_subints are not specified, the channels are zapped for the
    entire observation.
    """

    obs.comment('Zaps for "two-band" mid-week RFI.')
    freq_zaps = '\n'.join((
        'zap freq 1257:1264',
        'zap freq 1297:1304',
    ))
    satellite_zaps(obs,zaps,freq_zaps)

def midweek_twoband_other_zaps(obs,zaps):
    """ Apply channel zaps for the form of mid-week with two bright bands.

    This would typically occur when the mid-week is fainter -- so apply
    a somewhat narrower cut and only zap out two of the five bands.

    We additionally allow for full subint zapping, a la the satellites.
    If chan_subints are not specified, the channels are zapped for the
    entire observation.
    """

    obs.comment('Zaps for "other" "two-band" mid-week RFI.')
    freq_zaps = '\n'.join((
        'zap freq 1263:1268',
        'zap freq 1297:1304',
    ))
    satellite_zaps(obs,zaps,freq_zaps)

def rfi_zap_10cm_2630mhz_zaps(obs,zaps):
    obs.comment('Zaps for 2.630-2.650 and 2.650-2.670 4G signals.')
    freq_zaps = 'zap freq 2630:2670'
    satellite_zaps(obs,zaps,freq_zaps)

def rfi_zap_10cm_nbn_zaps(obs,zaps):
    obs.comment('Zaps for 2.670-2.690, 3.445-3.465, and 3.550-3.570 GHz NBNCo signals.')
    freq_zaps = '\n'.join((
        'zap freq 2670:2690',
        'zap freq 3445:3465',
        'zap freq 3550:3570',
    ))
    satellite_zaps(obs,zaps,freq_zaps)
