# code for handling dynamic spectra and fluxes

import os
import numpy as np
import cPickle
from scipy.signal import fftconvolve
from scipy.optimize import leastsq

from collections import deque

import util
import band

def prew_postd(image):
    """ Pre-whiten, FFT, and post-darken."""
    f = image
    M = f.shape[0]
    N = f.shape[1]
    pre_whitened = np.empty_like(image)
    # f will have shape M-1, N-1
    f = f[1:]-f[:-1]
    f = f[:,1:]-f[:,:-1]
    pre_whitened[1:,1:] = f
    #pre_whitened[0,:] = 0
    #pre_whitened[:,0] = 0
    pre_whitened[0,:] = 1
    pre_whitened[:,0] = 1
    fft = np.fft.fft2(pre_whitened)
    # assemble post-darkening kernel
    phase_1 = 2*np.sin(np.pi*np.fft.fftfreq(M))
    phase_2 = 2*np.sin(np.pi*np.fft.fftfreq(N))
    kernel = phase_1[:,np.newaxis]*phase_2
    fft = np.abs(fft)**2/kernel**2
    #fft[1:,1:] = np.abs(fft[1:,1:])**2/kernel[1:,1:]**2
    #fft[0,:] = 0.
    #fft[:,0] = 0.
    return np.real(fft)

def prew_postd2(image,zero_pad=True,second_order=False):
    """ Pre-whiten, FFT, and post-darken."""
    f = image
    M = f.shape[0]
    N = f.shape[1]
    if zero_pad:
        newM = 2**(int(np.log2(M)) + 2) + 1
        newN = 2**(int(np.log2(N)) + 2) + 1
        newf = np.empty((newM,newN))
        newf[0:M,0:N] = f
        newf[M:,:] = 0.
        newf[:,N:] = 0.
        f = newf
        M = newM
        N = newN
    pre_whitened = np.empty_like(f)
    # f will have shape M-1, N-1
    f = f[1:]-f[:-1]
    f = f[:,1:]-f[:,:-1]
    if second_order:
        f = f[1:]-f[:-1]
        f = f[:,1:]-f[:,:-1]
        pre_whitened[2:,2:] = f
        pre_whitened[0,:] = 1
        pre_whitened[1,:] = 1
        pre_whitened[:,0] = 1
        pre_whitened[:,1] = 1
    else:
        pre_whitened[1:,1:] = f
        pre_whitened[0,:] = 1
        pre_whitened[:,0] = 1
    fft = np.fft.fft2(pre_whitened)
    # assemble post-darkening kernel
    phase_1 = 2*np.sin(np.pi*np.fft.fftfreq(M))
    phase_2 = 2*np.sin(np.pi*np.fft.fftfreq(N))
    kernel = phase_1[:,np.newaxis]*phase_2
    if second_order:
        kernel *= kernel
    fft = np.abs(fft)**2/kernel**2
    #fft[1:,1:] = np.abs(fft[1:,1:])**2/kernel[1:,1:]**2
    #fft[0,:] = 0.
    #fft[:,0] = 0.
    fft = np.real(fft)
    return fft

def fit_spind(dss,diag_plot=None,nchan=32):
    """ Determine the best-fitting spectral index for a group of dynamic
    spectra.
    """
    #if len(dss) == 0:
        #return 10,-2
    # collapse in time -- NB some corrupt obs. give empty results???

    scrunches  = deque()
    flux_calibrated = deque()
    for ds in dss:
        scrunch = ds.get_fscrunch(nchan=nchan)
        if len(scrunch[0]) > 0:
            scrunches.append(scrunch)
            flux_calibrated.append(ds.flux_calibrated)
    flux_calibrated = np.asarray(flux_calibrated)

    if diag_plot is not None:
        import pylab as pl
        pl.figure(3); pl.clf()
        for iscrunch,scrunch in enumerate(scrunches):
            f,d = scrunch
            if flux_calibrated[iscrunch]:
                pl.plot(f,d,color='k',alpha=0.5)
            else:
                pl.plot(f,d,color='cyan',alpha=0.5)

    fiducial = 1400.
    freqs = np.concatenate([x[0] for x in scrunches])/fiducial
    data = np.concatenate([x[1] for x in scrunches])
    mask = np.concatenate([[flux_calibrated[i]]*len(scrunches[i][0]) for i in xrange(len(scrunches))])
    print mask.shape,freqs.shape
    mask &= ((freqs > 1000./fiducial) & (freqs < 4000./fiducial))
    if not np.any(mask):
        mask[:] = True
        #return 10,-2
    freqs = freqs[mask]
    data = data[mask]
    def chi(p):
        norm,spind = p
        model = norm*freqs**spind
        return data-model

    p = leastsq(chi,(np.mean(data),2))[0]
    if diag_plot is not None:
        freqs = np.linspace(1000,4000,1001)
        model = p[0]*(freqs/fiducial)**p[1]
        pl.plot(freqs,model,color='red',alpha=0.8)
        pl.savefig(diag_plot)

    return p

def sub_filter(ds,thresh=10,verbose=False):
    """ Use a crude median analysis to eliminate bad sub-integrations. """
    weights = ds.flux > 0
    cmask = np.any(weights,axis=0)
    smask = np.any(weights,axis=1)
    weights = weights[smask][:,cmask]
    flux = ds.flux[smask][:,cmask]
    m1 = np.average(flux,weights=weights,axis=1)
    m2 = np.average(flux**2,weights=weights,axis=1)
    dm1 = np.abs(m1-np.median(m1))
    dm2 = np.abs(m2-np.median(m1))
    mask = np.abs(dm1/np.median(dm1)) > thresh
    mask |= np.abs(dm2/np.median(dm2)) > thresh
    zapped = np.arange(ds.flux.shape[0])[smask][mask]
    if len(zapped) == 0:
        return 0
    if verbose:
        print 'Zapping subs %s.'%(','.join(['%d'%idx for idx in zapped]))
    ds.flux[zapped] = 0
    return len(zapped)

def chan_filter(ds,thresh=15,verbose=False):
    """ Use a crude median analysis to eliminate bad channels. """
    weights = ds.flux > 0
    cmask = np.any(weights,axis=0)
    smask = np.any(weights,axis=1)
    weights = weights[smask][:,cmask]
    flux = ds.flux[smask][:,cmask]
    m1 = np.average(flux,weights=weights,axis=0)
    m2 = np.average(flux**2,weights=weights,axis=0)
    dm1 = np.abs(m1-np.median(m1))
    dm2 = np.abs(m2-np.median(m1))
    mask = np.abs(dm1/np.median(dm1)) > thresh
    mask |= np.abs(dm2/np.median(dm2)) > thresh
    zapped = np.arange(ds.flux.shape[1])[cmask][mask]
    if len(zapped) == 0:
        return 0
    if verbose:
        print 'Zapping chans %s.'%(','.join(['%d'%idx for idx in zapped]))
    ds.flux[:,zapped] = 0
    return len(zapped)

def hot_pixel_mask(ds,thresh=10,verbose=False):
    """ Mask out hot pixels by doing a first difference."""
    dx = ds.flux[1:,1:] - ds.flux[:-1,:-1]
    scale = np.median(np.abs(dx-np.median(dx)))*1.4826
    mask = np.abs(dx) > (scale*thresh)
    # at this point we don't know which of the neighboring pixels was bad
    # do a simple expedient of excluding the one with the maximum abs val 
    p1mask = np.abs(ds.flux[1:,1:][mask]) > np.abs(ds.flux[:-1,:-1][mask])
    ds.flux[1:,1:][mask] = np.where(p1mask,0,ds.flux[1:,1:][mask])
    ds.flux[:-1,:-1][mask] = np.where(~p1mask,0,ds.flux[:-1,:-1][mask])
    if verbose:
        print 'Masked %d hot pixels.'%(mask.sum())

def rfi_filter(dss,sthresh=10,cthresh=15,verbose=False):
    """ Filter the DynamicSpectrum objects."""
    for ids,ds in enumerate(dss):
        total_czap = chan_filter(ds,thresh=cthresh+5)
        total_szap = sub_filter(ds,thresh=sthresh+5)
        for i in xrange(3):
            czap = chan_filter(ds,thresh=cthresh)
            szap = sub_filter(ds,thresh=sthresh)
            total_czap += czap
            total_szap += szap
            if (czap + szap) == 0:
                break
        if verbose:
            print '%04d total chans = %d; total subs = %d.'%(ids,total_czap,total_szap)

def get_dof(ds,no_avg=False,sub_rad=True):
    """ Using estimates of scatter and mean, determine effective number of
    scintles in a channel, in a sub-integration.
    """
    # TODO -- remove bandpass / spectral shape?
    mask = ds.flux != 0
    ok_sub = np.any(mask,axis=1)
    flux = ds.flux[ok_sub]
    mask = mask[ok_sub]
    m1 = np.average(flux,weights=mask,axis=1)
    m2 = np.average(flux**2,weights=mask,axis=1)
    var = m2 - m1**2
    if sub_rad:
        fluxe = ds.fluxe[ok_sub]
        m2p = np.average(fluxe**2,weights=mask,axis=1)
        var -= m2p
    dof = 2*(m1**2/var)
    #dof = var**0.5/m1
    if no_avg:
        return dof
    return np.mean(dof)

def DynamicSpectrum(fname,trim_edges=True):
    """ Factory class to load ASCII output version."""
    f = file(fname)
    epoch = None
    flux_calibrated = False
    for line in f.readlines():
        if line[0] == '#':
            if 'MJD0' in line:
                epoch = float(line.split(':')[1].strip())
            if 'Flux units' in line:
                if line.strip().split()[-1] == 'Jansky':
                    flux_calibrated = True
        else:
            break
    f.close()
    a = np.loadtxt(fname,comments='#')
    nchan = int(a[:,1].max())+1
    nsub = int(a[:,0].max())+1
    freqs = a[:nchan,3]
    times = a[::nchan,2]*60
    flux = np.empty([nsub,nchan])
    fluxe = np.empty_like(flux)
    for isub in xrange(nsub):
        flux[isub] = a[isub*nchan:(isub+1)*nchan,4]
        fluxe[isub] = a[isub*nchan:(isub+1)*nchan,5]
    return _DynamicSpectrum(freqs,times,flux,fluxe,
        flux_calibrated=flux_calibrated,epoch=epoch,trim_edges=trim_edges)

class DisplaySpectrum(object):
    """ Define a common framework for plotting spectra."""

    def get_display(self,**kwargs):
        """ Return x dimensions, y dimensions, and image."""
        raise NotImplementedError()

class _DynamicSpectrum(DisplaySpectrum):
    """ Encapsulate a dynamic spectrum."""

    def get_display(self,**kwargs):
        if 'spind' in kwargs:
            image = self.get_mean_subtracted(spind=kwargs['spind'])
        else:
            image = self.flux
        freqs = self.freqs
        times = self.times
        if 'tscrunch' in kwargs:
            for i in xrange(kwargs['tscrunch']):
                if image.shape[0] % 2 != 0:
                    image = image[:-1,:]
                    times = times[:-1]
                image = 0.5*(image[::2,:] + image[1::2,:])
                times = 0.5*(times[::2]+times[1::2])
        if 'fscrunch' in kwargs:
            for i in xrange(kwargs['fscrunch']):
                if image.shape[1] % 2 != 0:
                    image = image[:,:-1]
                    freqs = freqs[:-1]
                image = 0.5*(image[:,::2] + image[:,1::2])
                freqs = 0.5*(freqs[::2]+freqs[1::2])

        return freqs,'Frequency (MHz)',times,'Time (s)',image

    def __init__(self,freqs,times,flux,fluxe,
        flux_calibrated=True,epoch=None,trim_edges=True):
        """ freqs -- channel centre frequencies
            times -- subint time offsets
            flux -- image of fluxes (nsub * nchan)
            fluxe -- image of flux errors (nsub * nchan)
        """
        if trim_edges:
            indices = np.arange(len(freqs))[np.any(flux!=0,axis=0)]
            start,stop = indices[0],indices[-1]+1
            flux = flux[:,start:stop]
            fluxe = fluxe[:,start:stop]
            freqs = freqs[start:stop]
        self.freqs = freqs
        self.times = times
        self.flux = flux.copy()
        self.fluxe = fluxe
        self.nsub = self.times.shape[0]
        self.nchan = self.freqs.shape[0]
        self.flux_calibrated = flux_calibrated
        self.epoch = epoch
        #self.flux[self.flux==0] = np.mean(self.flux[self.flux!=0])

    def add(self,other):
        """ Attempt to join two dynamic spectra.  The spectra must
            match in frequency and time binning, and be reasonably close
            in epoch.
            
            The same regular gridding will be retained by shifting the
            second observation by a fractional subint.  Empty subints will
            be set to 0.
        """
        if other.freqs[0] != self.freqs[0]:
            raise ValueError('Dynamic spectra do not match freq!!')
        df1 = self.freqs[1]-self.freqs[0]
        df2 = other.freqs[1]-other.freqs[0]
        if df1 != df2:
            raise ValueError('Dynamic spectra do not match chbw!!')
        dt1 = self.times[1]-self.times[0]
        dt2 = other.times[1]-other.times[0]
        if abs(dt1 - dt2) > 1:
            raise ValueError('Dynamic spectra do not match tsub!!')

        if other.freqs[0] != self.freqs[0]:
            raise ValueError('Dynamic spectra do not match freq!!')
        if (self.epoch is not None) and (other.epoch is not None):
            if (self.epoch-other.epoch) > 1:
                raise ValueError('Dynamic spectra too far apart!')
        else:
            raise ValueError('No known epochs!')
        dt = other.epoch - self.epoch
        if dt < 0:
            first,second = other,self
            dt = -dt
        else:
            first,second = self,other
        gap = dt*86400
        gap -= first.times[-1]
        gap -= 0.5*(first.times[-1]-first.times[-2])
        gap -= second.times[0]
        dt = second.times[1]-second.times[0]
        nsub_gap = int(round(gap/dt))
        M = self.freqs.shape[0]
        N = nsub_gap + first.times.shape[0] + second.times.shape[0]
        freqs = self.freqs.copy()
        times = np.append(first.times,np.arange(1,nsub_gap+1)*dt + first.times[-1])
        times = np.append(times,second.times+times[-1])
        outflux = np.empty((N,M))
        outflux[:first.times.shape[0]] = first.flux
        outflux[first.times.shape[0]:first.times.shape[0]+nsub_gap] = 0
        outflux[first.times.shape[0]+nsub_gap:] = second.flux
        outfluxe = np.empty((N,M))
        outfluxe[:first.times.shape[0]] = first.fluxe
        outfluxe[first.times.shape[0]:first.times.shape[0]+nsub_gap] = 0
        outfluxe[first.times.shape[0]+nsub_gap:] = second.fluxe
        return _DynamicSpectrum(freqs,times,outflux,outfluxe,
            flux_calibrated=self.flux_calibrated,epoch=first.epoch)

    def get_cs(self,spind=None):
        """ Return the auto-correlation."""
        return CovarianceSpectrum(self,spind=spind)

    def get_params(self,spind=None,diag_plot=None):
        """ Return tau0 (s) and nu0 (MHz)."""
        cs = self.get_cs(spind=spind)
        return cs.do_fits(diag_plot=diag_plot)

    def get_fscrunch(self,nchan=32,get_error=False,tmax=None):
        """ Return a time and fscrunched version of data."""
        nscrunch = self.nchan / nchan
        weights = self.flux != 0
        freqs = np.empty(nchan)
        data = np.empty(nchan)
        datae = np.empty(nchan)
        if tmax is None:
            tmask = np.ones(self.flux.shape[0],dtype=np.bool)
        else:
            tmask = self.times < tmax
        for i in xrange(nchan):
            s = slice(i*nscrunch,(i+1)*nscrunch)
            w = weights[tmask,s]
            ws = np.sum(w)
            if ws == 0:
                data[i] = 0
                datae[i] = 0
                freqs[i] = np.mean(self.freqs[s])
            else:
                data[i] = np.sum(self.flux[tmask,s]*w)/ws
                datae[i] = (np.sum((self.fluxe[tmask,s]*w)**2)**0.5)/ws
                freqs[i] = np.sum(self.freqs[s]*w.sum(axis=0))/ws
        if get_error:
            return freqs,data,datae
        return freqs,data

    def get_time_series(self):
        """ Return integrated flux."""
        rvals = np.empty(self.flux.shape[0])
        weights = np.zeros(self.flux.shape[0])
        for i in xrange(self.flux.shape[0]):
            w = self.flux[i] != 0
            ws = np.sum(w)
            if ws == 0:
                rvals[i] = 0
            rvals[i] = np.sum(w*self.flux[i])/ws
        return rvals

    def get_weights(self):
        return np.asarray(self.flux != 0,dtype=float)

    def get_mean_subtracted(self,spind=None):
        if spind is None:
            mask = self.flux != 0
            q = self.flux.copy()
            q[mask] -= q[mask].mean()
            return q
        # average in time
        freqs,data = self.get_fscrunch(nchan=self.nchan)
        mask = data != 0
        m = (freqs[mask]/1400)**spind
        norm = np.sum(m*data[mask])/np.sum(m)
        model = (self.freqs/1400)**spind*norm
        q = self.flux.copy()
        for i in xrange(q.shape[0]):
            q[i] = np.where(q[i] != 0, q[i]-model, 0)
        mask = q != 0
        q[mask] -= q[mask].mean()
        #mask = self.flux != 0
        #q[mask] -= (model[np.newaxis,:])[mask]
        return q

    def get_mean_power_spectrum(self,spind=None):
        """ Return the average power spectrum for the observation."""
        d = self.get_mean_subtracted(spind=None)
        return np.mean(np.abs(np.fft.fft2(d))**2,axis=0)

    def do_chan_avg(self):
        """ Average channels together."""
        if (self.flux.shape[1] % 2) != 0:
            return

        w = (self.flux != 0).astype(float)
        f = (self.flux[:,::2] + self.flux[:,1::2])/(w[:,::2] + w[:,1::2])
        f[(w[:,::2] + w[:,1::2])==0] = 0
        fe = ((self.fluxe[:,::2]**2 + self.fluxe[:,1::2]**2)/(w[:,::2] + w[:,1::2]))**0.5
        # ignore weighting on freqs for now
        freqs = (self.freqs[::2] + self.freqs[1::2])
        self.flux = f
        self.fluxe = fe
        self.freqs = freqs

class CovarianceSpectrum(DisplaySpectrum):

    def __init__(self,ds,spind=None):
        self.ds = ds
        spec = ds.get_mean_subtracted(spind=spind)
        self.cs = fftconvolve(spec,spec[::-1,::-1])[ds.nsub-1:,ds.nchan-1:]
        self.cs *= (1./(self.cs.shape[0]*self.cs.shape[1]))

    def get_display(self,**kwargs):
        time_lags = self.ds.times - self.ds.times[0]
        freq_lags = self.ds.freqs - self.ds.freqs[0]
        return freq_lags,'Frequency Lag (MHz)',time_lags,'Time Lag (s)',self.cs

    def do_fits(self,diag_plot=None):

        """ Apply the models of Coles et al. 2015,

        Viz. estimate scintillation bandwidth and time-scale.

        ss[0,0] = A + B
        ss[n,0] = A exp(-[tau(n)/tau0]^5/3)(N-n)/N
        ss[0,m] = B exp(-[nu(m)/nu0])(M-m)/M
        """
        cs = self.cs
        N = cs.shape[0]
        dataN = cs[:,0]
        lagsN = np.arange(N,dtype=float)
        triangleN = ((N-np.arange(N,dtype=float))/N)

        # exclude 0 pt for double fit
        M = cs.shape[1]
        dataM = cs[0,:]
        lagsM = np.arange(M,dtype=float)
        triangleM = ((M-np.arange(M,dtype=float))/M)

        def chi(p):
            A,B,tau0,nu0 = p
            modelN = B*np.exp(-(lagsN/tau0)**(5./3))*triangleN
            modelM = B*np.exp(-(lagsM/nu0))*triangleM
            modelN[0] += A
            c1 = dataN-modelN
            c2 = (dataM-modelM)[1:] # exclude first pt so not double counted
            return np.append(c1,c2)
        #A,B,tau0,nu0 = leastsq(chi,(dataN[1]-dataN[0],dataM[1],10,10))[0]
        A,B,tau0,nu0 = leastsq(chi,(0,dataM[1],10,10))[0]

        if diag_plot is not None:
            
            modelN = B*np.exp(-(lagsN/tau0)**(5./3))*triangleN
            modelN[0] += A
            modelM = B*np.exp(-(lagsM/nu0))*triangleM
            modelM[0] += A
            import pylab as pl
            import plotting
            pl.figure(2);pl.clf()
            plotting.set_rcParams()
            axt = pl.axes([0.1,0.11,0.87,0.38])
            axt.plot(dataN,label='data',color='k',alpha=0.5,lw=2,marker='o')
            axt.plot(modelN,label='model',color='red',alpha=0.5,lw=2)
            axt.axhline(0,color='k')
            axt.set_xlabel('Lag (subint)',size='large')
            ymax = dataN[1] + 0.1*(dataN[0]-dataN[1])
            xmax = dataN.shape[0]
            axt.axis([-0.1,xmax,-ymax,ymax])
            #pl.legend()
            axf = pl.axes([0.1,0.59,0.87,0.38])
            axf.plot(dataM,label='data',color='k',alpha=0.5,lw=2)
            axf.plot(modelM,label='model',color='red',alpha=0.5,lw=2)
            #pl.legend()
            axf.axhline(0,color='k')
            axf.set_xlabel('Lag (channel)',size='large')
            ymax = dataM[1] + (dataM[0]-dataM[1])*0.1
            xmax = dataM.shape[0]
            axf.axis([-5,xmax,-ymax,ymax])
            if type(diag_plot) == type(''):
                pl.savefig(diag_plot)

        # scale to physical values
        tau0 *= self.ds.times[1] - self.ds.times[0]
        nu0 *= abs(self.ds.freqs[1] - self.ds.freqs[0])
        return tau0,nu0

class SecondarySpectrum(DisplaySpectrum):
    """ Form the secondary spectrum (power spectrum of dynamic spectrum).

    Apply a pre-whitening / post darkening scheme.
    """

    def __init__(self,ds,spind=None,pre_whiten=False):
        self.ds = ds
        im = self.ds.get_mean_subtracted(spind=spind)
        self.pre_whiten = pre_whiten
        if pre_whiten:
            self.image = prew_postd2(im)
        else:
            self.image = np.abs(np.fft.fft2(im))**2

    def get_display(self,**kwargs):
        # TODO -- fix off by one channel
        df = abs(self.ds.freqs[1]-self.ds.freqs[0])
        total_bw = df*self.ds.freqs.shape[0]
        dt = abs(self.ds.times[1]-self.ds.times[0])
        total_dt = dt*self.ds.times.shape[0]
        t_scale = dt#/self.image.shape[0]
        f_scale = df#/self.image.shape[1]
        freqs = np.fft.fftshift(
            np.fft.fftfreq(self.image.shape[1])/f_scale)
        times = np.fft.fftshift(
            np.fft.fftfreq(self.image.shape[0])/t_scale)
        mt = freqs >= 0
        #image = np.abs(np.fft.fftshift(self.image))**2
        image = np.fliplr(np.fft.fftshift(self.image)[:,mt].transpose())
        return times*1e3,'Doppler Shift (mHz)',freqs[mt],'Delay ($\mu$s)',image

    def do_fits(self,spind=-1.5,diag_plot=None):

        """ Apply the models of Coles et al. 2015, in the frequency domain.

        Viz. estimate scintillation bandwidth and time-scale.

        ss[0,0] = A + B
        ss[n,0] = A exp(-[tau(n)/tau0]^5/3)(N-n)/N
        ss[0,m] = B exp(-[nu(m)/nu0])(M-m)/M

        So take the Fourier transform of this model (excluding the window
        function) and then fit to the secondary spectrum.

        As a first stab, ignore the time domain and simply deal with the
        frequency portion, since it has an analytic FFT.
        """

        # exclude 0-frequency, 0-lag
        #if self.pre_whiten:
        #    data = np.abs(self.image[1][1:])#**2 # NB need to square for PSD
        #else:
        #    data = np.abs(self.image[0][1:])#**2 # NB need to square for PSD

        # trim out the edges -- no point in making window function worse
        # than it has to be!
        window = np.mean(self.ds.get_weights(),axis=0)
        print window.shape
        start_idx = 0
        for i in xrange(len(window)):
            if np.all(window[i]==0):
                start_idx += 1
            else:
                break
        stop_idx = 1
        for i in xrange(len(window)):
            if np.all(window[stop_idx]==0):
                stop_idx -= 1
            else:
                break
        print start_idx,stop_idx
        window = window[start_idx:stop_idx]
        im = self.ds.get_mean_subtracted(spind=spind)
        im = im[:,start_idx:stop_idx]
        #data = np.abs(np.fft.fft2(im)[0][1:])**2
        data = np.mean(np.abs(np.fft.fft2(im))**2,axis=0)[1:]

        df = abs(self.ds.freqs[1]-self.ds.freqs[0])
        model_freqs = np.fft.fftfreq(data.shape[0]+1)*(0.5/df)
        freqs = model_freqs[1:]
        # include contribution from channel window
        chan_corr = np.sinc(freqs*(df*np.pi))**2

        def eval_fmodel(p):
            norm,scale,const = p
            model = norm/(model_freqs**2+scale**2)#*chan_corr
            #model = norm/(model_freqs**2+scale**2)
            # convolve with window function
            model = np.fft.fft(np.fft.ifft(model**0.5)*window)
            #tmp = np.fft.ifft(model**0.5)*window
            #model = np.fft.fft(tmp-tmp.mean())
            # remove first freq
            return np.abs(model[1:])**2 + const

        def eval_fmodel_nofix(p):
            norm,scale,const = p
            model = norm/(freqs**2+scale**2)*chan_corr + const
            return model

        def chi(p):
            #return np.log(data/eval_fmodel(p))
            return np.log(data/eval_fmodel_nofix(p))

        max_freq = 0.8 * np.abs(freqs).max()
        const0 = data[np.abs(freqs) > max_freq].mean()
        scale0 = 10*(freqs[1]-freqs[0])*(2*np.pi)
        norm0 = (data[0]-const0)*scale0**2

        norm,scale,const = leastsq(chi,(norm0,scale0,const0))[0]
        scale = abs(scale)

        # const is the white noise level; with this scaling, mean = std
        # use this to approximate error bars on estimate
        x = 2*norm*scale/(freqs**2+scale**2)**2
        inv_var = np.sum(x**2)/const**2
        error = inv_var**-0.5

        # NB -- an easy check to do whether we can detect the scint. bw
        # is to compare the best-fit norm with the const

        if diag_plot is not None:
            import pylab as pl
            pl.figure(1); pl.clf()
            model = eval_fmodel((norm,scale,const))
            a = np.argsort(freqs)
            fw = np.abs(np.fft.fft(window))**2
            window_freqs = np.fft.fftfreq(len(window))
            awindow_freqs = np.argsort(window_freqs)
            #pl.semilogy(window_freqs[awindow_freqs],fw[awindow_freqs],
                #color='green',alpha=0.25,marker='.')
            #pl.semilogy(freqs[a],fw[1:][a]*const,
                #color='green',alpha=0.25,marker='.')
            pl.semilogy(freqs[a],data[a],color='k',alpha=0.5,marker='.')
            pl.semilogy(freqs[a],model[a],color='red',marker='.')
            #pl.semilogy(freqs[a],np.abs(data[a]-model[a]),color='k',alpha=0.5)
            model = eval_fmodel((norm0,scale0,const0))
            pl.semilogy(freqs[a],model[a],color='green')
            model = eval_fmodel_nofix((norm,scale,const))
            pl.semilogy(freqs[a],model[a],color='blue')
            pl.xlabel('Delay (mus)')
            pl.ylabel('Amplitude')
            pl.title('Bandwidth = %.2f'%(1./scale))
            if type(diag_plot)==type(''):
                pl.savefig(diag_plot)

        # bandwidth in MHz... not sure about pi, etc.
        #convert = 1./(2*np.pi)
        convert = 1/(np.pi**2)
        return (convert/scale),(convert/scale)*(error/scale)

    def do_fits_from_auto(self,diag_plot=None):
        t = self.image.copy()
        t[np.isinf(t)] = 0
        cs = np.fft.fft2(t).real
        cs = cs[:cs.shape[0]/2,:cs.shape[1]/2]

        N = cs.shape[0]
        dataN = cs[:,0]
        lagsN = np.arange(N,dtype=float)
        triangleN = ((N-np.arange(N,dtype=float))/N)

        # exclude 0 pt for double fit
        M = cs.shape[1]
        dataM = cs[0,:]
        lagsM = np.arange(M,dtype=float)
        triangleM = ((M-np.arange(M,dtype=float))/M)

        def chi(p):
            A,B,tau0,nu0 = p
            modelN = B*np.exp(-(lagsN/tau0)**(5./3))*triangleN
            modelN -= modelN.mean()
            #modelN[0] += A
            modelM = B*np.exp(-(lagsM/nu0))*triangleM
            #modelM[0] += A
            modelM -= modelM.mean()
            c1 = (dataN-modelN)[1:]
            #c2 = (dataM-modelM)[1:] # exclude first pt so not double counted
            c2 = (dataM-modelM)[1:] # exclude first pt so not double counted
            return np.append(c1-c1.mean(),c2-c2.mean())
            #x = np.append(c1,c2)
            #return x - x.mean()


        #A,B,tau0,nu0 = leastsq(chi,(dataN[1]-dataN[0],dataM[1],10,10))[0]
        A,B,tau0,nu0 = leastsq(chi,(0,dataM[1],10,10))[0]

        if diag_plot is not None:
            
            modelN = B*np.exp(-(lagsN/tau0)**(5./3))*triangleN
            modelN[0] += A
            modelM = B*np.exp(-(lagsM/nu0))*triangleM
            modelM[0] += A
            modelN -= modelN.mean()
            modelM -= modelM.mean()
            #c1 = dataN-modelN
            #c2 = (dataM-modelM)[1:] # exclude first pt so not double counted
            #x = np.append(c1,c2)
            #scale = x.mean()
            #modelN -= scale
            #modelM -= scale
            import pylab as pl
            import plotting
            pl.figure(2);pl.clf()
            plotting.set_rcParams()
            axt = pl.axes([0.1,0.11,0.87,0.38])
            axt.plot(dataN,label='data',color='k',alpha=0.5,lw=2,marker='o')
            axt.plot(modelN,label='model',color='red',alpha=0.5,lw=2)
            axt.axhline(0,color='k')
            axt.set_xlabel('Lag (subint)',size='large')
            ymax = dataN[1] + 0.1*(dataN[0]-dataN[1])
            xmax = dataN.shape[0]
            axt.axis([-0.1,xmax,-ymax,ymax])
            #pl.legend()
            axf = pl.axes([0.1,0.59,0.87,0.38])
            axf.plot(dataM,label='data',color='k',alpha=0.5,lw=2)
            axf.plot(modelM,label='model',color='red',alpha=0.5,lw=2)
            #pl.legend()
            axf.axhline(0,color='k')
            axf.set_xlabel('Lag (channel)',size='large')
            ymax = dataM[1] + (dataM[0]-dataM[1])*0.1
            xmax = dataM.shape[0]
            axf.axis([-5,xmax,-ymax,ymax])
            if type(diag_plot) == type(''):
                pl.savefig(diag_plot)

        # scale to physical values
        full_time = (self.ds.times[1]-self.ds.times[0])*len(self.ds.times)
        dt = full_time/(self.image.shape[0]-1)
        tau0 *= dt
        full_bw = abs(self.ds.freqs[1] - self.ds.freqs[0])*len(self.ds.freqs)
        df = full_bw/(self.image.shape[1]-1)
        nu0 *= df
        return tau0,nu0


def get_flux_data(procobs,verbosity=0):

    # bulletproof
    epoch=flux_freq=flux=fluxerr=flux_scale=chi2=None

    try:
        fname = procobs.obsscr.get_target(procobs.obs)+'.toa.analysis'
        an = cPickle.load(file(fname))
        flux,fluxerr = an.flux,an.fluxerr
        flux_scale = an.scale
        chi2 = an.chi2
        if flux_scale != 'Jansky':
            flux = fluxerr = None
        try:
            flux_freq = an.centre_frequency
        except AttributeError:
            flux_freq = procobs.obs.get_centre_frequency()
    except Exception as e:
        if verbosity > 0:
            print 'could not get flux parameters cause of ',e
        flux,fluxerr,chi2 = None,None,None
    epoch = procobs.obs.get_mjd()
    return epoch,flux_freq,flux,fluxerr,flux_scale,chi2

class ISMState(object):
    """ Encapsulate flux, scintillation parameters, and data quality
    flags to give a snapshot of the ISM for an observation."""

    def __init__(self,procobs,use_tscrunch=False):
        try:
            if use_tscrunch:
                fname = util.change_extension(
                    procobs.obsscr.get_target(procobs.obs),'spectrum')
            else:
                fname = util.change_extension(
                    procobs.obsscr.get_target(procobs.obs),'dynspec')
            self.ds = ds = DynamicSpectrum(fname)
        except Exception as e:
            self.ds = None

        epoch,self.flux_freq,self.flux,self.fluxerr,self.flux_scale,self.chi2 = get_flux_data(procobs)

        self.basename = procobs.obs.basename
        self.scint_params = [None,None]
        self.band = band.Band(procobs.obs)
        self.toa_flags = procobs.obs.get_toa_flags()
        self.tobs = procobs.obs.tobs
        self.bw = abs(procobs.obs['bandwidth'])
        self.nchan = procobs.obs['nchan']
        self.nsub = procobs.obs.nsub

        if self.ds is not None:
            if self.nsub < 2:
                self.tsub = self.tobs
            else:
                self.tsub = ds.times[1] - ds.times[0]
            self.chbw = abs(ds.freqs[1] - ds.freqs[0])
        else:
            self.tsub = self.chbw = None

    def get_scint_params(self,spind=None,diag_plot=None):
        try:
            ds = self.ds
            self.scint_params = ds.get_params(spind=spind,
                diag_plot=diag_plot)
        except Exception as e:
            print 'could not estimate scintillation parameters cause of ',e
        return self.scint_params,self.tsub,self.chbw

def extract_fluxes(procdb,jname,combo='20CM_MULTI'):
    """ Convenience method to extract flux densities."""
    procobs = procdb.get_obs(jname,combo=combo)
    # TODO
    return

def get_mean_power_spectrum(isms,spind=None,diag_plot=None):
    """ Average power spectra weighted according to their observing times.
    """
    tobs = np.asarray([x.tobs for x in isms])
    nchan = np.asarray([x.nchan for x in isms])
    common_nchan = util.most_common(nchan)
    weights = np.asarray(tobs/np.sum(tobs))
    isms = [x for x in isms if x.nchan==common_nchan]
    nsub = [x.nsub for x in isms]
    data = np.empty((np.sum(nsub),common_nchan))
    window = np.empty_like(data)
    current_nsub = 0
    for iism,ism in enumerate(isms):
        d = ism.ds.get_mean_subtracted(spind=spind)
        w = ism.ds.get_weights()
        data[current_nsub:current_nsub+nsub[iism],:] = d
        window[current_nsub:current_nsub+nsub[iism],:] = w * ism.tsub
        current_nsub += nsub[iism]

    start_idx = 0
    for i in xrange(len(window)):
        if np.all(window[:,i]==0):
            start_idx += 1
        else:
            break
    stop_idx = 1
    for i in xrange(len(window)):
        if np.all(window[:,stop_idx]==0):
            stop_idx -= 1
        else:
            break
    window = window[:,start_idx:stop_idx]
    data = data[:,start_idx:stop_idx]

    # correct for tsys
    tsys_corr = get_tsys_correction()[start_idx:stop_idx]
    #tsys_corr_f = (np.abs(np.fft.fft(tsys_corr))**2)[1:]
    tsys_corr_f = (np.abs(np.fft.fft(tsys_corr))**2)[1:]

    pspec = np.zeros(data.shape[1])
    for i in xrange(data.shape[0]):
        pspec += np.abs(np.fft.fft(data[i]))**2*np.mean(window[i])

    total_data = data
    total_window = window
    data = pspec[1:]
    window = np.mean(window,axis=0)

    df = abs(isms[0].ds.freqs[1]-isms[0].ds.freqs[0])
    model_freqs = np.fft.fftfreq(data.shape[0]+1)*(0.5/df)
    freqs = model_freqs[1:]

    def eval_fmodel(p):
        norm,scale,const,tsys_const = p
        model = norm/(model_freqs**2+scale**2)
        # convolve with window function
        model = np.fft.fft(np.fft.ifft(model**0.5)*window)
        # add in additional noise from tsys
        return np.abs(model[1:])**2 + tsys_const*tsys_corr_f + const

    def eval_fmodel_nofix(p):
        norm,scale,const = p
        model = norm/(freqs**2+scale**2) + const
        return model

    def chi(p):
        return np.log(data/eval_fmodel(p))

    const0 = data[np.abs(freqs) > 0.8].mean()
    scale0 = 30*(freqs[1]-freqs[0])
    norm0 = ((data[10]-const0)*scale0**2)/1024
    tsys_const0 = data[0]/tsys_corr_f[0]

    norm,scale,const,tsys_const = leastsq(chi,
        (norm0,scale0,const0,tsys_const0))[0]
    print norm0,scale0,const0,tsys_const0
    print norm,scale,const,tsys_const
    convert = 1./(2*np.pi)
    bw = abs(convert/scale)

    if diag_plot is not None:
        import pylab as pl
        pl.figure(1); pl.clf()
        model = eval_fmodel((norm,scale,const,tsys_const))
        a = np.argsort(freqs)
        fw = np.abs(np.fft.fft(window))**2
        window_freqs = np.fft.fftfreq(len(window))
        awindow_freqs = np.argsort(window_freqs)
        # show window complete with 0-freq delta
        #pl.semilogy(window_freqs[awindow_freqs],fw[awindow_freqs],
            #color='green',alpha=0.25,marker='.')
        # omit zero-freq
        #pl.semilogy(freqs[a],fw[1:][a]*const,
            #color='green',alpha=0.25,marker='.')
        pl.semilogy(freqs[a],data[a],color='k',alpha=0.5,marker='.')
        pl.semilogy(freqs[a],model[a],color='red',marker='.')
        #pl.semilogy(freqs[a],np.abs(data[a]-model[a]),color='k',alpha=0.5)
        model = eval_fmodel((norm0,scale0,const0,tsys_const0))
        pl.semilogy(freqs[a],model[a],color='green')
        model = eval_fmodel_nofix((norm,scale,const))
        pl.semilogy(freqs[a],model[a],color='blue')
        pl.xlabel('Delay (mus)')
        pl.ylabel('Amplitude')
        pl.title('Bandwidth = %.2f'%(bw))
        if type(diag_plot)==type(''):
            pl.savefig(diag_plot)

    # bandwidth in MHz... not sure about pi, etc.
    return bw,pspec,total_data,total_window

def get_tsys_correction():
    """ A very quick and dirty way to correct for the differing variance
    over the band due to changing system temperature.
    """

    fname = '/pulsar/ppta/PPTA_1/fluxcal/20cm/pdfb4_20cm_15.avfluxcal'
    import pyfits
    f = pyfits.open(fname)
    tsys = f['flux_cal'].data.field('s_sys')[0]
    pol0 = tsys[0]
    pol1 = tsys[1]
    q = (pol0**-2+pol1**-2)**-0.5
    q = 0.5*(q[::2]+q[1::2])
    mask = q > 0
    return q/np.mean(q[mask])

def make_light_curve(procdb,jname,combo='20CM_MULTI_PDFB',
        cfreq=None,verbosity=0):
    """ Produce (for now) a 20cm multi-beam light curve with flagged
    and unreliable observations removed.
    """

    # TOA flags to remove
    remove_flags = [
        'lowsn',
        'poor_profile',
        'bad_flux',
        'dfb2_quarter_band',
        'dfb3_quarter_band',
        'catastrophic_rfi',
        'poln_rfi',
        'pdfb4_phase_lock',
        'pdfb4_reset_bug',
        'nocal',
        'bad_config',
    ]

    def flag_filter(pobs):
        toa_flags = [x.split()[0][1:] for x in pobs.obs.get_toa_flags()]
        if len(toa_flags) > 0:
            for toa_flag in toa_flags:
                if toa_flag in remove_flags:
                    return False
        return True

    procobs = procdb.get_obs(jname,combo=combo)
    prev = len(procobs)
    procobs = filter(flag_filter,procobs)
    post = len(procobs)
    if verbosity > 0:
        print 'Removed %d for TOA flags.'%(prev-post)

    if cfreq is not None:
        procobs = [x for x in procobs if x.obs.get_centre_frequency()==cfreq]

    flux_info = map(get_flux_data,procobs)

    return procobs,flux_info

    # bad epochs 54268; 54744.1; -- not sure what these correspond to?

class IndependentDynamicSpectrum(_DynamicSpectrum):
    """ Implement dynamic spectrum computation outside of psrchive."""

    def __init__(self,profile,template):

        import profiles
        import psrchive

        #self.obs = obs
        self.profile = profile
        self.template = template

        # run pat to get phase consistent with TOA analysis
        cmd = 'pat -FT -jp -s %s -R %s'%(template,profile)
        rc,stdout,stderr = util.run(cmd,echo=False)
        try:
            phase_shift = float(
                stdout.split('<-turns')[0].strip().split()[-2])
        except Exception as e:
            phase_shift = 0.
        self.phase_shift = phase_shift

        # load up profile and template
        a_templ = psrchive.Archive_load(template)
        templ_amps = a_templ[0].get_Profile(0,0).get_amps()
        a_profl = psrchive.Archive_load(profile)
        a_profl.pscrunch()
        a_profl.dedisperse()

        nsub = a_profl.get_nsubint()
        nchan = a_profl.get_nchan()

        # rotate profile and template
        t = profiles.rotated(templ_amps,-phase_shift)

        # estimate radiometer noise
        bw = abs(a_profl.get_bandwidth()*1e6/nchan)
        tobs = a_profl[0].get_duration()/a_profl.get_nbin()
        nsamp = 2*bw*tobs
        frac_err = nsamp**-0.5

        results = np.empty([nsub,nchan,4])
        for i in xrange(nsub):
            for j in xrange(nchan):
                #if w == 0:
                    #results[i,j] = 0,0,0,0
                prof = a_profl[i].get_Profile(0,j)
                p = prof.get_amps()
                a,b,sp,st,snr,chi2,flux,fluxerr = profiles.fit_template(
                    p,t,frac_err)
                w = prof.get_weight()
                flux *= w
                fluxerr *= w
                results[i,j] = snr,chi2,flux,fluxerr

        self.results = results

        # make compatible with DynamicSpectrum so can handily inherit
        self.flux = self.results[...,2]
        self.fluxe = self.results[...,3]
        mask = np.isnan(self.flux) | np.isnan(self.fluxe)
        self.flux[mask] = 0
        self.fluxe[mask] = 0
        self.nsub = nsub
        self.nchan = nchan
        i0 = a_profl[0]
        e0 = a_profl[0].get_epoch()
        self.freqs = np.asarray([i0.get_Profile(0,i).get_centre_frequency() for i in xrange(nchan)])
        self.times = np.asarray([(a_profl[i].get_epoch()-e0).in_seconds() for i in xrange(nsub)])
        self.flux_calibrated = a_profl.get_scale() == 'Jansky'
        self.epoch = e0.in_days()
