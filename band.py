"""
Handle "bands", a rather nebulously defined concept in the PPTA and other
timing programs.  Common bands include 10cm, 20cm, 40cm, and 50cm, and each
band typically encapsulates more than one receiver.  Examples:

10cm -- 1050CM receiver -- 3094/3100/etc. MHz
20cm -- MULTI (1369 MHz), H-OH (1433 MHz)
40cm -- 1050CM @ 732 MHz
50cm -- 1050CM @ 685 MHz

author: Matthew Kerr <matthew.kerr@gmail.com>
"""

from jobs import psrsh_segment

def Band(obs):
    """ Factory to construct a Band_Base object for the given Observation.
    """
    if type(obs)==type(''):
        band = eval('Band_%s()'%(obs.lower()))
        return band
    freq = obs.get_centre_frequency()
    if (freq > 1000) and (freq < 1700):
        return Band_20cm()
    elif (freq > 2800) and (freq < 3500):
        return Band_10cm()
    elif (freq > 600) and (freq < 700):
        return Band_50cm()
    elif (freq > 700) and (freq < 800):
        return Band_40cm()
    elif (freq < 500):
        return Band_70cm()
    elif (freq > 5500) and (freq < 6500):
        return Band_5cm()
    elif (freq > 6500 ) and (freq < 10000):
        return Band_3cm()
    return Band_Unknown()

class Band_Base(object):

    def __eq__(self,other):
        return str(self) == str(other)

    def __ne__(self,other):
        return str(self) != str(other)

    def kluge_50cm_match(self,other):
        """ Allow matching of 50cm and 40cm bands."""
        s1 = str(self)
        if (s1=='40CM') or (s1=='50CM'):
            s2 = str(other)
            return (s2=='40CM') or (s2=='50CM')

    def cm(self):
        """ Return wavelength in CM."""
        return int(str(self).rstrip('CM'))

class Band_Unknown(Band_Base):

    def __str__(self):
        return 'UNKNOWN'

class Band_3cm(Band_Base):

    def __str__(self):
        return '3CM'

class Band_5cm(Band_Base):

    def __str__(self):
        return '5CM'

class Band_10cm(Band_Base):

    def __str__(self):
        return '10CM'

    def cm(self):
        return 10

class Band_20cm(Band_Base):

    def __str__(self):
        return '20CM'

    def cm(self):
        return 20

class Band_50cm(Band_Base):

    def __str__(self):
        return '50CM'

    def cm(self):
        return 50

class Band_40cm(Band_50cm):
    """ Inherit from 50cm to allow same RFI excision, etc."""

    def __str__(self):
        return '40CM'

    def cm(self):
        return 40
    
class Band_70cm(Band_Base):

    def __str__(self):
        return '70CM'

    def cm(self):
        return 70
