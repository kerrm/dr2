
def parse_log(fname):
    lines = file(fname).readlines()
    current_cal = None
    current_fluxcal = None
    current_cals = []
    results = dict()
    fluxcals = []
    total_cals = 0

    for line in lines:
        if not line.startswith('fluxcal'):
            continue
        toks = line.split()
        if toks[1] == 'loading':
            current_cal = toks[2]
            total_cals += 1
            continue
        if toks[1] == 'starting':
            current_cals.append(current_cal)
            continue
        if (toks[1] == 'observation') and (toks[2] == 'added'):
            current_cals.append(current_cal)
            continue
        if toks[1] == 'unloading':
            fluxcal = toks[2]        
            results[fluxcal] = current_cals
            current_cals = []
            fluxcals.append(fluxcal)
            continue

    print 'Total calibrators: %d.'%(total_cals)
    assoc = sum((len(results[x]) for x in results.keys()))
    print 'Calibrators associated with fluxcal: %d.'%(assoc)

    return fluxcals,results
