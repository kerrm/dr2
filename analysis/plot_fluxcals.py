import numpy as np
import pylab as pl
from matplotlib import cm
from matplotlib.ticker import MaxNLocator
import pyfits
import glob
from scipy import interpolate
import os
from collections import deque

import cubehelix

import observation
import util
import plotting
plotting.set_viridis()
plotting.set_rcParams(ticklabelsize='large')
colors = plotting.get_tableau20()

a_bad_cals = ['a051223_134247','a061120_203713','a070811_234256',
    'a071004_192822','a051110_175758','a051108_223057','a050905_025115',
    'a050906_223016','a060412_095038','a061105_212426','a060409_060712',
    'a060411_061407']
r_bad_cals = ['r071107_172715','r100106_181435','r070811_234157',
    'r070805_010217','r071012_190805','r091215_135345','r090829_212932','r090701_023745','r080301_090425','r080615_062006','r090522_073346','r070316_101523','r070523_065726','r070524_070046']
r_bad_cals.append('r070814_013009')
# weird frequency corruption for this one?
s_bad_cals = ['s110913_221315','s100713_002024','s121006_233955']
t_bad_cals = ['t081201_154253','t090425_090145','t090609_052325','t090728_042539','t091216_172333','t110913_221312','t130413_094404','t100425_064450','t111218_140600']
w_bad_cals = ['w041219_175121','w050902_234004','w040923_235751','w050417_064412','w040912_220931']
#t_bad_cals.extend(['t150319_095246','t150520_102404'])
bad_cals = a_bad_cals + r_bad_cals + s_bad_cals + t_bad_cals + w_bad_cals

def parse_avfluxcals():
    lines = util.readlines('avfluxcals.asc')
    goodcals = [x for x in lines if not x[0]=='@']
    return goodcals

def read_vals(fluxcal):
    f = pyfits.open(fluxcal)
    func = f['flux_cal'].data.field
    freqs = func('dat_freq')[0]
    if np.any(freqs==0):
        chbw = freqs[1]-freqs[0]
        freqs = freqs[0] + np.arange(len(freqs))*chbw
    nchan = len(freqs)
    #wts = func('dat_wts')
    vals = np.empty([4,2,nchan])
    keys = ['s_sys','s_syserr','s_cal','s_calerr']
    for ikey,key in enumerate(keys):
        vals[ikey] = func(key)[0]
    epoch = float(f['flux_cal']._header['epoch'])
    f.close()
    return epoch,freqs,vals#,wts

def read_fluxcal(fluxcal,cfreq=None,rcvr=None,mjd0=None,mjd1=None):
    if os.path.basename(fluxcal).split('.cf')[0] in bad_cals:
        #print 'Skipping %s.'%fluxcal
        return
    obs = observation.Observation(fluxcal)
    cf = obs.get_centre_frequency()
    rx = obs['frontend']
    mjd = obs.get_mjd()
    if (rcvr is not None) and (rcvr != rx):
        return
    if (cfreq is not None) and (cfreq != cf):
        return
    if (mjd0 is not None) and (mjd < mjd0):
        return
    if (mjd1 is not None) and (mjd > mjd1):
        return
    results = read_vals(fluxcal)
    epoch,freqs,vals = results
    #if len(freqs) < 512:
    #    return
    return results
    
def make_evolution_plot(fluxcals,cfreq=None,rcvr=None,mjd0=None,mjd1=None):
    epochs = deque()
    freqs = deque()
    vals = deque()
    accepted = deque()
    for fluxcal in fluxcals:
        try:
            e,f,v = read_fluxcal(fluxcal,cfreq=cfreq,rcvr=rcvr,mjd0=mjd0,mjd1=mjd1)
        except TypeError:
            continue
        epochs.append([e]*len(f))
        freqs.append(f)
        vals.append(v)
        accepted.append(fluxcal)
    return np.asarray(epochs),np.asarray(freqs),vals,accepted

def make_trace_plot(fluxcals,cfreq=None,rcvr=None,mjd0=None,mjd1=None,
    get_vals=False,fignum=1):

    epochs,freqs,vals,accepted = make_evolution_plot(
        fluxcals,cfreq=cfreq,rcvr=rcvr,mjd0=mjd0,mjd1=mjd1)

    print 'Plotting %d traces.'%(len(epochs))

    from plotting import get_tableau20,set_viridis
    cmap = set_viridis()
    colors = get_tableau20()

    tmin = np.min(epochs)
    tmax = np.max(epochs)
    # deal with stupid gridding...
    try:
        tmin = tmin[0]
        tmax = tmax[0]
    except IndexError:
        pass
    pl.figure(fignum); pl.clf()
    #cmap = cm.copper
    #cmap = cubehelix.cmap()
    for i in xrange(len(epochs)):
        color = cm.copper((epochs[i][0]-tmin)/(tmax-tmin))
        #color = cmap((epochs[i]-tmin)/(tmax-tmin))
        #color_idx = int(round((epochs[i]-tmin)/(tmax-tmin)*len(colors))*0.999)
        #color = colors[color_idx]
        v = vals[i][2][0]
        mask = v > 0
        print mask.sum(),len(mask),accepted[i]
        pl.plot(freqs[i][mask],v[mask],color=color,alpha=0.5)
        v = vals[i][2][1]
        mask = v > 0
        pl.plot(freqs[i][mask],v[mask],color=color,alpha=0.5)
        #pl.errorbar(freqs[i][mask],v[mask],yerr=vals[i][3][0][mask],color=color,alpha=0.5)

    pl.axis([pl.axis()[0],pl.axis()[1],300,1200])

    if get_vals:
        return epochs,freqs,vals,accepted

def make_trace_plot2(fluxcals,cfreq=None,rcvr=None,mjd0=None,mjd1=None,
    get_vals=False,fignum=1):

    epochs,freqs,vals,accepted = make_evolution_plot(
        fluxcals,cfreq=cfreq,rcvr=rcvr,mjd0=mjd0,mjd1=mjd1)

    epochs = np.asarray([x[0] for x in epochs])
    common_size = util.most_common([len(x) for x in freqs])
    print 'Common size: ',common_size
    for i in xrange(len(accepted)):
        if len(freqs[i]) != common_size:
            print 'Excluding %s due to unusual resolution.'%(accepted[i])
    epochs = np.asarray([x for x,y in zip(epochs,freqs) if len(y)==common_size])
    vals = np.asarray([x for x,y in zip(vals,freqs) if len(y)==common_size])
    accepted = [x for x,y in zip(accepted,freqs) if len(y)==common_size]
    freqs = np.asarray([x for x in freqs if len(x)==common_size])
    a = np.argsort(epochs)
    epochs = epochs[a]
    vals = vals[a,...]
    freqs = freqs[a,...]
    accepted = np.asarray(accepted)[a]

    print 'Plotting %d traces.'%(len(epochs))
    #return epochs,freqs,vals,accepted
    tcalAA = vals[:,2,0,:]
    tcalBB = vals[:,2,1,:]
    mvalAA = np.median(tcalAA,axis=0)
    mvalBB = np.median(tcalBB,axis=0)

    scale = np.std(mvalAA[1:]-mvalAA[:-1])*2
    #print scale
    scale = 300
    freq_offs = 250 if freqs[0].min() > 2000 else 70
    fleft = freqs[0].min()

    pl.figure(fignum); pl.clf()
    for i in xrange(len(epochs)):
        offs = scale*i
        mask = tcalAA[i] > 0
        pl.plot(freqs[i][mask],(tcalAA[i]-mvalAA)[mask] + offs,
            color='blue',alpha=0.5)
        mask = tcalBB[i] > 0
        pl.plot(freqs[i][mask],(tcalBB[i]-mvalBB)[mask] + offs,
            color='red',alpha=0.5)
        pl.plot([freqs[i].min(),freqs[i].max()],[offs,offs],color='k',alpha=0.5,ls='-',marker=None)
        pl.text(fleft-freq_offs,offs,os.path.basename(accepted[i]).split('.')[0])
        #pl.errorbar(freqs[i][mask],v[mask],yerr=vals[i][3][0][mask],color=color,alpha=0.5)

    #pl.axis([pl.axis()[0],pl.axis()[1],300,1200])
    pl.axis([freqs[0].min()-freq_offs*1.05,freqs[0].max()+10,-0.5*scale,len(epochs)*scale-0.5*scale])

    if get_vals:
        return epochs,freqs,vals,accepted
    
def make_interp_plot(fluxcals,cfreq=None,rcvr=None,fignum=1,
    vmin=None,vmax=None,plot_tsys=False,get_vals=False,polA=True,
    plot_arrows=True):
    epochs,freqs,vals,accepted = make_evolution_plot(
        fluxcals,cfreq=cfreq,rcvr=rcvr)
    if plot_tsys:
        t0 = np.concatenate(np.asarray([x[0][0] for x in vals]))
        t1 = np.concatenate(np.asarray([x[0][1] for x in vals]))
    else:
        t0 = np.concatenate(np.asarray([x[2][0] for x in vals]))
        t1 = np.concatenate(np.asarray([x[2][1] for x in vals]))
    t0[t0==0] = np.nan
    flat_epochs = np.concatenate(epochs)
    flat_freqs = np.concatenate(freqs)
    coords = np.asarray([flat_epochs,flat_freqs]).transpose()
    grid_epochs = np.linspace(flat_epochs.min(),flat_epochs.max(),500)
    grid_freqs = np.linspace(flat_freqs.min(),flat_freqs.max(),1024)
    xv,yv = np.meshgrid(grid_epochs,grid_freqs)
    output = np.asarray([np.ravel(xv),np.ravel(yv)]).transpose()
    h0 = interpolate.griddata(coords,t0,output,method='nearest')
    h1 = interpolate.griddata(coords,t1,output,method='nearest')
    #vmin = np.min(t0[t0>0])
    pl.figure(fignum); pl.clf()
    #ax = pl.gca()
    ax = pl.axes([0.12,0.1,0.88,0.84])
    to_plot = h0 if polA else h1
    to_plot *= 1e-3 # convert to Jy
    cax = ax.imshow(to_plot.reshape((1024,500)),aspect='auto',
        vmin=vmin,vmax=vmax,
        extent=[grid_epochs[0],grid_epochs[-1],grid_freqs[-1],grid_freqs[0]],
        interpolation='nearest',cmap='viridis')
    ax.set_xlabel('MJD',size='x-large')
    ax.set_ylabel('Frequency (MHz)',size='x-large')
    # plot observation epochs
    unique_epochs = sorted(set(flat_epochs))
    bw = grid_freqs[-1]-grid_freqs[0]
    ymin = grid_freqs[0]
    #ax.axis([grid_epochs[0],grid_epochs[-1],grid_freqs[-1],grid_freqs[0]]) 
    #ax.axis([53951.1,55326.3,grid_freqs[-1],grid_freqs[0]]) 
    if plot_arrows:
        for epoch in unique_epochs:
            ax.arrow(epoch,ymin,0,0.04*bw,fc='k',ec='k',
                head_width=0.3,head_length=0.3)
    pl.colorbar(cax)
    if get_vals:
        return epochs,freqs,vals,accepted

def make_six_month_trace_plots(fluxcals,twelve_months=False):
    mjds = np.asarray([util.atnf_to_mjd(x) for x in fluxcals])
    a = np.argsort(mjds)
    tstart = mjds[a][0]
    date = util.mjd_to_date(tstart)
    partitioned = deque()
    # move to prior six-month boundary
    start_year = date.year
    if (date.month < 7) or twelve_months:
        start_month = 1
    else:
        start_month = 7
    for i in xrange(100):
        mjd_start = util.date_to_mjd(start_year,start_month,1)
        if (start_month == 1) and (not twelve_months):
            mjd_stop = util.date_to_mjd(start_year,7,1)
        else:
            mjd_stop = util.date_to_mjd(start_year+1,1,1)
        if start_year > 2017:
            break
        #print mjd_start,mjd_stop
        mask = (mjds >= mjd_start) & (mjds < mjd_stop)
        if np.any(mask):
            cals = [x for x,y in zip(fluxcals,mask) if y]
            partitioned.append(cals)
            make_trace_plot2(cals,fignum=i)
            pl.title('%d %d'%(start_year,start_month))
        if (start_month == 1) and (not twelve_months):
            start_month = 7
        else:
            start_year += 1
            start_month = 1
    return partitioned


fluxcals = sorted(glob.glob('/home/ker14a/research/tmp/ipta_plots/fluxcals/*fluxcal'))
a_fluxcals = sorted(glob.glob('/home/ker14a/research/tmp/ipta_plots/fluxcals/a*fluxcal'))
r_fluxcals = sorted(glob.glob('/home/ker14a/research/tmp/ipta_plots/fluxcals/r*fluxcal'))
s_fluxcals = sorted(glob.glob('/home/ker14a/research/tmp/ipta_plots/fluxcals/s*fluxcal'))
t_fluxcals = sorted(glob.glob('/home/ker14a/research/tmp/ipta_plots/fluxcals/t*fluxcal'))
w_fluxcals = sorted(glob.glob('/home/ker14a/research/tmp/ipta_plots/fluxcals/w*fluxcal'))
good_cals = set(parse_avfluxcals())
good_t_fluxcals = [x for x in t_fluxcals if os.path.basename(x) in good_cals]
good_r_fluxcals = [x for x in r_fluxcals if os.path.basename(x) in good_cals]
good_a_fluxcals = [x for x in a_fluxcals if os.path.basename(x) in good_cals]
#r_fluxcals = sorted(glob.glob('/tmp/fluxcals/r*fluxcal'))
#make_trace_plot(a_fluxcals,cfreq=1369,rcvr='MULTI',fignum=1)
#make_trace_plot(r_fluxcals,cfreq=1369,rcvr='MULTI',fignum=2)
#make_interp_plot(a_fluxcals,cfreq=1369,rcvr='MULTI',fignum=3)
#make_interp_plot(r_fluxcals,cfreq=1369,rcvr='MULTI',fignum=4)
#make_interp_plot(t_fluxcals,cfreq=3100,rcvr='1050CM',fignum=3,vmin=300,vmax=900)
#make_interp_plot(t_fluxcals,cfreq=3100,rcvr='1050CM',fignum=3,vmin=16000,vmax=36000,plot_tsys=True)

#out_x = np.ravel(np.concatenate([[x]*len(f) for f in freqs]))
#out_f = np.ravel(freqs)
#out_coords = np.asarray([out_x,out_f]).transpose()

# make plots for IPTA meeting

#make_interp_plot(good_t_fluxcals,cfreq=1369,rcvr='MULTI',vmin=600,vmax=1100,plot_tsys=False,get_vals=False,fignum=1)
#make_interp_plot(good_t_fluxcals,cfreq=1369,rcvr='MULTI',vmin=16000,vmax=27000,plot_tsys=True,get_vals=False,fignum=2)

# example of flux cal testing
#epochs,freqs,vals,accepted = make_interp_plot(a_fluxcals,cfreq=1433,rcvr='MULTI',plot_tsys=False,get_vals=True,fignum=10,polA=True)
#epochs,freqs,vals,accepted = make_interp_plot(a_fluxcals,cfreq=1433,rcvr='MULTI',plot_tsys=False,get_vals=True,fignum=11,polA=False)

#epochs,freqs,vals,accepted = make_interp_plot(a_fluxcals,cfreq=1369,rcvr='MULTI',plot_tsys=False,get_vals=True,fignum=10,polA=True)
#epochs,freqs,vals,accepted = make_interp_plot(a_fluxcals,cfreq=1369,rcvr='MULTI',plot_tsys=False,get_vals=True,fignum=11,polA=False)

#epochs,freqs,vals,accepted = make_interp_plot(w_fluxcals,cfreq=1433,rcvr='MULTI',plot_tsys=False,get_vals=True,fignum=10,polA=True)
#epochs,freqs,vals,accepted = make_interp_plot(w_fluxcals,cfreq=1433,rcvr='MULTI',plot_tsys=False,get_vals=True,fignum=11,polA=False)

#epochs,freqs,vals,accepted = make_interp_plot(r_fluxcals,cfreq=1369,rcvr='MULTI',plot_tsys=False,get_vals=True,fignum=10,polA=True)
#epochs,freqs,vals,accepted = make_interp_plot(r_fluxcals,cfreq=1369,rcvr='MULTI',plot_tsys=False,get_vals=True,fignum=11,polA=False)

# make plots for papers
#epochs,freqs,vals,accepted = make_interp_plot(a_fluxcals+r_fluxcals+good_t_fluxcals,cfreq=1369,rcvr='MULTI',plot_tsys=False,get_vals=True,fignum=10,polA=True,vmax=1.2,vmin=0.4,plot_arrows=False)
#pl.savefig('/tmp/multi_hydra_tcal.pdf')
epochs,freqs,vals,accepted = make_interp_plot(a_fluxcals+r_fluxcals+good_t_fluxcals,cfreq=1369,rcvr='MULTI',plot_tsys=True,get_vals=True,fignum=11,polA=True,vmax=28,vmin=14,plot_arrows=False)
sefd_multi = np.median(np.asarray([x for x in vals if x.shape[-1]==2048]),axis=0)
freq_multi = np.median(np.asarray([y for x,y in zip(vals,freqs) if x.shape[-1]==2048]),axis=0)
#pl.savefig('/tmp/multi_hydra_tsys.pdf')
#epochs,freqs,vals,accepted = make_interp_plot(good_t_fluxcals,cfreq=3100,rcvr='1050CM',plot_tsys=True,get_vals=True,fignum=12,polA=True,vmax=36,vmin=18)

epochs,freqs,vals,accepted = make_interp_plot(good_r_fluxcals+good_t_fluxcals,cfreq=3100,rcvr='1050CM',plot_tsys=True,get_vals=True,fignum=12,polA=True,vmax=36,vmin=18,plot_arrows=False)
pl.savefig('/tmp/10cm_hydra_tsys.pdf')
sefd_10 = np.median(np.asarray([x for x in vals if x.shape[-1]==2048]),axis=0)
freq_10 = np.median(np.asarray([y for x,y in zip(vals,freqs) if x.shape[-1]==2048]),axis=0)
epochs,freqs,vals,accepted = make_interp_plot(good_r_fluxcals+good_t_fluxcals,cfreq=3100,rcvr='1050CM',plot_tsys=False,get_vals=True,fignum=13,polA=True,vmax=0.9,vmin=0.4,plot_arrows=False)
pl.savefig('/tmp/10cm_hydra_tcal.pdf')

#epochs,freqs,vals,accepted = make_interp_plot(t_fluxcals,cfreq=1369,rcvr='H-OH',plot_tsys=True,get_vals=True,fignum=11,polA=True)
epochs,freqs,vals,accepted = make_interp_plot(a_fluxcals,cfreq=1433,rcvr='H-OH',plot_tsys=True,get_vals=True,fignum=11,polA=True)
sefd_hoh = np.median(np.asarray([x for x in vals if x.shape[-1]==512]),axis=0)
freq_hoh = np.median(np.asarray([y for x,y in zip(vals,freqs) if x.shape[-1]==512]),axis=0)

epochs,freqs,vals,accepted = make_interp_plot(s_fluxcals,cfreq=732,rcvr='1050CM',plot_tsys=True,get_vals=True,fignum=12,polA=True,vmax=50,vmin=25,plot_arrows=False)
ax = pl.gca()
ax.xaxis.set_major_locator(MaxNLocator(6))
pl.savefig('/tmp/40cm_hydra_tsys.pdf')
sefd_40 = np.median(np.asarray([x for x in vals if x.shape[-1]==1024]),axis=0)
freq_40 = np.median(np.asarray([y for x,y in zip(vals,freqs) if x.shape[-1]==1024]),axis=0)
epochs,freqs,vals,accepted = make_interp_plot(s_fluxcals,cfreq=732,rcvr='1050CM',plot_tsys=False,get_vals=True,fignum=13,polA=True,vmax=7,vmin=4,plot_arrows=False)
ax = pl.gca()
ax.xaxis.set_major_locator(MaxNLocator(6))
pl.savefig('/tmp/40cm_hydra_tcal.pdf')

# make a nice plot of SEFDs
plotting.set_rcParams(ticklabelsize='medium')
pl.figure(200,(10,5)); pl.clf()

ax0 = pl.axes([0.08,0.12,0.26,0.8])
mask = (sefd_40[0,0] > 0)
ax0.plot(freq_40[mask],1e-3*sefd_40[0,0][mask],ls='-',color='k')
avg_40_0 = (sefd_40[0,0][mask]).mean()
#ax1.axhline(1e-3*avg_multi_0,xmin=0.4,xmax=0.6,ls='-',color='k')
mask = (sefd_40[0,1] > 0)
ax0.plot(freq_40[mask],1e-3*sefd_40[0,1][mask],ls='--',color='k')
avg_40_1 = (sefd_40[0,1][mask]).mean()
#ax1.axhline(1e-3*avg_multi_1,xmin=0.4,xmax=0.6,ls='--',color='k')
print '40cm SEFD:',0.5*(avg_40_0+avg_40_1)
ax0.axis([732-32,732+32,27,38])
ax0.set_xticks([700,720,740,760])
ax0.set_xlabel('Frequency (MHz)',size='large')
ax0.set_ylabel('System Equivalent Flux Density (Jy)',size='large')

ax1 = pl.axes([0.41,0.12,0.26,0.8])
mask = (sefd_multi[0,0] > 0)
ax1.plot(freq_multi[mask],1e-3*sefd_multi[0,0][mask],ls='-',color='k')
avg_multi_0 = (sefd_multi[0,0][mask]).mean()
#ax1.axhline(1e-3*avg_multi_0,xmin=0.4,xmax=0.6,ls='-',color='k')
mask = (sefd_multi[0,1] > 0)
ax1.plot(freq_multi[mask],1e-3*sefd_multi[0,1][mask],ls='--',color='k')
avg_multi_1 = (sefd_multi[0,1][mask]).mean()
#ax1.axhline(1e-3*avg_multi_1,xmin=0.4,xmax=0.6,ls='--',color='k')
print 'Multi-beam SEFD:',0.5*(avg_multi_0+avg_multi_1)
ax1.axis([1369-128,1433+128,13,26])
ax1.set_xlabel('Frequency (MHz)',size='large')
#ax1.set_ylabel('System Equivalent Flux Density (Jy)',size='x-large')

mask = (sefd_hoh[0,0] > 0)
ax1.plot(freq_hoh[mask],1e-3*sefd_hoh[0,0][mask],ls='-',color='gray')
avg_hoh_0 = (sefd_hoh[0,0][mask]).mean()
#ax1.axhline(1e-3*avg_hoh_0,xmin=0.4,xmax=0.6,ls='-',color='gray')
mask = (sefd_hoh[0,1] > 0)
ax1.plot(freq_hoh[mask],1e-3*sefd_hoh[0,1][mask],ls='--',color='gray')
avg_hoh_1 = (sefd_hoh[0,1][mask]).mean()
#ax1.axhline(1e-3*avg_hoh_1,xmin=0.4,xmax=0.6,ls='--',color='gray')
print 'H-OH SEFD:',0.5*(avg_hoh_0+avg_hoh_1)
ax1.axis([1369-128,1433+128,13,26])
#ax1.set_xlabel('Frequency (MHz)',size='x-large')
#ax1.set_ylabel('System Equivalent Flux Density (Jy)',size='x-large')
ax1.set_xticks([1250,1350,1450,1550])

ax2 = pl.axes([0.73,0.12,0.26,0.8])
mask = (sefd_10[0,0] > 0)
ax2.plot(freq_10[mask],1e-3*sefd_10[0,0][mask],ls='-',color='k')
avg_10_0 = (sefd_10[0,0][mask]).mean()
#ax2.axhline(1e-3*avg_10_0,xmin=0.4,xmax=0.6,ls='-',color='k')
mask = (sefd_10[0,1] > 0)
ax2.plot(freq_10[mask],1e-3*sefd_10[0,1][mask],ls='--',color='k')
avg_10_1 = (sefd_10[0,1][mask]).mean()
print '10cm SEFD:',0.5*(avg_10_0+avg_10_1)
#ax2.axhline(1e-3*avg_10_1,xmin=0.4,xmax=0.6,ls='--',color='k')
ax2.axis([3100-512,3100+512,19,40])
ax2.set_xlabel('Frequency (MHz)',size='large')
ax2.set_xticks([2600,3000,3400])
#ax2.yaxis.set_ticklabels(['']*len(ax2.yaxis.get_ticklabels()))
pl.savefig('/tmp/sefds.pdf')


plot_cals = [x for x in w_fluxcals if os.path.basename(x) in 
['w050523_071852.cf.fluxcal',
'w050524_072141.cf.fluxcal',
'w050528_050417.cf.fluxcal',
'w050731_230129.cf.fluxcal',
'w050905_025117.cf.fluxcal',
'w050906_223016.cf.fluxcal']]
"""
# make custom trace plots
plot_cals = [x for x in a_fluxcals if os.path.split(x)[1] in 
['a060211_101647.cf.fluxcal',
'a060303_121925.cf.fluxcal',
'a060305_091028.cf.fluxcal',
'a060322_093427.cf.fluxcal',
'a060411_071728.cf.fluxcal',
'a060421_100947.cf.fluxcal',
'a060509_050607.cf.fluxcal',
'a060510_071327.cf.fluxcal',
'a060511_095337.cf.fluxcal']]

plot_cals = [x for x in a_fluxcals if os.path.split(x)[1] in 
['a060527_082717.cf.fluxcal',
'a060528_090047.cf.fluxcal',
'a060614_052657.cf.fluxcal',
'a060614_055817.cf.fluxcal',
'a060701_031037.cf.fluxcal',
#'a060804_031943.cf.fluxcal',
#'a060804_224811.cf.fluxcal',
'a060808_043213.cf.fluxcal',
'a060808_225711.cf.fluxcal']]

plot_cals = [x for x in a_fluxcals if os.path.split(x)[1] in 
['a060821_215756.cf.fluxcal',
'a060917_014406.cf.fluxcal',
'a060930_222743.cf.fluxcal',
'a061003_225516.cf.fluxcal',
'a061018_202926.cf.fluxcal',
'a061101_171236.cf.fluxcal',
'a061125_200346.cf.fluxcal',
'a061126_194126.cf.fluxcal',]]

plot_cals = [x for x in a_fluxcals if os.path.split(x)[1] in 
['a070605_055616.cf.fluxcal',
'a070616_020046.cf.fluxcal',
'a070626_051045.cf.fluxcal',
'a070715_043201.cf.fluxcal',
'a070718_064036.cf.fluxcal',
'a070725_011801.cf.fluxcal',
'a070805_010213.cf.fluxcal',
'a070811_231956.cf.fluxcal',
'a070827_234501.cf.fluxcal',]]
plot_cals = [x for x in a_fluxcals if os.path.split(x)[1] in 
['a070916_220831.cf.fluxcal',
'a071001_200516.cf.fluxcal',
'a071012_190801.cf.fluxcal',
'a071025_010202.cf.fluxcal',
'a071107_172711.cf.fluxcal',
'a071202_194633.cf.fluxcal',
'a071217_143511.cf.fluxcal',
'a071230_133353.cf.fluxcal']]
"""
plot_cals = [x for x in r_fluxcals if os.path.split(x)[1] in 
['r070714_042734.cf.fluxcal',
#'r070915_234925.cf.fluxcal',
'r071013_224537.cf.fluxcal',
'r071107_170806.cf.fluxcal',
'r071204_151125.cf.fluxcal',
'r071218_143654.cf.fluxcal',
'r071231_133727.cf.fluxcal',
'r080122_122445.cf.fluxcal',
'r080211_115437.cf.fluxcal']]
plot_cals = [x for x in r_fluxcals if os.path.split(x)[1] in 
['r080227_085751.cf.fluxcal',
#'r080302_091055.cf.fluxcal',
'r080321_083003.cf.fluxcal',
'r080414_115432.cf.fluxcal',
'r080524_055333.cf.fluxcal',
'r080627_071328.cf.fluxcal',
'r080628_010346.cf.fluxcal']]
plot_cals = [x for x in r_fluxcals if os.path.split(x)[1] in 
['r090220_102753.cf.fluxcal',
'r090221_144824.cf.fluxcal',
#'r090317_150853.cf.fluxcal',
'r090522_091153.cf.fluxcal',
'r090522_094323.cf.fluxcal',]]
plot_cals = [x for x in r_fluxcals if os.path.split(x)[1] in 
['r090730_025455.cf.fluxcal',
'r090812_051743.cf.fluxcal',
'r090813_001545.cf.fluxcal',
'r090830_224603.cf.fluxcal',
'r091013_180223.cf.fluxcal',
'r091110_161022.cf.fluxcal',
'r091129_150701.cf.fluxcal',
'r091216_172333.cf.fluxcal'
'r100221_133233.cf.fluxcal',
'r100315_124313.cf.fluxcal',
'r100512_050341.cf.fluxcal']]
