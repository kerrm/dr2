""" Study the stability of differential gain over the course of an
observation based primarily on observations of the noise diode before
and after.

NB there is some additional code in profiles.py for studying the baselines
of the observations themselves.
"""

import numpy as np
import pyfits
import os
from os.path import join,isdir
from collections import deque

import util

def get_gain(fname):
    f1 = pyfits.open(fname)
    l = f1['feedpar'].data.field
    gain1,dgain1,dphase1 = l('data')[0].transpose()
    gain1e,dgain1e,dphase1e = l('dataerr')[0].transpose()
    freq1 = f1['feedpar'].data.field('dat_freq')[0]
    f1.close()
    return freq1,gain1,dgain1,dphase1,gain1e,dgain1e,dphase1e

def compare_cals(procobs,noplot=False,clobber=False,fignum=2,outplot=None,
    quiet=False,nomask=False):
    """ Compare diode cals before and after observation.

    If calibrator before and after observation are consistent
    (same levels, same position), form a "pacv" calibrator solution and
    compare the differential gain amplitude and phase.
    """

    if procobs.cal is None:
        if not quiet:
            print 'No valid calibrators found.'
        return
    comp = procobs.cal.get_diode_comparison(verbosity= not quiet)
    if comp is not None:
        c1,c2 = comp # two processed cals
    else:
        if not quiet:
            print 'No valid calibrators found.'
        return
    # get calibrators
    p1 = '.'.join(c1.split('.')[:-1]) + '.pacv'
    p2 = '.'.join(c2.split('.')[:-1]) + '.pacv'
    if clobber or (not os.path.isfile(p1)):
        util.run('pacv -D/NULL -u %s'%c1,echo=False)
    if clobber or (not os.path.isfile(p2)):
        util.run('pacv -D/NULL -u %s'%c2,echo=False)

    freq1,gain1,dgain1,dphase1,gain1e,dgain1e,dphase1e = get_gain(p1)
    freq2,gain2,dgain2,dphase2,gain2e,dgain2e,dphase2e = get_gain(p2)

    # convert to useful units
    # differential gain in "hyperbolic radians" -- I *think* this converts
    # to fractional differences
    dgain1 = (np.exp(dgain1*2)-1)
    dgain2 = (np.exp(dgain2*2)-1)
    dg = gain2 - gain1
    ddg = dgain2 - dgain1
    ddp = dphase2 - dphase1

    mad = lambda x: 1.4826*np.median(np.abs(x-np.median(x)))

    def mask_outliers(x,thresh=5):
        m = ~np.isnan(x)
        if nomask:
            return m
        t = np.sort(x[m])[0.1*m.sum():0.9*m.sum()]
        s = mad(t)
        mu = np.median(t)
        return m & (np.abs(x-mu)/s < thresh)

    mask_dg = mask_outliers(dg)
    mask_ddg = mask_outliers(ddg)
    mask_ddp = mask_outliers(ddp)

    try:
        mg = np.average(dg[mask_dg],
            weights=(gain2e**2+gain1e**2)[mask_dg]**-2)
    except ZeroDivisionError:
        mg = 0
    try:
        mdg = np.average(ddg[mask_ddg],
            weights=(dgain2e**2+dgain1e**2)[mask_ddg]**-2)
    except ZeroDivisionError:
        mdg = 0
    try:
        mdp = np.average(ddp[mask_ddp],
            weights=(dphase2e**2+dphase1e**2)[mask_ddp]**-2)
    except ZeroDivisionError:
        mdp = 0
    if noplot:
        return mg,mdg,mdp

    import pylab as pl
    pl.figure(fignum); pl.clf()
    ax = pl.axes([0.12,0.12,0.77,0.81])
    #ax.plot(freq1,gain2-gain1,color='gray',label='<$\Delta$Gain>=%.3f'%mg)
    x1,y1 = freq1[mask_ddg],ddg[mask_ddg]
    label1 = r'<$\Delta$DGainx100>=%.3f'%(mdg*100)
    lines1 = ax.plot(x1,y1,color='gray',label=label1)
    #ax.plot(x1,y1,color='gray')
    ax.axhline(mdg,color='gray')
    x2,y2 = freq1[mask_ddp],ddp[mask_ddp]
    ax_2 = ax.twinx()
    label2 = r'<$\Delta$DPhase(deg)>=%.3f'%(np.degrees(mdp))
    lines2 = ax_2.plot(x2,np.degrees(y2),color='salmon',label=label2)
    #ax_2.plot(x2,np.degrees(y2),color='salmon')
    ax_2.axhline(np.degrees(mdp),color='salmon')
    ax.set_xlabel('Frequency (MHz)',size='x-large')
    ax.set_ylabel('$\Delta$ DGain',size='x-large')
    ax_2.set_ylabel('$\Delta$ DPhase (deg.)',size='x-large')
    ax.axhline(0,color='gray',lw=2,ls='--')
    ax_2.axhline(0,color='salmon',lw=2,ls='--')
    #s1 = np.median(np.abs(y1-np.median(y1)))*1.4826
    #s2 = np.median(np.abs(y2-np.median(y2)))*1.4826
    #s = max(s1,s2)
    #ymin = min(-0.01,min(mdg,mdp)-2*s)
    #ymax = max(0.01,max(mdg,mdp)+2*s)
    ymin = ax.axis()[2]
    ymax = ax.axis()[3]
    xmin = np.min(freq1)-abs(freq1[1]-freq1[0])
    xmax = np.max(freq1)+abs(freq1[1]-freq1[0])
    ax.axis([xmin,xmax,ymin,ymax])
    ymin = ax_2.axis()[2]
    ymax = ax_2.axis()[3]
    ax_2.axis([xmin,xmax,ymin,ymax])
    title = '%s / %s / %s'%(procobs.obs['name'],
        procobs.cal.diodes[0].basename,
        procobs.cal.diodes[1].basename)
    ax.set_title(title)

    lines = [lines1[0],lines2[0]]
    labels = [label1,label2]
    pl.legend(lines,labels)
    if outplot is not None:
        pl.savefig(outplot)
    return mg,mdg,mdp

def compare_combo_cals(procdb,jname,combos,outpath,clobber=True,quiet=True):
    results = dict()
    if not hasattr(combos,'__iter__'):
        combos = [combos]
    for combo in combos:
        procobs = procdb.get_obs(jname,combo)
        outdir = join(outpath,'%s_%s'%(jname,combo))
        rvals = deque()
        if not isdir(outdir):
            os.mkdir(outdir)
        for pobs in procobs:
            if pobs is None:
                continue
            outplot = join(outdir,pobs.obs.basename+'.gainstab.png')
            t = compare_cals(pobs,outplot=outplot,clobber=clobber,
                quiet=quiet)
            if t is None:
                continue
            mg,mdg,mdp = t
            t = [pobs.obs.get_mjd(),pobs.obs.basename,mg,mdg,mdp]
            rvals.append(t)
        results[combo] = rvals
    return results

