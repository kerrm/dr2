# The second tcs system cannot correctly set the LO, but is able to identify
# when the LO mismatches the data.  Occasionally the observer runs the
# script that sets the centre freq to 3094 instead of 3100.  The channels
# are misidentified because tcs and the DFBs think the centre freq is
# 3100 MHz.  Hence, the channel labels and OBS_FREQ fields should be
# adjusted.

# TODO -- need to add cals to avoid frequency mismatch

# PSR J0437-4715
# two below are speculation
# in fact it seems like these are not actually such a case
# they are P999 obs. and probably something else is going on
#t091112_171835.cf
#t091112_172419.rf
#r091112_171834.cf
#r091112_172300.rf

# PSR J0613-0200
# speculation
r091217_132510.cf
r091217_132756.rf

# PSR J1022+1001
# speculation
r100106_185202.cf
r100106_185525.rf

# PSR J1045-4509
r100106_211231.cf
r100106_211521.rf
r100106_212915.cf
r100106_213201.rf

# PSR J1713+0747
r091217_035115.cf
r091217_035412.rf
r100106_224423.cf
r100106_224712.rf

# PSR J1857+0943
r100108_042028.cf
r100108_042258.rf # speculation, did not check log

#PSR J1909-3744
r091217_065404.cf
r091217_065702.rf
t120122_044746.cf
t120122_045051.rf
t130513_213551.cf
t130513_213838.rf

# PSR J2145-0750
# these are not verified!
r091217_083105.cf
r091217_083319.rf
t120122_055922.cf
t120122_060215.rf

# PSR J2241-5236
t120122_074635.cf
t120122_074951.rf

