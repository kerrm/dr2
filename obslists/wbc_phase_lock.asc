# There isn't an entry for this in the timeline, but there are data
# that show similar symptoms to the DFB phase lock issues.

# The marker below is used to apply the correct flag to the data.
@wbc_phase_lock

#PSR J0437-4715
# observations added by MTK Jan 2015
w040801_203129.rf
w040801_213521.rf
w040801_225117.rf
w040801_232728.rf
w040802_193857.rf
w040802_204248.rf
w040802_214727.rf
w040802_225549.rf
w040801_192319.rf
w040801_181735.rf
w040810_192148.rf
w040810_200701.rf

#PSR J0613-0200
# observations added by MTK Jan 2015
w040208_103645.rf
w040207_092816.rf

# PSR J0742-2282
w040518_024319.rf

# PSR J2124-3358
# observations added by MTK Jan 2015
# these observations look either like RFI or radical phase lock
w050204_043547.rf
w050218_053035.rf
w050218_053826.rf
w050218_060407.rf
w050323_035704.rf
w050324_023307.rf
w050408_025453.rf

