# This class signifies my acceptance of defeat on finding a "cause" for
# all outliers.  These observations are those for which I either can find
# nothing apparent wrong or know too little about the observing system
# to say anything definitive.  The profiles are generally OK for use in
# other science, but the TOAs should be ignored for precision timing.

# This is similar in spirit to the "6" flag in the dr1 scheme.

@notiming

# PSR J0536-7543
# NB an "other"
t090219_053126.rf

# PSR J0630-2834
s081023_150105.rf
s081116_140429.rf
s081219_110318.rf
t090807_195859.rf

# PSR J1119-6127
# these two are before a glitch, so exclude them without longer data set
r070722_002419.rf
a070724_003818.rf
# this one is a mysterious outlier
s081023_041620.rf

# PSR J1302-6350
# these are too near periastron for timing
s101106_193607.rf
s140330_085550.rf
s101120_025332.rf
s101125_182805.rf
s101127_205254.rf
s101129_040214.rf
s140419_084148.rf
r070722_013232.rf
a070724_014742.rf
s101220_161050.rf
s101230_022114.rf
s110101_161845.rf
t140517_071810.rf
s101229_010946.rf
s110104_153726.rf


# PSR J1524-5706
# these two TOAs lie 0.1 off in phase for no reason I can see
t090629_054259.rf
t090808_031849.rf

# PSR J1726-3530
s081006_100907.rf
t090629_110049.rf

# PSR J2048-1616
s081116_040954.rf
