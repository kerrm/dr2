"""
A grab bag of common utility code.

$Header: /nfs/slac/g/glast/ground/cvs/users/kerrm/tpipe/timing.py,v 1.14 2014/01/21 06:46:59 kerrm Exp $

author: Matthew Kerr <matthew.kerr@gmail.com>
"""

import sys
import subprocess
import os
import re
import tempfile
import shutil
import datetime

from collections import deque

class CommandLine(object):
    """ Manage execution of command in sub shell.
    
    Divide up the configuration and invocation to allow for granularity."""

    def __init__(self,cmd,ionice=True,nice=True,env=None):
        if ionice:
            cmd = 'ionice -c 3 %s'%cmd
        if nice:
            cmd = 'nice -n 10 %s'%cmd
        self.cmd = cmd
        if env is not None:
            self.env = os.environ.copy()
            self.env.update(env)
        else:
            self.env = None

    def __call__(self,echo=True,verbosity=0):
        if echo:
            print self.cmd
        sys.stdout.flush()
        p = subprocess.Popen(self.cmd,shell=True,
            stdout=subprocess.PIPE,stderr=subprocess.PIPE,env=self.env)
        stdout,stderr = p.communicate() # blocks
        if (p.returncode != 0) and (verbosity > 0):
            print 'While executing: \n     %s\nReturn code was not 0!'%cmd
        return p.returncode,stdout,stderr
        

def run(cmd,echo=True,ionice=True,nice=True,verbosity=0,env=None):
    """ Execute command in a shell, returns code,stdout,stderr.
    
    Retained for backwards-compatibility, but better to use CommandLine."""
    cl = CommandLine(cmd,ionice=ionice,nice=nice,env=env)
    return cl(echo=echo,verbosity=verbosity)

def regexp_filter(regexp,strings,basename=False):
    """ Filter a set of strings and return only those matching the passed
        regular expression.
        
        If the strings are file names and it is preferable to ignore the
        absolute path, use basename=True."""
    if regexp is None:
        return strings
    reg = re.compile(regexp)
    if basename:
        f = lambda s: reg.match(os.path.basename(s)) is not None
    else:
        f = lambda s: reg.match(s) is not None
    return filter(f,strings)

def fname_filter(fnames,valid_fnames):
    """ Only return those filenames within the list of valid filenames.

    Path information is excluded.
    """
    valid_fnames = set(map(os.path.basename,valid_fnames))
    basename = os.path.basename
    return filter(lambda s: basename(s) in valid_fnames,fnames)

def unique_basenames(fnames):
    """ Return from iteratable filenames only those with unique basenames.
    """
    unique_f = deque()
    unique_b = set()
    basename = os.path.basename
    for f in fnames:
        b = basename(f)
        if b in unique_b:
            continue
        unique_f.append(f)
        unique_b.add(b)
    return unique_f

def localize_toas(fname,output=None):
    """ Replace the profile name in TOA file with base name."""

    def id_toa_line(line):
        if line[0]=='#' or line[0]=='C':
            return False
        if 'FORMAT' in line:
            return False
        toks = line.split()
        if len(toks) < 3:
            return False
        mjd = toks[2].split('.')
        if len(mjd)==2 and len(mjd[0])==5:
            return True
        return False

    lines = deque()
    for line in file(fname).readlines():
        if id_toa_line(line):
            toks = line.split()
            profile = os.path.basename(toks[0])
            lines.append(profile + line.split(toks[0])[1])
        else:
            lines.append(line)
    if output is None:
        output = fname
    # NB lines maintain white space
    file(output,'w').write(''.join(lines))
    return output

def local_file(fname):
    """ Return absolute path for a file in the root of repository."""
    repository_path = os.path.abspath(os.path.dirname(__file__))
    return os.path.join(repository_path,fname)

def readlines(fname):
    """ Strip comments,trailing whitespace, and blank lines from a file."""
    lines = filter(lambda l: len(l)>0 and l[0] != '#',
        map(str.strip,file(fname).readlines()))
    lines = map(lambda l: l.split('#')[0].strip() if '#' in l else l,lines)
    return lines

def shrink_png(fignum,output):
    """ Save an active canvas as a png using a reduced palette.
        Maybe in the future can do with lower bit depth like pgplot..."""
    import pylab as pl
    pl.figure(fignum)
    try:
        import cStringIO
        from PIL import Image
        ram = cStringIO.StringIO()
        pl.savefig(ram, format='png',dpi=100)
        ram.seek(0)
        im = Image.open(ram)
        im2 = im.convert('RGB').convert('P', palette=Image.ADAPTIVE)
        im2.save(output,format='PNG')
    except ImportError:
        # if PIL absent
        pl.savefig(output)

def today_mjd():
    """ Return the MJD of today -- to modest accuracy."""
    dt = datetime.date.today()-datetime.date(2000,1,1)
    return dt.days + 51544

def mjd_to_date(mjd):
    dt = float(mjd)-51544
    return datetime.date(2000,1,1) + datetime.timedelta(dt)

def date_to_mjd(year,month,date):
    dt = datetime.date(year,month,date) - datetime.date(2000,1,1)
    return dt.days + 51544

def mjd_to_doy(mjd):
    """ Return 1-based day-of-year.  (Jan 1 == DOY 1)"""
    dt = float(mjd)-51544
    date = datetime.date(2000,1,1) + datetime.timedelta(dt)
    return (date - datetime.date(date.year,1,1)).days + 1

def atnf_to_mjd(s):
    """ Return the approximate MJD of an ATNF-style filename, e.g. 
        t081112_123456.rf.
    """
    s = os.path.basename(s)
    if not s.isdigit():
        s = s[1:]
    if len(s) < 6:
        raise ValueError('Not in correct format!')
    year = int(s[:2])
    if year < 70:
        year += 2000
    else:
        year += 1900
    month = int(s[2:4])
    date = int(s[4:6])
    dt = datetime.date(year,month,date) - datetime.date(2000,1,1)
    return dt.days + 51544

def most_common(l):
    """ Return the most comment element in a list.  Slow."""
    try:
        l = list(l)
        return max(set(l),key=l.count)
    except:
        return None

def change_extension(f,new_ext):
    """ Change the final extension of a file (given by .) to the argument.
    """
    idx = f.rfind('.')
    if idx == -1:
        raise ValueError('File has no extension.')
    return f[:idx+1] + new_ext

class TemporaryFile(object):
    """ Use tempfile.mkstemp to generate a temporary file, and remove the
        file from the disk when this object goes out of scope."""

    def __init__(self,s=None):
        # allocate a file -- go ahead and remove the OS handle to it
        h,self.fname = tempfile.mkstemp()
        os.close(h)
        if s is not None:
            self.write(s)

    def __del__(self):
        try:
            os.remove(self.fname)
        except OSError:
            pass

    def write(self,s,mode='w'):
        with open(self.fname,mode) as f:
            f.write(s)

    def __str__(self):
        return self.fname

    def copyfrom(self,other):
        """ Copy from another file into the temporary file."""
        shutil.copy(other,self.fname)
