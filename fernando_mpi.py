import matplotlib
matplotlib.use('Agg')

import numpy as np
import os
from os.path import join,isfile,abspath,expandvars,basename,isdir
from glob import glob
from collections import deque
import cPickle

import template
import ephemeris
import mpi
import html
import marsfield
import timing
import util
import processed

CLOBBER_CALDB = False
CLOBBER_OBSDB = False
CLOBBER_PROCDB = False
CLOBBER_DATA = False
CLOBBER_TOAS = False

# load "new" pulsars
jnames = ['J1903-7051']
directories = marsfield.get_directories(['DFB','CASPSR'])
regexp = None

outpath = '/u/ker14a/newt/fernando'
calpath = join(outpath,'cals') # one single place for P574 cals

def get_caldb(clobber=False):
    """ "Static" instantiation of invariant caldb objects.

    Since we use one set of calibration databases for P574, when looping
    through the many pulsars, this lets us just run this function the first
    time.
    """
    if not hasattr(get_caldb,'caldb'):
        if not isdir(calpath):
            os.makedirs(calpath)
        get_caldb.caldb = marsfield.make_p574_caldb(
            '%s/caldb.pickle'%(calpath),directories,clobber=clobber,
            regexp=regexp)
        get_caldb.pcmdb = marsfield.make_pcmdb('%s/pcmdb.pickle'%calpath,
            pcmdir=expandvars('$PPTA/pcm/20cm'),
            suppldir=join(outpath,'supplementary_pcm'),clobber=clobber)
        get_caldb.fluxdb = marsfield.make_fluxdb('%s/fluxdb.pickle'%calpath,
            fluxdir=expandvars('$PPTA/fluxcal'),
            suppldir=join(outpath,'supplementary_fluxcal'),clobber=clobber)
        get_caldb.templdb = template.P574_TemplateDatabase(
            '/pulsar/archive17/wel161/glast/pulsars',
            suppldir=join(outpath,'templates'))
        get_caldb.ephdb = ephemeris.P574_EphemerisDatabase(
            join(outpath,'ephemerides'))
    return (get_caldb.caldb,get_caldb.pcmdb,get_caldb.fluxdb,
        get_caldb.templdb,get_caldb.ephdb)

def get_analysis(jname,clobber_obsdb=False,clobber_caldb=False):

    print 'Analysis for %s.'%jname
    psrpath = join(outpath,jname)
    if not isdir(psrpath):
        os.makedirs(psrpath)

    obsdb = marsfield.make_p574_obsdb('%s/obsdb.pickle'%(psrpath),
        directories,jname,clobber=clobber_obsdb,regexp=regexp)
    caldb,pcmdb,fluxdb,templdb,ephdb = get_caldb(clobber=clobber_caldb)
    db = obsdb.append_db(caldb)

    db.add_zapping(util.local_file('obslists/manual_zap_p574.asc'))
    db.add_zapping(util.local_file('obslists/midweek_twoband.asc'))
    db.add_zapping(util.local_file('obslists/midweek_fiveband.asc'))
    db.add_zapping(util.local_file('obslists/dfb2_jul2008_pol_quarter_band.asc'))
    db.add_correction(util.local_file('corrections/dfb3_quarter_band.asc'))

    analysis = timing.P574_TimingAnalysis(db,calpath,psrpath,
        ephdb=ephdb,templdb=templdb,pcmdb=pcmdb,fluxdb=fluxdb,
        zap_subints='dry',install_eph=True,make_dynspec=True)

    return analysis

from mpi4py import MPI
comm = MPI.COMM_WORLD



def load_procdb():
    tstart = MPI.Wtime()
    procdb_fname = join(outpath,'procdb.pickle')
    if CLOBBER_PROCDB or (not isfile(procdb_fname)):
        procdb = processed.ProcDB()
    else:
        procdb = cPickle.load(file(procdb_fname))
    return MPI.Wtime()-tstart,procdb

def dump_procdb(procdb):
    # hmm
    procdb_fname = join(outpath,'procdb.pickle')
    cPickle.dump(procdb,file(procdb_fname,'w'),protocol=2)


def make_html(jnames=None,procdb=None):
    if procdb is None:
        tprocdb,procdb = load_procdb()
    tstart = MPI.Wtime()
    output = html.P574_BandOverviewTable2(procdb,outpath)
    output.add_entries(jnames)
    output.write(fname='band.html')
    return MPI.Wtime()-tstart

if comm.rank == 0:

    tstart = MPI.Wtime()

    tprocdb,procdb = load_procdb()

    analyses = [get_analysis(jname,
        clobber_obsdb=CLOBBER_OBSDB,clobber_caldb=CLOBBER_CALDB)
        for jname in jnames]

    work = deque()
    for analysis in analyses:
        w,all_work = analysis.make_work(
            clobber=CLOBBER_DATA,clobber_toas=CLOBBER_TOAS)
        work.extend(all_work)

    manager = mpi.MPI_Jobs(work,verbosity=1)
    manager.do_work() # automatically sleeps threads when finished

    mpi.thread_stats(manager)
    print 'CAL throughput --'
    mpi.throughput_stats(manager,do_obs=False)
    print 'OBS throughput --'
    mpi.throughput_stats(manager,do_obs=True)
    #mpi.throughput_plot(manager,join(outpath,'all_throughput.png'))


    manager.kill_threads()

    for jname,analysis in zip(jnames,analyses):
        #analysis.tarball(jname)
        procdb.add_analysis(analysis)
        procdb.get_toa_tarball(jname)

    # here's a weird thing -- some WBCORR obs have wrong jname... so they
    # get added to procdb as such.  Just delete them for now
    #procdb.delete_complement(jnames)
    dump_procdb(procdb)

    ttotal = (MPI.Wtime() - tstart)
    thtml = make_html(jnames=jnames,procdb=procdb)
    print 'procdb load time: %d seconds'%(int(round(tprocdb)))
    print 'HTML elapsed time: %.2f minutes.'%(thtml/60)
    print 'Total elapsed time: %.2f minutes'%(ttotal/60)

else:
    worker = mpi.MPI_Worker()
    worker.do_work()
