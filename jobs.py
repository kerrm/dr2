"""
Code for generating scripts to manage jobs.

******
Sadly, despite the elegance of Makefiles, this is largely deprecated now in favor of MPI.
******

$Header: /nfs/slac/g/glast/ground/cvs/users/kerrm/tpipe/timing.py,v 1.14 2014/01/21 06:46:59 kerrm Exp $

author: Matthew Kerr <matthew.kerr@gmail.com>
"""

from collections import deque
import os
from os.path import join,isdir,isfile,basename
from time import sleep

from util import run

def ncores():
    """ Attempt to suss out the available cores.
    
    Return default (4) if unable to determine.
    """

    try:
        cmd = 'cat /proc/cpuinfo | grep -c ^process'
        rc,stdout,stderr = run(cmd,echo=False)
        if (rc==0) and (int(stdout) > 0):
            return int(stdout)
    finally:
        return 4

class psrsh_segment(deque):
    """ Encapsulate a portion of a psrsh script.
    
    By virtue of inheritance from deque and a few extra methods, these
    segments can be added efficiently and ultimately written to a file that
    can be invoked by psrsh -v -n.
    """

    def comment(self,line):
        if line[0] != '#':
            line = '# ' + line
        self.append(line)

    def __str__(self):
        return '\n'.join(self)

    def add(self,*args):
        for arg in args:
            self.append(arg)

    def __add__(self,other):
        c = copy(self)
        c += other
        return c

    def copy(self):
        return psrsh_segment(self)

    def write(self,fname):
        file(fname,'w').write(str(self)+'\n')

    def to_bash(self,fname=None):
        """ Write an executable bash script.

        Note that, when executed in this manner, psrsh complains if it
        find comments.  Therefore they are stripped.
        """
        psrsh = '\n'.join(filter(lambda x: x[0]!= '#',map(str.strip,self)))
        s = '\n'.join(('psrsh << BANG',psrsh,'BANG\n'))
        if fname is not None:
            file(fname,'w').write(s)
            os.system('chmod 766 %s'%fname)
        return s
    
class MakefileEntry(object):
    """ Encapsulate an entry in a GNU Makefile."""

    def __init__(self,target,actions,dependencies=None):
        self.target = target
        if not hasattr(actions,'__iter__'):
            self.actions = [actions]
        else:
            self.actions = actions
        if dependencies is None:
            self.dependencies = []
        elif not hasattr(dependencies,'__iter__'):
            self.dependencies = [dependencies]
        else:
            self.dependencies = dependencies

    def get_target(self):
        if ':' in self.target:
            return ''
        return self._escape(self.target)

    def _escape(self,s):
        """ Escape $ and :.'"""
        s = s.replace('$','$$')
        #s = s.replace(':','\:')
        return s

    def __str__(self):
        if ':' in self.target:
            return ''
        t = self._escape(self.target)
        a = self._escape(''.join(map(lambda s: '\n\t-%s'%s,self.actions)))
        d = self._escape(' '.join(self.dependencies))
        return '%s: %s%s'%(t,d,a)

class Makefile(object):
    """ Encapsulate a GNU Makefile.

        Maintain dependencies to allow parallel execution.
    """

    def __init__(self):
        self.entries = deque()

    def add_target(self,*args):
        if not isinstance(args[0],MakefileEntry):
            entry = MakefileEntry(*args)
        self.entries.append(entry)

    def add_targets(self,jobs):
        self.entries.extend(map(MakefileEntry,jobs))

    def write(self,outpath,fname='Makefile',phony_targets=False):
        output = join(outpath,fname)
        targets = [m.get_target() for m in self.entries]
        target_lines = [str(m) for m in self.entries]
        if phony_targets:
            target_lines = ['.PHONY: %s\n%s'%(t,tl) for t,tl in zip(targets,target_lines)]
        phony_all = '.PHONY: all\n' if phony_targets else ''
        header = 'SHELL = /bin/bash\n\n%sall: %s\n\n'%(phony_all,' '.join(targets))
        content = header + '\n'.join(target_lines) + '\n'
        file(output,'w').write(content)

    def __iadd__(self,other):
        self.entries.extend(other.entries) 
        return self

    def append(self,entry):
        self.entries.append(entry)

class Makefile_psrsh(Makefile):
    """ Interpret all entries as psrsh commands."""

    def add_target(self,target,script,dependencies=None):
        action = 'psrsh -n -v %s &> %s'%(script,script+'.log')
        self.entries.append(MakefileEntry(target,action,dependencies))

class Makefile_ln(Makefile):
    """ Interpret all entries as soft links."""

    def add_target(self,src,dst):
        action = 'ln -s %s %s'%(src,dst)
        self.entries.append(MakefileEntry(dst,action,None))

class Makefile_toa(Makefile):
    """ Handle the generation of TOAs with pat."""

    def add_target(self,profile,template,flags):
        a = 'pat -s %s -f tempo2 %s 2> %s | grep -v "FORMAT 1" | awk -F"\\n" \'{print $1 "%s"}\' > %s'%(
            template,profile,profile+'.toa.log',flags,profile+'.toa')
        self.entries.append(MakefileEntry(profile+'.toa',[a],[profile]))

class Makefile_shift(Makefile):
    """ Estimate phase shifts with pat."""

    def add_target(self,profile,template):
        a = 'pat -R -s %s %s 2> %s | awk \'{print $1,$4,$5}\' > %s'%(
            template,profile,profile+'.shift.log',profile+'.shift')
        self.entries.append(MakefileEntry(profile+'.shift',[a],[profile]))

class GNUParallel(object):
    """ Generate a script that can invoke GNU Parallel.
    """

    def __init__(self,action,additional_options=''):
        self.action = action
        self.additional_options=additional_options
        self.targets = deque()

    def add_target(self,target):
        self.targets.append(target)

    def write(self,fname,whitespace='\t'):
        #first = '#!/usr/bin/parallel --shebang --gnu --xapply --colsep "%s" %s'%(whitespace.encode('string-escape'),self.action)
        targets = list(self.targets)[:18]
        #lines = '\n'.join(map(whitespace.join,self.targets[:12]))
        lines = '\n'.join(map(whitespace.join,targets))
        #file(fname,'w').write('%s\n%s'%(first,lines))
        file(fname,'w').write(lines)
        #os.system('chmod 766 %s'%fname)

class GNUParallel_ln(GNUParallel):
    """ Child class to soft link files."""

    def __init__(self):
        action = r'ln -s {1} {2}'
        super(GNUParallel_ln,self).__init__(action)

    def add_target(self,src,dst):
        self.targets.append([src,dst])

class GNUParallel_psrsh(GNUParallel):
    """ Child class to launch psrsh scripts and record to logs."""

    def __init__(self):
        #additional_options=' --delay 2' # delay job launches by 2s
        additional_options = '' # this appears not to work?
        action = r'psrsh -n -v {1} \&\> {2}'
        super(GNUParallel_psrsh,self).__init__(action,additional_options)

    def add_target(self,script):
        self.targets.append([script,script+'.log'])

class GNUParallel_pat(GNUParallel):
    """ Child class to launch pat and manage flags."""

    def __init__(self):
        action = r'pat -s {1} -f tempo2 {2} 2\> {3} \| grep -v \"FORMAT 1\" \| awk -F \"\\n\" \'{print \$1 \"{5}\"}\' \> {4}'
        super(GNUParallel_pat,self).__init__(action)

    def add_target(self,profile,template,flags):
        toa = profile + '.toa'
        log = toa + '.log'
        self.targets.append([template,profile,log,toa,flags])

class GNUParallels(object):
    """ Represent multiple stages of the computation.

        Generate column outputs for the stages, and a single Makefile which
        can be used to run the stages.
    """

    def __init__(self,stages=None):
        self.stages = stages or deque()

    def add_stage(self,stage):
        self.stages.append(stage)

    def __iadd__(self,stage):
        self.add_stage(stage)
        return self

    def write(self,outpath,fname='Makefile.parallel',whitespace='\t'):
        makefile = Makefile()
        max_jobs = max(4,ncores()) # temp kluge
        #args = ['parallel','gnu','nice 14','xapply','progress','resume',
        args = ['parallel','gnu','nice 14','xapply','progress',
            #'use-cpus-instead-of-cores',
            'jobs %d'%(max_jobs),
            r'colsep "%s"'%(whitespace.encode('string-escape'))]
        if 'GNUPAR_HOSTS' in os.environ:
            hosts = '-S %s'%os.environ['GNUPAR_HOSTS']    
        else:
            hosts = '-S :'
        base_action = '%s %s'%(' --'.join(args),hosts)
        for istage,stage in enumerate(self.stages):
            targets = join(outpath,'parallel%02d.col'%istage)
            stage.write(targets,whitespace=whitespace)
            action = '%s %s --joblog %s -a %s %s'%(
                base_action,stage.additional_options,
                targets.replace('col','log'),
                targets,stage.action)
            makefile.add_target(targets,action)
        makefile.write(outpath,fname=fname,phony_targets=True)

class CopyData(object):
    """ Build a list of files to transfer, then generate a suitable script.
    """

    def __init__(self):
        self.srcs = deque()
        self.dsts = deque()

    def add_database(self,database,dstpath,update=True):
        if not isdir(dstpath):
            os.makedirs(dstpath)
        for obs in database.obs:
            self.add(*obs.copy_file(dstpath,update=update))

    def add(self,src,dst):
        self.srcs.append(src)
        self.dsts.append(dst)

    def write_pbs(self,fname,clobber=False):
        if not clobber:
            f = lambda s,d: '\n'.join([
                'echo "cp %s %s"'%(s,d),
                'if [ ! -f %s ];'%d,
                '  then cp %s %s'%(s,d),
                'fi'
            ])
        else:
            f = lambda s,d: '\n'.join([
                'echo "cp %s %s"'%(s,d),
                'cp %s %s'%(s,d)
            ])
        lines = '\n'.join(map(f,self.srcs,self.dsts))
        header = '\n'.join([
            '#!/bin/bash',
            '#PBS -q io',
            '#PBS -lhost=bragg-l,vmem=2GB,nodes=1:ppn=8,walltime=8:00:00',
            '',
        ])
        file(fname,'w').write(header+lines)
        os.system('chmod 740 %s'%fname)
