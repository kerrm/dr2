# a set of classes to manage pulse profiles
from collections import deque,defaultdict
import os
from os.path import basename,join,abspath,isfile
from glob import glob
import re

from band import Band,Band_10cm,Band_20cm,Band_40cm,Band_50cm
import observation
import ephemeris

import alias

valid_backends = set(('CPSR2','CPSR2m','CPSR2n','WBCORR','PDFB1','PDFB2','PDFB3','PDFB4','APSR','CASPSR'))

def parse_fname(fname):
    bname = basename(fname)
    toks = bname.split('.std')[0].split('_')
    rvals = dict()
    rvals['jname'] = ephemeris.jname_from_fname(fname)
    if rvals['jname'] is None:
        raise ValueError('Could not determine JNAME for %s!.'%fname)
    rvals['band'] = None
    rvals['backend']= None
    rvals['nchan'] = None
    rvals['receiver'] = None
    rvals['nbin'] = None
    rvals['tmin'] = None
    rvals['tmax'] = None
    rvals['invint'] = False
    for tok in toks:
        if tok.endswith('cm'):
            rvals['band'] = eval('Band_%s'%tok)()
            continue
        if tok.endswith('be'):
            rvals['backend'] = tok.rstrip('be')
            continue
        if tok.endswith('ch'):
            rvals['nchan'] = int(tok.rstrip('ch'))
            continue
        if tok.endswith('rx'):
            rvals['receiver'] = tok.rstrip('rx')
            continue
        if tok.endswith('bin'):
            rvals['nbin'] = int(tok.rstrip('bin'))
            continue
        if tok.endswith('tmax'):
            rvals['tmax'] = float(tok.rstrip('tmax'))
            continue
        if tok.endswith('tmin'):
            rvals['tmin'] = float(tok.rstrip('tmin'))
            continue
        if tok == 'invint':
            rvals['invint'] = True
            continue
        if tok in valid_backends:
            rvals['backend'] = tok
    return rvals

class Template(object):
    """ Determine information from observations."""

    def __init__(self,fname,name,band=None,backend=None,receiver=None,
            type='std',invint=False,nchan=None,nbin=None,
            tmin=None,tmax=None):
        self.fname = fname
        self.name = alias.preferred_name(name)
        self.band = band
        self.backend = backend
        self.receiver = receiver
        self.invint = invint
        self.type = type
        self.nchan = nchan
        self.nbin = nbin
        self.tmin = tmin
        self.tmax = tmax

        # handle some observational metadata
        #obs = observation.Observation(fname)
        #self.centre_frequency = obs.get_centre_frequency()

    def score(self,obs,force_invint=False):
        """ Return a matching score based on band and backend.
        
        This has a kluge to allow the user to specify an invariant interval.
        """
        score = 0
        # + 1000 if both in same state (invariant interval or intensity)
        if bool(self.invint):
            if force_invint or bool(obs.is_invint()):
                score += 1000
            else:
                score -= 1000
        if (self.band is not None):
            b = Band(obs)
            # handle case where we want to use 50cm templates for 40cm
            if (self.band == b) or self.band.kluge_50cm_match(b):
                score += 100
            # default to 20cm when available
            if (self.band == Band_20cm()):
                score += 1
            ## additional bonus for same centre frequency
            #if self.centre_frequency == obs.get_centre_frequency():
                #score += 1
        if (self.backend is not None):
            if (self.backend == obs['backend']):
                score += 10
            elif (self.backend in obs['backend']):
                # handle cases like CPSR2m/n -- accept a "CPSR2" match
                score += 9
            # bonus points to prioritize PDFB3/4 in case of no backend match
            elif (self.backend in ['PDFB3','PDFB4']):
                if obs['backend'] in ['PDFB3','PDFB4']:
                    # allow DFB3/DFB4 backends as interchangeable
                    score += 8
                else:
                    score += 2
        if (self.receiver is not None):
            if (self.receiver == obs['receiver']):
                score += 5
            else:
                score -= 1
        if (self.nchan is not None):
            # need a better way to do this -- this will cause profiles that
            # match to win, but will make sure that if information is
            # provided but doesn't match, template is penalized so that the
            # default is selected instead
            if (self.nchan == obs['nchan']):
                score += 2
            else:
                score -= 2
        if (self.nbin is not None):
            # need a better way to do this -- this will cause profiles that
            # match to win, but will make sure that if information is
            # provided but doesn't match, template is penalized so that the
            # default is selected instead
            if (self.nbin == obs['nbin']):
                score += 2
            else:
                score -= 2
        # check time bounds -- award 2 points if within fully specified
        # interval, 1 point if only one side is specified and it agrees,
        # no points if no bounds specified or observation lies outside the
        # full bound
        if (self.tmin is not None):
            if (obs.get_mjd() > self.tmin):
                if (self.tmax is None):
                    score += 1
                elif (obs.get_mjd() < self.tmax):
                    score += 2
        elif (self.tmax is not None) and (obs.get_mjd() < self.tmax):
            score += 1
        return score

    def __str__(self):
        return basename(self.fname)

    def writeto(self,path=None):
        output_fname = '%s_%s_paas.std'%(self.name,str(self.band).lower())
        if path is not None:
            output_fname = join(path,output_fname)
        os.system('cp %s %s'%(self.fname,output_fname))

class TemplateDatabase(object):

    def __init__(self,templates):
        self.templates = templates
        self.cache = defaultdict(deque)
        for t in templates:
            self.cache[t.name].append(t) 

    def __call__(self,obs,force_invint=False):
        """ Perform a score-based match to templates in database."""
        templs = self.cache[obs['name']]
        best = None
        best_score = -1
        for templ in templs:
            score = templ.score(obs,force_invint=force_invint) 
            # >= allows supplementary templates with same score to
            # take precedence
            if score >= best_score:
                best_score = score
                best = templ
        return best

class GeneralTemplateDatabase(TemplateDatabase):
    """ This class implements a generic standard in which template
    properties are inferred from the names.  The format is as follows:

    JNAME_AAA_BBB_CCC...ZZZ.std

    Where AAA, BBB, etc. are pieces of metadata.  The supported codes are

    cm   : band, e.g. AAA = 10cm
    be   : backend, e.g. AAA = PDFB4be
    ch   : channels, e.g. AAA = 256ch
    rx   : receiver, e.g. AAA = MULTIrx
    tmin : min time, e.g. AAA = 55678tmax
    tmin : max time, e.g. AAA = 55678tmin
    invint: invariant interval, AAA = invint
    
    As an example, 
    
    J1603-7202_20cm_ana_PDFB1be_MULTIrx_256bin_a060701_052213.std

    would parse with a valid band, backend, receiver, and binning, and
    additionally indicates it is an analytic template from the observation
    a060701_052213.  (The latter two data are not used for matching.)
    """

    def make_template(self,fname):
        rvals = parse_fname(fname)
        jname = rvals.pop('jname')
        return Template(abspath(fname),jname,**rvals)

    def __init__(self,templdir):
        """ Read in the templates in templdir."""

        templates = map(self.make_template,glob(join(templdir,'*std')))
        super(GeneralTemplateDatabase,self).__init__(templates)

class PPTA_TemplateDatabase(GeneralTemplateDatabase):
    """ Use PPTA-style template directory to make Templates."""

    def __init__(self,templdir=None,suppldir=None):
        if templdir is None:
            try:
                templdir = os.environ['PPTA_TEMPLATES']
            except KeyError:
                raise ValueError('Could not locate the template directory!')

        templates = map(self.make_template,glob(join(templdir,'*std')))
        if suppldir is not None:
            templates.extend(
                map(self.make_template,glob(join(suppldir,'*std'))))
        super(GeneralTemplateDatabase,self).__init__(templates)

class P574_TemplateDatabase(TemplateDatabase):

    # NB below is not I/O efficient
    def __init__(self,psrpath,suppldir=None):
        # old code for a directory hierarchy with pulsar<-->directory
        #tmp = re.compile(
            #r'J[0-2][0-9][0-5][0-9][-,+][0-8][0-9][0-5][0-9][A-Z]?')
        #jnames = filter(lambda x: tmp.match(x) is not None,
        #    map(basename,glob(join(psrpath,'J*'))))
        # new code for templates in same directory
        jnames = list(set([
            os.path.split(x)[-1].split('_')[0] for x in glob(
            join(psrpath,'*.std'))]))
        templates = deque()
        for jname in jnames:
            bands = [Band_10cm(),Band_20cm(),Band_50cm()]
            for band in bands:
                tname = '%s_%s_paas.std'%(jname,str(band).lower())
                fname = join(psrpath,tname)
                if isfile(fname):
                    templates.append(Template(fname,jname,band))

        def make_template(fname):
            # file name is arbitrary format, try to figure out from data
            obs = observation.Observation(fname)
            name = obs['name']
            band = Band(obs)
            backend = obs['backend']
            return Template(
                abspath(fname),name,band=band,backend=backend)

        if suppldir is not None:
            for fname in glob(join(suppldir,'*.std')):
                try:
                    templates.append(make_template(fname))
                except Exception as e:
                    print e
                    continue

        super(P574_TemplateDatabase,self).__init__(templates)


def ppta_default():
    try:
        return PPTA_TemplateDatabase()
    except ValueError:
        print 'Warning! No templates found!'

