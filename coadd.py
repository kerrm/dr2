import numpy as np
from collections import deque,defaultdict
import shutil
from os.path import join,basename,abspath,isfile
import cPickle

import psrchive

from jobs import psrsh_segment
import util
import profiles

# TODO -- bins and channels and frequency


class Coadd(object):

    def __init__(self,matcher):
        self.matcher = matcher
        self.matchers = deque([])

    def add_ops(self,ops):
        matched = False
        for matcher in self.matchers:
            matched = matched or matcher(ops)
            if matched:
                break
        if not matched:
            self.matchers.append(self.matcher(ops))

class Matcher(object):
    """ Group observations according to a set of common propertiers."""

    freq_tol = 0.1

    def __init__(self,ops):
        self.matches = deque([ops])
        self.init()
        self.kw_dict = dict()
        for kw in self.keywords:
            self.kw_dict[kw] = ops.obs[kw]

    def __call__(self,obs):
        raise NotImplementedError


class P574_Matcher(Matcher):
    
    def init(self):
        self.keywords = ['name','freq','bandwidth','receiver']

    def __str__(self):
        d = self.kw_dict
        return '%s_%.1f_%s'%(d['name'],d['freq'],d['receiver'])
        
    
    def __call__(self,ops):
        match = True
        for kw in self.keywords:
            val = self.kw_dict[kw]
            if kw == 'freq':
                match = match and (abs(val-ops.obs['freq'])<Matcher.freq_tol)
            else:
                match = match and (val==ops.obs[kw])
        if match:
            self.matches.append(ops)
            return True
        return False

class PPTA_Matcher(Matcher):

    def init(self):
        self.keywords = ['name','freq','bandwidth','receiver','backend']

    def __str__(self):
        d = self.kw_dict
        return '%s_%.1f_%s_%s'%(d['name'],d['freq'],d['receiver'],d['backend'])
    
class P574_Coadd(Coadd):

    def __init__(self,jname):
        matchers = map(lambda b:P574_Matcher(jname,b ,bands))
        super(P574_Coadd,self).__init__(matchers)

def coadd_P574(analysis,clobber=False):

    c = Coadd(P574_Matcher)
    scr = analysis._pscr

    # produce sets of matches
    map(c.add_ops,analysis.psr_ops)

    for matcher in c.matchers:
        obs = [ops.obs for ops in matcher.matches]

        # probably want to hook this into template matcher...
        ephemeris = matcher.matches[0].eph
        template = matcher.matches[0].template

        profiles = map(scr.get_scrunched_poln,obs)
        output = join(scr.outpath,str(matcher)+'.summed')
        if (not clobber) and isfile(output):
            continue

        # invoke psradd with ephemeris to join the obs
        cmd = 'psradd -E %s -o %s %s'%(ephemeris.fname,output,' '.join(profiles))
        rc,stdout,stderr = util.run(cmd,echo=False)
        print stderr

        # now invoke pat to get phase shifts for each integration
        cmd = 'pat -R -s %s %s'%(template.fname,output)
        rc,stdout,stderr = util.run(cmd,echo=False)

        def parse_line(line):
            toks = line.split()
            if (len(toks) < 5) or (toks[0] != output):
                return None
            fname,subint,poln,shift,shifte = toks[:5]
            return int(subint),float(shift),float(shifte)

        # use psrchive to rotate the subints
        a = psrchive.Archive_load(output)
        for line in stdout.split('\n'):
            toks = parse_line(line) 
            if toks is not None:
                subint,shift,shift_error = toks
                integration = a[subint]
                integration.rotate_phase(shift+0.5)
        a.unload(output)

def new_coadd(procdb,jname,combo='20CM_MULTI',outpath=None,
    retain_freq=False,clobber=False,name_only=False,no_wbc=True):
    """ Co-add profiles with "the same" observational set up.
    
    Sameness is complicated, but generally requires that the centre
    frequency and bandwidth agree.  Additionally, the co-addition is based
    on the TOA analysis, so the same templates used there are applied here,
    and any co-added data must use the same template.

    Some quality control on chi^2/SN is applied, but ultimately relies on
    good flagging.  In this department, any profiles whose corresponding
    TOA is flagged as
        -- nocal
        -- others?
    will be excluded from co-addition.
    
    TODO
    -- handle cases were nbin/nchan is factor of 2^n different
    """

    # basic match on band and receiver; ignore backend
    # sorted by date
    procobs = sorted(procdb.get_obs(jname,combo=combo))

    # somewhat of a kluge to get correct extension and path
    output = None
    for p in procobs:
        if p is not None:

            if retain_freq:
                data = p.obsscr.get_best_frequency_resolution(p.obs)
            else:
                data = p.get_scrunched(P=True,F=False,T=False)
            extension = data.split('.')[-1]
            if outpath is None:
                outpath = p.obsscr.outpath
            output = join(
                outpath,'%s_%s_coadded.%s'%(jname,combo,extension))
            break
    if output is None:
        return None

    if name_only or ((not clobber) and isfile(output)):
        return output

    # initially filter obs without TOAs
    procobs = [p if p.get_toa() is not None else None for p in procobs]
    print len(filter(None,procobs))

    # load up TOA profile analyses
    # read in TOAs to find flags analyses; do from obs instead?
    prof_ans = [None]*len(procobs)
    toas = [None]*len(procobs)
    for i in xrange(len(procobs)):
        if procobs[i] is None:
            continue
        # deprecate this approach for now and rely instead on procobs'
        # encapsulation
        try:
            toa = procobs[i].get_toa()
            prof_ans[i] = cPickle.load(file(toa+'.analysis'))
        except IOError:
            procobs[i] = None
            continue
        toas[i] = procobs[i].get_toa_string()
        if toas[i] is None:
            procobs[i] = None

    # do quality control on data flags -- make sure this stays up to date
    bad_flags = ['nocal','poor_profile','badcal','lowsn','phase_lock',
        'reset_bug','commissioning','solar_wind']
    removed = 0
    for i in xrange(len(procobs)):
        if procobs[i] is None:
            continue
        chi2,snr = prof_ans[i].chi2,prof_ans[i].snr
        #if (chi2 > 3) or (snr < 10) or ('nocal' in toas[i]):
        if (toas[i] is not None):
            for flag in bad_flags:
                if flag in toas[i]:
                    print '%s removed for quality (%s).'%(
                        procobs[i].obs.basename,flag)
                    procobs[i] = None
                    removed += 1
                    break
    print 'Removed %d flagged observations.'%removed

    # remove duplicates -- should be temporary...
    removed = 0
    basenames = set()
    for i in xrange(len(procobs)):
        if procobs[i] is None:
            continue
        if procobs[i].obs.basename in basenames:
            procobs[i] = None
            removed += 1
        else:
            basenames.add(procobs[i].obs.basename)
    print 'Removed %d duplicates (should be 0).'%removed

    # remove simulataneous observations
    removed = 0
    for i in xrange(len(procobs)):
        if procobs[i] is None:
            continue
        oi = procobs[i].obs
        for j in xrange(i+1,len(procobs)):
            if procobs[j] is None:
                continue
            oj = procobs[j].obs
            if oi.overlap(oj) > 0:
                bei = oi['backend']
                bej = oj['backend']
                # if DFB data, just use the more modern system
                if ('DFB' in bei) and ('DFB' in bej):
                    if int(bei[-1]) > int(bej[-1]):
                        procobs[j] = None
                    else:
                        procobs[i] = None
                # otherwise, use the longer observation
                else:
                    if oi.tobs > oj.tobs:
                        procobs[j] = None
                    else:
                        procobs[i] = None
                removed += 1
    print 'Removed %d simulataneous observations.'%removed

    # remove WBC if requested
    if no_wbc:
        removed = 0
        for i in xrange(len(procobs)):
            if procobs[i] is None:
                continue
            if procobs[i].obs['backend'] == 'WBCORR':
                procobs[i] = None
                removed += 1
    print 'Removed %d WBC observations.'%removed

    # check to see we have at least one obs.!
    at_least_one = False
    for p in procobs:
        if p is not None:
            at_least_one = True
            break
    if not at_least_one:
        print 'No valid observations for %s/%s.'%(jname,combo)
        return

    # TEMPORARY? -- kluge to select same rx; this is necessary because some
    # early observations with H-OH seem to have incorrect PA swing
    rx = util.most_common(
        [p.obs['receiver'] for p in procobs if p is not None])
    print 'receiver filter:' 
    for i in xrange(len(procobs)):
        if procobs[i] is None:
            continue
        if procobs[i].obs['receiver'] != rx:
            print procobs[i].obs['receiver'],rx
            procobs[i] = None
    print len(filter(None,procobs))

    # TEMPORARY? -- kluge to select same frequency
    cfreq = util.most_common(
        [p.obs.get_centre_frequency() for p in procobs if p is not None])
    print 'frequency filter:' 
    for i in xrange(len(procobs)):
        if procobs[i] is None:
            continue
        if procobs[i].obs.get_centre_frequency() != cfreq:
            print procobs[i].obs.get_centre_frequency(),cfreq
            procobs[i] = None
    print len(filter(None,procobs))

    # TEMPORARY? -- kluge to select same nbin
    nbin = util.most_common(
        [p.obs['nbin'] for p in procobs if p is not None])
    print 'nbin filter:' 
    for i in xrange(len(procobs)):
        if procobs[i] is None:
            continue
        if procobs[i].obs['nbin'] != nbin:
            print procobs[i].obs['nbin'],nbin
            procobs[i] = None
    print len(filter(None,procobs))

    # NB nchan filter not necessary because everything scrunched down to
    # same number of channels (hopefully)
    # THIS IS NO LONGER TRUE since we have high-resolution output
    # TEMPORARY? -- kluge to select same nbin
    nchan = util.most_common(
        [p.obs['nchan'] for p in procobs if p is not None])
    print 'nchan filter:' 
    for i in xrange(len(procobs)):
        if procobs[i] is None:
            continue
        if procobs[i].obs['nchan'] != nchan:
            print procobs[i].obs['nchan'],nchan
            procobs[i] = None
    print len(filter(None,procobs))

    # TEMPORARY? -- kluge to select same bandwidth
    bandwidth = filter(None,
        [p.obs['bandwidth'] if p is not None else None for p in procobs])
    bandwidth = util.most_common(bandwidth)
    print 'bandwidth filter:' 
    for i in xrange(len(procobs)):
        if procobs[i] is None:
            continue
        if procobs[i].obs['bandwidth'] != bandwidth:
            print procobs[i].obs['bandwidth'],bandwidth
            procobs[i] = None
    print len(filter(None,procobs))

    # TEMPORARY? -- kluge to select same template
    templates = filter(None,
        [prof.template if p is not None else None for p,prof in zip(procobs,prof_ans)])
    template = util.most_common(templates)
    for i in xrange(len(procobs)):
        if procobs[i] is None:
            continue
        if prof_ans[i].template != template:
            procobs[i] = None

    print 'template'
    print len(filter(None,procobs))

    # get good polarimetry and psradd
    #data = [p.get_scrunched(P=True,F=retain_freq,T=False) for p in procobs if p is not None]
    if retain_freq:
        data = [p.obsscr.get_best_frequency_resolution(p.obs) for p in procobs if p is not None]
    else:
        data = [p.get_scrunched(P=True,F=False,T=False) for p in procobs if p is not None]
    eph = procdb.get_ephemeris(jname)
    if eph is None:
        print 'No valid ephemeris for %s/%s'%(jname,combo)
        return
    eph = util.TemporaryFile(eph)
    par = str(eph)
    
    # tscrunch in case some time resolution remains in obs.
    cmd = 'psradd -jT -E %s -o %s %s'%(par,output,' '.join(data))
    rc,stdout,stderr = util.run(cmd,echo=False)
    if (not rc==0) or (not isfile(output)):
        print 'psradd failed with following output:'
        print stdout
        print stderr
        return

    # dedisperse -- this seems to be necessary.  makes sense
    cmd = 'pam -mD %s'%output
    rc,stdout,stderr = util.run(cmd,echo=False)

    # TODO -- this *should* work if the profiles have the right (same!)
    # ephemeris installed -- but if they have the original obs., it may not
    # so to be on the safe side, use pat on the psradded archive

    ## now psrchive to align the subints
    #a = psrchive.Archive_load(output)
    #counter = 0
    #for p,prof in zip(procobs,prof_ans):
        #if p is None:
            #continue
        #a[counter].rotate_phase(prof.phase_shift)
        #counter += 1
    #a.unload(output)

    # TODO -- need to make sure templates are consistent
    template = filter(None,prof_ans)[0].template

    # now invoke pat to get phase shifts for each integration
    cmd = 'pat -jF -R -s %s %s'%(template,output)
    rc,stdout,stderr = util.run(cmd,echo=False)

    def parse_line(line):
        toks = line.split()
        if (len(toks) < 5) or (toks[0] != output):
            return None
        fname,subint,poln,shift,shifte = toks[:5]
        return int(subint),float(shift),float(shifte)

    # use psrchive to rotate the subints
    a = psrchive.Archive_load(output)
    for line in stdout.split('\n'):
        toks = parse_line(line) 
        if toks is not None:
            subint,shift,shift_error = toks
            integration = a[subint]
            integration.rotate_phase(shift+0.5)
    a.unload(output)

    # finally, tscrunch; produce a fully tscrunched version to keep from
    # taking up too much space

    print 'Coadded %d profiles for %s/%s.'%(len(data),jname,combo)
    return output

def coadd_residual(coadded):
    """ Form a residual to a coadded profile.

    First, fully scrunch the profile to form a global mean.
    Then, go back to each subint and form a residual and scale it to the
    baseline variance.

    Ideally, could use a template to reject any RFI in the mean template...
    """

    a = psrchive.Archive_load(coadded)
    a.fscrunch()
    a.pscrunch()

    # load up profiles
    p = np.empty([len(a),a.get_nbin()],dtype=float)
    mjd = np.empty(len(a))
    for i in xrange(len(a)):
        p[i] = a[i].get_Profile(0,0).get_amps()
        mjd[i] = a[i].get_start_time().in_days()
    r = np.empty_like(p)

    # mean
    m = p.sum(axis=0)
    b_mean,s_mean = profiles.iterative_baseline_noerr(m,get_dev=True)
    m -= b_mean
    M2 = np.sum(m**2)

    # now breeze through and form residuals
    for i in xrange(len(a)):
        b,s = profiles.iterative_baseline_noerr(p[i],get_dev=True)
        p[i] -= b
        scale = np.sum(p[i]*m)/M2
        r[i] = (p[i]-scale*m)/(s**2 + (scale*s_mean)**2)**0.5

    return p,r,mjd


        









# below is not used but keep around for example of psrsh usage
def coadd_psrsh(fnames,rotations,output):
    primary = fnames[0]
    psrsh = psrsh_segment()
    #psrsh.add('load primary %s'%primary,'push primary',
        #'rotate %s'%(rotations[0]))
    #psrsh.add('load primary %s'%primary,'push primary',
        #'rotate %s'%(rotations[0]))
    if len(fnames) > 1:
        for fname,turns in zip(fnames[1:],rotations[1:]):
            if turns is None:
                print 'Skipping %s.'%fname
                continue
            psrsh.add('load %s %s'%(basename(fname),fname),
                'push %s'%(basename(fname)),'rotate %s'%turns,
                'pop','append %s'%(basename(fname)))
    psrsh.add('unload %s'%output)
    return psrsh
