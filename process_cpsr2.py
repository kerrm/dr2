""" Re-process timer-format cpsr2 data."""

import os
from os.path import expandvars,join,split,basename,isfile,splitext
import glob
from collections import deque,defaultdict

import psrchive

import util,jobs

import socket
hostname = socket.gethostname()
if hostname=='hydra':
    root = '/data/HYDRA_2/ker14a/P140'
else:
    root = '/DATA/HYDRA_2/ker14a/P140'

def get_directories(path):
    """ Return any directories immediately under path."""
    directories = filter(os.path.isdir,glob.glob(join(path,'*')))
    return directories

def gen_psrsh(filterbanks,jname,output_archive,output_psrsh,tscrunch=1):
    """ Generate a psrsh file that can create a PSRFITS file from the
    provided set of filterbanks."""
    psrsh = jobs.psrsh_segment()
    psrsh.add('load primary %s'%filterbanks[0],'push primary')
    for i,fb in enumerate(filterbanks[1:]):
        psrsh.add('load subint%d %s'%(i,fb),'append subint%d'%i)
    if tscrunch > 1:
        # compute desired number of subints
        nsub = len(filterbanks) / tscrunch
        psrsh.add('tscrunch %d'%nsub)
    psrsh.add('edit name=%s'%jname)
    psrsh.add('edit site=PARKES')
    psrsh.add('convert PSRFITS')
    psrsh.add('unload %s'%output_archive)
    psrsh.write(output_psrsh)

class CPSR2_Filterbank(object):
    """ Handle frequency detection etc. for an individual time obs."""

    def __init__(self,fname):
        self._fname = fname
        self._freq = None
        self._tsub = None
        self._name = None
        self._valid = True

    def get_band(self):
        """ Return the "label" associated with the file."""
        return split(self._fname)[-1][0]

    def get_freq(self):
        if self._freq is None:
            try:
                a = psrchive.Archive_load(self._fname)
                self._tsub = a[0].get_duration()
                self._freq = a.get_centre_frequency()
                self._name = a.get_source()
            except Exception as e:
                self._valid = False
                print 'Failed to read %s as valid.'%self._fname
        return self._freq

    def get_tsub(self):
        if self._tsub is None:
            self.get_freq()
        return self._tsub

    def get_name(self):
        if self._name is None:
            self.get_freq()
        return self._name

    def __cmp__(self,other):
        if self._fname <= other._fname:
            if self._fname < other._fname:
                return -1
            else:
                return 0
        return 1

class CPSR2_Band_Observation(object):
    """ Group CPSR2 observations by band."""

    def __init__(self,obsname,filterbanks):
        self._obsname = obsname
        self._filterbanks = filterbanks

    def to_psrfits(self,outpath,jname,simulate=False,atnf_style=False,
        clobber=False):
        """
        Merge filterbanks and output to PSRFITS.  Optionally tscrunch.
        """
        iscal = jname.endswith('_R')
        if atnf_style:
            output = join(outpath,self.atnf_style_name())
        else:
            output = join(outpath,self.cpa_style_name())
        output += '.cf' if iscal else '.rf'
        for fb in self._filterbanks:
            if not fb._valid:
                print 'Skipping %s due to invalid filterbanks.'%output
                return
        if (not clobber) and isfile(output):
            print 'Skipping %s due to no clobber.'%output
            return
        fnames = [x._fname for x in self._filterbanks]
        tobs = [x.get_tsub() for x in self._filterbanks]
        name = self._filterbanks[0].get_name()
        if 'CAL' in name:
            name = jname
        else:
            if not name.startswith('J'):
                name = 'J'+name
        for t in tobs:
            if t is None:
                return
        mean_tsub = float(sum(tobs)) / len(tobs)
        if mean_tsub < 1:
            # corrupt data, ignore it I reckon
            print 'Skipping %s due to short integrations.'%output
            return
        if iscal:
            if mean_tsub < 10:
                tscrunch = 2
            else:
                tscrunch = 1
        else:
            if mean_tsub < 20:
                tscrunch = 4
            elif mean_tsub < 40:
                tscrunch = 2
            else:
                tscrunch = 1
        print 'Processing %s with mean_tsub=%.2f and tscrunch=%d.'%(
            output,mean_tsub,tscrunch)
        # don't make too long of a command
        stride = 200
        nstep = int(len(fnames)/stride)+1
        for step in xrange(nstep):
            my_fnames = fnames[step*stride:(step+1)*stride]
            cmd0 = ' && '.join(
                ('ln -s %s %s'%(x,outpath) for x in my_fnames))
            util.run(cmd0,echo=False)
        links = [join(outpath,split(x)[-1]) for x in fnames]

        output_psrsh = splitext(output)[0] + '.psrsh'
        gen_psrsh(links,name,output,output_psrsh,
            tscrunch=tscrunch)
        cmd1 = 'psrsh -n -v %s'%output_psrsh
        #cmd1 = 'psradd -o %s %s'%(output,' '.join(links))
        #if tscrunch > 1:
        #    cmd2 = 'pam -t %d -m -a PSRFITS %s'%(tscrunch,output)
        #else:
        #    cmd2 = 'pam -m -a PSRFITS %s'%(output)
        #cmd3 = 'rm %s'%(' '.join(links))
        if simulate:
            cmd = 'touch %s'%output
            util.run(cmd,echo=False)
        else:
            #util.run(cmd0,echo=False)
            util.run(cmd1,echo=False)
            #util.run(cmd2,echo=False)
            #util.run(cmd3,echo=False)

        # TODO -- add timer file information to PSRFITS processing hist

    def atnf_style_name(self):
        """ Return a file name in the cYYMMDD_HHMMSS style."""
        fname = split(sorted(self._filterbanks)[0]._fname)[-1]
        char = fname[0]
        year,month,date,time = self._obsname.split('-')
        hour,minute,second = time.split(':')
        return '%s%s%s%s_%s%s%s'%(char,year[-2:],month,date,hour,minute,second)

    def cpa_style_name(self):
        fname = split(sorted(self._filterbanks)[0]._fname)[-1]
        char = fname[0]
        return '%s%s'%(char,self._obsname)


class CPSR2_Observation(object):
    """ Encapsulate a CPSR2 observation and the underlying timer files.
    
    TODO -- we basically want this to do a couple of things:
    (1) group together Timer files for processing into PSRFITS
    (2) provide a "manifest" for each psrfits file with the original
        filterbank names
    (3) be able to compare these manifests among different realizations of
        the CPSR2 data
    """

    def __init__(self,path,jname,iscal=False):
        self._path = path
        self._filterbanks = None
        self._bands = None
        self._name = split(path)[-1]
        self._jname = jname
        self._iscal = iscal

    def get_bands(self):
        if self._filterbanks is None:
            self._filterbanks = deque()
            for x in os.walk(self._path):
                dirpath,subdirs,dirfiles = x
                filterbanks = [CPSR2_Filterbank(join(dirpath,x)) for x in dirfiles if x.endswith('fb')]
                self._filterbanks.extend(filterbanks)

        bands = defaultdict(deque)
        for filterbank in self._filterbanks:
            bands[filterbank.get_freq()].append(filterbank)

        band_obs = [CPSR2_Band_Observation(self._name,bands[freq]) for freq in bands.keys()]
        return band_obs

    def write_bands(self,outpath,simulate=False,clobber=False):
        bands = self.get_bands()
        for band in bands:
            band.to_psrfits(outpath,self._jname,simulate=simulate,
                clobber=clobber)

# assemble a database of *fb files from the current environment
cp_archives = ['$CP1','$CP2','$CP3']

def get_all_directories():
    directories = deque()
    for archive in cp_archives:
        base = expandvars(archive)
        directories.extend(get_directories(base))
    return directories

def get_observations(jname,archive=False):
    if archive:
        basedirs = get_all_directories()
    else:
        basedirs = get_directories(root)
    directories = filter(lambda d: d.endswith(jname),basedirs)
    observations = deque()
    for directory in directories:
        observations.extend(glob.glob(join(directory,'20*')))
    return sorted(observations)

def process_pulsar(jname,outpath,simulate=False,clobber=False):
    iscal = jname.endswith('_R')
    observations = get_observations(jname)
    outpath = join(outpath,jname)
    if not os.path.isdir(outpath):
        os.mkdir(outpath)
    for obs in observations:
        obs = CPSR2_Observation(obs,jname,iscal=iscal)
        obs.write_bands(outpath,simulate=simulate,clobber=clobber)

class MyFile(object):

    def __init__(self,fname):
        self._fname = fname
        self._basename = basename(fname)
        self._size = os.path.getsize(fname)

def s20(s):
    return ' '*(25-len(s[:25])) + s[:25]

def s25(s):
    return ' '*(25-len(s[:25])) + s[:25]

def s15(s):
    return ' '*(15-len(s[:15])) + s[:15]

def compare_files(jname,outpath):
    """ Compare files in the "official" CPA archive with ones in outpath."""
    old_outpath = outpath
    outpath = join(outpath,jname)
    ext = '*.cf' if jname.endswith('_R') else '*.rf'
    files1 = glob.glob(join(outpath,ext))
    files2 = glob.glob(join(expandvars('$CPA'),jname,ext))
    myfiles1 = dict()
    for f in files1:
        f = MyFile(f)
        myfiles1[f._basename] = f
    myfiles2 = dict()
    for f in files2:
        f = MyFile(f)
        myfiles2[f._basename] = f
    sfiles1 = set(myfiles1.keys())
    sfiles2 = set(myfiles2.keys())
    all_files = sorted(sfiles1.union(sfiles2))
    strings = deque()
    for f in all_files:
        s = s20(f)
        if f in myfiles1:
            s += s15(str(myfiles1[f]._size))
        else:
            s += s15('----')
        if f in myfiles2:
            s += s15(str(myfiles2[f]._size))
        else:
            s += s15('----')
        strings.append(s) 
    strings.appendleft('%s%s%s'%(s20(''),s20(old_outpath),s20('CPA')))
    return strings

