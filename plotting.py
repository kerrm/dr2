"""
Routines for diagnostic plots.

$Header: /nfs/slac/g/glast/ground/cvs/users/kerrm/tpipe/timing.py,v 1.14 2014/01/21 06:46:59 kerrm Exp $

author: Matthew Kerr <matthew.kerr@gmail.com>
"""

import numpy as np
from os.path import join,basename,splitext
from jobs import Makefile,MakefileEntry,GNUParallel
from scipy.integrate import simps
from scipy import interpolate
from glob import glob
from collections import deque
import cPickle

from external import parfiles

import util

def set_rcParams(ticklabelsize='medium',bigticks=False):
    import matplotlib
    try:
        pass
        #matplotlib.rcParams.pop('font.cursive')
    except KeyError:
        pass
    matplotlib.rcParams['font.family'] = 'serif'
    matplotlib.rcParams['font.serif'] = ['DejaVu Serif'] + matplotlib.rcParams['font.serif']
    # NB -- this is a kluge; by default, mathtext.cal points to cursive,
    # so ideally we'd find the right font to put in here; but for now, do
    # this to prevent the really annoying error messages
    matplotlib.rcParams['font.cursive'] = ['DejaVu Serif'] + matplotlib.rcParams['font.cursive']
    matplotlib.rcParams['mathtext.fontset'] = 'custom'
    # FreeSerif is similar to Adobe Times
    #matplotlib.rcParams['font.serif'] = ['FreeSerif'] + matplotlib.rcParams['font.serif']
    matplotlib.rcParams['xtick.major.pad'] = 6
    matplotlib.rcParams['xtick.major.size'] = 6
    matplotlib.rcParams['xtick.minor.pad'] = 4
    matplotlib.rcParams['xtick.minor.size'] = 4
    matplotlib.rcParams['ytick.major.pad'] = 4
    matplotlib.rcParams['ytick.major.size'] = 6
    matplotlib.rcParams['ytick.minor.pad'] = 4
    matplotlib.rcParams['ytick.minor.size'] = 3
    matplotlib.rcParams['xtick.labelsize'] = ticklabelsize
    matplotlib.rcParams['ytick.labelsize'] = ticklabelsize
    matplotlib.rcParams['ps.usedistiller'] = 'xpdf'
    #matplotlib.rcParams['ps.fonttype'] = 42
    #matplotlib.rcParams['pdf.fonttype'] = 42
    #matplotlib.font_manager.warnings.filterwarnings(
        #'once',message='not found')

def get_tableau20(idx=None,only_dark=False):
    # These are the "Tableau 20" colors as RGB.  
    tableau20 = [(31, 119, 180), (174, 199, 232), (255, 127, 14), 
        (255, 187, 120), (44, 160, 44), (152, 223, 138), (214, 39, 40), 
        (255, 152, 150), (148, 103, 189), (197, 176, 213), (140, 86, 75), 
        (196, 156, 148), (227, 119, 194), (247, 182, 210), (127, 127, 127), 
        (199, 199, 199),  (188, 189, 34), (219, 219, 141), (23, 190, 207), 
        (158, 218, 229)]  

    if only_dark:
        tableau20 = [tableau20[i] for i in xrange(0,len(tableau20),2)]
  
    # Scale the RGB values to the [0, 1] range for matplotlib
    for i in range(len(tableau20)):  
        r, g, b = tableau20[i]  
        tableau20[i] = (r / 255., g / 255., b / 255.)

    if idx is None:
        return tableau20
    return tableau20[idx%(len(tableau20))]

def set_viridis():
    _viridis_data = [
        [0.267004, 0.004874, 0.329415],
        [0.268510, 0.009605, 0.335427],
        [0.269944, 0.014625, 0.341379],
        [0.271305, 0.019942, 0.347269],
        [0.272594, 0.025563, 0.353093],
        [0.273809, 0.031497, 0.358853],
        [0.274952, 0.037752, 0.364543],
        [0.276022, 0.044167, 0.370164],
        [0.277018, 0.050344, 0.375715],
        [0.277941, 0.056324, 0.381191],
        [0.278791, 0.062145, 0.386592],
        [0.279566, 0.067836, 0.391917],
        [0.280267, 0.073417, 0.397163],
        [0.280894, 0.078907, 0.402329],
        [0.281446, 0.084320, 0.407414],
        [0.281924, 0.089666, 0.412415],
        [0.282327, 0.094955, 0.417331],
        [0.282656, 0.100196, 0.422160],
        [0.282910, 0.105393, 0.426902],
        [0.283091, 0.110553, 0.431554],
        [0.283197, 0.115680, 0.436115],
        [0.283229, 0.120777, 0.440584],
        [0.283187, 0.125848, 0.444960],
        [0.283072, 0.130895, 0.449241],
        [0.282884, 0.135920, 0.453427],
        [0.282623, 0.140926, 0.457517],
        [0.282290, 0.145912, 0.461510],
        [0.281887, 0.150881, 0.465405],
        [0.281412, 0.155834, 0.469201],
        [0.280868, 0.160771, 0.472899],
        [0.280255, 0.165693, 0.476498],
        [0.279574, 0.170599, 0.479997],
        [0.278826, 0.175490, 0.483397],
        [0.278012, 0.180367, 0.486697],
        [0.277134, 0.185228, 0.489898],
        [0.276194, 0.190074, 0.493001],
        [0.275191, 0.194905, 0.496005],
        [0.274128, 0.199721, 0.498911],
        [0.273006, 0.204520, 0.501721],
        [0.271828, 0.209303, 0.504434],
        [0.270595, 0.214069, 0.507052],
        [0.269308, 0.218818, 0.509577],
        [0.267968, 0.223549, 0.512008],
        [0.266580, 0.228262, 0.514349],
        [0.265145, 0.232956, 0.516599],
        [0.263663, 0.237631, 0.518762],
        [0.262138, 0.242286, 0.520837],
        [0.260571, 0.246922, 0.522828],
        [0.258965, 0.251537, 0.524736],
        [0.257322, 0.256130, 0.526563],
        [0.255645, 0.260703, 0.528312],
        [0.253935, 0.265254, 0.529983],
        [0.252194, 0.269783, 0.531579],
        [0.250425, 0.274290, 0.533103],
        [0.248629, 0.278775, 0.534556],
        [0.246811, 0.283237, 0.535941],
        [0.244972, 0.287675, 0.537260],
        [0.243113, 0.292092, 0.538516],
        [0.241237, 0.296485, 0.539709],
        [0.239346, 0.300855, 0.540844],
        [0.237441, 0.305202, 0.541921],
        [0.235526, 0.309527, 0.542944],
        [0.233603, 0.313828, 0.543914],
        [0.231674, 0.318106, 0.544834],
        [0.229739, 0.322361, 0.545706],
        [0.227802, 0.326594, 0.546532],
        [0.225863, 0.330805, 0.547314],
        [0.223925, 0.334994, 0.548053],
        [0.221989, 0.339161, 0.548752],
        [0.220057, 0.343307, 0.549413],
        [0.218130, 0.347432, 0.550038],
        [0.216210, 0.351535, 0.550627],
        [0.214298, 0.355619, 0.551184],
        [0.212395, 0.359683, 0.551710],
        [0.210503, 0.363727, 0.552206],
        [0.208623, 0.367752, 0.552675],
        [0.206756, 0.371758, 0.553117],
        [0.204903, 0.375746, 0.553533],
        [0.203063, 0.379716, 0.553925],
        [0.201239, 0.383670, 0.554294],
        [0.199430, 0.387607, 0.554642],
        [0.197636, 0.391528, 0.554969],
        [0.195860, 0.395433, 0.555276],
        [0.194100, 0.399323, 0.555565],
        [0.192357, 0.403199, 0.555836],
        [0.190631, 0.407061, 0.556089],
        [0.188923, 0.410910, 0.556326],
        [0.187231, 0.414746, 0.556547],
        [0.185556, 0.418570, 0.556753],
        [0.183898, 0.422383, 0.556944],
        [0.182256, 0.426184, 0.557120],
        [0.180629, 0.429975, 0.557282],
        [0.179019, 0.433756, 0.557430],
        [0.177423, 0.437527, 0.557565],
        [0.175841, 0.441290, 0.557685],
        [0.174274, 0.445044, 0.557792],
        [0.172719, 0.448791, 0.557885],
        [0.171176, 0.452530, 0.557965],
        [0.169646, 0.456262, 0.558030],
        [0.168126, 0.459988, 0.558082],
        [0.166617, 0.463708, 0.558119],
        [0.165117, 0.467423, 0.558141],
        [0.163625, 0.471133, 0.558148],
        [0.162142, 0.474838, 0.558140],
        [0.160665, 0.478540, 0.558115],
        [0.159194, 0.482237, 0.558073],
        [0.157729, 0.485932, 0.558013],
        [0.156270, 0.489624, 0.557936],
        [0.154815, 0.493313, 0.557840],
        [0.153364, 0.497000, 0.557724],
        [0.151918, 0.500685, 0.557587],
        [0.150476, 0.504369, 0.557430],
        [0.149039, 0.508051, 0.557250],
        [0.147607, 0.511733, 0.557049],
        [0.146180, 0.515413, 0.556823],
        [0.144759, 0.519093, 0.556572],
        [0.143343, 0.522773, 0.556295],
        [0.141935, 0.526453, 0.555991],
        [0.140536, 0.530132, 0.555659],
        [0.139147, 0.533812, 0.555298],
        [0.137770, 0.537492, 0.554906],
        [0.136408, 0.541173, 0.554483],
        [0.135066, 0.544853, 0.554029],
        [0.133743, 0.548535, 0.553541],
        [0.132444, 0.552216, 0.553018],
        [0.131172, 0.555899, 0.552459],
        [0.129933, 0.559582, 0.551864],
        [0.128729, 0.563265, 0.551229],
        [0.127568, 0.566949, 0.550556],
        [0.126453, 0.570633, 0.549841],
        [0.125394, 0.574318, 0.549086],
        [0.124395, 0.578002, 0.548287],
        [0.123463, 0.581687, 0.547445],
        [0.122606, 0.585371, 0.546557],
        [0.121831, 0.589055, 0.545623],
        [0.121148, 0.592739, 0.544641],
        [0.120565, 0.596422, 0.543611],
        [0.120092, 0.600104, 0.542530],
        [0.119738, 0.603785, 0.541400],
        [0.119512, 0.607464, 0.540218],
        [0.119423, 0.611141, 0.538982],
        [0.119483, 0.614817, 0.537692],
        [0.119699, 0.618490, 0.536347],
        [0.120081, 0.622161, 0.534946],
        [0.120638, 0.625828, 0.533488],
        [0.121380, 0.629492, 0.531973],
        [0.122312, 0.633153, 0.530398],
        [0.123444, 0.636809, 0.528763],
        [0.124780, 0.640461, 0.527068],
        [0.126326, 0.644107, 0.525311],
        [0.128087, 0.647749, 0.523491],
        [0.130067, 0.651384, 0.521608],
        [0.132268, 0.655014, 0.519661],
        [0.134692, 0.658636, 0.517649],
        [0.137339, 0.662252, 0.515571],
        [0.140210, 0.665859, 0.513427],
        [0.143303, 0.669459, 0.511215],
        [0.146616, 0.673050, 0.508936],
        [0.150148, 0.676631, 0.506589],
        [0.153894, 0.680203, 0.504172],
        [0.157851, 0.683765, 0.501686],
        [0.162016, 0.687316, 0.499129],
        [0.166383, 0.690856, 0.496502],
        [0.170948, 0.694384, 0.493803],
        [0.175707, 0.697900, 0.491033],
        [0.180653, 0.701402, 0.488189],
        [0.185783, 0.704891, 0.485273],
        [0.191090, 0.708366, 0.482284],
        [0.196571, 0.711827, 0.479221],
        [0.202219, 0.715272, 0.476084],
        [0.208030, 0.718701, 0.472873],
        [0.214000, 0.722114, 0.469588],
        [0.220124, 0.725509, 0.466226],
        [0.226397, 0.728888, 0.462789],
        [0.232815, 0.732247, 0.459277],
        [0.239374, 0.735588, 0.455688],
        [0.246070, 0.738910, 0.452024],
        [0.252899, 0.742211, 0.448284],
        [0.259857, 0.745492, 0.444467],
        [0.266941, 0.748751, 0.440573],
        [0.274149, 0.751988, 0.436601],
        [0.281477, 0.755203, 0.432552],
        [0.288921, 0.758394, 0.428426],
        [0.296479, 0.761561, 0.424223],
        [0.304148, 0.764704, 0.419943],
        [0.311925, 0.767822, 0.415586],
        [0.319809, 0.770914, 0.411152],
        [0.327796, 0.773980, 0.406640],
        [0.335885, 0.777018, 0.402049],
        [0.344074, 0.780029, 0.397381],
        [0.352360, 0.783011, 0.392636],
        [0.360741, 0.785964, 0.387814],
        [0.369214, 0.788888, 0.382914],
        [0.377779, 0.791781, 0.377939],
        [0.386433, 0.794644, 0.372886],
        [0.395174, 0.797475, 0.367757],
        [0.404001, 0.800275, 0.362552],
        [0.412913, 0.803041, 0.357269],
        [0.421908, 0.805774, 0.351910],
        [0.430983, 0.808473, 0.346476],
        [0.440137, 0.811138, 0.340967],
        [0.449368, 0.813768, 0.335384],
        [0.458674, 0.816363, 0.329727],
        [0.468053, 0.818921, 0.323998],
        [0.477504, 0.821444, 0.318195],
        [0.487026, 0.823929, 0.312321],
        [0.496615, 0.826376, 0.306377],
        [0.506271, 0.828786, 0.300362],
        [0.515992, 0.831158, 0.294279],
        [0.525776, 0.833491, 0.288127],
        [0.535621, 0.835785, 0.281908],
        [0.545524, 0.838039, 0.275626],
        [0.555484, 0.840254, 0.269281],
        [0.565498, 0.842430, 0.262877],
        [0.575563, 0.844566, 0.256415],
        [0.585678, 0.846661, 0.249897],
        [0.595839, 0.848717, 0.243329],
        [0.606045, 0.850733, 0.236712],
        [0.616293, 0.852709, 0.230052],
        [0.626579, 0.854645, 0.223353],
        [0.636902, 0.856542, 0.216620],
        [0.647257, 0.858400, 0.209861],
        [0.657642, 0.860219, 0.203082],
        [0.668054, 0.861999, 0.196293],
        [0.678489, 0.863742, 0.189503],
        [0.688944, 0.865448, 0.182725],
        [0.699415, 0.867117, 0.175971],
        [0.709898, 0.868751, 0.169257],
        [0.720391, 0.870350, 0.162603],
        [0.730889, 0.871916, 0.156029],
        [0.741388, 0.873449, 0.149561],
        [0.751884, 0.874951, 0.143228],
        [0.762373, 0.876424, 0.137064],
        [0.772852, 0.877868, 0.131109],
        [0.783315, 0.879285, 0.125405],
        [0.793760, 0.880678, 0.120005],
        [0.804182, 0.882046, 0.114965],
        [0.814576, 0.883393, 0.110347],
        [0.824940, 0.884720, 0.106217],
        [0.835270, 0.886029, 0.102646],
        [0.845561, 0.887322, 0.099702],
        [0.855810, 0.888601, 0.097452],
        [0.866013, 0.889868, 0.095953],
        [0.876168, 0.891125, 0.095250],
        [0.886271, 0.892374, 0.095374],
        [0.896320, 0.893616, 0.096335],
        [0.906311, 0.894855, 0.098125],
        [0.916242, 0.896091, 0.100717],
        [0.926106, 0.897330, 0.104071],
        [0.935904, 0.898570, 0.108131],
        [0.945636, 0.899815, 0.112838],
        [0.955300, 0.901065, 0.118128],
        [0.964894, 0.902323, 0.123941],
        [0.974417, 0.903590, 0.130215],
        [0.983868, 0.904867, 0.136897],
        [0.993248, 0.906157, 0.143936]]

    from matplotlib.colors import ListedColormap
    viridis = ListedColormap(_viridis_data, name='viridis')
    viridis_r = ListedColormap(_viridis_data[::-1], name='viridis_r')
    import pylab as pl
    pl.register_cmap(name='viridis', cmap=viridis)
    pl.register_cmap(name='viridis_r', cmap=viridis_r)
    return viridis
    #pl.set_cmap(viridis)

def set_magma():
    _magma_data = [
        [0.001462, 0.000466, 0.013866],
        [0.002258, 0.001295, 0.018331],
        [0.003279, 0.002305, 0.023708],
        [0.004512, 0.003490, 0.029965],
        [0.005950, 0.004843, 0.037130],
        [0.007588, 0.006356, 0.044973],
        [0.009426, 0.008022, 0.052844],
        [0.011465, 0.009828, 0.060750],
        [0.013708, 0.011771, 0.068667],
        [0.016156, 0.013840, 0.076603],
        [0.018815, 0.016026, 0.084584],
        [0.021692, 0.018320, 0.092610],
        [0.024792, 0.020715, 0.100676],
        [0.028123, 0.023201, 0.108787],
        [0.031696, 0.025765, 0.116965],
        [0.035520, 0.028397, 0.125209],
        [0.039608, 0.031090, 0.133515],
        [0.043830, 0.033830, 0.141886],
        [0.048062, 0.036607, 0.150327],
        [0.052320, 0.039407, 0.158841],
        [0.056615, 0.042160, 0.167446],
        [0.060949, 0.044794, 0.176129],
        [0.065330, 0.047318, 0.184892],
        [0.069764, 0.049726, 0.193735],
        [0.074257, 0.052017, 0.202660],
        [0.078815, 0.054184, 0.211667],
        [0.083446, 0.056225, 0.220755],
        [0.088155, 0.058133, 0.229922],
        [0.092949, 0.059904, 0.239164],
        [0.097833, 0.061531, 0.248477],
        [0.102815, 0.063010, 0.257854],
        [0.107899, 0.064335, 0.267289],
        [0.113094, 0.065492, 0.276784],
        [0.118405, 0.066479, 0.286321],
        [0.123833, 0.067295, 0.295879],
        [0.129380, 0.067935, 0.305443],
        [0.135053, 0.068391, 0.315000],
        [0.140858, 0.068654, 0.324538],
        [0.146785, 0.068738, 0.334011],
        [0.152839, 0.068637, 0.343404],
        [0.159018, 0.068354, 0.352688],
        [0.165308, 0.067911, 0.361816],
        [0.171713, 0.067305, 0.370771],
        [0.178212, 0.066576, 0.379497],
        [0.184801, 0.065732, 0.387973],
        [0.191460, 0.064818, 0.396152],
        [0.198177, 0.063862, 0.404009],
        [0.204935, 0.062907, 0.411514],
        [0.211718, 0.061992, 0.418647],
        [0.218512, 0.061158, 0.425392],
        [0.225302, 0.060445, 0.431742],
        [0.232077, 0.059889, 0.437695],
        [0.238826, 0.059517, 0.443256],
        [0.245543, 0.059352, 0.448436],
        [0.252220, 0.059415, 0.453248],
        [0.258857, 0.059706, 0.457710],
        [0.265447, 0.060237, 0.461840],
        [0.271994, 0.060994, 0.465660],
        [0.278493, 0.061978, 0.469190],
        [0.284951, 0.063168, 0.472451],
        [0.291366, 0.064553, 0.475462],
        [0.297740, 0.066117, 0.478243],
        [0.304081, 0.067835, 0.480812],
        [0.310382, 0.069702, 0.483186],
        [0.316654, 0.071690, 0.485380],
        [0.322899, 0.073782, 0.487408],
        [0.329114, 0.075972, 0.489287],
        [0.335308, 0.078236, 0.491024],
        [0.341482, 0.080564, 0.492631],
        [0.347636, 0.082946, 0.494121],
        [0.353773, 0.085373, 0.495501],
        [0.359898, 0.087831, 0.496778],
        [0.366012, 0.090314, 0.497960],
        [0.372116, 0.092816, 0.499053],
        [0.378211, 0.095332, 0.500067],
        [0.384299, 0.097855, 0.501002],
        [0.390384, 0.100379, 0.501864],
        [0.396467, 0.102902, 0.502658],
        [0.402548, 0.105420, 0.503386],
        [0.408629, 0.107930, 0.504052],
        [0.414709, 0.110431, 0.504662],
        [0.420791, 0.112920, 0.505215],
        [0.426877, 0.115395, 0.505714],
        [0.432967, 0.117855, 0.506160],
        [0.439062, 0.120298, 0.506555],
        [0.445163, 0.122724, 0.506901],
        [0.451271, 0.125132, 0.507198],
        [0.457386, 0.127522, 0.507448],
        [0.463508, 0.129893, 0.507652],
        [0.469640, 0.132245, 0.507809],
        [0.475780, 0.134577, 0.507921],
        [0.481929, 0.136891, 0.507989],
        [0.488088, 0.139186, 0.508011],
        [0.494258, 0.141462, 0.507988],
        [0.500438, 0.143719, 0.507920],
        [0.506629, 0.145958, 0.507806],
        [0.512831, 0.148179, 0.507648],
        [0.519045, 0.150383, 0.507443],
        [0.525270, 0.152569, 0.507192],
        [0.531507, 0.154739, 0.506895],
        [0.537755, 0.156894, 0.506551],
        [0.544015, 0.159033, 0.506159],
        [0.550287, 0.161158, 0.505719],
        [0.556571, 0.163269, 0.505230],
        [0.562866, 0.165368, 0.504692],
        [0.569172, 0.167454, 0.504105],
        [0.575490, 0.169530, 0.503466],
        [0.581819, 0.171596, 0.502777],
        [0.588158, 0.173652, 0.502035],
        [0.594508, 0.175701, 0.501241],
        [0.600868, 0.177743, 0.500394],
        [0.607238, 0.179779, 0.499492],
        [0.613617, 0.181811, 0.498536],
        [0.620005, 0.183840, 0.497524],
        [0.626401, 0.185867, 0.496456],
        [0.632805, 0.187893, 0.495332],
        [0.639216, 0.189921, 0.494150],
        [0.645633, 0.191952, 0.492910],
        [0.652056, 0.193986, 0.491611],
        [0.658483, 0.196027, 0.490253],
        [0.664915, 0.198075, 0.488836],
        [0.671349, 0.200133, 0.487358],
        [0.677786, 0.202203, 0.485819],
        [0.684224, 0.204286, 0.484219],
        [0.690661, 0.206384, 0.482558],
        [0.697098, 0.208501, 0.480835],
        [0.703532, 0.210638, 0.479049],
        [0.709962, 0.212797, 0.477201],
        [0.716387, 0.214982, 0.475290],
        [0.722805, 0.217194, 0.473316],
        [0.729216, 0.219437, 0.471279],
        [0.735616, 0.221713, 0.469180],
        [0.742004, 0.224025, 0.467018],
        [0.748378, 0.226377, 0.464794],
        [0.754737, 0.228772, 0.462509],
        [0.761077, 0.231214, 0.460162],
        [0.767398, 0.233705, 0.457755],
        [0.773695, 0.236249, 0.455289],
        [0.779968, 0.238851, 0.452765],
        [0.786212, 0.241514, 0.450184],
        [0.792427, 0.244242, 0.447543],
        [0.798608, 0.247040, 0.444848],
        [0.804752, 0.249911, 0.442102],
        [0.810855, 0.252861, 0.439305],
        [0.816914, 0.255895, 0.436461],
        [0.822926, 0.259016, 0.433573],
        [0.828886, 0.262229, 0.430644],
        [0.834791, 0.265540, 0.427671],
        [0.840636, 0.268953, 0.424666],
        [0.846416, 0.272473, 0.421631],
        [0.852126, 0.276106, 0.418573],
        [0.857763, 0.279857, 0.415496],
        [0.863320, 0.283729, 0.412403],
        [0.868793, 0.287728, 0.409303],
        [0.874176, 0.291859, 0.406205],
        [0.879464, 0.296125, 0.403118],
        [0.884651, 0.300530, 0.400047],
        [0.889731, 0.305079, 0.397002],
        [0.894700, 0.309773, 0.393995],
        [0.899552, 0.314616, 0.391037],
        [0.904281, 0.319610, 0.388137],
        [0.908884, 0.324755, 0.385308],
        [0.913354, 0.330052, 0.382563],
        [0.917689, 0.335500, 0.379915],
        [0.921884, 0.341098, 0.377376],
        [0.925937, 0.346844, 0.374959],
        [0.929845, 0.352734, 0.372677],
        [0.933606, 0.358764, 0.370541],
        [0.937221, 0.364929, 0.368567],
        [0.940687, 0.371224, 0.366762],
        [0.944006, 0.377643, 0.365136],
        [0.947180, 0.384178, 0.363701],
        [0.950210, 0.390820, 0.362468],
        [0.953099, 0.397563, 0.361438],
        [0.955849, 0.404400, 0.360619],
        [0.958464, 0.411324, 0.360014],
        [0.960949, 0.418323, 0.359630],
        [0.963310, 0.425390, 0.359469],
        [0.965549, 0.432519, 0.359529],
        [0.967671, 0.439703, 0.359810],
        [0.969680, 0.446936, 0.360311],
        [0.971582, 0.454210, 0.361030],
        [0.973381, 0.461520, 0.361965],
        [0.975082, 0.468861, 0.363111],
        [0.976690, 0.476226, 0.364466],
        [0.978210, 0.483612, 0.366025],
        [0.979645, 0.491014, 0.367783],
        [0.981000, 0.498428, 0.369734],
        [0.982279, 0.505851, 0.371874],
        [0.983485, 0.513280, 0.374198],
        [0.984622, 0.520713, 0.376698],
        [0.985693, 0.528148, 0.379371],
        [0.986700, 0.535582, 0.382210],
        [0.987646, 0.543015, 0.385210],
        [0.988533, 0.550446, 0.388365],
        [0.989363, 0.557873, 0.391671],
        [0.990138, 0.565296, 0.395122],
        [0.990871, 0.572706, 0.398714],
        [0.991558, 0.580107, 0.402441],
        [0.992196, 0.587502, 0.406299],
        [0.992785, 0.594891, 0.410283],
        [0.993326, 0.602275, 0.414390],
        [0.993834, 0.609644, 0.418613],
        [0.994309, 0.616999, 0.422950],
        [0.994738, 0.624350, 0.427397],
        [0.995122, 0.631696, 0.431951],
        [0.995480, 0.639027, 0.436607],
        [0.995810, 0.646344, 0.441361],
        [0.996096, 0.653659, 0.446213],
        [0.996341, 0.660969, 0.451160],
        [0.996580, 0.668256, 0.456192],
        [0.996775, 0.675541, 0.461314],
        [0.996925, 0.682828, 0.466526],
        [0.997077, 0.690088, 0.471811],
        [0.997186, 0.697349, 0.477182],
        [0.997254, 0.704611, 0.482635],
        [0.997325, 0.711848, 0.488154],
        [0.997351, 0.719089, 0.493755],
        [0.997351, 0.726324, 0.499428],
        [0.997341, 0.733545, 0.505167],
        [0.997285, 0.740772, 0.510983],
        [0.997228, 0.747981, 0.516859],
        [0.997138, 0.755190, 0.522806],
        [0.997019, 0.762398, 0.528821],
        [0.996898, 0.769591, 0.534892],
        [0.996727, 0.776795, 0.541039],
        [0.996571, 0.783977, 0.547233],
        [0.996369, 0.791167, 0.553499],
        [0.996162, 0.798348, 0.559820],
        [0.995932, 0.805527, 0.566202],
        [0.995680, 0.812706, 0.572645],
        [0.995424, 0.819875, 0.579140],
        [0.995131, 0.827052, 0.585701],
        [0.994851, 0.834213, 0.592307],
        [0.994524, 0.841387, 0.598983],
        [0.994222, 0.848540, 0.605696],
        [0.993866, 0.855711, 0.612482],
        [0.993545, 0.862859, 0.619299],
        [0.993170, 0.870024, 0.626189],
        [0.992831, 0.877168, 0.633109],
        [0.992440, 0.884330, 0.640099],
        [0.992089, 0.891470, 0.647116],
        [0.991688, 0.898627, 0.654202],
        [0.991332, 0.905763, 0.661309],
        [0.990930, 0.912915, 0.668481],
        [0.990570, 0.920049, 0.675675],
        [0.990175, 0.927196, 0.682926],
        [0.989815, 0.934329, 0.690198],
        [0.989434, 0.941470, 0.697519],
        [0.989077, 0.948604, 0.704863],
        [0.988717, 0.955742, 0.712242],
        [0.988367, 0.962878, 0.719649],
        [0.988033, 0.970012, 0.727077],
        [0.987691, 0.977154, 0.734536],
        [0.987387, 0.984288, 0.742002],
        [0.987053, 0.991438, 0.749504]]

    from matplotlib.colors import ListedColormap
    magma = ListedColormap(_magma_data, name='magma')
    import pylab as pl
    pl.register_cmap(name='magma', cmap=magma)
    return magma

class PSRCHIVE_Plot(object):
    """ Encapsulate a particular type of plot so that the appropriate
    actions to generate can be determined for a given observation.
    """

    def __init__(self,additional_flags='',ext='png'):
        self.env = {'PGPLOT_BACKGROUND':'white',
                    'PGPLOT_FOREGROUND':'black'}
        self.init()
        self.cmd_init()
        self.ext = ext
        self.flags += ' %s'%additional_flags

    def get_makefile(self,obsns,scrunches):
        outpath = scrunches.outpath
        makefile = Makefile()
        for obs in obsns:
            fname = self.get_input(obs,scrunches)
            if fname is None:
                print 'Could not find data file to plot for %s.'%obs.fname
                continue
            target = join(outpath,basename(fname)+
                '.%s.%s'%(self.label,self.ext))
            action = '%s %s %s %s/%s %s'%(
                self.cmd,self.flags,self.grdev,target,self.ext,fname)
            makefile.add_target(target,action,[scrunches.get_target(obs)])
        return makefile

    def get_parallel(self,obsns,scrunches):
        outpath = scrunches.outpath
        action =r'%s %s %s {1} {2}'%(self.cmd,self.flags,self.grdev)
        parallel = GNUParallel(action)
        for obs in obsns:
            fname = self.get_input(obs,scrunches)
            if fname is None:
                print 'Could not find data file to plot for %s.'%obs.fname
                continue
            target = join(outpath,basename(fname)+'.%s.%s/%s'%(
                self.label,self.ext,self.ext))
            parallel.add_target([target,fname])
        return parallel

    def get_mpi(self,obs,scrunches):
        """ Return the appropriate target/command to generate MPI job.
        
        Dependencies are handled elsewhere.
        """
        fname = self.get_input(obs,scrunches)
        if fname is None:
            return None
        target = join(scrunches.outpath,basename(fname)+
            '.%s.%s'%(self.label,self.ext))
        # a workaround since vap/pgplot don't seem to like filenames
        # longer than 90 characters
        if len(target) > 90:
            tmp_target = util.TemporaryFile()
            cmd = '%s %s %s %s/%s %s'%(
                self.cmd,self.flags,self.grdev,tmp_target,self.ext,fname)
            cmd += ' && mv %s %s'%(tmp_target,target)
            cmd += ' && chmod 644 %s'%(target)
            action = util.CommandLine(cmd,env=self.env)
            action.tmp_target = tmp_target
            return target,action
        cmd = '%s %s %s %s/%s %s'%(
            self.cmd,self.flags,self.grdev,target,self.ext,fname)
        action = util.CommandLine(cmd,env=self.env)
        return target,action

    def get_target(self,obs,scrunches):
        if obs is None:
            # this is a kluge to allow more seamless processing of output
            return None
        fname = self.get_input(obs,scrunches)
        if fname is None:
            return None
        return join(scrunches.outpath,basename(fname)+
            '.%s.%s'%(self.label,self.ext))

    def get_input(self,obs,scrunches):
        """ Base implementation uses maximally scrunched file that still
            retains time, freq, and poln data.
        """
        return scrunches.get_scrunched_3d(obs) 

    def get_cmd(self,archive,output):
        """ Generate a command line for given archive."""
        cmd = '%s %s %s %s/%s %s'%(
            self.cmd,self.flags,self.grdev,output,self.ext,archive)
        action = util.CommandLine(cmd,env=self.env)
        return action

    def valid(self,obs):
        """ Return True if this plot can be generated for the given obs."""
        return True

class pav_Plot(PSRCHIVE_Plot):
    
    def cmd_init(self):
        self.cmd = 'pav'
        self.grdev = '-g'

class StokesPlot(pav_Plot):
    
    def init(self):
        self.label = 'stokes'
        self.flags = '-SFT'

    def get_input(self,obs,scrunches):
        return scrunches.get_scrunched_poln(obs)

class AmpFreqPhasePlot(pav_Plot):

    def init(self):
        self.label = 'freqph'
        self.flags = '-GdTp'

    def get_input(self,obs,scrunches):
        return scrunches.get_best_frequency_resolution(obs)

class AmpTimePhasePlot(pav_Plot):

    def init(self):
        self.label = 'timeph'
        self.flags = '-YdFp'

    def get_input(self,obs,scrunches):
        return scrunches.get_best_time_resolution(obs)

class OriginalBandpassPlot(pav_Plot):

    def init(self):
        self.label = 'origbp'
        self.flags = '-B'

    def get_input(self,obs,scrunches):
        # use the most scrunched version to cut I/O
        return scrunches.get_target(obs)

    def valid(self,obs):
        if 'CASPSR' in obs['backend']:
            return False
        return True

class psrplot_Plot(PSRCHIVE_Plot):

    def cmd_init(self):
        self.cmd = 'psrplot'
        self.grdev = '-D'

class GainsPlot(psrplot_Plot):

    def init(self):
        self.label = 'gains'
        self.flags = '-pM'

    def get_input(self,obs,scrunches):
        return scrunches.get_target(obs)


def plot_profile_residuals(scaled_profile,scaled_template,snr,chi2,
        select=None,label=None,output=None):
    """ Produce a plot of profile residuals to the template.

    Elsewhere, the profile scaling and noise level should be computed, and
    this function is simply for display.
    """

    import pylab as pl
    from matplotlib.ticker import MaxNLocator
    set_rcParams()

    sp = scaled_profile
    st = scaled_template

    # make plots!
    pl.figure(1,figsize=(6.58,4.84),dpi=100); pl.clf()
    axlo = pl.axes([0.125,0.1,0.775,0.42])
    axhi = pl.axes([0.125,0.54,0.775,0.42])

    # TODO -- do an "offpulse" chi^2 as well for radiometer noise guess
    dom = np.linspace(0,1,len(sp)+1)[:-1]
    axhi.plot(dom,sp,label=label,color='gray')
    axhi.plot(dom,st,label=label,color='k')
    axhi.text(0.10,0.78,'$S/N = %.2f$'%snr,transform=axhi.transAxes,
        color='red',size='xx-large')
    axhi.text(0.70,0.78,'$\chi^2 = %.2f$'%chi2,transform=axhi.transAxes,
        color='red',size='xx-large')
    axhi.axis([0,1,-5,1.05*sp.max()])
    axhi.yaxis.set_major_locator(MaxNLocator(5))
    axhi.set_ylabel('Sigma')
    #axhi.legend(loc='upper right',ncol=1,fontsize='medium')
    #pl.legend(loc='upper right',ncol=1,fontsize='medium')
    #pl.legend(loc='upper right',ncol=1)
    pl.xticks(np.arange(0,1.01,0.1),visible=False)

    axlo.plot(dom,sp-st,color='gray')
    axlo.axhline(0,color='red',ls='-')
    axlo.axhline(3,color='red',ls='--')
    axlo.axhline(-3,color='red',ls='--')
    axlo.yaxis.set_major_locator(MaxNLocator(5))
    axlo.set_ylabel('Residual')
    axlo.set_xlabel('Pulse Phase')
    axlo.set_xticks(np.linspace(0,1,11)[:-1])
    axlo.set_xticks(np.arange(0,1.01,0.1))
    #pl.title('%s / %s'%(basename(templ),basename(profl)))
    if output is not None:
        pl.savefig(output,dpi=100)

def plot_fluxes_old(path,fignum=2,use_pdv=False,use_snr=False):
    pickles = glob(join(path,'*.toa.analysis'))
    """ This is probably obsolete now with ProcDB."""
    band = deque()
    tflux = np.empty(len(pickles))
    tfluxe = np.empty_like(tflux)
    pflux = np.empty_like(tflux)
    pfluxe = np.empty_like(tflux)
    mjd = np.empty_like(pflux)
    snr = np.empty_like(tflux)
    jansky = np.empty(len(pickles),dtype=bool)
    for ipickle,pickle in enumerate(pickles):
        p = cPickle.load(file(pickle))
        jname = p.obs['name']
        mjd[ipickle] = p.mjd
        combo = p.combo
        snr[ipickle] = p.snr
        band.append(combo.split('_')[0])
        jansky[ipickle] = p.scale=='Jansky'
        tflux[ipickle] = p.flux
        tfluxe[ipickle] = p.fluxerr
        pflux[ipickle] = p.pdv_flux
        pfluxe[ipickle] = p.pdf_fluxerr # fix when propagates

    band = np.asarray(band)
    print band
    print jansky

    import pylab as pl
    pl.figure(fignum); pl.clf()
    ax = pl.gca()
    ax.set_yscale('log')

    bands = ['50CM','40CM','20CM','10CM']
    colors = ['salmon','salmon','green','blue']
    for b,c in zip(bands,colors):
        if use_snr:
            mask = band==b
        else:
            mask = (band==b) & jansky
        x = mjd[mask]
        a = np.argsort(x)
        x = x[a]
        if use_snr:
            y = snr[mask][a]
            ax.plot(x,y,color=c,lw=2,marker='.')
            ax.set_ylabel('S/N')
        else:
            if use_pdv:
                y = pflux[mask][a]
                ye = pfluxe[mask][a]
            else:
                y = tflux[mask][a]
                ye = tfluxe[mask][a]
            ax.errorbar(x,y,yerr=ye,color=c,ls='-',marker='.')
            ax.set_ylabel('Flux (mJy)')
    ax.set_xlabel('MJD')

    return tflux,tfluxe,band,jansky,mjd,mask

def plot_toa_residuals(inpar,intim,select=None,outplot=None,
        snr_cut=8,chi2_cut=10,fignum=2,plot_years=True):
    """ Produce a TOA residuals plot with S/N and goodness-of-fit cuts."""
    import pylab as pl
    import matplotlib

    fig = pl.figure(fignum,(10,7)); pl.clf()
    ax = pl.axes([0.10,0.12,0.84,0.83])
    axhi = ax.twiny()
    x_formatter = matplotlib.ticker.ScalarFormatter(useOffset=False)
    ax.xaxis.set_major_formatter(x_formatter)
    axhi.xaxis.set_major_formatter(x_formatter)

    select_lines = '\n'.join((
        'LOGIC -snr < %d REJECT'%snr_cut,
        'LOGIC -gof > %d REJECT'%chi2_cut))
    if select is None:
        select = util.TemporaryFile(select_lines)
    else:
        other_lines = '\n'.join(util.readlines(select))
        select = util.TemporaryFile(select_lines+'\n'+other_lines)
    rvals = resids,residse,chi2,dof,mjds,frqs = parfiles.get_resids(
        inpar,intim,
        phase=True,get_mjds=True,jitter=None,nofit=True,select=str(select),
        echo=False)

    if resids is None:
        # tempo2 failed (select file bailed) save a blank plot
        # TODO -- catch this before it happens
        if outplot is not None:
            pl.savefig(outplot)
        return

    year = (mjds-51544.)/365.24+2000 # approximate calendar year

    resids-=np.median(resids)
    resids[resids > 0.5] -= 1
    resids[resids < -0.5] += 1

    # compute the plot range so we can display outliers -- "5 sigma"
    toa_range = 1000*5*1.4826*np.median(np.abs(resids-np.median(resids)))
    toa_range = min(toa_range,1+1000*np.abs(resids).max())
    toa_range = min(max(5,toa_range),550)

    colormasks = [
        (frqs>0)&(frqs<1000),
        (frqs>1000)&(frqs<2000),
        frqs>2000]
    colors = ['red','green','blue',]
    size = [9,9,9] # marker sizes

    tmin = np.inf
    tmax = -np.inf

    total_toas = 0

    for icm,cm in enumerate(colormasks):
        if not(np.any(cm)):
            continue
        color = colors[icm%len(colors)]
        ms = size[icm%len(size)]
        y = resids[cm]*1000
        x = year[cm]
        yerr = residse[cm]*1000
        tmin = min(x.min(),tmin)
        tmax = max(x.max(),tmax)
        # display outliers
        om = np.abs(y) > toa_range
        if np.any(om):
            if np.any(~om):
                ax.errorbar(x[~om],y[~om],yerr=yerr[~om],
                    color=color,ls=' ',marker='o',ms=ms)
            myy = (0.97*toa_range*np.where(y<0,-1,1))[om]
            ax.errorbar(x[om],myy,yerr=0,color=color,ls=' ',marker='<',ms=8)
            total_toas += len(myy)
        else:
            ax.errorbar(x,y,yerr=yerr,color=color,ls=' ',marker='.',ms=ms)
            total_toas += len(y)
    ax.set_ylabel('Residual (milliP)')
    ax.set_xlabel('Year')
    ax.axhline(0,color='k',ls='-')
    ax.grid(True)
    dt = tmax-tmin
    year_lo,year_hi = tmin-dt*0.03,tmax+dt*0.03
    ax.axis([year_lo,year_hi,-abs(toa_range),abs(toa_range)])
    mjd_lo,mjd_hi = (np.asarray([year_lo,year_hi])-2000)*365.24+51544.
    axhi.axis([mjd_lo,mjd_hi,-abs(toa_range),abs(toa_range)])
    try:
        ax.yaxis.set_tick_params(which='both',top=True,labeltop=False,
            bottom=True,labelbottom=True)
    except AttributeError:
        # handles case of older matplotlib
        pass
    if total_toas == 0:
        # don't save an empty plot!
        return
    if outplot is not None:
        pl.savefig(outplot)

def plot_toa_sn_spectrum(intim,norm=300,outplot=None,fignum=2):
    """ Make an S/N histogram for a given set of TOAs.
    
    Norm is the time (s) to which the S/Ns are scaled.
    """

    #tf = TemporaryFile()
    #parfiles.flag_filter_tim(intim,'-B',str(tf),val=band)

    #sn = np.asarray(parfiles.flag_values(str(tf),'-snr'),dtype=float)
    #ti = np.asarray(parfiles.flag_values(str(tf),'-length'),dtype=float)
    sn = np.asarray(parfiles.flag_values(intim,'-snr'),dtype=float)
    ti = np.asarray(parfiles.flag_values(intim,'-length'),dtype=float)
    if len(sn) == 0:
        return

    sn = sn*(ti/norm)**-0.5

    import pylab as pl
    fig = pl.figure(fignum,(10,7)); pl.clf()
    ax = pl.axes([0.10,0.12,0.84,0.83])

    bins = np.arange(0,4.01,0.25)
    #try:
        #color = {'10CM':'blue','20CM':'green','50CM':'blue'}[band]
    #except KeyError:
        #color = 'gray'
    color = 'gray'
    try:
        ax.hist(np.log10(sn),bins=bins,histtype='step',lw=3,color=color)
    except ValueError:
        # no values within bins
        return
    ax.set_xlabel('$\log_{10}$ S/N (5 min.)')

    if outplot is not None:
        pl.savefig(outplot)

def plot_2d_profile(profiles,residuals=False,snr_cut=8,chi2_cut=10,
    outplot=None,fignum=10):
    """ Profiles should be a list of ProfileAnalysis objects."""
    import pylab as pl
    pl.figure(fignum); pl.clf()
    ax = pl.axes([0.10,0.12,0.84,0.83])
    mjds = np.asarray([p.mjd for p in profiles])
    a = np.argsort(mjds)
    mjds = mjds[a]
    profiles = np.asarray(profiles)[a]
    resids = deque()
    good_mjds = deque()
    nbin = 1024
    #chi2 = deque()
    #for i,pi in enumerate(profiles):
        #if '20CM' not in pi.combo:
            #continue
        #if not np.isnan(pi.snr):
            #chi2.append(pi.chi2)
    #chi2_cut = np.mean(chi2) + 4*np.std(chi2)
    for i,pi in enumerate(profiles):
        #if pi.chi2 > chi2_cut:
            #continue
        #if pi.snr < snr_cut:
            #continue
        if np.isnan(pi.snr):
            continue
        prof = pi.get_profile(nbin=nbin,residuals=False)
        t = np.sort(prof)[0.1*nbin:0.9*nbin]
        s = np.median(np.abs(t-np.median(t)))*1.4826
        #prof *= (1./s)
        prof *= 1./prof.max()
        resids.append(prof)
        good_mjds.append(mjds[i])
    print good_mjds
    resids = np.asarray(resids)
    xgrid = np.arange(nbin)
    ygrid = np.linspace(np.min(good_mjds)-1,np.max(good_mjds)+1,1000)
    ogrid = np.empty([len(ygrid),len(xgrid)])
    for i in xrange(len(ygrid)):
        xidx = np.searchsorted(good_mjds,ygrid[i])-1
        ogrid[i] = resids[xidx]
    mu = resids.mean(axis=0)#*resids.shape[0]**0.5
    blah = ax.imshow(ogrid,interpolation='nearest',aspect='auto')
    pl.colorbar(blah)

    if outplot is not None:
        pl.savefig(outplot)
    
def plot_coadd_residual(coadded,time_scale=False,max_range=5,
    outplot=None,colorbar=False,fignum=40):
    """ Make a "carpet plot" from a coadded archive."""
    
    import coadd
    profile,residual,mjds = coadd.coadd_residual(coadded)

    import pylab as pl
    pl.figure(fignum); pl.clf()
    ax = pl.axes([0.10,0.12,0.84,0.83])

    if not time_scale:
        blah = ax.imshow(residual,interpolation='nearest',aspect='auto',vmin=-max_range,vmax=max_range,origin='lower',cmap='jet')
        ax.set_xlabel('Phase Bin')
        ax.set_ylabel('Sub-integration Index')
        
    else:
        xgrid = np.arange(residual.shape[1])
        ygrid = np.linspace(np.min(mjds)-1,np.max(mjds)+1,1000)
        ogrid = np.empty([len(ygrid),len(xgrid)])
        for i in xrange(len(ygrid)):
            xidx = np.searchsorted(mjds,ygrid[i])-1
            ogrid[i] = residual[xidx]
        blah = ax.imshow(ogrid,interpolation='nearest',aspect='auto',vmin=-max_range,vmax=max_range,origin='lower')
        ax.set_ylabel('I dunno days or something')

    ax.set_xlabel('Phase Bin')
    if colorbar:
        pl.colorbar(blah)
    if outplot is not None:
        pl.savefig(outplot)

def plot_ism(jname,procdb,combo=None,fignum=10,tbounds=None,fbounds=None,
        flux_bounds=None,nchan_spind=32,save_fluxes=False):
    import dynspec
    procobs = procdb.get_obs(jname,combo=combo,combo_is_regexp=True)
    if len(procobs) == 0:
        return
    mjds = deque()
    bands = deque()
    taus = deque()
    nus = deque()
    fluxes = deque()
    fluxerrs = deque()
    freqs = deque()
    tobs = deque()
    tsubs = deque()
    bws = deque()
    chbws = deque()
    isms = map(dynspec.ISMState,procobs)
    # uuughhh whatever
    new_procobs = deque()
    new_isms = deque()
    for procob,ism in zip(procobs,isms):
        if ism.ds is None:
            continue
        if len(procob.obs.get_toa_flags()) > 0:
            #print 'skipping ',procob.obs.basename,' '.join(procob.obs.get_toa_flags())
            continue
        new_procobs.append(procob)
        new_isms.append(ism)
    procobs = new_procobs
    isms = new_isms

    # compute spectral index
    norm,spind = dynspec.fit_spind((x.ds for x in isms),
        diag_plot='/u/ker14a/newt/tmp/%s_spind.png'%jname,nchan=nchan_spind)
    print 'Norm = %.2f; Spectral index = %.2f'%(norm,spind)


    for procob,ism in zip(procobs,isms):
        try:
            bands.append(ism.band.cm())
        except ValueError:
            continue
        ism.get_scint_params(spind=spind,diag_plot='/u/ker14a/newt/tmp/%s_%s_fits.png'%(jname,procob.obs.basename))
        plot_dynamic_spectrum(ism.ds,output='/u/ker14a/newt/tmp/%s_%s_ds.png'%(jname,procob.obs.basename),spind=spind)
        mjds.append(procob.obs.get_mjd())
        tobs.append(ism.tobs)
        tsubs.append(ism.tsub)
        bws.append(ism.bw)
        chbws.append(ism.chbw)
        if ism.flux is None:
            fluxes.append(np.nan)
            fluxerrs.append(np.nan)
            freqs.append(np.nan)
        else:
            fluxes.append(ism.flux)
            fluxerrs.append(ism.fluxerr)
            freqs.append(ism.flux_freq)
        if ism.scint_params[0] is None:
            taus.append(np.nan)
            nus.append(np.nan)
        else:
            taus.append(ism.scint_params[0])
            nus.append(ism.scint_params[1])

    mjds = np.asarray(mjds)
    bands = np.asarray(bands)
    taus = np.asarray(taus)
    nus = np.asarray(nus)
    fluxes = np.asarray(fluxes)
    fluxerrs = np.asarray(fluxerrs)
    tobs = np.asarray(tobs)
    tsubs = np.asarray(tsubs)
    bws = np.abs(np.asarray(bws))
    chbws = np.asarray(chbws)
    freqs = np.asarray(freqs)

    # compare spectral index with one from averaged fluxes
    mask = ~np.isnan(fluxes) & (freqs > 1000) & ( freqs < 4000)
    def chi(p):
        norm,spind = p
        model = norm*(freqs/1400)**spind
        return ((fluxes-model)/fluxerrs)[mask]
    from scipy.optimize import leastsq
    p = leastsq(chi,[np.mean(fluxes[mask]),-2])
    av_norm,av_spind = p[0]
    print 'av_norm=%.2f, av_spind=%.2f'%(av_norm,av_spind)
        

    # rack unphysical values
    if tbounds is None:
        tmin = 1e1
        tmax = 1e4
    else:
        tmin,tmax = tbounds
    if fbounds is None:
        fmin = 1e-1
        fmax = 2e3
    else:
        fmin,fmax = fbounds
    np.clip(taus,tmin,tmax,out=taus)
    np.clip(nus,fmin,fmax,out=nus)
    if flux_bounds is None:
        flux_min = 0.1
        if jname not in ['J0437-4715','J0835-4510']:
            flux_max = 1e2
        elif jname == 'J0835-4510':
            flux_max = 1e4
        else:
            flux_max = 1e3
    else:
        flux_min,flux_max = flux_bounds
    np.clip(fluxes,flux_min,flux_max,out=fluxes)

    unique_bands = sorted(set(bands))

    import pylab as pl
    #pl.close(fignum);pl.figure(fignum,(8,5))
    pl.figure(fignum);pl.clf();
    dy = 0.26
    axflux = pl.axes([0.12,0.12+0*dy,0.78,dy])
    axfreq = pl.axes([0.12,0.12+1*dy,0.78,dy])
    axtime = pl.axes([0.12,0.12+2*dy,0.78,dy])
    pl.figure(fignum+1); pl.clf()
    axflux2 = pl.axes()
    axflux2.set_yscale('linear')
    if flux_max/flux_min > 30:
        axflux.set_yscale('log')
        #axflux2.set_yscale('log')
    if fmax/fmin > 10:
        axfreq.set_yscale('log')
    if tmax/tmin > 10:
        axtime.set_yscale('log')
    axfreq.yaxis.tick_right()
    axfreq.yaxis.set_ticks_position('both')
    axfreq.yaxis.set_label_position('right')
    flux_mask = ~np.isnan(fluxes)
    scin_mask = ~np.isnan(nus)
    for band in unique_bands:
        if band == 20:
            color = 'green'
            fiducial = 1369.
        elif band == 10:
            color = 'blue'
            fiducial = 3100.
        elif (band == 40) or (band == 50):
            color = 'red'
            fiducial = None
        else:
            continue
        band_mask = bands == band
        mask = flux_mask & band_mask
        if mask.sum() > 0:
            axflux.errorbar(mjds[mask],fluxes[mask],fluxerrs[mask],
                ls=' ',marker='o',color=color,alpha=0.5)
            axflux2.errorbar(mjds[mask],fluxes[mask],fluxerrs[mask],
                ls=' ',marker='o',color=color,alpha=0.5)
            if save_fluxes:
                np.savetxt('%s_%s_fluxes.asc'%(jname,str(band)),np.asarray(
                    [mjds[mask],fluxes[mask],fluxerrs[mask]]).transpose())
            if fiducial is not None:
                axflux.axhline(av_norm*(fiducial/1400)**av_spind,color=color)
                axflux2.axhline(av_norm*(fiducial/1400)**av_spind,color=color)
        mask = scin_mask & band_mask
        if mask.sum() > 0:
            x = mjds[mask]; m = mask; c = color
            axfreq.plot(x,nus[m],ls=' ',marker='o',color=c,alpha=0.5)
            axfreq.plot(x,bws[m],ls=' ',marker='v',ms=3,color=c,alpha=0.5)
            axfreq.plot(x,chbws[m],ls=' ',marker='^',ms=3,color=c,alpha=0.5)
            axtime.plot(x,taus[m],ls=' ',marker='o',color=c,alpha=0.5)
            axtime.plot(x,tobs[m],ls=' ',marker='v',ms=3,color=c,alpha=0.5)
            axtime.plot(x,tsubs[m],ls=' ',marker='^',ms=3,color=c,alpha=0.5)

    axflux.axis([axflux.axis()[0],axflux.axis()[1],flux_min,flux_max])
    axtime.axis([axflux.axis()[0],axflux.axis()[1],tmin,tmax])
    axfreq.axis([axflux.axis()[0],axflux.axis()[1],fmin,fmax])
    axfreq.xaxis.set_ticklabels([])
    axtime.xaxis.set_ticklabels([])
    axflux.set_xlabel('MJD')
    axflux2.set_xlabel('MJD',size='x-large')
    axflux.set_ylabel('Flux (mJy)')
    axflux2.set_ylabel('Flux Density (mJy)',size='x-large')
    axfreq.set_ylabel(r'$\nu_0$ (MHz)')
    axtime.set_ylabel(r'$\tau_0$ (s)')

    pl.title(jname)
    pl.savefig('/u/ker14a/newt/tmp/%s_ism.png'%jname)
    pl.savefig('/u/ker14a/newt/tmp/%s_ism.pdf'%jname)

def plot_dynamic_spectrum(ds,fignum=2,output=None,transform=None,
    plot_contours=False,axis=None,figsize=None,axessize=None,closefig=True,
    **kwargs):
    """ Plot either the dynamic or the secondary spectrum.
    ds == an instance of DisplaySpectrum
    """
    try:
        interpolation = kwargs.pop('interpolation')
    except KeyError:
        interpolation = 'nearest'
    try:
        vmin = kwargs.pop('vmin')
    except KeyError:
        vmin = None
    try:
        vmax = kwargs.pop('vmax')
    except KeyError:
        vmax = None
    if figsize is None:
        figsize = (11,5)
    if axessize is None:
        axessize = [0.10,0.13,0.83,0.80]
        
    import pylab as pl
    if closefig:
        pl.close(fignum);
    pl.figure(fignum,figsize); pl.clf()
    ax = pl.axes(axessize)

    xgrid,xlabel,ygrid,ylabel,image = ds.get_display(**kwargs)
    if xgrid[1] < xgrid[0]:
        xgrid = xgrid[::-1]
        image = np.fliplr(image)
    if transform is not None:
        if transform == 'log':
            image = np.log10(image)
        elif transform == 'sqrt':
            image = np.sqrt(image)
        else:
            print 'Unrecognized transformation.  Reverting to linear.'
    cax = ax.imshow(image,interpolation=interpolation,
        aspect='auto',origin='lower',
        extent=[xgrid[0],xgrid[-1],ygrid[0],ygrid[-1]],
        vmin=vmin,vmax=vmax,cmap='viridis')
    ax.set_xlabel(xlabel,size='x-large')
    ax.set_ylabel(ylabel,size='x-large')
    if plot_contours:
        ax.contour(xgrid,ygrid,image,levels = [image.max()*0.5],colors=['black'])
    if axis is not None:
        ax.axis(axis)
    axcbar = pl.axes([axessize[0]+axessize[2]+0.01,0.13,0.02,0.80])
    cbar = pl.colorbar(cax,cax = axcbar)
    if output is not None:
        pl.savefig(output)
    return ax,axcbar,cbar


def plot_longterm_dynamic_spectrum(dss,nchan=1024,output=None,
    use_spind=False,vmin=None,vmax=None,logflux=False,logscint=True,
    min_scint=0.5,max_scint=None,no_scint=False,
    min_flux=None,max_flux=None,dm_data=None,dm_min=None,dm_max=None,
    cmap=None,
    interpolation='linear',fignum=2):
    """ Make a Jamie/Keith style plot showing the spectrum over long
    time-scales.
    """

    import pylab as pl
    if hasattr(dss[0],'ds'):
        sss = dss
        dss = [x.ds for x in dss]
    else:
        sss = None
    dss = sorted(dss,key=lambda x: x.epoch)
    epochs = deque()
    freqs = deque()
    vals = deque()
    total_flux = deque()
    total_epochs = deque()
    flux_mask = deque()
    for ds in dss:
        f,v = ds.get_fscrunch(nchan=nchan)
        a = np.argsort(f)
        if np.isnan(f[0]):
            flux_mask.append(False)
            continue
        flux_mask.append(True)
        epochs.append([ds.epoch]*nchan)
        freqs.append(f[a])
        vals.append(v[a])
        total_flux.append(np.mean(v))
        total_epochs.append(ds.epoch)
    vals = np.ravel(vals)
    epochs = np.asarray(epochs)
    freqs = np.asarray(freqs)
    total_flux = np.asarray(total_flux)
    flux_mask = np.asarray(flux_mask)
    if use_spind:
        import dynspec
        norm,spind = dynspec.fit_spind(dss,nchan=nchan) 
        print 'Using norm = %.2f and spind = %.2f'%(norm,spind)
        model = norm*(np.ravel(freqs)/1400)**spind
        vals /= model
    coords = np.asarray([np.ravel(epochs),np.ravel(freqs)]).transpose()
    nepoch = 1000
    grid_epochs = np.linspace(epochs.min(),epochs.max(),nepoch)
    grid_freqs = np.linspace(freqs.min(),freqs.max(),nchan*2)
    #xv,yv = np.meshgrid(grid_epochs,freqs[0])
    xv,yv = np.meshgrid(grid_epochs,grid_freqs)
    grid_output = np.asarray([np.ravel(xv),np.ravel(yv)]).transpose()
    h0 = interpolate.griddata(coords,vals,grid_output,method=interpolation)
    if vmax is None:
        vmax = int(np.max(vals))+1
        if use_spind:
            vmax = np.max(vals)+0.1
    if vmin is None:
        vmin = int(np.min(vals[vals>0]))
        if use_spind:
            vmin = max(0,np.min(vals[vals>0])-0.1)
    pl.close(fignum);pl.figure(fignum,(11,7)); pl.clf()
    map_height = 0.55 if dm_data is None else 0.40
    ax = pl.axes([0.10,0.10,0.82,map_height])
    cmap = ax.imshow(h0.reshape((len(grid_freqs),nepoch)),aspect='auto',
        vmin=vmin,vmax=vmax,cmap=cmap,
        extent=[grid_epochs[0],grid_epochs[-1],grid_freqs[-1],grid_freqs[0]])
    #pl.colorbar(cmap,ax=ax)
    cbar_ax = pl.axes([0.93,0.10,0.02,map_height])
    pl.colorbar(cmap,cax=cbar_ax)
    ax.set_xlabel('Epoch (MJD)',size='x-large')
    ax.set_ylabel(r'Obs. Freq. ($\nu$, MHz)',size='x-large')
    # plot observation epochs
    unique_epochs = sorted(set(np.ravel(epochs)))
    unique_freqs = set(np.ravel(freqs))
    bw = max(unique_freqs)-min(unique_freqs)
    ymin = min(unique_freqs)#+0.01*bw
    ax.axis([grid_epochs[0],grid_epochs[-1],grid_freqs[-1],grid_freqs[0]]) 
    for epoch in unique_epochs:
        ax.arrow(epoch,ymin,0,0.04*bw,fc='white',ec='white',
            head_width=0.3,head_length=0.3)
    ax.axis([grid_epochs[0],grid_epochs[-1],grid_freqs[-1],grid_freqs[0]]) 
    #pl.plot(unique_epochs,y,marker='v',color='white',ls=' ')

    #color = 'blue'
    color = get_tableau20()[0]
    trace_start = 0.65 if dm_data is None else 0.50
    trace_height = 0.30 if dm_data is None else 0.225
    ax2 = pl.axes([0.10,trace_start,0.82,trace_height])
    if logflux:
        ax2.set_yscale('log')
    if min_flux is None:
        min_flux = min(total_flux)
    if max_flux is None:
        max_flux = max(total_flux)
    total_flux = np.clip(total_flux,min_flux,max_flux)
    ax2.plot(total_epochs,total_flux,marker='o',ls=' ',color=color,
        ms=8,alpha=1.0 if no_scint else 0.8 )
    ax2.set_ylabel(r'F$_{\nu}$ (mJy)',size='x-large')
    ax2.axis([grid_epochs[0],grid_epochs[-1],min_flux,max_flux])
    ax2.get_xaxis().set_ticks([])

    if not no_scint:
        color = get_tableau20(only_dark=True)[1]
        if sss is None:
            import dynspec
            sss = [dynspec.SecondarySpectrum(x,spind=spind) for x in dss]
        sss = sorted(sss,key=lambda x: x.ds.epoch)
        sss = [x for i,x in enumerate(sss) if flux_mask[i]]
        scales = np.abs([x.do_fits() for x in sss])
        bws = scales[:,0]
        ax2_twin = ax2.twinx()
        ax2_twin.set_ylabel('B$_{d}$ (MHz)',size='x-large')
        if max_scint is None:
            max_scint = max(bws)
        bws = np.clip(bws,min_scint,max_scint)
        if logscint:
            ax2_twin.set_yscale('log')
        ax2_twin.plot(total_epochs,bws,
            marker='*',ls=' ',mec='k',color=color,
            ms=10,alpha=0.8)
        ax2_twin.axis([grid_epochs[0],grid_epochs[-1],min_scint,max_scint])
        ax2_twin.get_xaxis().set_ticks([])

    if dm_data is not None:
        #color = 'k'
        color = get_tableau20()[0]
        ax3 = pl.axes([0.10,0.725,0.82,0.225])
        mjds,dm,dme = dm_data
        ax3.errorbar(mjds,dm,yerr=dme,capsize=0,marker='o',ls=' ',
            color=color)
        #ax3.set_ylabel('Dispersion Measure',size='x-large')
        ax3.set_ylabel('$\Delta$DM',size='x-large')
        ymin = dm_min or ax3.axis()[2]
        ymax = dm_max or ax3.axis()[3]
        ax3.axis([grid_epochs[0],grid_epochs[-1],ymin,ymax])
        ax3.get_xaxis().set_ticks([])

    if output is not None:
        pl.savefig(output)

def plot_longterm_dynamic_spectrum_from_procdb(
    jname,procdb,combo='20CM_MULTI',nchan=128):

    import dynspec

    procobs = procdb.get_obs(jname,combo=combo)
    #procobs1 = procdb.get_obs(jname,combo='20CM_MULTI')
    #procobs2 = procdb.get_obs(jname,combo='10CM_1050CM')
    #procobs = procobs1 + procobs2
    isms = map(lambda x: dynspec.ISMState(x,use_tscrunch=False), procobs)
    dss = [x.ds for x in isms if (x.ds is not None) and (x.flux is not None) and len(x.toa_flags)==0]
    output = '%s_long_ds.png'%jname
    plot_longterm_dynamic_spectrum(dss,nchan=nchan,output=output)
    return dss

def plot_longterm_spectrum_traces(dss,nchan=1024,output=None):
    import pylab as pl
    from matplotlib import cm
    cmap = cm.copper
    dss = sorted(dss,key=lambda x: x.epoch)
    epochs = deque()
    freqs = deque()
    vals = deque()
    for ds in dss:
        f,v = ds.get_fscrunch(nchan=nchan)
        a = np.argsort(f)
        if np.isnan(f[0]):
            continue
        epochs.append(ds.epoch*nchan)
        freqs.append(f[a])
        vals.append(v[a])
    time = np.asarray(epochs)
    tcoord = (time-time.min())/(time.max()-time.min())
    pl.clf()
    for f,v,t in zip(freqs,vals,tcoord):
        color = cmap(t)[:-1]
        pl.plot(f,v,marker=' ',color=color)

def plot_spectra(dss,nchan=1024,max_flux=None,outstem=None,epochs=None):
    """ Quick and dirty plot for selecting spectra."""
    import pylab as pl
    from matplotlib import cm
    cmap = cm.copper
    dss = sorted(dss,key=lambda x: x.epoch)

    if epochs is not None:
        good_dss = deque()
        for epoch in epochs:
            for ds in dss:
                if abs(epoch-ds.epoch) < 0.02:
                    good_dss.append(ds)
                    break
        print 'Keeping %d epochs.'%len(good_dss)
        dss = good_dss

    epochs = deque()
    freqs = deque()
    vals = deque()
    errs = deque()
    good_dss = deque()
    for ds in dss:
        f,v,e = ds.get_fscrunch(nchan=nchan,get_error=True)
        a = np.argsort(f)
        if np.isnan(f[0]):
            continue
        epochs.append(ds.epoch*nchan)
        freqs.append(f[a])
        vals.append(v[a])
        errs.append(e[a])
        good_dss.append(ds)
    time = np.asarray(epochs)
    tcoord = (time-time.min())/(time.max()-time.min())
    pl.clf()
    for f,v,e,t,d in zip(freqs,vals,errs,tcoord,good_dss):
        color = cmap(t)[:-1]
        mask = v > 0
        pl.errorbar(f[mask],v[mask],yerr=e[mask],marker='o',ls='',
            color=color)
        if outstem is not None:
            pl.title('%.2f'%d.epoch)
            if max_flux is not None:
                pl.axis([pl.axis()[0],pl.axis()[1],0,max_flux])
            pl.savefig('%s_%.2f.png'%(outstem,d.epoch))
            pl.clf()

def plot_spectra2(dss,nchan=1024,max_flux=None,log_scale=False,
    show_lines=False):
    """ Quick and dirty plot for selecting spectra."""

    import pylab as pl
    dss = sorted(dss,key=lambda x: x.epoch)

    set_rcParams(ticklabelsize='large')
    colors = get_tableau20(only_dark=False)
    pl.clf()
    ax = pl.axes([0.10,0.10,0.86,0.86])

    counter = 0
    for ds in dss:
        f,v,e = ds.get_fscrunch(nchan=nchan,get_error=True)
        if np.isnan(f[0]):
            continue
        a = np.argsort(f)
        f = f[a]; v = v[a]; e = e[a]
        mask = v > 0
        color = colors[counter%len(colors)]
        counter += 1
        if log_scale:
            ax.set_yscale('log')
        ls = '-' if show_lines else ''
        marker = None if show_lines else 'o'
        #ax.errorbar(f[mask],v[mask],yerr=e[mask],marker=marker,ls=ls,
            #color=color,label='%d'%(round(ds.epoch)))
        ax.errorbar(f[mask],v[mask],marker=marker,ls=ls,
            color=color,label='%d'%(round(ds.epoch)))
        ax.text(f[mask][0]-32,v[mask][0],'%d'%(round(ds.epoch)),
            color=color,size='large')

    #pl.legend(loc='upper right',ncol=3,numpoints=1,prop={'size':'medium'})
    ax.set_xlabel('Observing Frequency (MHz)',size='x-large')
    ax.set_ylabel('Flux Density (mJy)',size='x-large')

set_viridis()
