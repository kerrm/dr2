import numpy as np
from collections import deque
import cPickle
import os
from os.path import join,isfile,isdir,split,abspath,basename
import shutil

from observation import Observation
import parse_corrections
import zapping
import util

def merge_databases(dbs):
    """ NB this is a "fast merge" as the dbs are not checked for
        consistency with respect to corrections."""
    if len(dbs)==1:
        return dbs[0].copy()
    newdb = dbs[0].copy()
    for db in list(dbs)[1:]:
        newdb.obs.extend(db.obs)
    newdb._update_cache()
    return newdb

def merge_saved_databases(fnames):
    dbs = map(Database,fnames)
    return merge_databases(dbs)

def Database(fname=None,clobber=False,verbosity=1,checkpoint=None):
    """ Factory to generate a Database.
    
    Determine whether we're instantiating a new Database or loading one 
    from a pickle.
    """
    if (fname is None) or clobber:
        return Database_Base()
    # keep track of observations that fail to load correctly
    blacklist = deque()
    if hasattr(fname,'__iter__'):
        db = Database_Base()
        for fi,f in enumerate(fname):
            if (checkpoint is not None) and fi%50==0 and fi!=0:
                # checkpoint the current database
                cPickle.dump(db,file(checkpoint,'w'))
            if verbosity > 0:
                print '%4d working on %s...'%(fi,f)
            try:
                db.add_obs(Observation(f))
            except Exception as e:
                print e.__class__,e
                blacklist.append(f)
        db.blacklist = list(blacklist)
        return db
    return cPickle.load(file(fname))

class Database_Base(object):
    """ Manage records of observations."""

    def __init__(self):
        self.obs = deque()
        self.blacklist = []
        self.correction_files = []
        self.zapping_files = []
        self.corrections = []
        self._update_cache()
        # I think this is in because cache can't be OK if empty?
        self.cache_ok = False

    def __setstate__(self,state):
        # temporary
        if not hasattr(self,'zapping_files'):
            state['zapping_files'] = []
        self.__dict__.update(state)

    def __ieq__(self,other):
        """ Concatenate another database."""
        raise NotImplementedError
        self.cache_ok = False
        self.obs.extend(other.obs)

    def __len__(self):
        return len(self.obs)

    def copy(self):
        db = Database_Base()
        db.__dict__.update(self.__dict__)
        db.obs = deque()
        db.obs.extend(self.obs)
        db._update_cache()
        return db

    def update(self,fnames):
        """ Examine indicated (obs) files and add to database if absent.

        This is based on a simple examination of the filename, so should be
        speedy for differential updates.
        """
        current_fnames = set((x.basename for x in self.obs))
        fnames = filter(lambda x: basename(x) not in current_fnames,fnames)
        if len(fnames) == 0:
            return
        new_db = Database(fnames)
        map(new_db.add_correction,self.correction_files)
        self.append_db(new_db)

    def delete(self,fnames):
        """ Remove entries with matching file names."""
        if not hasattr(fnames,'__iter__'):
            fnames = [fnames]
        delete_fnames = map(basename,fnames)
        nobs = len(self.obs)
        self.obs = filter(
            lambda x: basename(x.fname) not in delete_fnames,self.obs)
        nchange = nobs-len(self.obs)
        print 'Deleted %d entries.'%nchange
        self._update_cache()

    def delete_complement(self,fnames):
        """ Remove entries that do NOT match indicated file names."""
        fnames = set(map(lambda x:basename(x),fnames))
        self.obs = filter(lambda x: basename(x.fname) in fnames,self.obs)
        self._update_cache()

    def delete_startswith(self,expr):
        """ Delete all entries whose basename starts with expr.

        E.g., to delete all "no SK" CASPSR entries, one could use "p_sk".
        """
        nobs = len(self.obs)
        self.obs = filter(
            lambda x: not x.basename.startswith(expr),self.obs)
        nchange = nobs-len(self.obs)
        print 'Deleted %d entries.'%nchange
        self._update_cache()

    def _update_cache(self,force=True):
        if (not force) and self.cache_ok:
            return
        self.sources = np.asarray(
            [obs['name'].split('_')[0] for obs in self.obs])
        self.modes = np.asarray([obs['OBS_MODE'] for obs in self.obs])
        self.valid = np.asarray([obs.valid() for obs in self.obs])
        self.cache_ok = True

    def add_obs(self,obs):
        self.cache_ok = False
        self.obs.append(obs)
        #if obs.valid:
            #self.obs.append(obs)
        for corrs in self.corrections:
            for corr in corrs:
                corr(obs)

    def dedup(self):
        """ Remove entries with the same file (modulo path).

        The Marsfield data store has duplicate entries of some files, and
        it is more efficient to eliminate them from the database than to
        cull them at read time.

        Note that differential updates to the database are automatically
        deduplicated.
        """
        unique_o = deque()
        unique_b = set()
        for o in self.obs:
            if o.basename in unique_b:
                continue
            unique_o.append(o)
            unique_b.add(o.basename)
        self.cache_ok = False
        self.obs = unique_o

    def add_correction(self,correction_file,verbosity=0):
        """ Parse a correction file and store the object.

        The list of corrections is maintained in memory and serialized when
        the database is stored.  Any new observations added to the database
        will have the corrections applied to them iff they have not already
        been applied.  (E.g., when merging two identically-processed
        database objects.)
        """
        if basename(correction_file) in map(basename,self.correction_files):
            return
        self.cache_ok = False
        self.correction_files.append(correction_file)
        corrs = parse_corrections.parse_file(correction_file,
            verbosity=verbosity)
        self.corrections.append(corrs)
        map(lambda corr: map(corr,self.obs), corrs)

    def reapply_corrections(self):
        """ Manually re-apply corrections to the database.

        This might be used if the correction implementation has changed.
        NB that it does not restore the state of the original Observations,
        so caveat emptor.
        """
        # loops through groups of corrections and members of group
        for corrs in self.corrections:
            for corr in corrs:
                map(corr,self.obs)

    def add_zapping(self,zap_file,check_changed=True):
        """ Parse a zap file and apply the relevant psrsh to Observations.

        After a given zap file has been processed once, produce a cache of
        the zaps.  Then, if there are new/changed zaps in future runs, make
        sure the Observation object is set to be clobbered in order to
        apply the new zaps.
        """
        if basename(zap_file) in map(basename,self.zapping_files):
            return
        self.cache_ok = False
        self.zapping_files.append(zap_file)
        label,zaps = zapping.parse_zaps(zap_file)
        print 'Adding zap %s'%zap_file

        # keys are observation basename
        keys = zaps.keys()

        # e.g. func = zapping.galileo_zaps
        try:
            func = eval('zapping.%s_zaps'%label)
        except AttributeError as e:
            print 'Could not interpret zapping file %s.'%zap_file
            raise e

        try:
            os.mkdir(util.local_file('state'))
        except OSError:
            pass
        cache_fname = util.local_file('state/%s.pickle'%basename(zap_file))
        cache = None
        if check_changed:
            try:
                cache = cPickle.load(file(cache_fname))
            except IOError: 
                # write out a cache for next time around
                cPickle.dump(zaps,file(cache_fname,'w'))

        for o in self.obs:
            if o.basename in keys:
                func(o,zaps[o.basename])
                if (cache is not None):
                    # element-wise list equality
                    if ((o.basename not in cache.keys()) or
                        (cache[o.basename] != zaps[o.basename])):
                        print 'Will clobber %s due to new zaps.'%(
                            o.basename)
                        o.clobber = True
                        # only update the cache when a given observation
                        cache[o.basename] = zaps[o.basename]
                    
        if (cache is not None):
            cPickle.dump(cache,file(cache_fname,'w'))

    def get_obs(self,modes=['PSR'],require_valid=True,date_sort=False,
        new_only=False):
        """ Return entries with PSR type.
        
            date_sort -- if True, return most recent observations first"""
        if len(self.obs) == 0:
            return []
        self._update_cache(force=False)
        mask = self.modes==modes[0]
        for mode in modes[1:]:
            mask |= (self.modes==mode)
        if require_valid:
            mask &= self.valid
        if new_only and hasattr(self,'new_mask'):
            mask &= self.new_mask
        obs = [self.obs[i] for i in np.ravel(np.argwhere(mask))]
        if date_sort:
            return sorted(obs,key=lambda o: o.get_mjd(),reverse=True)
        return obs

    def get_cals(self,require_valid=True):
        return self.get_obs(modes=['CAL','LEVCAL','FOF','FON'],
            require_valid=require_valid)

    def get_valid(self):
        """ Return all valid Observation objects."""
        self._update_cache(force=False)
        mask = self.valid
        return [self.obs[i] for i in np.ravel(np.argwhere(mask))]

    def save(self,fname):
        """ Pickle to indicated file, or to existing file if none provided.
        
        Use a temporary file for write, then copy over when write out is
        successful.  This helps to prevent corruption of the database if
        interrupted during the save process."""

        tf = util.TemporaryFile()
        cPickle.dump(self,file(str(tf),'w'),protocol=2)
        shutil.move(str(tf),fname)

    def _copy_state(self,other):
        """ Copy everything BUT the Observation objects."""
        d = self.__dict__.copy()
        d.pop('obs')
        other.__dict__.update(d)
            
    def filter_db(self,filt):
        """ Apply the filter to each observation in the database, and return
            a new database object with only those passing the criteria."""
        db = Database()
        self._copy_state(db)
        db.obs = filter(filt,self.obs)
        db._update_cache()
        return db

    def append_db(self,other,update_cache=True):
        if (sorted(map(basename,self.correction_files)) != 
            sorted(map(basename,other.correction_files))):
            #raise ValueError('Incompatible correction levels.')
            print 'WARNING! Databases have different correction levels.'
        self.obs.extend(other.obs)
        if update_cache:
            self._update_cache()
        return self

    def extend_db(self,others):
        for other in others:
            self.append_db(other,update_cache=False)
        self._update_cache()
        return self

    def get_overlapping_obs(self,obs,min_overlap=None):
        """ Return any observations overlapping the indicated observation.

        This boils down to having both overlapping time and frequency.
        """
        min_overlap = 0 if min_overlap is None else min_overlap
        overlap_obs = filter(
            lambda o: obs.overlap(o) > min_overlap,self.obs)
        return overlap_obs,map(obs.overlap,overlap_obs)

    def get_by_basename(self,name):
        """ Find an Observation by basename if it exists."""
        for obs in self.obs:
            if obs.basename == name:
                return obs
        
"""
from mpi import MPI_Jobs
class MPI_Database_Maker(MPI_Jobs):

    def __init__(self,files,work_constructor,verbosity=0):
        from mpi4py import MPI
        comm = MPI.COMM_WORLD
        work = deque()
        stride = max(1,comm.size-1)
        for i in xrange(stride):
            work.append(work_constructor(files[i::stride]))
        super(MPI_Database_Maker,self).__init__(work,verbosity=verbosity)

    def get_db(self):
        if len(self.done) == 0:
            self.do_work(max_threads=2,kill_threads=False)
        fnames = [x._db for x in self.done]
        db = merge_saved_databases(fnames)
        for fname in fnames:
            os.remove(fname)
        return db
"""
