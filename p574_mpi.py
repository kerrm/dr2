import matplotlib
matplotlib.use('Agg')

import numpy as np
import os
from os.path import join,isfile,abspath,expandvars,basename,isdir
from glob import glob
from collections import deque
import cPickle

import template
import ephemeris
import mpi
import html
import marsfield
import timing
import util
import processed

CLOBBER_CALDB = False
CLOBBER_OBSDB = False
CLOBBER_PROCDB = False
CLOBBER_DATA = False
CLOBBER_TOAS = False
CLOBBER_HTML_PLOTS = False
DO_QUICKLOOK = True
INSTALL_EPHEMERIS = True
STRICT_EPHEMERIS = False

MODE = 'paper'
#MODE = 'other'

# load "new" pulsars
if MODE=='paper':
    plist = 'pulsarlists/p574paper.asc'
else:
    plist = 'pulsarlists/P574_diff.asc'
jnames = sorted(util.readlines(plist))
#jnames = ['J0835-4510','J1057-5226']
directories = marsfield.get_directories(['AFB','WBC','DFB'])
#directories = marsfield.get_directories(['AFB'])
#regexp = 's1404.*' # get April obs. only
#regexp = '[st]1[34].*' # all 2013-2014 obs.
regexp = None

if MODE == 'paper':
    outpath = '/u/ker14a/newt/P574/test'
else:
    outpath = '/u/ker14a/newt/P574/other'
calpath = join(outpath,'cals') # one single place for P574 cals

def get_caldb(clobber=False):
    """ "Static" instantiation of invariant caldb objects.

    Since we use one set of calibration databases for P574, when looping
    through the many pulsars, this lets us just run this function the first
    time.
    """
    if not hasattr(get_caldb,'caldb'):
        if not isdir(calpath):
            os.makedirs(calpath)
        get_caldb.caldb = marsfield.make_p574_caldb(
            '%s/caldb.pickle'%(calpath),directories,clobber=clobber,
            regexp=regexp)
        get_caldb.pcmdb = marsfield.make_pcmdb('%s/pcmdb.pickle'%calpath,
            pcmdir=expandvars('$PPTA/pcm/20cm'),
            suppldir=join(outpath,'supplementary_pcm'),clobber=clobber)
        get_caldb.fluxdb = marsfield.make_fluxdb('%s/fluxdb.pickle'%calpath,
            fluxdir=expandvars('$PPTA/fluxcal'),
            suppldir=join(outpath,'supplementary_fluxcal'),clobber=clobber)
        get_caldb.templdb = template.P574_TemplateDatabase(
            join(outpath,'old_p574_pipeline_templates'),
            suppldir=join(outpath,'templates'))
        get_caldb.ephdb = ephemeris.P574_EphemerisDatabase(
            join(outpath,'ephemerides'))
        add_caldb_zapping(get_caldb.caldb)
        get_caldb.caldb.add_correction(
            util.local_file('corrections/dfb3_quarter_band.asc'))
    return (get_caldb.caldb,get_caldb.pcmdb,get_caldb.fluxdb,
        get_caldb.templdb,get_caldb.ephdb)

def add_caldb_zapping(caldb):
    files = ['manual_zap_p574_cals','p574_cals_zap_dfb2_july_2007',
        'dfb2_jul2008_pol_quarter_band']

    for zap in files:
        caldb.add_zapping(util.local_file('obslists/%s.asc'%zap))

def add_obsdb_zapping(obsdb):
    files = ['midweek_twoband','midweek_twoband_other',
        'midweek_fiveband','compass_zaps','compass_full_zaps',
        'galileo_zaps','galileo_full_zaps','dfb2_jul2008_pol_quarter_band',
        'dfb1_artefact_zap']
    if MODE == 'paper':
        files.append('manual_zap_p574')
    else:
        files.append('manual_zap_p574_other')
    for zap in files:
        obsdb.add_zapping(util.local_file('obslists/%s.asc'%zap))

def get_analysis(jname,clobber_obsdb=False,clobber_caldb=False):

    print 'Analysis for %s.'%jname
    psrpath = join(outpath,jname)
    if not isdir(psrpath):
        os.makedirs(psrpath)

    obsdb = marsfield.make_p574_obsdb('%s/obsdb.pickle'%(psrpath),
        directories,jname,clobber=clobber_obsdb,regexp=regexp)
    obsdb.add_correction(
        util.local_file('corrections/dfb3_quarter_band.asc'))
    obsdb.add_correction(
        util.local_file('corrections/fix_early_data.asc'))
    add_obsdb_zapping(obsdb)

    caldb,pcmdb,fluxdb,templdb,ephdb = get_caldb(clobber=clobber_caldb)

    db = obsdb.append_db(caldb)

    analysis = timing.P574_TimingAnalysis(db,calpath,psrpath,
        ephdb=ephdb,templdb=templdb,pcmdb=pcmdb,fluxdb=fluxdb,
        zap_subints='dry',install_eph=INSTALL_EPHEMERIS,
        strict_eph=STRICT_EPHEMERIS,make_dynspec=True)

    return analysis

from mpi4py import MPI
comm = MPI.COMM_WORLD



def load_procdb():
    tstart = MPI.Wtime()
    procdb_fname = join(outpath,'procdb.pickle')
    if CLOBBER_PROCDB or (not isfile(procdb_fname)):
        procdb = processed.ProcDB()
    else:
        procdb = cPickle.load(file(procdb_fname))
    return MPI.Wtime()-tstart,procdb

def dump_procdb(procdb):
    # hmm
    procdb_fname = join(outpath,'procdb.pickle')
    cPickle.dump(procdb,file(procdb_fname,'w'),protocol=2)


def make_html(jnames=None,procdb=None,manager=None):
    if procdb is None:
        tprocdb,procdb = load_procdb()
    tstart = MPI.Wtime()
    output = html.P574_BandOverviewTable2(procdb,outpath)
    clobber_plots = CLOBBER_HTML_PLOTS
    output.add_entries(jnames,manager=manager,clobber_plots=clobber_plots)
    output.write(fname='band.html')

    if DO_QUICKLOOK:
        output = html.PPTA_QuickLook(procdb,outpath=outpath,tspan=45)
        output.to_html(fname=join(outpath,'quicklook.html'))

    return MPI.Wtime()-tstart

if comm.rank == 0:

    tstart = MPI.Wtime()

    tprocdb,procdb = load_procdb()

    analyses = [get_analysis(jname,
        clobber_obsdb=CLOBBER_OBSDB,clobber_caldb=CLOBBER_CALDB)
        for jname in jnames]

    work = deque()
    for analysis in analyses:
        w,all_work = analysis.make_work(
            clobber=CLOBBER_DATA,clobber_toas=CLOBBER_TOAS)
        work.extend(all_work)

    manager = mpi.MPI_Jobs(verbosity=1)
    manager.do_work(work) # automatically sleeps threads when finished

    mpi.thread_stats(manager)
    print 'CAL throughput --'
    mpi.throughput_stats(manager,do_obs=False)
    print 'OBS throughput --'
    mpi.throughput_stats(manager,do_obs=True)
    #mpi.throughput_plot(manager,join(outpath,'all_throughput.png'))

    coadd_work = deque()

    for jname,analysis in zip(jnames,analyses):
        #analysis.tarball(jname)
        procdb.add_analysis(analysis)
        procdb.get_toa_tarball(jname,strict_select=False)
        #coadd_work.extend(procdb.get_coadded_work(jname))

    #manager.do_work(coadd_work)

    # here's a weird thing -- some WBCORR obs have wrong jname... so they
    # get added to procdb as such.  Just delete them for now
    #procdb.delete_complement(jnames)
    dump_procdb(procdb)

    ttotal = (MPI.Wtime() - tstart)
    thtml = make_html(jnames=jnames,procdb=procdb,manager=manager)
    print 'procdb load time: %d seconds'%(int(round(tprocdb)))
    print 'HTML elapsed time: %.2f minutes.'%(thtml/60)
    print 'Total elapsed time: %.2f minutes'%(ttotal/60)

    manager.kill_threads()

else:
    worker = mpi.MPI_Worker()
    worker.do_work()
