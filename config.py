""" Parse configuration file and handle defaults."""

import util

class Configuration(object):

    input_options = {
        'pulsars' : [None, 'specify the list of pulsars to process, one pulsar name per line', True],
        'regexp' : [None, 'select files via regular expression', False],
        'data_dirs': [None, 'specify a comma-separated list of directories in which to search for data; within the directories, data should be arranged according to pulsar name, e.g. data_dir1/J0835-4510.',True],
        'ephemeris_dir' : [None, 'specify a directory with only ephemerides of the form JNAME*.par', False],
        'template_dir' : [None, 'specify a directory with only templates in it; form specified in template.py', False],
        'fluxcal_dir' : [None, 'specify a directory with only fluxcal files in it', False],
        'pcmcal_dir' : [None, 'specify a directory with only pcm (MEM) files in it', False],
        'zap_files' : [None, 'specify a comma-separated list of zap files', False],
     }

    output_options = {
        'clobber_caldb' : [False, 'clobber the calibration database', False],
        'clobber_obsdb' : [False, 'clobber the observation database', False],
        'clobber_procdb' : [False, 'clobber the database storing processing results', False],
        'clobber_data' : [False, 're-process all data', False],
        'clobber_toas' : [False, 're-process TOAs', False],
        'label' : [None, 'label to tag output files with', False],
        'output_dir' : [None, 'directory to store output files', True],
        #'logfile' : ['log.asc', '
        'make_html' : [True, 'produce output HTML pages', False],
        'make_dynspec' : [False, 'produce dynamic spectrum output', False],
    }

    processing_options = {
        'do_pcmcal' : [False, 'apply a pcm calibration to data', False],
        'do_fluxcal' : [True, 'apply a flux calibration to data', False],
        'zap_subints' : ['dry', 'apply automatic subint zapping (experimental!)', False],
        'pat_args': [None, 'additional command line arguments for pat/TOA extractions', False]
    }


    def init(self):

        self.all_options = [
            Configuration.input_options,
            Configuration.output_options,
            Configuration.processing_options
        ]

        for opt in self.all_options:
            for k in opt.keys():
                self.__dict__[k] = opt[k][0]

    def __init__(self,fname):

        self.init()

        for line in util.readlines(fname):
            left,right = line.split('=')
            key = left.strip()
            if key not in self.__dict__.keys():
                raise ValueError(
                    'Unrecognized configuration option [%s]!'%key)
            val = right.strip()
            if val in ['True','False','true','false']:
                if val in ['True','true']:
                    self.__dict__[key] = True
                    continue
                if val in ['False','false']:
                    self.__dict__[key] = False
                    continue
            self.__dict__[key] = val

        # parse file lists
        if self.zap_files is not None:
            self.zap_files = [x.strip() for x in self.zap_files.split(',')]
        else:
            self.zap_files = []

        self.check_defaults()

        self.data_dirs = [x.strip() for x in self.data_dirs.split(',')]

    def check_defaults(self):
        """ Make sure everything necessary has been set."""
        for opt in self.all_options:
            for k in opt.keys():
                if (self.__dict__[k] is None) and opt[k][2]:
                    raise ValueError('Must specify %s.'%k)

def make_template(fname):
    """ Generate a template file with default values. """
    output = []
    confs = [
        Configuration.input_options,
        Configuration.output_options,
        Configuration.processing_options
    ]
    labels = [
        '#### INPUT OPTIONS ###########',
        '#### OUTPUT OPTIONS ##########',
        '#### PROCESSING OPTIONS ######'
    ]
    for opt,label in zip(confs,labels):
        output.append("\n%s\n"%label)
        for k in opt.keys():
            default,comment,mandatory = opt[k]
            output.append('# %s'%comment)
            if (default is None):
                if mandatory:
                    output.append('%s = MANDATORY\n'%k)
                else:
                    output.append('#%s = OPTIONAL\n'%k)
            else:
                output.append('%s = %s\n'%(k,default))
        output.append("\n##############################\n")

    file(fname,'w').write('\n'.join(output))

if __name__ == '__main__':
    import sys
    if len(sys.argv) > 1:
        make_template(sys.argv[1])
    else:
        make_template('generic.conf')
