#!/usr/bin/env python
import sys
import os
from os.path import join

# TODO -- write an optparse version for 2.6?
import argparse

# dr2 stuff
import alias
import util
import make_dsets

PIPEPATH = '/DATA/NEWTON_1/pulsar/ker14a/pipeline'
DATAPATH = '/DATA/NEWTON_1/pulsar/ker14a/ppta/test'

def do_apply_flags(jname,par,tim,sel):
    """ Run tempo2 in order to apply the SELECT file to the data."""
    cmd = 'tempo2 -f %s %s -select %s -nofit -writetim'%(par,tim,sel)
    code,stdout,stderr = util.run(cmd,echo=False)
    os.system('mv out.tim %s.tim'%jname)

def get_all():
    """ Return the list of "standard" PPTA pulsars."""
    fname = file(PIPEPATH+'/pulsarlists/ppta_pulsars.asc').readlines()


if __name__ == '__main__':
    desc = 'Get TOAs and ephemerides for specified pulsars with optional selection in time, receiver, backend, etc.'
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('pulsars',nargs='*',default='all',help='Specify a list of pulsars for which to get TOAs, e.g. J0437-4715 J1909-3744.  If no pulsars are specified, defaults to all "standard" PPTA pulsars.')
    parser.add_argument('--apply_flags',type=bool,default=True,help='Filter out TOAs that have been flagged as unreliable.  Defaults to True.  Otherwise, a select file for use with tempo2 will be provided.')
    parser.add_argument('--interval',default='all',help='Specify a time interval for TOAs.',choices=['all','year','month','week'])
    parser.add_argument('--backend',default='all',help='Specify a backend for TOAs.',choices=['all','dfb4','dfb3','dfb2','dfb1','dfb','caspsr'])
    parser.add_argument('--band',default='all',help='Specify a band for TOAs.',choices=['all','10cm','20cm','40cm','50cm'])

    args = parser.parse_args()

    if args.pulsars[0] == 'all':
        plist = 'pulsarlists/ppta_pulsars.asc'
        pulsars = sorted(map(alias.preferred_name,util.readlines(plist)))
    else:
        pulsars = args.pulsars

    # first, process each file into a local set of tims
    for pulsar in pulsars:
        print 'Working on pulsar %s.'%pulsar
        par = join(DATAPATH,pulsar,'%s.par'%pulsar)
        if not os.path.isfile(par):
            print 'Could not find ephemeris for %s.'%(pulsar)
            continue
        os.system('cp %s .'%par)
        sel = join(DATAPATH,pulsar,'%s.select'%pulsar)
        if not os.path.isfile(sel):
            print 'Could not find select file for %s.'%(pulsar)
            continue
        tim = join(DATAPATH,pulsar,'%s.toa'%pulsar)
        if not os.path.isfile(tim):
            print 'Could not find TOAs for %s.'%(pulsar)
            continue
        if args.apply_flags:
            do_apply_flags(pulsar,par,tim,sel)
        else:
            os.system('cp %s .'%sel)
            os.system('cp %s .'%tim)


        # now parse the individual cuts out
        import make_dsets as mds
        tim = '%s.tim'%pulsar
        toas = mds.parse_toas(tim)

        if args.interval!='all':
            print 'Selecting only TOAs within 1 %s of final TOA.'%(
                args.interval)
            min_mjd = 0
            for toa in toas:
                if toa.mjd > min_mjd:
                    min_mjd = toa.mjd
            mapping = dict(year=365,month=31,week=7)
            min_mjd = min_mjd - mapping[args.interval]
            toas = [x for x in toas if x.mjd >= min_mjd]

        if args.backend != 'all':
            backend = args.backend.upper()
            print 'Selecting only TOAs with backend==%s.'%(backend)
            toas = [x for x in toas if backend in x.backend()]

        if args.band != 'all':
            band = args.band.upper()
            print 'Selecting only TOAs with band==%s.'%(band)
            toas = [x for x in toas if x.band()==band]

        output = ''.join((x.line for x in toas))
        file(tim,'w').write('%s\n%s'%('FORMAT 1',output))
