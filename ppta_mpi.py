import matplotlib
matplotlib.use('Agg')

import os
import itertools
from os.path import join,isfile,abspath,expandvars,basename,isdir
import cPickle

import mpi
import marsfield
import html
import timing
import util
import processed
import ephemeris
import template
import alias

CLOBBER_CALDB = False
CLOBBER_OBSDB = False
CLOBBER_PROCDB = False
CLOBBER_DATA = False
CLOBBER_TOAS = False
NEW_ONLY = False
DO_PCM = True
DO_FLUXCAL = True
MAKE_PCM = False
MAKE_HTML = True
DO_QUICKLOOK = True
INSTALL_EPHEMERIS = True

plist = 'pulsarlists/ppta_pulsars.asc'
all_jnames = sorted(map(alias.preferred_name,util.readlines(plist)))
all_jnames.extend(['HYDRA_N','HYDRA_O','HYDRA_S'])
#all_jnames = jnames = ['HYDRA_N','HYDRA_O','HYDRA_S']
jnames = all_jnames
jnames = ['J0437-4715']

if MAKE_PCM:
    all_jnames = jnames = ['J0437-4715']

#outpath = '/u/ker14a/newt/ppta/test'
outpath = '/DATA/NEWTON_1/pulsar/ker14a/ppta/test'

directories = marsfield.get_directories(['WBC','DFB','NEW_CPSR2','CASPSR'])
#directories = marsfield.get_directories(['WBC'])
#regexp = '[st]1[4,5].*'
regexp = None

def get_analysis(jname,clobber_obsdb=False,clobber_caldb=False,
    do_pcm=True,do_fluxcal=True,make_invint=False):

    hydra = 'HYDRA' in jname
    psrpath = join(outpath,jname)
    if hydra:
        calpath = psrpath
    else:
        calpath = join(psrpath,'cals')

    if MAKE_PCM:
        psrpath = join(outpath,'pcm',jname)
        # if doing PCM cals, keep only full-track observations
        fnames = util.readlines(util.local_file('obslists/pcm_obs.asc'))
    else:
        fnames = None

    if not isdir(calpath):
        os.makedirs(calpath)
    if not isdir(psrpath):
        os.makedirs(psrpath)

    caldb = marsfield.make_ppta_caldb('%s/caldb.pickle'%(calpath),
        directories,jname,clobber=clobber_caldb,update=True,regexp=regexp)
    obsdb = marsfield.make_ppta_obsdb('%s/obsdb.pickle'%(psrpath),
        directories,jname,clobber=clobber_obsdb,update=True,regexp=regexp,
        fnames=fnames)
    if not (hydra or MAKE_PCM):
        if do_pcm:
            pcmdb = marsfield.make_pcmdb('%s/pcmdb.pickle'%calpath,
                pcmdir=expandvars('$PPTA/pcm/20cm'),clobber=clobber_caldb,
                suppldir=join(outpath,'supplementary_pcm'))
        else:
            pcmdb = None
        if do_fluxcal:
            fluxdb = marsfield.make_fluxdb('%s/fluxdb.pickle'%calpath,
                fluxdir=expandvars('$PPTA/fluxcal'),clobber=clobber_caldb,
                suppldir=join(outpath,'supplementary_fluxcal'))
        else:
            fluxdb = None
        ephdb = ephemeris.PPTA_EphemerisDatabase(
            ephdir=expandvars('$PPTA_EPHEMERIDES'),
            suppldir=join(outpath,'supplementary_ephemerides'))
        templdb = template.PPTA_TemplateDatabase(
            templdir=expandvars('$PPTA_TEMPLATES'),
            suppldir=join(outpath,'supplementary_templates'))

    print 'found %d obs'%(len(obsdb))

    # temporary test
    # TODO -- these ultimately need to be placed back in fix_ppta_data
    obsdb.add_correction(util.local_file('corrections/caspsr_onesec.asc'))
    obsdb.add_correction(util.local_file('corrections/dfb3_quarter_band.asc'))
    # NB but this one is probalby best for PPTA only since it's a very
    # small shift
    obsdb.add_correction(util.local_file('corrections/dfb4_first_subint.asc'))

    db = caldb.append_db(obsdb)

    # add manual zapping (zap obs & cals)
    files = ['manual_zap_ppta','galileo_zaps','compass_zaps',
        'dfb1_artefact_zap','ppta_midweek_fiveband',
        'rfi_zap_10cm_nbn','rfi_zap_10cm_2630mhz']
    if hydra:
        files.append('manual_zap_hydra')
    for fname in files:
        db.add_zapping(util.local_file('obslists/%s.asc'%fname))

    if hydra:
        analysis = timing.FluxCalibratorAnalysis(db,calpath,psrpath,
            zap_subints='dry')
    elif MAKE_PCM:
        label = 'make_pcm'
        analysis = timing.PCMCalibratorAnalysis(db,calpath,psrpath,
            zap_subints='dry')
    else:
        label = 'pcm' if do_pcm else 'nopcm'
        #if not do_fluxcal:
        #    label += '.nofluxcal'
        analysis = timing.PPTA_TimingAnalysis(db,calpath,psrpath,
            ephdb=ephdb,templdb=templdb,pcmdb=pcmdb,fluxdb=fluxdb,
            label=label,zap_subints='no',make_dynspec=False,
            install_eph=INSTALL_EPHEMERIS,make_invint=make_invint)

    return analysis

from mpi4py import MPI
comm = MPI.COMM_WORLD

def load_procdb(jname,clobber=False):
    tstart = MPI.Wtime()
    if not MAKE_PCM:
        procdb_fname = join(outpath,jname,'procdb.pickle')
    else:
        procdb_fname = join(outpath,'pcm',jname,'procdb.pickle')
    if clobber or (not isfile(procdb_fname)):
        procdb = processed.ProcDB()
    else:
        procdb = cPickle.load(file(procdb_fname))
    procdb.select_jname(jname) # TMP
    return MPI.Wtime()-tstart,procdb

def dump_procdb(procdb,jname):
    # hmm
    if not MAKE_PCM:
        procdb_fname = join(outpath,jname,'procdb.pickle')
    else:
        procdb_fname = join(outpath,'pcm',jname,'procdb.pickle')
    cPickle.dump(procdb,file(procdb_fname,'w'),protocol=2)

def make_html(jnames=None,procdb=None):
    if procdb is None:
        if jnames is None:
            raise ValueError
        tprocdb,procdb = load_procdb(jnames[0])
        for jname in jnames[1:]:
            tother,other = load_procdb(jname)
            tprocdb += tother
            try:
                procdb.add_procdb(other)
            except KeyError:
                print 'Problem adding procdbs.'
                print 'DB one has: ',procdb.jnames.keys()
                print 'DB two has: ',other.jnames.keys()
    tstart = MPI.Wtime()
    if not MAKE_PCM:
        output = html.PPTA_BandOverviewTable2(procdb,outpath)
    else:
        output = html.PPTA_BandOverviewTable2(
            procdb,os.path.join(outpath,'pcm'))
    output.add_entries(jnames,clobber_plots=True)
    output.write(fname='band.html')

    if DO_QUICKLOOK:
        output = html.PPTA_QuickLook(procdb,outpath=outpath,
            exclude_nosk=True)
        output.to_html(fname=join(outpath,'quicklook.html'))
        output = html.PPTA_QuickLook(procdb,outpath=outpath,
            exclude_nosk=False)
        output.to_html(fname=join(outpath,'quicklook_nosk.html'))

    return MPI.Wtime()-tstart

if comm.rank == 0:

    tstart = MPI.Wtime()

    for jname in jnames:

        invint = jname == 'J0437-4715'

        tprocdb,procdb = load_procdb(jname,clobber=CLOBBER_PROCDB)

        print 'Working on %s'%jname
        t1 = MPI.Wtime()

        analysis = get_analysis(
            jname,clobber_obsdb=CLOBBER_OBSDB,clobber_caldb=CLOBBER_CALDB,
            do_pcm=DO_PCM,do_fluxcal=DO_FLUXCAL,make_invint = invint)

        if NEW_ONLY:
            procdb.flag_new_obs(analysis.db)

        t2 = MPI.Wtime()

        if ('HYDRA' in jname) or MAKE_PCM:
            work,all_work = analysis.make_work(clobber=CLOBBER_DATA)
        else:
            work,all_work = analysis.make_work(
                clobber=CLOBBER_DATA,clobber_toas=CLOBBER_TOAS,
                new_only=NEW_ONLY)
        manager = mpi.MPI_Jobs(verbosity=1)
        manager.do_work(all_work,kill_threads=False)

        t3 = MPI.Wtime()

        #analysis.tarball(jname)
        #if not MAKE_PCM:
        procdb.add_analysis(analysis)
        t4p = MPI.Wtime()
        if not MAKE_PCM:
            procdb.get_toa_tarball(jname,invint=invint)

        t4 = MPI.Wtime()

        print '%s database elapsed time: %.2f minutes.'%(jname,(t2-t1)/60)
        print '%s total work wall time: %.2f minutes.'%(jname,(t3-t2)/60)
        mpi.thread_stats(manager)
        print 'CAL throughput --'
        mpi.throughput_stats(manager,do_obs=False)
        print 'OBS throughput --'
        mpi.throughput_stats(manager,do_obs=True)
        print '%s procdb elapsed time: %.2f minutes'%(jname,(t4-t3)/60)
        print '(  on toa tarball: elapsed time: %.2f minutes)'%((t4-t4p)/60)
        print 'Total %s time: %.2f minutes'%(jname,(t4-t1)/60)
        #mpi.throughput_plot(manager,join(outpath,'%s.png'%jname))

        #if not MAKE_PCM:
        dump_procdb(procdb,jname)

    manager.kill_threads()

    #import cProfile
    #cProfile.runctx('thtml = make_html(jnames=all_jnames,procdb=None)',
    #    globals(),locals(),'make_html.prof')
    #if MAKE_HTML and (not MAKE_PCM):
    if MAKE_HTML:
        thtml = make_html(jnames=all_jnames,procdb=None)
    else:
        thtml = 0

    ttotal = (MPI.Wtime() - tstart)
    print 'procdb load time: %d seconds'%(int(round(tprocdb)))
    print 'HTML elapsed time: %.2f minutes'%(thtml/60)
    print 'Total elapsed time: %.2f minutes'%(ttotal/60)

else:
    worker = mpi.MPI_Worker()
    worker.do_work()

