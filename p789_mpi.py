import matplotlib
matplotlib.use('Agg')

import os
import itertools
from os.path import join,isfile,abspath,expandvars,basename,isdir
import cPickle

import mpi
import marsfield
import html
import timing
import util
import processed
import ephemeris
import template
import alias

CLOBBER_CALDB = False
CLOBBER_OBSDB = False
CLOBBER_PROCDB = False
CLOBBER_DATA = False
CLOBBER_TOAS = False
NEW_ONLY = False
DO_PCM = True

plist = 'pulsarlists/p789_pulsars.asc'
all_jnames = sorted(map(alias.preferred_name,util.readlines(plist)))
jnames = all_jnames

outpath = '/u/ker14a/newt/P789'
directories = marsfield.get_directories(['DFB','CASPSR'])
#jnames = ['HYDRA_N','HYDRA_S','HYDRA_O']
#regexp = '[st]14.*'
regexp = None

def get_analysis(jname,clobber_obsdb=False,clobber_caldb=False,do_pcm=True):

    psrpath = join(outpath,jname)
    calpath = join(psrpath,'cals')
    for path in [psrpath,calpath]:
        if not isdir(path):
            os.makedirs(path)

    if do_pcm:
        pcmdb = marsfield.make_pcmdb('%s/pcmdb.pickle'%calpath,
            pcmdir=expandvars('$PPTA/pcm/20cm'),clobber=clobber_caldb,
            suppldir=join(outpath,'supplementary_pcm'))
    else:
        pcmdb = None
    fluxdb = marsfield.make_fluxdb('%s/fluxdb.pickle'%calpath,
        fluxdir=expandvars('$PPTA/fluxcal'),clobber=clobber_caldb)
    caldb = marsfield.make_ppta_caldb('%s/caldb.pickle'%(calpath),
        directories,jname,clobber=clobber_caldb,update=True,regexp=regexp)
    obsdb = marsfield.make_ppta_obsdb('%s/obsdb.pickle'%(psrpath),
        directories,jname,clobber=clobber_obsdb,update=True,regexp=regexp)
    ephdb = ephemeris.P574_EphemerisDatabase(join(outpath,'ephemerides'))
    templdb = template.PPTA_TemplateDatabase(join(outpath,'templates'))

    obsdb.add_correction(util.local_file('corrections/caspsr_onesec.asc'))
    obsdb.add_correction(util.local_file('corrections/dfb3_quarter_band.asc'))

    db = caldb.append_db(obsdb)

    # add manual zapping (zap obs & cals)
    db.add_zapping(util.local_file('obslists/manual_zap_p789.asc'))
    #db.add_zapping(util.local_file('obslists/dfb1_artefact_zap.asc'))

    label = 'pcm' if do_pcm else 'nopcm'
    analysis = timing.PPTA_TimingAnalysis(db,calpath,psrpath,
        ephdb=ephdb,templdb=templdb,pcmdb=pcmdb,fluxdb=fluxdb,
        label=label,zap_subints='no',make_dynspec=True,cal_tmax=3./24)

    return analysis

from mpi4py import MPI
comm = MPI.COMM_WORLD

def load_procdb(jname):
    tstart = MPI.Wtime()
    procdb_fname = join(outpath,jname,'procdb.pickle')
    if CLOBBER_PROCDB or (not isfile(procdb_fname)):
        procdb = processed.ProcDB()
    else:
        procdb = cPickle.load(file(procdb_fname))
    procdb.select_jname(jname) # TMP
    return MPI.Wtime()-tstart,procdb

def dump_procdb(procdb,jname):
    # hmm
    procdb_fname = join(outpath,jname,'procdb.pickle')
    cPickle.dump(procdb,file(procdb_fname,'w'),protocol=2)

def make_html(jnames=None,procdb=None):
    if procdb is None:
        if jnames is None:
            raise ValueError
        tprocdb,procdb = load_procdb(jnames[0])
        for jname in jnames[1:]:
            tother,other = load_procdb(jname)
            tprocdb += tother
            try:
                procdb.add_procdb(other)
            except KeyError:
                print 'Problem adding procdbs.'
                print 'DB one has: ',procdb.jnames.keys()
                print 'DB two has: ',other.jnames.keys()
    tstart = MPI.Wtime()
    output = html.PPTA_BandOverviewTable2(procdb,outpath)
    output.add_entries(jnames)
    output.write(fname='band.html')
    return MPI.Wtime()-tstart

if comm.rank == 0:

    tstart = MPI.Wtime()

    for jname in jnames:

        tprocdb,procdb = load_procdb(jname)

        print 'Working on %s'%jname
        t1 = MPI.Wtime()

        analysis = get_analysis(
            jname,clobber_obsdb=CLOBBER_OBSDB,clobber_caldb=CLOBBER_CALDB)

        if NEW_ONLY:
            procdb.flag_new_obs(analysis.db)

        t2 = MPI.Wtime()

        work,all_work = analysis.make_work(
            clobber=CLOBBER_DATA,clobber_toas=CLOBBER_TOAS,
            new_only=NEW_ONLY)

        manager = mpi.MPI_Jobs(all_work,verbosity=1)
        manager.do_work(kill_threads=False)

        t3 = MPI.Wtime()

        #analysis.tarball(jname)
        procdb.add_analysis(analysis)
        t4p = MPI.Wtime()
        procdb.get_toa_tarball(jname)

        t4 = MPI.Wtime()

        print '%s database elapsed time: %.2f minutes.'%(jname,(t2-t1)/60)
        print '%s total work wall time: %.2f minutes.'%(jname,(t3-t2)/60)
        mpi.thread_stats(manager)
        print 'CAL throughput --'
        mpi.throughput_stats(manager,do_obs=False)
        print 'OBS throughput --'
        mpi.throughput_stats(manager,do_obs=True)
        print '%s procdb elapsed time: %.2f minutes'%(jname,(t4-t3)/60)
        print '(  on toa tarball: elapsed time: %.2f minutes)'%((t4-t4p)/60)
        print 'Total %s time: %.2f minutes'%(jname,(t4-t1)/60)
        #mpi.throughput_plot(manager,join(outpath,'%s.png'%jname))

        dump_procdb(procdb,jname)

    manager.kill_threads()

    #import cProfile
    #cProfile.runctx('thtml = make_html(jnames=all_jnames,procdb=None)',
    #    globals(),locals(),'make_html.prof')
    thtml = make_html(jnames=all_jnames,procdb=None)

    ttotal = (MPI.Wtime() - tstart)
    print 'procdb load time: %d seconds'%(int(round(tprocdb)))
    print 'HTML elapsed time: %.2f minutes'%(thtml/60)
    print 'Total elapsed time: %.2f minutes'%(ttotal/60)

else:
    worker = mpi.MPI_Worker()
    worker.do_work()

