import matplotlib
matplotlib.use('Agg')

import os
import itertools
from os.path import join,isfile,abspath,expandvars,basename,isdir
import cPickle
import sys

import mpi
import marsfield
import html
import timing
import util
import processed
import ephemeris
import template
import alias
import config

# parse the configuration file
try:
    conf = config.Configuration(sys.argv[1])
except IndexError:
    print 'Must provide configuration file as first argument!'

all_jnames = sorted(map(alias.preferred_name,util.readlines(conf.pulsars)))
jnames = all_jnames

def get_analysis(jname,clobber_obsdb=False,clobber_caldb=False,
    do_pcm=True,make_invint=False):

    psrpath = join(conf.output_dir,jname)
    calpath = join(psrpath,'cals')

    if not isdir(calpath):
        os.makedirs(calpath)
    if not isdir(psrpath):
        os.makedirs(psrpath)

    caldb = marsfield.make_ppta_caldb('%s/caldb.pickle'%(calpath),
        conf.data_dirs,jname,clobber=clobber_caldb,update=True,
        regexp=conf.regexp)
    obsdb = marsfield.make_ppta_obsdb('%s/obsdb.pickle'%(psrpath),
        conf.data_dirs,jname,clobber=clobber_obsdb,update=True,
        regexp=conf.regexp)

    fluxdb = pcmdb = ephdb = templdb = None
    if conf.do_pcmcal and (conf.pcmcal_dir is not None):
        pcmdb = marsfield.make_generic_db('%s/pcmdb.pickle'%calpath,
            conf.ephemeris_dir,clobber=clobber_caldb)
    if conf.do_fluxcal and (conf.fluxcal_dir is not None):
        fluxdb = marsfield.make_generic_db('%s/fluxdb.pickle'%calpath,
            conf.fluxcal_dir,clobber=clobber_caldb)
    if (conf.ephemeris_dir is not None):
        ephdb = ephemeris.GeneralEphemerisDatabase(conf.ephemeris_dir)
    if (conf.template_dir is not None):
        templdb = template.GeneralTemplateDatabase(conf.template_dir)

    print 'found %d obs'%(len(obsdb))

    # temporary test
    # TODO -- these ultimately need to be placed back in fix_ppta_data
    #obsdb.add_correction(util.local_file('corrections/caspsr_onesec.asc'))
    #obsdb.add_correction(util.local_file('corrections/dfb3_quarter_band.asc'))
    # NB but this one is probalby best for PPTA only since it's a very
    # small shift
    #obsdb.add_correction(util.local_file('corrections/dfb4_first_subint.asc'))

    db = caldb.append_db(obsdb)

    # add manual zapping (zap obs & cals)
    #files = ['manual_zap_ppta','galileo_zaps','compass_zaps',
    #    'dfb1_artefact_zap','ppta_midweek_fiveband']
    #if hydra:
    #    files.append('manual_zap_hydra')
    for zap_file in conf.zap_files :
        db.add_zapping(zap_file)

    #label = 'pcm' if do_pcm else 'nopcm'
    analysis = timing.PPTA_TimingAnalysis(db,calpath,psrpath,
        ephdb=ephdb,templdb=templdb,pcmdb=pcmdb,fluxdb=fluxdb,
        label=conf.label,
        zap_subints=conf.zap_subints,make_dynspec=conf.make_dynspec,
        make_invint=make_invint,pat_args=conf.pat_args)

    return analysis

from mpi4py import MPI
comm = MPI.COMM_WORLD

def load_procdb(jname,clobber=False):
    tstart = MPI.Wtime()
    procdb_fname = join(conf.output_dir,jname,'procdb.pickle')
    if clobber or (not isfile(procdb_fname)):
        procdb = processed.ProcDB()
    else:
        procdb = cPickle.load(file(procdb_fname))
    procdb.select_jname(jname) # TMP
    return MPI.Wtime()-tstart,procdb

def dump_procdb(procdb,jname):
    procdb_fname = join(conf.output_dir,jname,'procdb.pickle')
    cPickle.dump(procdb,file(procdb_fname,'w'),protocol=2)

def make_html(jnames=None,procdb=None):
    if procdb is None:
        if jnames is None:
            raise ValueError
        tprocdb,procdb = load_procdb(jnames[0])
        for jname in jnames[1:]:
            tother,other = load_procdb(jname)
            tprocdb += tother
            try:
                procdb.add_procdb(other)
            except KeyError:
                print 'Problem adding procdbs.'
                print 'DB one has: ',procdb.jnames.keys()
                print 'DB two has: ',other.jnames.keys()
    output = html.P574_BandOverviewTable2(procdb,conf.output_dir)
    output.add_entries(jnames,clobber_plots=True)
    output.write(fname='band.html')

if comm.rank == 0:

    tstart = MPI.Wtime()

    for jname in jnames:

        invint = jname == 'J0437-4715'

        tprocdb,procdb = load_procdb(jname,clobber=conf.clobber_procdb)

        print 'Working on %s.'%jname

        analysis = get_analysis(
            jname,clobber_obsdb=conf.clobber_obsdb,
            clobber_caldb=conf.clobber_caldb,
            do_pcm=conf.do_pcmcal,make_invint = invint)

        work,all_work = analysis.make_work(
            clobber=conf.clobber_data,clobber_toas=conf.clobber_toas)
        manager = mpi.MPI_Jobs(verbosity=1)
        manager.do_work(all_work,kill_threads=False)

        procdb.add_analysis(analysis)
        procdb.get_toa_tarball(jname,invint=invint)
        dump_procdb(procdb,jname)

    manager.kill_threads()

    if conf.make_html:
        make_html(jnames=all_jnames,procdb=None)

else:
    worker = mpi.MPI_Worker()
    worker.do_work()

