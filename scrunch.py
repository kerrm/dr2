"""
Manage successive "scrunches" (reductions in dimension) in data cubes during
the course of pipeline processing.

The is important as some observations happen naturally at various scrunch
levels, and in particular DifferentialScrunchScheme provides a mechanism
for applying operations immediately before a prohibitive scrunch would
happen.

author: Matthew Kerr <matthew.kerr@gmail.com>
"""
from os.path import join

from jobs import psrsh_segment

class Operation(object):
    """ Represent a psrsh operation, e.g. pscrunch, clone..."""

    def get_psrsh(self,obs):
        raise NotImplementedError

    def is_scrunch(self):
        return False

class Scrunch(Operation):
    """ Represent a possible data scrunch.
    
    This concept is now slightly extended with the addition of forming the
    invariant interval, which is "parallel" to a hierarchical scrunch."""

    #def __setstate__(self,state):
    #    # temp
    #    if 'invint' not in state.keys():
    #        state['invint'] = False
    #    self.__dict__.update(state)
#
    def __init__(self,extension,
        tscrunch=False,fscrunch=False,pscrunch=False,invint=False):
        self.extension = extension
        self.tscrunch = int(tscrunch)
        self.fscrunch = int(fscrunch)
        self.pscrunch = pscrunch
        self.invint = invint

    def is_scrunch(self):
        return True

    def get_psrsh(self,obs):
        psrsh = psrsh_segment()
        # NB -- do invariant interval before any frequency scrunching
        # this is somewhat at odds with the rest of the hierarchy, since
        # the previous channelization will affect the S/N and thus the
        # reliability of the II; nevertheless, this seems to be the only
        # feasible way to get this into a single script/hierarchy
        if self.tscrunch:
            tscrunch = min(self.tscrunch,obs.get_nsub())
            psrsh.append('tscrunch %d'%tscrunch)
        if self.invint:
            psrsh.append('invint')
        if self.fscrunch:
            fscrunch = min(self.fscrunch,obs['nchan'])
            psrsh.append('fscrunch %d'%fscrunch)
        if self.pscrunch:
            psrsh.append('pscrunch')
        return psrsh

    def is_tscrunched(self):
        return self.tscrunch == 1

    def is_fscrunched(self):
        return self.fscrunch == 1

    def is_pscrunched(self):
        #return self.pscrunch or self.invint
        return self.pscrunch

    def is_invint(self):
        return self.invint

class Fork(Operation):
    """ Represent a forking of the current state via psrsh clone/push.

    This is used for making scrunches that aren't hierarchical, e.g.
    suppose one wants both a time-scrunched, full spectral resolution output
    and an output with coarse sub-ints and channels.  This can be achieved
    by e.g.
    Orig --> Fork --> T+output --> Pop --> t + f + output
    """

    def get_psrsh(self,obs):
        psrsh = psrsh_segment()
        psrsh.add('clone')
        psrsh.add('push')
        return psrsh

class Pop(Operation):
    """ Represent the removal of a forked state."""

    def get_psrsh(self,obs):
        psrsh = psrsh_segment()
        psrsh.add('pop')
        return psrsh

class ScrunchState(object):
    """ Keep track of the state of scrunch of an observation.
    
    This object is truly hierarchical, as successive operations can only
    change the state to a more restrictive one."""

    def __init__(self):
        self.pscrunch = False
        self.tscrunch = 0
        self.fscrunch = 0
        self.invint = False

    def add_scrunch(self,scrunch):

        self.pscrunch = self.pscrunch or scrunch.is_pscrunched()

        if (self.tscrunch == 1) or (scrunch.tscrunch == 1):
            self.tscrunch = 1 # max scrunch
        else:
            self.tscrunch = max(self.tscrunch,scrunch.tscrunch)

        if (self.fscrunch == 1) or (scrunch.fscrunch == 1):
            self.fscrunch = 1 # max scrunch
        else:
            self.fscrunch = max(self.fscrunch,scrunch.fscrunch)

        self.invint = scrunch.invint

    def clone(self):
        ss = ScrunchState()
        ss.__dict__.update(self.__dict__)
        return ss

class DifferentialScrunchState(object):
    """ Keep track of the state of a progression of scrunches.

    In particular, this class implements supports parallel workflow by
    keeping track of multiple ScrunchStates.  For each Fork operation, a
    new ScrunchState pair is added

    Note when an "edge" is found so relevant operations can be applied.

    In general, the "pre" methods below represent the first time a scrunch
    is applied, e.g. going from 1024->32 channels.  Whereas the "post"
    method represents the maximal scrunch, e.g. 32-->1 channel.

    Because the functional definition of these is to do operations at either
    extreme, we allow ClonedScrunch for the "pre" version, but not for the
    "post" version.
    """

    def __init__(self):
        self.curr = ScrunchState()
        self.prev = ScrunchState()
        self.curr_scrunch = None
        self.prev_scrunch = None

    def add_scrunch(self,scrunch):
        self.prev,self.curr = self.curr,self.curr.clone()
        self.curr.add_scrunch(scrunch)
        self.prev_scrunch,self.curr_scrunch = self.curr_scrunch,scrunch

    def pre_fscrunch(self):
        """ Return True if the the previous state is un-fscrunched and the
            current state is fscrunched.
        """
        return (self.prev.fscrunch == 0) and (self.curr.fscrunch > 0)

    def post_fscrunch(self):
        """ Return True if the previous state is not fully-fscrunched and
            the current one is.
        """
        return (self.prev.fscrunch != 1) and (self.curr.fscrunch == 1)

    def pre_tscrunch(self):
        """ Return True if the the previous state is un-tscrunched and the
            current state is tscrunched.
        """
        return (self.prev.tscrunch == 0) and (self.curr.tscrunch > 0)

    def post_tscrunch(self):
        """ Return True if the previous state is not fully-tscrunched and
            the current one is.
        """
        return (self.prev.tscrunch != 1) and (self.curr.tscrunch == 1)

    def pre_pscrunch(self):
        """ Return True if the the previous state is un-pscrunched and the
            current state is pscrunched.

        Since there are only two states, pre and post return the same thing.
        """
        return (not self.prev.pscrunch) and self.curr.pscrunch

    def post_pscrunch(self):
        """ Return True if the the previous state is un-pscrunched and the
            current state is pscrunched.

        Since there are only two states, pre and post return the same thing.
        """
        return self.pre_pscrunch()

    def pre_invint(self):
        return (self.prev.invint == 0) and (self.curr.invint)

    def post_invint(self):
        return self.curr.invint

    def post_fptscrunch(self):
        return self.post_tscrunch() and self.post_fscrunch() and self.post_pscrunch()

    def clone(self):
        dss = DifferentialScrunchState()
        dss.curr = self.curr.clone()
        dss.prev = self.prev.clone()
        dss.curr_scrunch = self.curr_scrunch
        dss.prev_scrunch = self.prev_scrunch
        return dss


class ScrunchScheme(object):
    """ Specify a consecutive set of scrunches to be performed, and
        corresponding files to be produced.
    """

    # note to self about this -- 

    def __init__(self,outpath,label=None):
        self.outpath = outpath
        self.label = label
        self.scrunches = self._get_scrunches()

        dss = [DifferentialScrunchState()]
        list_stack = [dss]
        idx_stack = [0]
        for scrunch in self.scrunches:
            if scrunch.is_scrunch():
                # previous dss in this chain
                prev_dss = list_stack[-1][idx_stack[-1]].clone()
                prev_dss.add_scrunch(scrunch)
                list_stack[-1].append(prev_dss)
                idx_stack[-1] = len(list_stack[-1])-1
            else:
                if isinstance(scrunch,Fork):
                    prev_dss = list_stack[-1][idx_stack[-1]]
                    new_list = [scrunch,prev_dss.clone()]
                    list_stack[-1].append(new_list)
                    list_stack.append(new_list)
                    idx_stack.append(1)
                if isinstance(scrunch,Pop):
                    list_stack[-1].append(scrunch)
                    list_stack.pop()
                    idx_stack.pop()

        def process_list(l):
            if not hasattr(l,'__iter__'):
                return l
            for i in xrange(len(l)):
                if isinstance(l[i],DifferentialScrunchState):
                    l.pop(i)
                    break
            return [process_list(x) for x in l]

        # remove dummies
        self.dss = process_list(dss)
        #self.dss = dss

        # NB this only works for single nesting level... deal with
        # general version if it actually comes up
        flat_dss = []
        for x in self.dss:
            if hasattr(x,'__iter__'):
                flat_dss.extend(x)
            else:
                flat_dss.append(x)
        self.flat_dss = flat_dss
        self.flat_scrunches = [x for x in flat_dss if isinstance(x,DifferentialScrunchState)]
        
    def _get_scrunches(self):
        raise NotImplementedError('Virtual method')

    def __iter__(self):
        #self._dss = DifferentialScrunchState()
        self._counter = 0
        return self

    def next(self):
        if self._counter == len(self.flat_dss):
            raise StopIteration
        dss = self.flat_dss[self._counter]
        self._counter += 1
        if isinstance(dss,DifferentialScrunchState):
            return dss.curr_scrunch,dss
        return dss,None

    def obs2file(self,obs,scrunch):
        return join(self.outpath,obs.basename+'.'+scrunch.extension)

    def get_target(self,obs):
        """ Return the final, most highly scrunched profile."""
        return self.obs2file(obs,self.scrunches[-1])

    def get_channelized_target(self,obs):
        """ Return the first tscrunched, pscrunched target, retaining
        channels if possible."""
        for dss in self.flat_scrunches:
            cs = dss.curr_scrunch
            if cs.is_tscrunched() and cs.is_pscrunched():
                return self.obs2file(obs,cs)
        return self.get_target(obs)

    def get_invariant_interval(self,obs):
        for dss in self.flat_scrunches:
            if dss.post_invint():
                return self.obs2file(obs,dss.curr_scrunch)

    def has_invariant_interval(self):
        return False

    def get_scrunched_3d(self,obs):
        """ Return the most scrunched data retaining time and freq info."""

        for dss in self.flat_scrunches:
            if (dss.pre_fscrunch() or dss.pre_tscrunch()):
                if dss.curr_scrunch.is_tscrunched() or dss.curr_scrunch.is_fscrunched():
                    continue
                return self.obs2file(obs,dss.curr_scrunch)
        cs = dss.curr_scrunch
        if cs.is_fscrunched() or cs.is_tscrunched():
            return None
        return self.obs2file(obs,cs)

    def get_scrunched_poln(self,obs):
        """ Return the most scrunched data retaining poln info."""
        for dss in self.flat_scrunches:
            if (dss.post_pscrunch()):
                return self.obs2file(obs,dss.prev_scrunch)
        cs = dss.curr_scrunch
        if cs.is_pscrunched():
            return None
        return self.obs2file(obs,cs)

    def get_scrunched_poln_time(self,obs):
        """ Return the most scrunched data retaining both poln and time."""
        for dss in self.flat_scrunches:
            if dss.post_pscrunch():
                return self.obs2file(obs,dss.prev_scrunch)
            if dss.pre_tscrunch():
                return self.obs2file(obs,dss.curr_scrunch)
        cs = dss.curr_scrunch
        if cs.is_pscrunched() or cs.is_tscrunched():
            return None
        return self.obs2file(obs,cs)

    def get_scrunched(self,obs,T=True,F=True,P=True):
        """ Return the most-scrunched data retaining specified dimension.

        NB this is not necessarily unique with advent of parallel workflows!

        E.g., T=True,F=False,P=True will return a datacube with time and
        polarization info with F as scrunched as possible.
        """
        for dss in self.flat_scrunches:
            if (T and dss.post_tscrunch()):
                if dss.prev_scrunch is None:
                    if dss.curr_scrunch.is_tscrunched():
                        continue
                    return self.obs2file(obs,dss.curr_scrunch)
                return self.obs2file(obs,dss.prev_scrunch)
            if (P and dss.post_pscrunch()):
                if dss.prev_scrunch is None:
                    if dss.curr_scrunch.is_pscrunched():
                        continue
                    return self.obs2file(obs,dss.curr_scrunch)
                return self.obs2file(obs,dss.prev_scrunch)
            if (F and dss.post_fscrunch()):
                if dss.prev_scrunch is None:
                    if dss.curr_scrunch.is_fscrunched():
                        continue
                    return self.obs2file(obs,dss.curr_scrunch)
                return self.obs2file(obs,dss.prev_scrunch)
            #if (F and dss.pre_fscrunch()):
                #return self.obs2file(obs,dss.curr_scrunch)
        s0 = dss.curr_scrunch
        if (T and s0.is_tscrunched()):
            return None
        if (F and s0.is_fscrunched()):
            return None
        if (P and s0.is_pscrunched()):
            return None
        return self.obs2file(obs,s0)

    def get_best_frequency_resolution(self,obs):
        """ Return the first occurring instance of a non-scrunched product.
        """
        best_dss = None
        best = -1
        for dss in self.flat_scrunches:
            if dss.curr_scrunch.fscrunch == 0:
                best_dss = dss.curr_scrunch
                best = 0
            if best == 0:
                continue
            if dss.curr_scrunch.fscrunch > best:
                best_dss = dss.curr_scrunch
                best = best_dss.fscrunch
        return self.obs2file(obs,best_dss)

    def get_best_time_resolution(self,obs):
        """ Return the first occurring instance of a non-scrunched product.
        """
        best_dss = None
        best = -1
        for dss in self.flat_scrunches:
            if dss.curr_scrunch.tscrunch == 0:
                best_dss = dss.curr_scrunch
                best = 0
            if best == 0:
                continue
            if dss.curr_scrunch.tscrunch > best:
                best_dss = dss.curr_scrunch
                best = best_dss.tscrunch
        return self.obs2file(obs,best_dss)

class PPTA_Psr_Scrunch(ScrunchScheme):
    """ Make the default files for PPTA analysis."""

    def _get_scrunches(self):
        label_string = '%s.'%self.label if self.label is not None else ''
        return [
            #Scrunch('dzt8',tscrunch=8),
            #Scrunch('dzt8f32',fscrunch=32),
            Fork(),
            Scrunch('%sdzT'%label_string,tscrunch=1),
            Pop(),
            Fork(),
            Scrunch('%sdzF'%label_string,fscrunch=1),
            Pop(),
            Scrunch('%sdzt8f32'%label_string,tscrunch=8,fscrunch=32),
            Scrunch('%sdzTF'%label_string,tscrunch=1,fscrunch=1),
            Scrunch('%sdzTFp'%label_string,tscrunch=1,fscrunch=1,
                pscrunch=True)
        ]

class PPTA_Psr_Scrunch_InvInt(ScrunchScheme):
    """ Make the default files for PPTA analysis."""

    def _get_scrunches(self):
        label_string = '%s.'%self.label if self.label is not None else ''
        return [
            Fork(),
            Scrunch('%sdzT'%label_string,tscrunch=1),
            Pop(),
            Fork(),
            Scrunch('%sdzF'%label_string,fscrunch=1),
            Pop(),
            Scrunch('%sdzt8f32'%label_string,tscrunch=8,fscrunch=32),
            Fork(),
            Scrunch('%sdzTFi'%label_string,tscrunch=1,fscrunch=1,
                invint=True),
            Pop(),
            Scrunch('%sdzTF'%label_string,tscrunch=1,fscrunch=1),
            Scrunch('%sdzTFp'%label_string,tscrunch=1,fscrunch=1,
                pscrunch=True)
        ]

    def has_invariant_interval(self):
        return True

class P574_Psr_Scrunch(ScrunchScheme):
    """ Make default files for P574 analysis.
    
    Create the following products:
    (1) Full frequency resolution, Tscrunched.
    (2) Coarse frequency resolution, full time resolution.
    (3) Full frequency & time scrunch.
    (4) Fully scrunched.
    """

    def _get_scrunches(self):
        label_string = '%s.'%self.label if self.label is not None else ''
        return [
            Fork(),
            Scrunch('%sdzT'%label_string,tscrunch=1),
            Pop(),
            Scrunch('%sdzf32'%label_string,fscrunch=32),
            Scrunch('%sdzTF'%label_string,tscrunch=1,fscrunch=1),
            Scrunch('%sdzTFp'%label_string,tscrunch=1,fscrunch=1,
                pscrunch=True)
        ]

class PPTA_FluxCal_Scrunch(ScrunchScheme):
    """ Scrunch in time but maintain frequency resolution for cals."""
    def _get_scrunches(self):
        return [Scrunch('dzT',tscrunch=1)]

PPTA_Cal_Scrunch = PPTA_FluxCal_Scrunch

class PPTA_PCMCal_Scrunch(ScrunchScheme):
    """ Maintain full frequency resolution and 16 subints.
    
    12 subints is chosen to offer some flexibility for the different ways
    in which long track observations have been recorded.
    
    1) 64-minute observations with 64 subints (--> 16 4-min subints)
    2) 16-minute observations with 16 subints (--> 16 1-min subints)
    3) 10-minute observations with 10 subints (--> 10 1-min subints)

    The ideal sub-int length is 5-8 minutes.  By scrunching to 16 sub-ints,
    we save some space, preserve 1-4 minute resolution, allowing post-hoc
    scrunching to get to the 5-8 minute level.
    """

    def _get_scrunches(self):
        return [Scrunch('dzt16',tscrunch=16),
                Scrunch('dzT',tscrunch=1)]

class NoScrunch(ScrunchScheme):
    """ An "identity" class."""
    def _get_scrunches(self):
        return [Scrunch('dz')]
