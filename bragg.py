"""
Routines for managing file system on the Bragg cluster.

Note that file storage is collection-based, which typically means arranged
by project and semester (trimester for older data).  This means that
searching for sources/backends position in the filesystem tree is not an
option.  Ultimately, this should be handled via database query, but for now,
we will simply process full collections into the cache.

$Header: /nfs/slac/g/glast/ground/cvs/users/kerrm/tpipe/timing.py,v 1.14 2014/01/21 06:46:59 kerrm Exp $

author: Matthew Kerr <matthew.kerr@gmail.com>
"""
from marsfield import make_ppta_db
from glob import glob
from os.path import join
from collections import deque

class ReadCollections(object):
    """ Little functor to handle loading the filenames in a collection."""

    def __init__(self,collections,wildcard):
        if not hasattr(collections,'__iter__'):
            collections = [collections]
        self._collections = collections
        if not wildcard.startswith('*'):
            wildcard = '*%s'%wildcard
        self._wildcard = wildcard 

    def __call__(self):
        fnames = deque() 
        for coll in self._collections:
            fnames.extend(glob(join(coll,self._wildcard)))
        return list(fnames)

def make_ppta_caldb(fname,collections,clobber=False,update=True,test=False):
    """ Return a database of calibrators.
    
    Collections is a collection (heh) of mount points of DAP collections.
    """
    if test:
        # for test, just get DFB4 data
        search_function = ReadCollections(collections,'t*.cf')
    else:
        search_function = ReadCollections(collections,'.cf')
    return make_ppta_db(fname,search_function,clobber=clobber,update=update)

def make_ppta_obsdb(fname,collections,clobber=False,update=True,test=False):
    """ Return a database of observations.
    
    Collections is a collection (heh) of mount points of DAP collections.
    """
    if test:
        # for test, just get DFB4 data
        search_function = ReadCollections(collections,'t*.rf')
    else:
        search_function = ReadCollections(collections,'.rf')
    return make_ppta_db(fname,search_function,clobber=clobber,update=update)

# temporary function to read in files in flush directory

def make_flush_caldb(fname,path,clobber=False,update=True):
    search_function = lambda: glob(join(path,'*.cf'))
    return make_ppta_db(fname,search_function,clobber,update)

def make_flush_obsdb(fname,path,clobber=False,update=True):
    search_function = lambda: glob(join(path,'*.rf'))
    return make_ppta_db(fname,search_function,clobber,update)
