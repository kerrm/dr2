"""
Fit analytic profiles to archives.

$Header: /nfs/slac/g/glast/ground/cvs/users/kerrm/tpipe/timing.py,v 1.14 2014/01/21 06:46:59 kerrm Exp $

author: Matthew Kerr <matthew.kerr@gmail.com>
"""

import numpy as np
import psrchive
import pymultinest

from rn.multinest import BoundedParameter,MultiNestLikelihood
from uw.pulsar import lcprimitives,lctemplate

def make_bounded_parameters(lct):
    prims = lct.primitives
    lct.norms.free[0] = False
    vals = lct.get_parameters()
    mins = np.asarray(lct.get_bounds())[:,0]
    maxs = np.asarray(lct.get_bounds())[:,1]
    names = []
    for p in prims:
        names += p.pnames
    names += lct.norms.pnames[1:]
    bps = map(BoundedParameter,vals,mins,maxs,names)
    return bps

def get_template(n):
    """ Return a template with n Von Mises functions."""
    #prims = [lcprimitives.LCVonMises() for i in xrange(n)]
    #prims = [lcprimitives.LCGaussian() for i in xrange(n)]
    prims = [lcprimitives.LCVonMises() for i in xrange(n)]
    for p in prims:
        p.bounds[0][0] = 0.0015 # 1 milliperiod
        #p.bounds[0][0] = 0.005 # 5 milliperiod
        p.bounds[0][1] = 0.250
        p.bounds[1][0] = 0 # phase = 0
        p.bounds[1][1] = 1 # phase = 1
    lct = lctemplate.LCTemplate(prims)
    return lct

def load_scrunched_archive(archive):
    a = psrchive.Archive_load(archive)
    a.fscrunch()
    a.tscrunch()
    a.pscrunch()
    return a[0].get_Profile(0,0).get_amps().copy()

class LogLikelihood(MultiNestLikelihood):
    
    def __init__(self,profile,template):

        # handle profile things and make cache
        self.profile = profile
        self.N = len(profile)
        self.D = np.sum(profile)
        self.dom = np.linspace(0,1,self.N+1)[:-1]

        self.template = template
        self.parameters = make_bounded_parameters(template)

        self.model = self.profile.copy()
        
        self.best_logl = np.inf
        self.counter = 0

    def loglikelihood(self):
        """ Return the (negative) log likelihood.
        
        This is profiled over the nuisance parameters of the signal amplitude,
        pedestal, and white noise level.
        """
        self.counter += 1
        if self.counter % 1000 == 0:
            print 'Called likelihood %d times.'%self.counter
        s = self.template(self.dom)
        S = np.sum(s)
        s2 = np.sum(s**2)
        sd = np.sum(self.profile*s)
        a = (S*self.D/self.N-sd)/(S**2/self.N-s2)
        if a < 0:
            # allow negative signals
            #pass
            return 1e100
        b = (self.D-a*S)/self.N
        if b < 0:
            return 1e100 # not sure when this would happen, but...
        #print a,b
        self.model = m = b + a*s
        var = np.sum((self.profile-m)**2)/self.N
        logl = 0.5*self.N*(1.+np.log(var))
        #print logl,var,a,b,S,sd,s2
        #print self.template.get_parameters()
        if logl < self.best_logl:
            self.best_logl = logl
            #print logl
        return logl

    def update_parameters(self):
        external = [p() for p in self.parameters]
        self.template.set_parameters(external)

def grid_scan(profile,prim=None,template=None,npeak=5,resolution=0.005):
    if prim is None:
        prim = lcprimitives.LCVonMises()
        prim.p[0] = 0.002
    from copy import deepcopy
    prim = deepcopy(prim)
    prim.free[:] = True
    if template is None:
        lct = lctemplate.LCTemplate([prim])
    else:
        lct = template.add_primitive(prim)
    logl = LogLikelihood(profile,lct)
    dom = np.arange(0,1,0.005)
    logls = np.zeros_like(dom)
    for i in xrange(len(dom)):
        lct.primitives[0].p[-1] = dom[i]
        logls[i] = logl.loglikelihood()
    lct.primitives[0].p[-1] = dom[np.argmin(logls)]
    logl.fit(nlive=40)

    for i in xrange(npeak-1):

        lct = lct.add_primitive(lct.primitives[0],norm=0.2)
        lct.norms.free[:] = True
        logl = LogLikelihood(profile,lct)
        for i in xrange(len(dom)):
            lct.primitives[0].p[-1] = dom[i]
            logls[i] = logl.loglikelihood()
        lct.primitives[0].p[-1] = dom[np.argmin(logls)]
        logl.fit(nlive=40)

    return logl

def refine_parameters(logl,add_peak=False):
    """ After an initial fit, revise priors on BoundedParameter objects to
    cut down on the fitting time required.

    Don't bother with norms yet.
    """
    lct = logl.template
    for prim in lct.primitives:
        # lock down width to \pm 30% of current
        prim.bounds[0][0] = prim.p[0]*0.7
        prim.bounds[0][1] = prim.p[0]*1.3
        # lock down phase to 0.1 of current location
        prim.bounds[-1][0] = prim.p[-1] - 0.1
        prim.bounds[-1][1] = prim.p[-1] + 0.1

    if add_peak:
        lct = lct.add_primitive(lcprimitives.LCVonMises())
        lct.norms.free[:] = True

    return LogLikelihood(logl.profile,lct)




import pylab as pl
import cPickle
import cProfile
import os
data = 'data/s130125_092234.rf'
profile = '/tmp/cache'
if not os.path.isfile(profile):
    prof = load_scrunched_archive(data)
    cPickle.dump(prof,file(profile,'w'),protocol=2)
else:
    prof = cPickle.load(file(profile))
template = get_template(3)
logl = LogLikelihood(prof,template)
#mno = logl.fit(nlive=20)
prim = lcprimitives.LCVonMises()
prim.p[0] = 0.002
