"""
Currently, a bit of a mess, but manages the generation of code for
a timing analysis.

$Header: /nfs/slac/g/glast/ground/cvs/users/kerrm/tpipe/timing.py,v 1.14 2014/01/21 06:46:59 kerrm Exp $

author: Matthew Kerr <matthew.kerr@gmail.com>
"""

import numpy as np
import os
from os.path import join,isfile,splitext,abspath,relpath,realpath,expandvars
import shutil
from collections import defaultdict,deque

import calibration
import scrunch
import ephemeris
import template
import zapping
import rfi
import plotting
import band
import util
import jobs

class PPTA_Cal(calibration.Cal):
    """ Implement the PPTA preferences for cals and emit psrsh."""

    def __init__(self,diode_scrunches,
        diode1,diode2=None,fluxcal=None,pcm=None):
        super(PPTA_Cal,self).__init__(diode1,diode2,fluxcal,pcm,
            require_before=True)
        self.diode_scrunches = diode_scrunches

    def get_dependencies(self):
        if self.valid():
            return [self.diode]
        return []

    def get_diode_file(self):
        return self.diode_scrunches.get_target(self.diode)

    def get_diode_comparison(self,verbosity=0):
        """ Return two (processed) cals for comparison if compatible."""
        d1,d2 = self.diodes
        if (d1 is None) or (d2 is None):
            if verbosity > 0:
                print 'Did not have two diodes.'
            return None
        a1 = d1.get_attenuator_settings()
        a2 = d2.get_attenuator_settings()
        if (a1 is None) and (a2 is None):
            c = True
        elif (a1 is None) or (a2 is None):
            c = False
        else:
            # sometimes calibrators have different numbers of sub-ints, but
            # I guess this is OK!  So check for internal consistency then
            # then compare single values.
            c = np.all(a1[0]==a1) and np.all(a2[0]==a2)
            c = c and np.all(a1[0]==a2[0])
        if not c:
            if verbosity > 0:
                print 'Diode attenuator settings not consistent.'
            return None
        t1 = self.diode_scrunches.get_target(d1)
        t2 = self.diode_scrunches.get_target(d2)
        if isfile(t1) and isfile(t2):
            return t1,t2

class ScrunchDepOps(object):
    """ Perform operations on an archive depending on scrunch state."""

    def init(self):
        self.zap_subints = 'no'
        self.make_dynspec = False
        self.install_eph = False
        self.strict_eph = True
        self.eph = None
        self.template = None

    def _default_zap(self,scrunches):
        return zapping.ppta_default_zap(self.obs)

    def pre_scrunch_psrsh(self,scrunches):
        """ Default implementation: corrections, zapping, calibration."""

        # start with corrections
        psrsh = self.obs.get_psrsh()

        # add in RFI excision
        psrsh += self._default_zap(scrunches)

        # pol'n calibration
        psrsh += self.cal_psrsh(scrunches)

        # update RM and DM
        if self.eph is not None:
            psrsh += self.eph.get_dm_rm_psrsh(self.obs)

            # optionally install entire ephemeris
            if self.install_eph:
                psrsh += self.eph.get_eph_psrsh(self.obs,
                    strict=self.strict_eph)

        if self.make_dynspec:
            psrsh.comment('Compute full resolution dynamic spectrum.')
            dynspec_output = util.change_extension(
                scrunches.get_target(self.obs),'dynspec')
            if self.template is not None:
                psrsh.add('dynspec %s %s'%(
                    dynspec_output,self.template.fname))
            else:
                psrsh.add('dynspec %s'%(dynspec_output))

        return psrsh

    def pre_tscrunch_psrsh(self,scrunches):
        return jobs.psrsh_segment()
    def pre_fscrunch_psrsh(self,scrunches):
        return jobs.psrsh_segment()
    def post_tscrunch_psrsh(self,scrunches):
        return jobs.psrsh_segment()
    def post_fscrunch_psrsh(self,scrunches):
        return jobs.psrsh_segment()

    def get_psrsh(self,scrunches):
        """ Iterate through scrunches and scrunch-dep actions to produce 
        psrsh.
        """

        # introduce a bit of a kluge; with "forks" and such, a scrunch-dep
        # operation can happen twice; make the choice that they should
        # happen only once, the first time a scrunch state applies
        pre_tscrunch = True
        pre_fscrunch = True
        post_tscrunch = True
        post_fscrunch = True

        psrsh = self.pre_scrunch_psrsh(scrunches)
        for scrunch,state in scrunches:
        
            # this handles things like clone/push/pop
            if not scrunch.is_scrunch():
                psrsh += scrunch.get_psrsh(self.obs)
                continue

            # "pre" operations
            if pre_tscrunch and state.pre_tscrunch():
                psrsh += self.pre_tscrunch_psrsh(scrunches)
                pre_tscrunch = False
            if pre_fscrunch and state.pre_fscrunch():
                psrsh += self.pre_fscrunch_psrsh(scrunches)
                pre_fscrunch = False

            psrsh += scrunch.get_psrsh(self.obs)

            # "post" operations
            if post_tscrunch and state.post_tscrunch():
                psrsh += self.post_tscrunch_psrsh(scrunches)
                post_tscrunch = False

            if post_fscrunch and state.post_fscrunch():
                psrsh += self.post_fscrunch_psrsh(scrunches)
                post_fscrunch = False

            output = scrunches.obs2file(self.obs,scrunch)
            psrsh.append('unload %s\n'%output)
            #psrsh += scrunch.get_post_psrsh(self.obs)

        return psrsh

class Diode_ScrunchDepOps(ScrunchDepOps):
    """ Default scrunch-dependent ops for noise calibrators.

    NB that this class isn't currently used in the DR2 pipeline!
    """

    def __init__(self,obs):
        self.init()
        self.obs = obs

    def pre_scrunch_psrsh(self,scrunches):
        """ Default implementation: corrections and zapping."""
        psrsh = self.obs.get_psrsh()
        psrsh += zapping.ppta_default_zap(self.obs)
        psrsh += zapping.check_cal_subints(self.obs,zaps=psrsh)
        return psrsh

class P574_Diode_ScrunchDepOps(ScrunchDepOps):
    """ Specific scrunch-dependent ops.

    This class is actually used for both P574 and PPTA (P456).
    """

    def __init__(self,obs,zap_subints='yes'):
        self.init()
        self.obs = obs
        self.zap_subints = zap_subints
        #self.check_cal_backends = ['CASPSR','PDFB4']

    def pre_scrunch_psrsh(self,scrunches):
        """ Default implementation: corrections and zapping."""
        psrsh = self.obs.get_psrsh()
        psrsh += zapping.ppta_default_zap(self.obs)
        #if self.obs['backend'] in self.check_cal_backends:
        psrsh += zapping.check_cal_subints(self.obs,zaps=psrsh)
        return psrsh

class PPTA_ScrunchDepOps(ScrunchDepOps):

    """ Gather an observation, its calibrator, an ephemeris, and a template.
    """

    def __init__(self,obs,cal,eph,template,zap_subints='dry',
        install_eph=True,strict_eph=False,make_dynspec=False):
        """ zap_subints options: yes,no,dry
            install_eph: [True/False] -- align subints to specified ephem.
        """
        self.init()
        self.obs = obs
        self.cal = cal
        self.eph = eph
        self.template = template
        self.zap_subints = zap_subints
        self.install_eph = install_eph
        self.strict_eph = strict_eph
        self.make_dynspec = make_dynspec

    def _default_zap(self,scrunches):
        if self.zap_subints in ['yes','dry']:
            outplot = join(
                scrunches.outpath,self.obs.basename+'.subint_zapping.png')
            dry = self.zap_subints=='dry'
            return rfi.p574_default_zap(self.obs,outplot=outplot,dry=dry)
        else:
            return zapping.ppta_default_zap(self.obs)

    def cal_psrsh(self,scrunches):
        return self.cal.calibrate(self.obs)

class P574_ScrunchDepOps(PPTA_ScrunchDepOps):

    def post_tscrunch_psrsh(self,scrunches):
        psrsh = jobs.psrsh_segment()
        if self.make_dynspec:
            psrsh.comment('Compute tscrunched spectrum.')
            dynspec_output = util.change_extension(
                scrunches.get_scrunched(self.obs),'spectrum')
            if self.template is not None:
                psrsh.add('dynspec %s %s'%(
                    dynspec_output,self.template.fname))
            else:
                psrsh.add('dynspec %s'%(dynspec_output))
        # in this case, install ephemeris after tscrunch if it was specified
        # not to do so before (thus aligning the subints)
        if (not self.install_eph) and (self.eph is not None):
            psrsh.append(
                self.eph.get_eph_psrsh(self.obs,strict=self.strict_eph))
        return psrsh
        #return jobs.psrsh_segment()

class TimingAnalysis(object):
    """ Generate scripts from a database and ancillary classes.

    The primary purpose of this class is to collate all that we know about
    the raw data and the corrections that need to / have been made to it.
    
    Then, given some guidelines, it performs additional analysis and
    generates the jobs that need to be run to produce psrsh output and,
    optionally, to execute those scripts to produce full output.

    Finally, it generates products used elsewhere to generate diagnostic
    output.
    """

    def __init__(self,database,cal_matcher,
            cal_scrunches,psr_scrunches,
            cal_scrunch_dep_ops,psr_scrunch_dep_ops,
            ephdb=None,pcmdb=None,fluxdb=None,templdb=None,
            hydra=False,pat_args=None):
        """ 
            cal_matcher -- instance of MatchCal
            cal_scrunches -- instance of Scrunch for calibrators
            psr_scrunches -- instance of Scrunch for observations
            cal_scrunch_dep_ops -- a factory to produce an instance of
                ScrunchDepOps when given an Observation object
            psr_scrunch_dep_ops -- a factory to produce an instance of
                ScrunchDepOps when given an Observation object
        """

        self._cscr = cal_scrunches
        self._pscr = psr_scrunches

        self.db = database
        self.ephdb = ephdb
        self.templdb = templdb
        self.fluxdb = fluxdb
        self.hydra = hydra
        self.pat_args = pat_args

        self.psrobs = database.get_obs(require_valid=False)
        self.calobs = database.get_cals(require_valid=False)

        # if we have only cals, interpret them as obs, e.g. if we are
        # processing HYDRA data
        if hydra and (len(self.psrobs)==0) and (len(self.calobs) > 0):
            self.psrobs,self.calobs = self.calobs,self.psrobs

        self.empty = len(self.psrobs)==0
        if self.empty:
            return

        # match up diode cals with observations
        self.cal_matches = map(cal_matcher.find_diodes,self.psrobs)
        if len(self.cal_matches)==0:
            self.cal_matches = [None,None]
        self.unique_diodes = cal_matcher.get_matched_cals()

        # match up pcm cals with observations
        if pcmdb is not None:
            cals = pcmdb.get_obs(modes=['PCM'])
            matcher = calibration.MatchCal_PCM(cals)
            self.pcm_matches = map(matcher.find_cal,self.psrobs)
        else:
            self.pcm_matches = [None]

        # match up flux cals with observations
        if fluxdb is not None:
            cals = fluxdb.get_obs(modes=['PCM'])
            matcher = calibration.MatchCal_Flux(cals)
            self.flux_matches = map(matcher.find_cal,self.psrobs)
        else:
            self.flux_matches = [None]

        cals = self.cals = map(
            lambda d,p,f:PPTA_Cal(self._cscr,d[0],d[1],pcm=p,fluxcal=f),
            self.cal_matches,self.pcm_matches,self.flux_matches)

        for obs,cal in zip(self.psrobs,cals):
            if not cal.valid():
                # flag observation as having no calibrator
                obs.add_data_flag('no_cal')

        ephs = map(ephdb,self.psrobs) if ephdb is not None else [None]
        templs = map(templdb,self.psrobs) if templdb is not None else [None]
        
        if not hydra:
            self.cal_ops = map(cal_scrunch_dep_ops,self.unique_diodes)
            self.psr_ops = map(
                psr_scrunch_dep_ops,self.psrobs,cals,ephs,templs)

            self.cal_plots = [plotting.StokesPlot(),plotting.GainsPlot()]
            self.psr_plots = [plotting.StokesPlot('-r0.5'),
                              plotting.AmpFreqPhasePlot('-r0.5'),
                              plotting.AmpTimePhasePlot('-r0.5'),
                              plotting.OriginalBandpassPlot()]
        else:
            self.psr_ops = map(cal_scrunch_dep_ops,self.psrobs)
            self.cal_ops = []

            self.psr_plots = [plotting.StokesPlot('-r0.5'),
                              plotting.AmpFreqPhasePlot('-r0.5'),
                              plotting.AmpTimePhasePlot('-r0.5'),
                              plotting.OriginalBandpassPlot(),
                              plotting.GainsPlot()]
            self.cal_plots = []

    def _calpath(self):
        return self._cscr.outpath

    def _psrpath(self):
        return self._pscr.outpath

    def link_files(self):
        """ Link obs. and cal. files into output directory."""

        def link_obs(allobs,path):
            for obs in allobs:
                link_target = join(path,obs.basename)
                try:
                    os.symlink(obs.fname,link_target)
                except OSError:
                    pass

        link_obs(self.unique_diodes,self._calpath())
        link_obs([ops.obs for ops in self.psr_ops],self._psrpath())

    def make_work(self,clobber=False,combine_psrsh=True,clobber_toas=False,
        make_toas=True,new_only=False):
        """ Generate jobs to be executed via the MPI queue.

        This method is responsible for generating the work with the correct
        dependencies, but it is executed elsewhere.

        combine_psrsh: To generate psrsh, we have often read in the entire
        raw data file, and it is advantageous from an I/O standpoint to try
        to process it while it is cached.  If this option is true, attempt
        to do this.  The drawback is a loss of granularity in the
        computation: e.g., the psrsh may be generated without issue but fail
        of execution.  Every time the pipeline is run, the psrsh will be
        re-generated fruitlessly.
        
        *******
        *** I am not sure if combine_psrsh = False will run correclty now!
        *******
        """

        # NB -- the dictionaries are used to keep track of targets
        # and dependencies when deps needed in multiple places
        # basically, save the object associated with a particular output so
        # it can be used as a dependency for another object with that as
        # input

        if self.empty:
            return [],[]

        self.link_files()

        from mpi import MPI_GeneratePsrsh,MPI_ExecutePsrsh,\
                        MPI_GeneratePlot,MPI_GenerateTOAs,\
                        MPI_CombinedPsrsh

        cal_dict = dict()
        cal_mpsrsh = deque() # work for making psrsh
        cal_epsrsh = deque() # work for executing or executing&making psrsh
        cal_plots = deque() # work for making diagnostic plots for cals

        # generate cal psrsh & plots
        for ops in self.cal_ops:

            clob = clobber or ops.obs.clobber

            if new_only:
                if hasattr(ops.obs,'new') and (not ops.obs.new):
                    continue

            if not combine_psrsh:
                m = MPI_GeneratePsrsh(ops,self._cscr,clobber=clob)
                e = MPI_ExecutePsrsh(ops,self._cscr,clobber=clob,
                        dependencies=[m])
                m.suggested_work = e # suggest linking these for I/O effic.
                cal_mpsrsh.append(m)
                cal_epsrsh.append(e)
            else:
                e = MPI_CombinedPsrsh(ops,self._cscr,clobber=clob,
                        dependencies=[])
                cal_epsrsh.append(e)
            cal_dict[e.target] = e # map object to its output
            p = map(lambda p: MPI_GeneratePlot(p,ops,self._cscr,
                clobber=clob,dependencies=[e]),self.cal_plots)
            cal_plots.extend(p)

        # generate obs psrsh
        obs_dict = dict()
        obs_mpsrsh = deque() # work for making psrsh
        obs_epsrsh = deque() # work for executing or executing&making psrsh
        obs_plots = deque() # work for making diagnostic plots for obs
        obs_toas = deque() # work for making TOAs
        for ops in self.psr_ops:

            # clobber if calibrator being clobbered too
            clob = clobber or ops.obs.clobber
            try:
                for diode in ops.cal.get_diodes():
                    clob = clob or diode.clobber
            except AttributeError:
                pass

            if new_only:
                if hasattr(ops.obs,'new') and (not ops.obs.new):
                    continue

            # identify the objects associated with processing cals; these
            # are the dependencies for EXECUTING obs. psrsh
            if self.hydra:
                deps = []
            else:
                deps = [cal_dict[self._cscr.get_target(x)] for x in ops.cal.get_dependencies()]
            if not combine_psrsh:
                m = MPI_GeneratePsrsh(ops,self._pscr,clobber=clob,
                    label=self._pscr.label)
                deps.append(m) # add the psrsh generation to deps
                e = MPI_ExecutePsrsh(ops,self._pscr,clobber=clob,
                        dependencies=deps,label=self._pscr.label)
                m.suggested_work = e
                obs_mpsrsh.append(m)
                obs_epsrsh.append(e)
            else:
                e = MPI_CombinedPsrsh(ops,self._pscr,clobber=clob,
                        dependencies=deps,label=self._pscr.label)
                obs_epsrsh.append(e)

            valid_plots = [x for x in self.psr_plots if x.valid(ops.obs)]
            obs_plots.extend(
                map(lambda p: MPI_GeneratePlot(p,ops,self._pscr,
                    clobber=clob,dependencies=[e]),valid_plots))

            if make_toas:
                clob = clobber_toas or clob
                if ops.template is None:
                    print 'No template for %s/%s!.'%(
                        ops.obs['name'],ops.obs.basename)
                else:
                    t = MPI_GenerateTOAs(ops,self._pscr,
                        clobber=clob,dependencies=[e],
                        pat_args=self.pat_args)
                    obs_toas.append(t)
                    if self._pscr.has_invariant_interval():
                        template = self.templdb(ops.obs,force_invint=True)
                        t = MPI_GenerateTOAs(ops,self._pscr,
                            clobber=clob,dependencies=[e],
                            use_invint=True,override_template=template,
                            pat_args=self.pat_args)
                        obs_toas.append(t)

        # put work in general order it should be done
        work = [cal_mpsrsh,cal_epsrsh,obs_mpsrsh,obs_epsrsh,
            cal_plots,obs_plots,obs_toas]
        all_work = deque()
        map(all_work.extend,work)
        return work,all_work

    def gather_ephemeris_string(self,jname):
        """ Return a string that can be written out as an ephemeris."""
        if self.ephdb is None:
            return
        ephemeris = self.ephdb(jname)
        if ephemeris is None:
            return
        return file(ephemeris.fname).read()

    def get_combos(self,jname=None,band_sort=False):
        """ Return unique band/rcvr/backend combinations from observations.
        
        Default -- return combos sorted by number of observations.  If
        requested, make primary sort on band, with secondary sort on number
        of observations.
        """
        if jname is not None:
            ops = filter(lambda x: x.obs.name==jname,self.psr_ops)
        else:
            ops = self.psr_ops
        combos = map(lambda x: x.obs.get_combo(),ops)
        unique_combos = list(set(combos))
        unique_combos.sort(key=lambda c: combos.count(c), reverse=True)
        if band_sort:
            unique_combos.sort(key=lambda c: c.split('_')[0])
        return unique_combos

class PPTA_TimingAnalysis(TimingAnalysis):
    """ Subclass to with defaults set up for PPTA."""

    def __init__(self,database,calpath,psrpath,
            ephdb=None,pcmdb=None,fluxdb=None,templdb=None,
            label=None,zap_subints='dry',install_eph=True,
            make_dynspec=False,make_invint=False,pat_args=None,
            cal_tmax=1./24,rfi_pipeline=False):
        if ephdb is None:
            ephdb = ephemeris.ppta_default()
        if templdb is None:
            templdb = template.ppta_default()
        cal_matcher = calibration.MatchCal_Diode_PPTA(database.get_cals(),
            tmax=cal_tmax)
        cal_scrunches = scrunch.PPTA_Cal_Scrunch(abspath(calpath))
        constructor = scrunch.PPTA_Psr_Scrunch_InvInt if make_invint else scrunch.PPTA_Psr_Scrunch
        if rfi_pipeline:
            constructor = scrunch.P574_Psr_Scrunch
        psr_scrunches = constructor(abspath(psrpath),label=label)
        def psr_ops(*args):
            kwargs = dict(zap_subints=zap_subints,install_eph=install_eph,
                make_dynspec=make_dynspec)
            return PPTA_ScrunchDepOps(*args,**kwargs)
        def cal_ops(*args):
            kwargs = dict(zap_subints=zap_subints)
            return P574_Diode_ScrunchDepOps(*args,**kwargs)
        super(PPTA_TimingAnalysis,self).__init__(database,cal_matcher,
            cal_scrunches,psr_scrunches,cal_ops,psr_ops,
            ephdb=ephdb,pcmdb=pcmdb,fluxdb=fluxdb,templdb=templdb,
            pat_args=pat_args)

class P574_TimingAnalysis(TimingAnalysis):
    """ Subclass with defaults set up for P574."""

    def __init__(self,database,calpath,psrpath,
            ephdb=None,pcmdb=None,fluxdb=None,templdb=None,
            label=None,zap_subints='yes',install_eph=True,strict_eph=False,
            make_dynspec=False):
        cal_matcher = calibration.MatchCal_Diode_P574(database.get_cals())
        cal_scrunches = scrunch.PPTA_Cal_Scrunch(abspath(calpath))
        psr_scrunches = scrunch.P574_Psr_Scrunch(abspath(psrpath))
        def psr_ops(*args):
            kwargs = dict(zap_subints=zap_subints,install_eph=install_eph,
                strict_eph=strict_eph,make_dynspec=make_dynspec)
            return P574_ScrunchDepOps(*args,**kwargs)
        def cal_ops(*args):
            kwargs = dict(zap_subints=zap_subints)
            return P574_Diode_ScrunchDepOps(*args,**kwargs)
        super(P574_TimingAnalysis,self).__init__(database,cal_matcher,
            cal_scrunches,psr_scrunches,cal_ops,psr_ops,
            ephdb=ephdb,pcmdb=pcmdb,fluxdb=fluxdb,templdb=templdb)

class CalibratorAnalysis(TimingAnalysis):
    """ Run calibrator observations (e.g. pcm, fluxcal) through pipeline.

    These differ from standard timing observations as
    (1) they aren't independently calibrated
    (2) we want to retain full frequency resolution, and, for pcm, some
        nominal time resolution

    This is shoehorned into the timing analysis framework, so is
    necessarily a little bit klugey.

    By default, no TOAs are generated from the data.
    """
    def __init__(self,database,calpath,psrpath,
            ephdb=None,templdb=None,zap_subints='dry',make_toas=False):
        cal_matcher = calibration.MatchCal_NoCal(database.get_cals())
        cal_scrunches = scrunch.PPTA_Cal_Scrunch(abspath(calpath))
        psr_scrunches = scrunch.NoScrunch(abspath(psrpath))
        self.make_toas = make_toas
        def psr_ops(*args):
            kwargs = dict(zap_subints=zap_subints)
            return PPTA_ScrunchDepOps(*args,**kwargs)
        def cal_ops(*args):
            kwargs = dict(zap_subints=zap_subints)
            return P574_Diode_ScrunchDepOps(*args,**kwargs)
        super(CalibratorAnalysis,self).__init__(database,cal_matcher,
            cal_scrunches,psr_scrunches,cal_ops,psr_ops,
            ephdb=ephdb,pcmdb=None,fluxdb=None,templdb=templdb,
            hydra=True)

    def make_work(self,clobber=False):
        return super(CalibratorAnalysis,self).make_work(
            clobber=clobber,make_toas=self.make_toas)

class FluxCalibratorAnalysis(TimingAnalysis):
    """ Process flux calibrator observations.
    
    These observations differ from standard observations in that
    (1) they are all CAL mode (so aren't independently calibrated!) and
    (2) require full frequency resolution

    We do not require any additional timing resolution -- so produce a
    single integration, full-frequency output.

    Obviously we also ignore all of the TOA stuff.
    """
    def __init__(self,database,calpath,psrpath,
            ephdb=None,templdb=None,zap_subints='dry',make_toas=False):
        cal_matcher = calibration.MatchCal_NoCal(database.get_cals())
        cal_scrunches = scrunch.NoScrunch(abspath(psrpath))
        psr_scrunches = scrunch.PPTA_FluxCal_Scrunch(abspath(calpath))
        self.make_toas = make_toas
        # NB -- because we treat calibrators as "pulsar" observations, use
        # the diode scrunch dependent operations here
        def psr_ops(*args):
            kwargs = dict(zap_subints=zap_subints)
            return PPTA_ScrunchDepOps(*args,**kwargs)
            #return P574_Diode_ScrunchDepOps(*args,**kwargs)
        def cal_ops(*args):
            kwargs = dict(zap_subints=zap_subints)
            return P574_Diode_ScrunchDepOps(*args,**kwargs)
            #return PPTA_ScrunchDepOps(*args,**kwargs)
        super(FluxCalibratorAnalysis,self).__init__(database,cal_matcher,
            cal_scrunches,psr_scrunches,cal_ops,psr_ops,
            ephdb=ephdb,pcmdb=None,fluxdb=None,templdb=templdb,
            hydra=True)

    def make_work(self,clobber=False):
        return super(FluxCalibratorAnalysis,self).make_work(
            clobber=clobber,make_toas=self.make_toas)

class PCMCalibratorAnalysis(TimingAnalysis):
    """ Process long-track observations for receiver calibration.
    
    These observations differ from standard observations in that
    (1) they should not be independently calibrated
    (2) they require full frequency resolution and nominal time resolution

    In this case, we also don't produce TOAs (which should be done instead
    via the usual pipeline.)
    """
    def __init__(self,database,calpath,psrpath,
            ephdb=None,templdb=None,zap_subints='dry',make_toas=False):
        cal_matcher = calibration.MatchCal_NoCal(database.get_cals())
        cal_scrunches = scrunch.PPTA_FluxCal_Scrunch(abspath(calpath))
        psr_scrunches = scrunch.PPTA_PCMCal_Scrunch(abspath(psrpath))
        self.make_toas = make_toas
        def psr_ops(*args):
            kwargs = dict(zap_subints=zap_subints)
            return PPTA_ScrunchDepOps(*args,**kwargs)
        def cal_ops(*args):
            kwargs = dict(zap_subints=zap_subints)
            return P574_Diode_ScrunchDepOps(*args,**kwargs)
        super(PCMCalibratorAnalysis,self).__init__(database,cal_matcher,
            cal_scrunches,psr_scrunches,cal_ops,psr_ops,
            ephdb=ephdb,pcmdb=None,fluxdb=None,templdb=templdb,
            hydra=False)

    def make_work(self,clobber=False):
        return super(PCMCalibratorAnalysis,self).make_work(
            clobber=clobber,make_toas=self.make_toas)
