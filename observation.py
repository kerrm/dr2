"""
Encapsulate properties of a PSRFITS pulsar observation.

$Header: /nfs/slac/g/glast/ground/cvs/users/kerrm/tpipe/timing.py,v 1.14 2014/01/21 06:46:59 kerrm Exp $

author: Matthew Kerr <matthew.kerr@gmail.com>
"""

import numpy as np
import pyfits
import os
from os.path import basename,abspath,join,splitext,isfile
from collections import deque
import json
import cPickle

import psrchive

from jobs import psrsh_segment
from flagging import flag_factory
from util import local_file
import band
import alias

# make dictionary with PSRCHIVE as lookup key
props = json.load(file(local_file('properties.asc')))
for key in props:
    for default in ['valid','common']:
        if default not in props[key]:
            props[key][default] = None

# make a dictionary with PSRFITS as lookup key
rprops = dict()
for key in props:
    rkey = props[key]['psrfits'][0]
    if rkey in rprops:
        raise KeyError('Multiple keys for same property!')
    rprops[rkey] = key

# make a dictionary with common names as lookup key
cprops = dict()
for key in props:
    rkeys = props[key]['common']
    if rkeys is None:
        continue
    for rkey in rkeys:
        cprops[rkey] = key

# add in a few extra keys that are set at runtime
recognized_keys = list(set(['mjd','tbin']+props.keys()+rprops.keys()+cprops.keys()))

class Observation(object):
    """ Encapsulate an observation and a history of changes to it.
    
    This is a primary class in the pipeline, as it is used for matching up
    calibrators, determining zapping, etc.
    """
    
    def __init__(self,fname):
        self.fname = abspath(fname)
        self.ext = os.path.splitext(fname)[-1]
        self.basename = basename(fname)
        self.actions = psrsh_segment()
        self.log = deque()
        self.props = dict()
        self.tobs = -1
        self.nsub = 0
        self.datamb = 0.
        self.atten = None
        self.invint = False
        self.centre_freq = 0.
        self.chan_bw = 0.

        self.clobber = False

        # flags for observations -- 
        # delay_flags -- a list of delays to be flagged at TOA level
        # toa_flags -- additional flags to accompany TOAs
        # data_flags -- data quality flags determined during processing
        self.delay_flags = dict()
        self.toa_flags = []
        self.data_flags = []

        f = pyfits.open(fname,memmap=True)

        # check whether the observation is empty
        try:
            header = f['subint']
        except KeyError:
            # it might be pcm/fluxcal
            if f['primary']._header['obs_mode'] != 'PCM':
                self.add_data_flag('no_data')

        for hdu in f:
            header = hdu._header
            if (hdu.name=='SUBINT'):
                if hdu.data is None:
                    self.add_data_flag('no_data')
                else:
                    self.nsub = header['NAXIS2']
                    if self.nsub==0:
                        self.add_data_flag('no_data')
                    self.datamb = float(
                        header['NAXIS1'])*self.nsub/2**20
                    if self.nsub > 0:
                        #self.tobs = np.sum(hdu.data.field('tsubint'))
                        self.tobs = psrchive.get_tobs(fname)
                    else:
                        self.tobs = 0.
                    if self.tobs < 1:
                        self.add_data_flag('no_data')
                    if 'POL_TYPE' in header.keys():
                        self.invint = header['POL_TYPE'] == 'INVAR'
            """
            if (hdu.name=='DIG_STAT'):
                # record attenuator levels
                try:
                    self.atten = hdu.data.field('atten').copy()
                except KeyError:
                    print 'No attenuator levels found in %s!'%self.basename
            """
            if (hdu.name=='HISTORY'):

                centre_freq = hdu.data.field('ctr_freq')
                if len(centre_freq) > 0:
                    self.centre_freq = centre_freq[-1]

                self.props['tbin'] = hdu.data.field('tbin')[0]

                chan_bw = hdu.data.field('chan_bw')
                if len(chan_bw) > 0:
                    self.chan_bw = chan_bw[-1]

                nchan = hdu.data.field('nchan')
                if len(nchan) > 0:
                    self.props['nchan'] = nchan[-1]

            for key in header.keys():
                if key in rprops:
                    prop = rprops[key]
                    self.props[prop] = header[key]

        # handle TBIN especially
        try:
            self.props['tbin'] = f['history'].data.field('tbin')[0]
        except:
            self.props['tbin'] = f['subint']['tbin']

        # make sure nbin set properly
        try:
            self['nbin']
        except KeyError:
            try:
                self.props['nbin'] = f['history'].data.field('nbin')[0]
            except:
                self.props['nbin'] = 0

        f.close()
        self._init_obskw()
        self.props['mjd'] = self.get_mjd() # kluge for grammar
        self.props['addsat'] = 0. # allow delays in TOAs
        #self.props['centre_freq'] = self.centre_freq
        self.props['centre_freq'] = self.get_centre_frequency()

        # NB OBSBW is always +ve per FITS convention; not always written out
        # so by tcs
        if (self.chan_bw != 0) and (self['nchan'] != 0):
            self.props['bandwidth'] = self.chan_bw * self['nchan']

        # kluge to handle names with extra white space
        if self['name'] != self['name'].strip():
            self.comment('Strip whitespace from JNAME.')
            self['name'] = self['name'].strip()

        # TODO -- move to corrections
        if not self['name'].startswith('J'):
            self['name'] = 'J'+self['name']

        # TODO -- move to corrections
        if len(self['site'].strip()) == 0:
            self['site'] = 'PARKES'

        # add an entry for sk zapping
        self.props['nosk'] = 'p_sk' in self.basename

        # TEMPORARY
        # add an entry for "orig" file
        self.props['orig'] = 'orig' in self.basename
        # END TEMPORARY

    def _init_obskw(self):
        # kluge -- initialize these values if they are missing
        for key in ['OBSFREQ','OBSNCHAN','OBSBW']:
            k = rprops[key]
            if k not in self.props:
                self.props[k] = 0.
            elif self.props[k]=='*':
                self.props[k] = 0.

    def valid(self):
        """ Determine if observation is fit for processing."""
        # TODO -- this can be made more granular, and logic in flag module
        for flag in self.data_flags:
            if not flag.can_process():
                return False
        return True

    def log(self,line):
        self.log.append(line)

    def comment(self,line):
        self.actions.comment(line)

    def command(self,*lines):
        self.actions.add(*lines)

    def _key(self,key):
        if key in self.props:
            return key
        if key in cprops:
            return cprops[key]
        if key in rprops:
            return rprops[key]
        return key

    def __getitem__(self,key):
        return self.props[self._key(key)]

    def __setitem__(self,key,value):
        key = self._key(key)
        self.props[key] = value
        cmd = 'edit %s=%s'%(key,value)
        self.actions.append(cmd)

    def __setstate__(self,state):
        # temporary to bring old pickles up to date
        if 'invint' not in state.keys():
            state['invint'] = False
        #if 'centre_freq' not in state.keys():
            #state['centre_freq'] = 0.
        #if 'centre_freq' not in state['props'].keys():
            #state['props']['centre_freq'] = state['centre_freq']
        self.__dict__.update(state)
        if 'centre_freq' not in state.keys():
            self.centre_freq = 0
        self.props['centre_freq'] = self.get_centre_frequency()
        if 'hist:tbin' in self.props:
            self.props['tbin'] = self.props['hist:tbin']
        try:
            self['nbin']
        except KeyError:
            self['nbin'] = 0
        if 'clobber' not in state.keys():
            self.clobber = False

        # temporary to bring old pickles up to date
        if 'nosk' not in state.keys():
            self.props['nosk'] = 'p_sk' in self.basename

        # TEMPORARY
        if 'orig' not in state.keys():
            # add an entry for "orig" file
            self.props['orig'] = 'orig' in self.basename
        # END TEMPORARY

        if not self.props['name'].startswith('J'):
            self.__setitem__('name','J'+self.props['name'])

        try:
            if len(self.props['site'].strip()) == 0:
                self.__setitem__('site','PARKES')
        except KeyError:
            self.__setitem__('site','PARKES')

    def set(self,key,value):
        """ Update an entry WITHOUT emitting psrsh."""
        key = self._key(key)
        self.props[key] = value

    def get_psrsh(self,absolute=True):
        """ Emit the record of psrsh commands generated by changes."""
        c = self.actions.copy()
        fname = self.fname if absolute else self.basename
        c.appendleft('load %s'%fname)
        if not self.valid():
            for flag in self.data_flags:
                c.appendleft('# data flag: %s'%flag)
            c.appendleft('test 0')
            c.appendleft('# Fail on invocation.')
        return c

    def get_link(self):
        """ Return a command to link the absolute path to the cwd."""
        return 'ln -s %s %s'%(self.fname,self.basename)

    def get_stem(self,outpath):
        """ Return an absolute path to which an extension can be added."""
        #return join(outpath,splitext(self.basename)[0])
        return join(outpath,self.basename)

    def get_ancillary(self,outpath):
        """ Return a dictionary of ancillary information."""
        fname = self.get_stem(outpath) + '.ancillary.pickle'
        if isfile(fname):
            try:
                return cPickle.load(file(fname))
            except:
                return dict()
        return dict()

    def set_ancillary(self,state,outpath):
        fname = self.get_stem(outpath) + '.ancillary.pickle'
        if isfile(fname):
            try:
                d = cPickle.load(file(fname))
                d.update(state)
                state = d
            except Exception as e:
                print e
        cPickle.dump(state,file(fname,'w'))

    def get_mjd(self):
        try:
            imjd = self.props['ext:stt_imjd']
        except KeyError:
            return None
        if imjd == '*':
            return None
        smjd = self.props['ext:stt_smjd']
        offs = self.props['ext:stt_offs']
        return imjd + (smjd+offs)/86400

    def get_centre_frequency(self):
        f = self['frequency']
        if f > 0:
            return f
        if self.centre_freq > 0:
            return self.centre_freq
        return 0.

    def copy_file(self,dstpath,update=True):
        """ Return (src,dst) such that "cp src dst" will copy the raw file
        associate with this observation to the path.

        Additionally, if update is set, adjust the base filename to reflect
        the new location.
        """
        src = self.fname
        dst = join(dstpath,self.basename)
        if update:
            self.fname = dst
        return src,dst

    def get_nsub(self):
        return self.nsub

    def is_cal(self):
        mode = self['OBS_MODE']
        # FOF == Flux cal OFF; FON == Flux cal ON
        return mode=='CAL' or mode=='LEVCAL' or mode=='FOF' or mode=='FON'

    def is_invint(self):
        return self.invint

    def apply_correction(self,corr):
        corr(self)

    def add_data_flag(self,flag,comment=None):
        """ Add a DataQualityFlag, either via object or code.
        """
        if type(flag)==type(''):
            flag = flag_factory(flag,comment=comment)
        for f in self.data_flags:
            if f==flag:
                return
        self.data_flags.append(flag)

    def add_toa_flag(self,flag):
        """ Add an explicit TOA flag.

        NB that TOA flags associated with DataQualityFlag objects are 
        handled separately.  All flags are returned by get_toa_flags.
        """
        if flag not in self.toa_flags:
            self.toa_flags.append(flag)

    def valid_toa(self):
        """ A convenience method for plotting -- return False if any of
        the data flags associated with obs. have a TOA flag.
        """
        toa_flags = filter(lambda l: l is not None,
            [x.toa_flag() for x in self.data_flags])
        return len(toa_flags) == 0

    def get_toa_flags(self,add_info=False):
        """ Return TOA flags from DataQualityFlags and those added 
        explicitly.

        If specified (add_info==True), also return some helpful identifying
        information about the observation, e.g. the backend configuration,
        the project id, etc.
        """
        # get TOA flags from data flags if present
        toa_flags = filter(lambda l: l is not None,
            [x.toa_flag() for x in self.data_flags])
        if add_info:
            try:
                toa_flags.append('-projid %s'%(self['projid']))
            except KeyError:
                toa_flags.append('-projid none')
            try:
                beconfig = self['be:config'].strip()
                if (len(beconfig)==0):
                    beconfig = 'none'
                toa_flags.append('-beconfig %s'%(beconfig))
            except KeyError:
                toa_flags.append('-beconfig none')
        return self.toa_flags + toa_flags

    def get_combo(self,exclude_backend=False):
        """ Return band/rcvr/backend combinations."""
        b = band.Band(self)
        # add an entry for sk zapping
        self.props['sk'] = 'p_sk' in self.basename
        if exclude_backend:
            return '%s_%s'%(band.Band(self),self['receiver'])
        if self.props['nosk']:
            backend = 'NOSK'+self['backend']
        else:
            backend = self['backend']
        return '%s_%s_%s'%(band.Band(self),self['receiver'],backend)

    def overlap(self,obs):
        """ Estimate the extent to which an observation overlaps another in
        terms of observing and bandwidth, viz.

        overlap = t_overlap/tobs*b_overlap/BW

        The purpose is to choose observation with correlated noise for
        backend alignment, and/or to exclude simultaneous observations from
        co-added profiles.
        """

        # do not allow overlap with self!
        if obs.basename == self.basename:
            return 0.

        if (not self.valid()) or (not obs.valid()):
            return

        t1 = self.get_mjd()
        t2 = obs.get_mjd()
        tmax = min(t1+self.tobs/86400,t2+obs.tobs/86400)
        tmin = max(t1,t2)
        ttot = max(self.tobs/86400,obs.tobs/86400)
        dt = tmax-tmin
        if dt < 0:
            return 0.
        bw_1 = self['bandwidth']
        bw_2 = obs['bandwidth']
        f1_c = self.get_centre_frequency()
        f1_0,f1_1 = f1_c-0.5*bw_1,f1_c+0.5*bw_1
        if f1_1 < f1_0:
            f1_0,f1_1 = f1_1,f1_0 # correct sideband
        f2_c = obs.get_centre_frequency()
        f2_0,f2_1 = f2_c-0.5*bw_2,f2_c+0.5*bw_2
        if f2_1 < f2_0:
            f2_0,f2_1 = f2_1,f2_0 # correct sideband
        fmax = min(f1_1,f2_1)
        fmin = max(f1_0,f2_0)
        ftot = max(abs(bw_1),abs(bw_2))
        df = fmax-fmin
        if df < 0:
            return 0.
        return dt/ttot*df/ftot

    def get_attenuator_settings(self):
        """ Return the primary attenuator levels."""

        f = pyfits.open(self.fname,memmap=True)
        try:
            hdu = f['dig_stat']
            return hdu.data.field('atten').copy()
        except KeyError:
            return None

    def partial_subint(self):
        """ Return True if last subint is partial.  Return False if
        cannot infer or not partial."""
        # This is needed to handle e.g. partial subints with weights of 1.
        #TODO
        pass
