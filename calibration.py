"""
Manage calibrators, and matching calibrators to observations.

$Header: /nfs/slac/g/glast/ground/cvs/users/kerrm/tpipe/timing.py,v 1.14 2014/01/21 06:46:59 kerrm Exp $

author: Matthew Kerr <matthew.kerr@gmail.com>
"""

import numpy as np
import psrchive
from os.path import join,basename

from jobs import psrsh_segment
import band

class MatchCal(object):
    """ Parent class for set of calibration matching strategies."""

    def __init__(self,obs,match_keys):
        """ obs -- a _set_ of Observation objects corresponding to cals
            match_keys -- a set of properties to require for match
        """
        self.obs = obs
        self.empty = len(obs) == 0
        if self.empty:
            print 'Oh my goodness!  I am so empty inside.'
        self.unique_cals = set()

        # construct a cache for array operations
        c = self.cache = dict()
        for key in match_keys:
            if self.empty:
                break
            t = type(self.obs[0][key])
            c[key] = np.asarray([obs[key] for obs in self.obs],dtype=t)
        # overwrite name entry without '_R'
        c['name'] = np.asarray(
            [obs['name'].split('_')[0] for obs in self.obs])
        # TEMPORARY
        # add a temporary entry for ".orig" files
        c['orig'] = np.asarray(
            ['orig' in obs.basename for obs in self.obs],dtype=bool)
        match_keys.append('orig')
        # END TEMPORARY
        self._match_keys = match_keys
        self.mjds = np.asarray([obs.get_mjd() for obs in self.obs])
        self._fail_cache = set()

    def match_keys(self,obs):
        """ Return a mask for observations matching indicated keys."""
        mask = np.asarray([True]*len(self.obs))
        if not self.empty:
            for key in self._match_keys:
                mask &= obs[key]==self.cache[key]
        return mask

    def match_key(self,obs,key):
        """ Return a mask for observations matching indicated keys."""
        if self.empty:
            return False
        if key not in self.cache.keys():
            t = type(obs[key])
            self.cache[key] = np.asarray([o[key] for o in self.obs],dtype=t)
        return obs[key]==self.cache[key]

    def match_levels(self):
        # TODO
        pass

    def get_matched_cals(self):
        """ Return all cals that have been matched."""
        return list(self.unique_cals)

    def _fail(self,obs,verbosity=1,id_string=''):
        """ Print out a failure message."""
        # suppress message for invalid observations
        if not obs.valid():
            return
        id_string = ' '
        if '_' in self.__class__.__name__:
            id_string = ' %s cal '%(self.__class__.__name__.split('_')[1])
        if verbosity > 0:
            output = '%s %s %s'%(
                obs['centre_freq'],obs['receiver'],obs['backend'])
            if output in self._fail_cache:
                return
            self._fail_cache.add(output)
            print 'no%smatch! %s %s'%(id_string,obs.basename,output)
            print 'Suppressing further error messages...'

class MatchCal_Diode(MatchCal):
    """ Match observations with cals via injected noise.
    
    These typically directly precede a pulsar observation.
    """

    def __init__(self,obs,match_keys,tmax):
        super(MatchCal_Diode,self).__init__(obs,match_keys)
        self.tmax = tmax

    def find_diodes(self,obs):
        """ Return the diode cals closest in time to observation, within
            tmax days of the midpoint of the observation."""

        if obs['ext:obs_mode'] != 'PSR':
            raise ValueError('Observation did not have mode PSR!')
        best_cals = [None,None]
        try:
            mask = self.match_keys(obs)
            obs_mjd = obs.get_mjd()
            if obs_mjd is None:
                # try to weed out corrupt observations
                return best_cals
            dt = self.mjds-(obs_mjd+(0.5*obs.tobs)/86400)
            dts = np.where(mask,dt,np.nan)
            # do cals before obs
            idx = np.nanargmax(np.where(dts<0,dts,-np.inf))
            if (not np.isnan(idx)) and (abs(dts[idx]) < self.tmax):
                best_cals[0] = self.obs[idx]
                self.unique_cals.add(self.obs[idx])
            # do cals after obs
            idx = np.nanargmin(np.where(dts>0,dts,+np.inf))
            if (not np.isnan(idx)) and (abs(dts[idx]) < self.tmax):
                best_cals[1] = self.obs[idx]
                self.unique_cals.add(self.obs[idx])
        except Exception as e:
            # because we allow both good and corrupted obs. through, no
            # telling in advance what may fail here -- simply catch it,
            # report it, and move on
            print 'In MatchCal_Diode caught exception %s'%e
        return best_cals

class MatchCal_Diode_PPTA(MatchCal_Diode):
    """ Match diode cals specifically for PPTA pulsars.
    
    These always directly precede the observation and are at a nearby sky
    position.  Some pulsars also have a diode observation following.
    """

    def __init__(self,obs,tmax=1.5/24):
        match_keys = ['name','centre_freq','bandwidth','channels',
            'backend','receiver','nosk']
        super(MatchCal_Diode_PPTA,self).__init__(obs,match_keys,tmax)

class MatchCal_Diode_P574(MatchCal_Diode):
    """ Match diode cals specifically for P574, or similar style, obs.

    Because this program times a large number of pulsars, a limited number
    of calibration observations are performed, and the "closest" cal to an
    observation may be separated by many degrees and up to about an hour.
    """

    def __init__(self,obs,tmax=3./24):
        match_keys = ['centre_freq','bandwidth','channels',
            'backend','receiver','nosk']
        super(MatchCal_Diode_P574,self).__init__(obs,match_keys,tmax)

class MatchCal_PCM(MatchCal):
    """ Manage a pcm-type calibration.
        
        This is a base class, so should be extended to handle more
        complicated cases, but right now just apply avpcm."""

    def __init__(self,obs):
        for o in obs:
            #print o['frequency'],' ',o['receiver'],' ',o['backend']
            if not o['OBS_MODE'] == 'PCM':
                raise ValueError('Not a PCM file!')
        match_keys = ['centre_freq','receiver','bandwidth']
        super(MatchCal_PCM,self).__init__(obs,match_keys)

    def find_cal(self,obs):
        if ((obs['receiver'] != 'MULTI') and 
            (band.Band(obs) != band.Band_40cm())):
            return None
        if obs['ext:obs_mode'] != 'PSR':
            raise ValueError('Observation did not have mode PSR!')
        mask = self.match_keys(obs)
        if not np.any(mask):
            return self._fail(obs)
        idx = np.ravel(np.argwhere(mask))

        # only one candidate
        if len(idx) == 1:
            return self.obs[idx[0]]

        # first, see if we can match backend exactly
        backend_mask = mask & self.match_key(obs,'backend')
        if backend_mask.sum() == 1:
            idx = np.ravel(np.argwhere(backend_mask))
            return self.obs[idx[0]]

        #print 'WARNING: found more than one pcm cal for %s.'%obs.fname

        matches = [self.obs[i] for i in idx]
        pref_match = 0

        # TODO _- encapsulate this backend matching aspect...

        # next, check if we're PDFB4
        if obs['backend'] == 'PDFB4':
            for imatch,match in enumerate(matches):
                # prefer PDFB3
                if match['backend'] == 'PDFB3':
                    pref_match = imatch
                    break

        elif obs['backend'] == 'PDFB2':
            for imatch,match in enumerate(matches):
                # prefer PDFB1 over WBCORR
                if match['backend'] == 'PDFB1':
                    pref_match = imatch
                    break

        """
        for imatch,match in enumerate(matches):
            if imatch==pref_match:
                print basename(match.fname),'  <--- Using'
            else:
                print basename(match.fname)
        """
        return matches[pref_match]

class MatchCal_Flux(MatchCal):
    """ Manage flux calibration.

    Achieve basic consistency (frequency, receiver), then find the
    closest fluxcal in time.
    
    For the PPTA avfluxcals, the PSRFITS header seems to give the data
    of the first observation used, so should match by finding avfluxcal
    closest in time and before obs date.
    
    If we use per-session fluxcals, then the criterion is more like diode,
    save with timescale of ~few days.
    """
        

    def __init__(self,obs):
        for o in obs:
            # NB -- avfluxcals also seem to be labelled "PCM"
            if not o['OBS_MODE'] == 'PCM':
                raise ValueError('Not a fluxcal file!')
        match_keys = ['centre_freq','receiver','bandwidth']
        super(MatchCal_Flux,self).__init__(obs,match_keys)

    def find_cal(self,obs):
        """ Match frequency, receiver, and bandwidth.

        Beyond that, allow PDFB backend interchange, and try to get a cal
        taken before the obs.
        """

        if obs['ext:obs_mode'] != 'PSR':
            return ValueError('Observation did not have mode PSR!')

        #Currently, there are no "PPTA" fluxcals for CASPSR, but some of the
        #APSR cals are (erroneously) created with CASPSR data, and will be
        #picked up but fail to match.  Thus, just automatically fail on
        #CASPSR data until this gets straightened out.
        if obs['backend'] == 'CASPSR':
            return self._fail(obs)

        mask = self.match_keys(obs)

        # try to match backend exactly but allow PDFB interchange
        backend_mask = mask & self.match_key(obs,'backend')
        if not np.any(backend_mask):
            if 'PDFB' not in obs['backend']:
                return self._fail(obs)
            for i in xrange(len(mask)):
                if mask[i]:
                    backend_mask[i] = 'PDFB' in self.obs[i]['backend']

        if not np.any(backend_mask):
            return self._fail(obs)

        mjd = obs.get_mjd()
        if mjd is None:
            return self._fail(obs)
        dt = self.mjds-(obs.get_mjd()+(0.5*obs.tobs)/86400)

        # now try to get closest cal beforehand
        m = backend_mask & (dt < 0)
        if  np.any(m):
            idx = np.argmax(np.where(m,dt,-np.inf))
        else:
            idx = np.argmin(np.where(backend_mask,dt,np.inf))
        return self.obs[idx]

class MatchCal_NoCal(MatchCal):
    """ Dummy class that always fails to find a calibrator.

    This would be used, e.g., in processing data meant to be ingested
    into a pcm solution.
    """
        
    def __init__(self,obs):
        self.obs = obs
        self.unique_cals = obs

    def find_diodes(self,obs):
        """ Find nothing."""
        return [None,None]

    def find_cal(self,obs):
        """ Find nothing."""
        return None

    def get_matched_cals(self):
        return []


class Cal(object):
    """ Encapsulate the calibrators for a given observation.
    
        The base implementation allows for one or two diode cals,
        a fluxcal, and a pcm cal.
        
    """
    def __init__(self,diode1,diode2=None,fluxcal=None,pcm=None,
        prefer_before=True,require_before=False):
        """ 
        diode1 == Observation corresponding to the primary diode cal.
        diode2 == An optional secondary diode cal.
        fluxcal == A flux calibrator.
        pcmcal == A pcm calibrator.
        outpath == Directory to which files will be written.
        prefer_before == If two diode cals found, prefer preceding one.
        require_before == Cal must precede observation to be valid.
        """
        self._valid = True
        self.fluxcal = fluxcal
        self.pcm = pcm
        self.diodes = [None,None]
        self.diode = None
        if (diode1 is None) and (require_before or self.diode2 is None):
            # no suitable diode cal
            self._valid = False
            return
        self.diodes = [diode1,diode2]
        if prefer_before:
            self.diode = diode1
        else:
            # do some magic here perhaps to break symmetry...
            # TODO
            self.diode = diode1
        if self.diode is None:
            # sanity check we shouldn't need
            self._valid = False
            return

    def valid(self):
        """ Return true if calibrators satisfy criteria."""
        return self._valid

    def get_diodes(self):
        """ Return valid Observations for diode cals."""
        return [d for d in self.diodes if d is not None]

    def get_diode_file(self):
        """ Return filename for diode cal.  Might be a scrunched version of
            the raw data.
        """
        return self.diode.fname

    def get_fluxcal_file(self):
        if self.fluxcal is not None:
            return self.fluxcal.fname

    def get_pcm_file(self):
        if self.pcm is not None:
            return self.pcm.fname

    def calibrate(self,obs):
        """ Generate the appropriate psrsh to cal the Observation."""

        psrsh = psrsh_segment()
        if not self.valid():
            psrsh.add('# No calibrator found for this observation!')
            return psrsh
        diode_file = self.get_diode_file()
        pcm_file = self.get_pcm_file()
        fluxcal_file = self.get_fluxcal_file()

        # normalize by absolute gain if APSR
        if obs['backend'] == 'APSR':
            psrsh.comment('Normalize APSR bandpass by absolute gain.')
            psrsh.add('cal gain')

        # do PCM if we have a calibrator and are at 20cm
        b = band.Band(obs)
        if ((pcm_file is not None) and 
            (b==band.Band_20cm() or b==band.Band_40cm())):
            psrsh.add('cal load hybrid %s %s'%(diode_file,pcm_file))
        else:
            psrsh.add('cal load %s'%diode_file)
        psrsh.add('cal','cal frontend')

        # fluxcal if available
        if fluxcal_file is not None:
            psrsh.add('cal load %s'%fluxcal_file,'cal flux')

        return psrsh

