import numpy as np
from numpy import fft
import pylab as pl

from bisect import bisect_left
from scipy.optimize import fmin,brent
from scipy.linalg import cholesky_banded,solve_banded,LinAlgError
from scipy.stats import norm
from collections import deque,defaultdict
import cPickle
import glob
import os

import psrchive

import band
import profiles
import util

# prevents small numerical errors from contributing non-overlapping profs
MIN_OVERLAP = 0.4

dms = {
    'J0437-4715':2.64404,
    'J0613-0200':38.77919,
    'J0711-6830':18.4066,
    'J1017-7156':94.2256,
    'J1022+1001':10.2521,
    'J1024-0719':6.48539,
    'J1045-4509':58.1662,
    'J1125-6014':52.951,
    'J1446-4701':55.8340,
    'J1545-4550':68.390,
    'J1600-3053':52.3267,
    'J1603-7202':38.0471,
    'J1643-1224':62.4097,
    'J1713+0747':15.9915,
    'J1730-2304':9.617,
    'J1732-5049':56.822,
    'J1744-1134':3.13908,
    'J1824-2452A':119.894,
    'J1832-0836':28.18,
    'J1857+0943':13.2968,
    'J1909-3744':10.3934,
    'J1939+2134':71.0398,
    'J2124-3358':4.59602,
    'J2129-5721':31.8532,
    'J2145-0750':8.9977,
    'J2241-5236':11.41085,
}
class RNM_Delay(object):
    """ Encapsulate one of the RNM-style delays between two instruments.
    """

    # delay, uncertainty, MJD0, MJD1
    rnm_info = {
        'cpsr2m_1341' : [-1720,30],
        'cpsr2n_1405' : [-1800,88],
        'pdfb1_pre_2006' : [-1130,100,53542,53736],
        'pdfb1_post_2006' : [-130,100,53736,54709],
        'pdfb2_1024_MHz' : [-5435,160,54272,55377],
        'pdfb2_256MHz_1024_ch': [-11395,66,54272,55377],
        'pdfb2_256MHz_2048_ch': [-14350,113,54272,55377],
        #'pdfb3_1024_MHz': [1030,30,54676,54947.96],
        #'pdfb3_256MHz_1024ch' : [4295,142,54676,54947.97],
        #'pdfb3_256MHz_2048ch' : [8320,240,54676,54947.97],
        #'pdfb3_1024_256_512' : [2450,35,54676,54947.97],
        'pdfb3_1024_MHz': [1030,30,54676,55319.18],
        'pdfb3_256MHz_1024ch' : [4295,142,54676,55319.18],
        'pdfb3_256MHz_2048ch' : [8320,240,54676,55319.18],
        'pdfb3_1024_256_512' : [2450,35,54676,55319.18],
        'dfb3_J0437_55319_56160': [220,0,55319.18,56160],
        'dfb3_J0437_56160_60000': [450,0,56160,60000],
        'pdfb4.*_1024_[1,2]...': [2230,188,54751.30,55319.18],
        'pdfb4_1024_1024_512' : [1680,0,54751.30,55319.18],
        'pdfb4_2048_1024_1024': [2410,188,54751.30,55319.18],
        'pdfb4_256MHz_2048ch': [9220,140,54751.30,55319.18],
        'pdfb4_256MHz_1024ch': [5050,150,54751.30,55319.18],
        'pdfb4_1024_256_512': [3230,20,54751.30,55319.18],
        'pdfb4_55319_56055_cals':[927,16,55319.18,56055],
        'pdfb4_56055_56110_cals':[382,16,56055,56110],
        'pdfb4_56110_56160_cals':[541,11,56110,56160],
        'pdfb4_56160_60000_cals':[425,43,56160,60000],
    }

    @staticmethod
    def get_valid_delays():
        delays = deque()
        keys = RNM_Delay.rnm_info.keys()
        for ikey,key in enumerate(keys):
            for key2 in keys[ikey+1:]:
                d = RNM_Delay(key,key2,None)
                if d.valid():
                    delays.append(d)
        return delays
            

    def __init__(self,f1,f2,delay):
        rnm_info = RNM_Delay.rnm_info
        self.f1 = f1
        self.f2 = f2
        if delay is None:
            delay = rnm_info[f1][0]-rnm_info[f2][0]
        self.delay = delay # in ns
        if f1==f2:
            self.delay_error = 0
        else:
            e1 = rnm_info[f1]
            e2 = rnm_info[f2]
            d = e1[0]-e2[0]
            de = (e1[1]**2+e2[1]**2)**0.5
            self.delay_error = de
            # not sure what this assert is supposed to be doing, but it's
            # causing trouble so I commented it out...
            #assert(abs(float(d)/delay-1) < 1e-6)
        if (len(rnm_info[f1]) >= 4) and (len(rnm_info[f2]) >= 4):
            self.mjd_start = max(rnm_info[f1][2],rnm_info[f2][2])
            self.mjd_stop = min(rnm_info[f1][3],rnm_info[f2][3])
        else:
            self.mjd_start = np.inf
            self.mjd_stop = -np.inf
        self.delays = []

    def valid(self):
        return self.mjd_stop > self.mjd_start

    def __hash__(self):
        return hash('%s%s'%(self.f1,self.f2))

    def __eq__(self,other):
        if other is None:
            return False
        return (self.f1 == other.f1) and (self.f2 == other.f2)

def load_singles(obs,set_dm0=False,exclude_edges=0.10,bscrunch=1,
        chan_stride=16,initial_offset=0,fscrunch=1):
    """ Return SubintProfiles representing the AA and BB coherency
    parameters for each channel of each subint.
    
    Exclude channels within exclude_edges of the band edge."""
    fname = obs.fname
    is_cal = fname.endswith('.cf')
    a = psrchive.Archive_load(fname)
    # buuuut this doesn't help with two bin because it only fixes meta
    # data, so would need to actually copy it.
    #for correction in obs.get_psrsh():
    #    if correction[0] == '#' or ('load' in correction):
    #        continue
    #    a.execute(correction.encode('ascii'))
    if bscrunch > 1:
        print 'Applying a bin scrunch of %d to %s.'%(bscrunch,obs.basename)
        a.bscrunch(bscrunch)
    #a.set_dispersion_measure(dms[obs['name']])
    #a.dedisperse()
    #if fscrunch > 1:
    #    print 'Applying a chan scrunch of %d to %s.'%(
    #        fscrunch,obs.basename)
    #    a.fscrunch(fscrunch)
    pred = a.get_predictor()
    #pred.set_observing_frequency(1e6)
    if set_dm0:
        a.set_dispersion_measure(0)
    #a.dedisperse()
    nsub = a.get_nsubint()
    nchan = a.get_nchan()
    nbin = a.get_nbin()
    chan_bw = float(abs(a.get_bandwidth()))/nchan

    edge = int(round(exclude_edges*nchan))
    allowed_chans = np.arange(nchan)
    if edge > 0:
        allowed_chans = allowed_chans[edge:-edge]

    # exclude some channels for speed
    allowed_chans = allowed_chans[::chan_stride]

    AA_subints = deque()
    BB_subints = deque()

    if nsub < 8:
        stride = 1
    elif nsub < 32:
        stride = 2
    else:
        stride = 4
    # ACHTUNG! V
    stride = 8

    for i in xrange(0,nsub,stride):
        sub = a[i]
        if sub.get_duration() == 0:
            # this happens in some integrations -- yikes
            continue
        AA_profiles = deque()
        BB_profiles = deque()
        for j in allowed_chans:
            AA = sub.get_Profile(0,int(j))
            AA_amps = AA.get_amps().copy()
            BB = sub.get_Profile(1,int(j))
            BB_amps = BB.get_amps().copy()
            if (AA_amps[0] == 0) or (BB_amps[0] == 0):
                continue
            AA_profiles.append(
                SingleProfile(AA_amps,AA.get_centre_frequency(),chan_bw,
                    is_cal=is_cal))
            BB_profiles.append(
                SingleProfile(BB_amps,BB.get_centre_frequency(),chan_bw,
                    is_cal=is_cal))
        if len(AA_profiles) == 0:
            print 'I do not yet know why this would happen, but found 0 profiles for this subint.'
            continue
        epoch = sub.get_epoch()
        tobs = sub.get_duration()
        AA_subints.append(
            SubintProfiles(AA_profiles,epoch,tobs,pred,pol=0,
                initial_offset=initial_offset))
        BB_subints.append(
            SubintProfiles(BB_profiles,epoch,tobs,pred,pol=1,
                initial_offset=initial_offset))

    return AA_subints,BB_subints

def check_nbin(fname1,fname2):
    """ Return bscrunch factors for reducing two obs. to identical nbin.
    """
    a1 = psrchive.Archive_load(fname1)
    a2 = psrchive.Archive_load(fname2)
    n1 = a1.get_nbin()
    n2 = a2.get_nbin()
    if n1 > n2:
        if (n1%n2 > 0):
            return None,None
        return n1/n2,1
    elif n2 > n1:
        if (n2%n1 > 0):
            return None,None
        return 1,n2/n1
    return 1,1

def check_nchan(fname1,fname2):
    """ Return fscrunch factors for reducing two obs. to identical
    channel bandwidths.
    """
    a1 = psrchive.Archive_load(fname1)
    a2 = psrchive.Archive_load(fname2)
    n1 = float(a1.get_nchan())/abs(a1.get_bandwidth())
    n2 = float(a2.get_nchan())/abs(a2.get_bandwidth())
    if n1 > n2:
        if (n1%n2 > 0):
            return None,None
        return int(n1/n2),1
    elif n2 > n1:
        if (n2%n1 > 0):
            return None,None
        return 1,int(n2/n1)
    return 1,1

class SingleProfile(object):
    """ Encapsulate the profile for a single channel."""

    def __init__(self,profile,freq,bw,is_cal=False):
        """ profile is the "raw" profile data -- not baseline subtracted
        """
        self.profile = profile
        self.nbin = len(profile)
        self.bw = bw
        self.f0 = freq - 0.5*bw
        self.f1 = freq + 0.5*bw
        self.is_cal = is_cal

    def get_err(self):
        raise NotImplementedError('too primitive?')
        return profiles.std_mad(self.profile)

    def __len__(self):
        return self.nbin
    
    def __cmp__(self,other):
        """ Arrange by starting frequency."""
        #return self.profile_info.__cmp__(other.profile_info)
        if self.f0 < other.f0:
            return -1
        if self.f0 > other.f0:
            return 1
        return 0

    def overlap(self,other):
        #return self.profile_info.overlap(other.profile_info)
        fmax = min(self.f1,other.f1)
        fmin = max(self.f0,other.f0)
        df = fmax-fmin
        if df < 0:
            return 0
        return df/max(self.bw,other.bw)

class SubintProfiles(object):
    """ Encapsulate the SingleProfile for each channel of a subint."""

    def __init__(self,sps,epoch,tobs,predictor,pol=0,initial_offset=0):
        self.sps = sorted(sps)
        self.nbin = sps[0].nbin
        self.freqs = np.asarray([x.f0 for x in sps])
        self.epoch = epoch
        self.tobs = tobs
        self.t0 = self.epoch.in_seconds() - 0.5*tobs
        self.t1 = self.t0 + tobs
        self.predictor = predictor
        self.period = 1./predictor.frequency(self.epoch)
        self.pol = pol
        self.initial_offset = initial_offset

    def overlap(self,other):
        tmax = min(self.t1,other.t1)
        tmin = max(self.t0,other.t0)
        dt = tmax - tmin
        if dt < 0:
            return 0
        return dt/max(self.tobs,other.tobs)

    def get_logl(self,other,use_new=True):
        """ Return an iterator over overlapping profiles.

        Perform sanity check requiring at least MIN_OVERLAP fractional
        overlap between profiles.  This is e.g. to exclude subints that
        only overlap for fractions of a pulsar period, as happens
        commonly.
        
        sp -- another SubintProfiles object
        """
        toverlap = self.overlap(other)
        if toverlap < MIN_OVERLAP:
            return
        sps1 = self.sps
        sps2 = other.sps
        offset = self.get_offset(other)
        if sps2[0].f0 > sps1[0].f0:
            sps1,sps2 = sps2,sps1
            offset = -offset

        # start with inner loop at lower frequencies, and advance
        plls = deque()
        idx2 = 0
        for sp1 in sps1:
            for sp2 in sps2[idx2:]:
                if sp2.f1 <= sp1.f0:
                    idx2 += 1
                    continue
                if sp2.f0 >= sp1.f1:
                    break
                foverlap = sp1.overlap(sp2)
                if foverlap > 0:
                    total_overlap = foverlap*toverlap
                    if total_overlap > MIN_OVERLAP:
                        if not use_new:
                            plls.append(PairwiseLogLikelihood(sp1,sp2,total_overlap,delta_phi=offset))
                        else:
                            plls.append(PairwiseLogLikelihood2(sp1,sp2,total_overlap,delta_phi=offset))

        if use_new:
            pll = SubintPairwiseLogLikelihood2(self,other,plls)
        else:
            pll = SubintPairwiseLogLikelihood(self,other,plls)
        return pll

    def get_offset(self,other):
        """ Compute the phase difference between the start time of this
        subintegration and of the other  It includes any overall offset
        (e.g. the two-bin correction) as well as the phase difference
        from the local start time difference.

        NB -- later DFBs start simultaneously, while earlier instruments
        start some microseconds apart.  (+/- N correlator cycles as well)
        """

        # evalulate phase difference at center of overlap
        pred1 = self.predictor
        pred2 = other.predictor
        dphi1 = (pred1.phase(other.epoch)-pred1.phase(self.epoch))
        dphi2 = (pred2.phase(other.epoch)-pred2.phase(self.epoch))
        discrepancy = (dphi1-dphi2).fracturns()
        if discrepancy > 0.5:
            discrepancy -= 1
        if discrepancy < -0.5:
            discrepancy += 1
        # this is about ns precision -- would like better, but some DFB1
        # DFB2 predictions are out at this level
        if abs(discrepancy) > 2e-6:
            raise ValueError('predictors do not match! %s'%discrepancy)
        dphi = dphi1.fracturns() + self.initial_offset
        #print 'start time offset phase: ',dphi1.fracturns()
        if dphi > 0.5:
            return dphi - 1
        if dphi < -0.5:
            return dphi + 1
        return dphi

class PairwiseLogLikelihood(object):
    """ Given two partially or completely overlapping profiles, calculate
    the log likelihood as a function of overlap.
    """

    def __init__(self,sp1,sp2,overlap,delta_phi=0):
        """ sp1/sp2 == SingleProfile objects """
        self.sp1 = sp1
        self.sp2 = sp2
        self.nbin = len(self.sp1.profile)
        assert(self.sp1.nbin==self.sp2.nbin)
        self._overlap = overlap
        self._delta_phi = delta_phi

        # estimate baselines and standard deviations of the profiles
        # y1/y2 represent the scaled residuals
        # TODO -- why aren't these baseline subtracted?
        # TODO -- should probably verify that the scaled residuals are
        # individually white in the profiles
        if not sp1.is_cal:
            self.b1,self.err1 = profiles.iterative_baseline_noerr(
                sp1.profile,get_dev=True)
            self.b2,self.err2 = profiles.iterative_baseline_noerr(
                sp2.profile,get_dev=True)
            #y1 = sp1.profile/self.err1
            #y2 = sp2.profile/self.err2
            y1 = (sp1.profile-self.b1)/self.err1
            y2 = (sp2.profile-self.b2)/self.err2
        else:
            y1,self.b1,b_on,self.err1,s_on = profiles.cal_baseline(
                sp1.profile,get_residuals=True)
            y2,self.b2,b_on,self.err2,s_on = profiles.cal_baseline(
                sp2.profile,get_residuals=True)

        # FFT and discard the mean term
        fft1 = fft.fft(y1)[1:]
        fft2 = fft.fft(y2)[1:]
        self._shift_arg = fft.fftfreq(self.nbin)[1:]*(2j*np.pi*self.nbin)
        self._fft_cross = (fft1.conj()*fft2).astype(np.complex64)

        self._X1_2 = np.sum(fft1.real**2) + np.sum(fft1.imag**2)
        self._X2_2 = np.sum(fft2.real**2) + np.sum(fft2.imag**2)

        self.set_profile_rho(profile_rho=True)

    def get_var(self,delta):
        """ Return the std of the profiles with the second profile
        shift by amount delta.  Quite slow as implemented."""
        return np.std(self.y1-profiles.rotated(self.y2,delta))**2

    def set_profile_rho(self,profile_rho=True):
        """ Set likelihood function: profile rho or fit it."""
        if profile_rho:
            self.loglikelihood = self._loglikelihood_profile_rho
        else:
            self.loglikelihood = self._loglikelihood_fit_rho
        self._profile_rho = profile_rho

    def _get_rho_eff(self,X12):
        """ Solve cubic equation for rho which maximizes likelihood.
        
        In all cases, the root we want is the one associated with the real
        cube root of unity.  See Wikipedia for rough notation guide."""
        N = self.nbin
        # original version
        """
        a = 1; b = -X12/N**2; c = (self._X1_2+self._X2_2)/N**2-1; d = b
        D0 = b**2-3*a*c
        D1 = 2*b**3-9*a*b*c+27*a**2*d
        t = D1**2-4*D0**3
        D = t/(-27*a**2) # discriminant
        if D >= 0:
            return -1
        C = (0.5*(D1 + t**0.5))**(1./3)
        rho_eff = (-1./3)*(b+C+D0/C)
        """
        # optimized version with a=1,b=d implemented
        b = -X12/N**2; c = (self._X1_2+self._X2_2)/N**2-1
        D0 = b**2-3*c
        D1 = 2*b**3-9*b*c+27*b
        t = D1**2-4*D0**3
        # discriminant proportional to t
        if t < 0:
            # three real roots case -- return root associated with the
            # single real third root of unity
            C = (0.5*(D1 + complex(t)**0.5))**(1./3)
            r1 = (-1./3)*(b+C+D0/C)
            return abs(r1.real)
        # one real, two complex roots -- return real root

        C = (0.5*(D1 + t**0.5))**(1./3)
        return abs((-1./3)*(b+C+D0/C))

    def _loglikelihood_profile_rho(self,p,phase_term=None):
        if phase_term is None:
            phase_term = np.exp(self._shift_arg*(p[0]-self._delta_phi))
            #phase_term = np.exp(self._shift_arg*(p[0]+0))
        # factor of 0.5 from summetry
        X12 = self.X12 = np.sum(np.real(self._fft_cross*phase_term))

        # compute rho from solution to profile likelihood = 0
        #rho_eff = self.rho_eff = self._get_rho_eff(X12)*self._overlap**0.5
        o = self._overlap
        fac = (o**2/(o**2+(1-o)**2))**0.5
        rho_eff = self.rho_eff = self._get_rho_eff(X12)*fac

        v = 1.-rho_eff**2
        z = (self._X1_2 - 2.*rho_eff*X12 + self._X2_2)*(1./self.nbin)
        return 0.5*(z/v + self.nbin*np.log(v))

    def _loglikelihood_fit_rho(self,p,phase_term=None):
        dphi,rho = p
        #rho_eff = rho*self._overlap**0.5
        o = self._overlap
        fac = (o**2/(o**2+(1-o)**2))**0.5
        rho_eff = rho*fac

        if phase_term is None:
            phase_term = np.exp(self._shift_arg*(dphi-self._delta_phi))
        # factor of 0.5 to correct for double counting
        X12 = self.X12 = np.sum(np.real(self._fft_cross*phase_term))

        v = 1.-rho_eff**2
        z = (self._X1_2 - 2.*rho_eff*X12 + self._X2_2)*(1./self.nbin)
        return 0.5*(z/v + self.nbin*np.log(v))

    def _loglikelihood_fix_rho(self,p,rho=1,phase_term=None):
        dphi = p[0]
        #rho_eff = rho*self._overlap**0.5
        o = self._overlap
        fac = (o**2/(o**2+(1-o)**2))**0.5
        rho_eff = rho*fac

        if phase_term is None:
            phase_term = np.exp(self._shift_arg*(dphi-self._delta_phi))
        # factor of 0.5 to correct for double counting
        X12 = self.X12 = np.sum(np.real(self._fft_cross*phase_term))

        v = 1.-rho_eff**2
        z = (self._X1_2 - 2.*rho_eff*X12 + self._X2_2)*(1./self.nbin)
        return 0.5*(z/v + self.nbin*np.log(v))

    def chi2(self,p,phase_term=None,profile_rho=True):
        if profile_rho:
            self._loglikelihood_profile_rho([p[0]],phase_term=phase_term)
            rho_eff = self.rho_eff
            X12 = self.X12
        else:
            self._loglikelihood_fit_rho(p,phase_term=phase_term)
            dphi,rho = p
            #rho_eff = rho*self._overlap**0.5
            o = self._overlap
            fac = (o**2/(o**2+(1-o)**2))
            rho_eff = rho*fac
            X12 = self.X12
        z = (self._X1_2 - 2.*rho_eff*X12 + self._X2_2)*(1./self.nbin)
        v = 1.-rho_eff**2
        return 0.5*(z/v)

    def fit(self,x0=0,rho0=0.80,profile_rho=True,fix_rho=False,
        get_err=True):
        """ Quick and dirty fit..."""
        self.set_profile_rho(profile_rho)
        if fix_rho:
            print 'blah'
            self.loglikelihood = self._loglikelihood_fix_rho
            self.profile_rho = profile_rho = False
        logl = self.loglikelihood
        # quick brute force search to allow for shifts of a few bins
        dom = np.linspace(-0.01,0.01,21) + x0
        if profile_rho:
            cod = [logl([x]) for x in dom]
            x0 = dom[np.argmin(cod)]
            p0 = fmin(logl,[x0],disp=0,xtol=1e-8)
            corr = self.rho_eff/self._overlap
        elif fix_rho:
            cod = [logl([x],rho0) for x in dom]
            x0 = dom[np.argmin(cod)]
            p0 = fmin(logl,[x0],args=(rho0,None),disp=0,xtol=1e-8)
            corr = rho0/self._overlap
        else:
            cod = [logl([x,rho0]) for x in dom]
            x0 = dom[np.argmin(cod)]
            p0 = fmin(logl,[x0,rho0],disp=0)
        if not get_err:
            return p0,0
        dom = p0[0] + np.linspace(-1e-5,1e-5,3)
        if profile_rho:
            cod = [logl((x,)) for x in dom]
        elif fix_rho:
            cod = [logl((x,),rho0) for x in dom]
        else:
            cod = [logl((x,p0[1])) for x in dom]
        err = (0.5*np.polyfit(dom,cod,2)[0])**-0.5
        if profile_rho:
            p0 = np.asarray([p0[0],corr])
        return p0,err

    def dof(self):
        return len(self.sp1)

class PairwiseLogLikelihood2(object):
    """ Given two partially or completely overlapping profiles, calculate
    the log likelihood as a function of overlap.

    This version correctly handles the profile as a block diagonal matrix,
    under the assumption that the profiles have been brought to within one
    bin of alignment.

    Here, the degree of offset actually sets the overall noise budget, and
    we don't actually need to shift the profile (except perhaps for a
    rotation by one or more bins.
    """

    def __init__(self,sp1,sp2,overlap,delta_phi=0):
        """ sp1/sp2 == SingleProfile objects """
        self.sp1 = sp1
        self.sp2 = sp2
        self.nbin = len(self.sp1.profile)
        assert(self.sp1.nbin==self.sp2.nbin)
        self._overlap = o = overlap
        self._overlap_fac = o**2/(o**2+(1-o)**2)
        #self._overlap_fac = o/(o**2+(1-o)**2)**0.5
        self._delta_phi = delta_phi

        if not sp1.is_cal:
            self.b1,self.err1 = profiles.iterative_baseline_noerr(
                sp1.profile,get_dev=True)
            self.b2,self.err2 = profiles.iterative_baseline_noerr(
                sp2.profile,get_dev=True)
            #y1 = sp1.profile/self.err1
            #y2 = sp2.profile/self.err2
            y1 = (sp1.profile-self.b1)/self.err1
            y2 = (sp2.profile-self.b2)/self.err2
        else:
            y1,self.b1,b_on,self.err1,s_on = profiles.cal_baseline(
                sp1.profile,get_residuals=True)
            y2,self.b2,b_on,self.err2,s_on = profiles.cal_baseline(
                sp2.profile,get_residuals=True)

        self.y1 = y1
        self.y2 = y2

    def _loglikelihood(self,p):
        """ Construct the covariance matrix ignoring boundary conditions by
        truncating the final entries."""
        a,b = self.y1,self.y2
        n = len(a)
        if len(p) == 2:
            d,rho = p
            s1,s2,m1,m2 = 1.,1.,0.,0.
        else:
            d,rho,s1,s2,m1,m2 = p
        #if rho >= 1:
            #return np.inf
        if rho < 0:
            return np.inf

        # zero the relative phase offset
        d -= self._delta_phi

        # first, roll the input as necessary
        roll_idx = round(d*n)
        if roll_idx != 0:
            b = np.roll(b,-int(roll_idx)).copy()
        d -= roll_idx/n
        if d < 0:
            a,b = b,a
            d = -d

        # subint overlap
        rho_eff = rho*self._overlap_fac
        if rho_eff >= 1:
            return np.inf

        # bin overlap
        overlap = (1-d*n) # fraction of phase bin; 1 for complete alignment
        rho_11 = overlap*rho_eff
        rho_12 = (1-overlap)*rho_eff

        # form a banded matrix (lower format)
        t = np.empty((2,2*(n-1)))
        t[0,::2] = s1
        t[0,1::2] = s2
        s = (s1*s2)**0.5
        t[1,0:-1:2] = rho_11*s
        t[1,1:-1:2] = rho_12*s
        
        # form likelihood
        v = np.empty(2*(n-1))
        v[::2] = b[:-1]-m2
        v[1::2] = a[:-1]-m1

        try:
            #Lt = cholesky_banded(t,check_finite=False,lower=True)
            Lt = cholesky_banded(t,lower=True)
            det = np.sum(np.log(Lt[0]))
            #q = solve_banded((1,0),Lt,v,check_finite=False,overwrite_b=True)
            q = solve_banded((1,0),Lt,v,overwrite_b=True)
        except LinAlgError:
            return np.inf

        # NB the factors of 2 cancel from the L = sqrt(C) and the sqrt(det(C))
        return det + 0.5*np.inner(q,q)

    def _profile_loglikelihood(self,p0,delta):

        f = lambda p,d: self._loglikelihood([d]+list(p))
        return fmin(f,p0,args=(delta,),disp=0,full_output=1)[1]

    def loglikelihood(self,p,profile=False):
        """ Generalized interface."""
        if len(p) == 1:
            return self._profile_loglikelihood([0.8],p[0])
        if profile:
            return self._profile_loglikelihood(p[1:],p[0])
        return self._loglikelihood(p)

    def fit(self,x0=0,rho0=0.80,get_err=True,profile_rho=False,
            fit_nuisance=False,profile_error=False,do_coarse=False,
            focus=True,no_grid=True):
        """ Quick and dirty fit..."""
        if profile_rho:
            raise NotImplementedError('Does not support profiling rho.')
        logl = self.loglikelihood
        # the likelihood has a comb-like structure around a phase shift
        # giving an integral number of bins, so a good structure is to
        # roughly put test points on bin changes (if shift is integral)
        # and between
        n = len(self.y1)

        if not no_grid:
            if do_coarse:
                # round 1
                idx = round(x0*n)
                didx = 8 # total phase bins
                pidx = 32 # points per phase bin
                idx_min = idx - didx/2
                idx_max = idx + didx/2
                dom1 = np.append(np.linspace(idx_min,idx_max,pidx*didx+1)/n,x0)
                cod1 = [logl([x,rho0]) for x in dom1]
                x0 = dom1[np.argmin(cod1)]

            if not focus:
                # round 2
                idx = round(x0*n)
                didx = 2 # total phase bins
                pidx = 32 # points per phase bin
                idx_min = idx - didx/2
                idx_max = idx + didx/2
                dom = np.append(np.linspace(idx_min,idx_max,pidx*didx+1)/n,x0)
                cod = [logl([x,rho0]) for x in dom]
                x0 = dom[np.argmin(cod)]
            else:
                dom = x0 + np.linspace(-0.5,0.5,51)/n
                cod = [logl([x,rho0]) for x in dom]
                x0 = dom[np.argmin(cod)]

        if fit_nuisance:
            p0,logl0 = fmin(logl,[x0,rho0,1.,1.,0.,0.],disp=0,full_output=1)[:2]
        else:
            p0,logl0 = fmin(logl,[x0,rho0],disp=0,full_output=1)[:2]
        if not get_err:
            return p0,0
        delta = 1e-4
        dom = p0[0] + np.linspace(-delta,delta,3)
        cod = [logl([p0[0]-delta]+list(p0[1:]),profile=profile_error),logl0,logl([p0[0]+delta]+list(p0[1:]),profile=profile_error)]
        err = (2*np.polyfit(dom,cod,2)[0])**-0.5
        return p0,err

class SubintPairwiseLogLikelihood(object):
    """ Group PairwiseLogLikelihoods for two overlapping subints."""

    def __init__(self,sps1,sps2,plls):
        """ sps1/sps2 = SubintProfiles, plls the corresponding likelihoods
        for the individual (channel) data.
        """
        self.sps1 = sps1
        self.sps2 = sps2
        self.plls = plls
        self.nbin = sps1.nbin
        self._shift_arg = np.fft.fftfreq(self.nbin)[1:]*(2j*np.pi*self.nbin)
        self._shift_arg = self._shift_arg.astype(np.complex64)

        # compute delay between two subints
        self._delta_phi = sps1.get_offset(sps2)
        self._profile_rho = True

    def __len__(self):
        return len(self.plls)

    def get_var(self,delta):
        return sum((x.get_var(delta) for x in self.plls))

    def set_profile_rho(self,profile_rho=True):
        if self.plls[0]._profile_rho==profile_rho:
            return
        for pll in self.plls:
            pll.set_profile_rho(profile_rho)

    def loglikelihood(self,p,use_new=False):
        if not use_new:
            phase_term = np.exp(self._shift_arg*(p[0]-self._delta_phi))
            logl = sum((pll.loglikelihood(p,phase_term=phase_term) for pll in self.plls))
        else:
            logl = sum((pll.loglikelihood(p) for pll in self.plls))
        return logl

    def chi2(self,p,profile_rho=True):
        phase_term = np.exp(self._shift_arg*(p[0]-self._delta_phi))
        chi2 = sum((pll.chi2(p,phase_term=phase_term,profile_rho=profile_rho) for pll in self.plls))
        return chi2

    def dof(self):
        return sum((pll.dof() for pll in self.plls))

    def fit(self,x0=0,rho0=0.80,profile_rho=True):
        """ Quick and dirty fit..."""
        self.set_profile_rho(profile_rho)
        if profile_rho:
            p0 = fmin(self.loglikelihood,[x0],disp=0)
        else:
            p0 = fmin(self.loglikelihood,[x0,rho0],disp=0)
        dom = p0[0] + np.linspace(-1e-5,1e-5,3)
        if profile_rho:
            cod = [self.loglikelihood([x]) for x in dom]
        else:
            cod = [self.loglikelihood((x,p0[1])) for x in dom]
        err = (0.5*np.polyfit(dom,cod,2)[0])**-0.5
        if profile_rho:
            p0 = np.asarray([p0[0],-1])
        return p0,err

    def target_fit(self,guess,get_err=True):
        logl = lambda p: self.loglikelihood(p,use_new=True)
        p0 = fmin(logl,[guess],disp=0)
        if not get_err:
            return p0
        dom = p0[0] + np.linspace(-1e-7,1e-7,3)
        cod = [logl([x]) for x in dom]
        err = (2*np.polyfit(dom,cod,2)[0])**-0.5
        return p0,err

class SubintPairwiseLogLikelihood2(object):
    """ Group PairwiseLogLikelihoods for two overlapping subints."""

    def __init__(self,sps1,sps2,plls):
        """ sps1/sps2 = SubintProfiles, plls the corresponding likelihoods
        for the individual (channel) data.
        """
        self.sps1 = sps1
        self.sps2 = sps2
        self.plls = plls
        self.nbin = sps1.nbin
        # compute delay between two subints
        self._delta_phi = sps1.get_offset(sps2)

    def typical_overlap(self):
        return np.median([pll._overlap for pll in self.plls])

    def __len__(self):
        return len(self.plls)

    def loglikelihood(self,p,profile=False):
        logl = sum((pll.loglikelihood(p,profile=profile) for pll in self.plls))
        return logl

    def dof(self):
        return sum((pll.dof() for pll in self.plls))

    def fit(self,x0=0,get_err=True):
        """ Perform a coherent fit with profile likelihood."""
        logl = lambda p: self.loglikelihood(p,profile=True)
        p0 = fmin(logl,[x0],disp=0)
        if not get_err:
            return p0
        dom = p0[0] + np.linspace(-5e-6,5e-6,3)
        cod = [logl([x]) for x in dom]
        err = (2*np.polyfit(dom,cod,2)[0])**-0.5
        return p0,err

class ObservationPairwiseLogLikelihood(object):
    """ Manage a set of PLLs, e.g. as from the subints and channels of
    two observations.
    """

    def __init__(self,sps1,sps2,use_new=True):
        """ sps1/sps2 = deques of SubintProfiles """
        plls = deque()
        # build a collection of SubintPairwiseLogLikelihoods
        for sp1 in sps1:
            for sp2 in sps2:
                pll = sp1.get_logl(sp2,use_new=use_new)
                if pll is not None:
                    plls.append(pll)
        self.plls = plls
        self.epochs = [pll.sps1.epoch for pll in self.plls]
        self.typical_overlaps = [pll.typical_overlap() for pll in self.plls]
        self.use_new = use_new

    def get_var(self,delta):
        return sum((x.get_var(delta) for x in self.plls))

    def loglikelihood(self,p,profile=True):
        logl = sum((pll.loglikelihood(p,profile=profile) for pll in self.plls))
        return logl

    def chi2(self,p,profile_rho=True):
        chi2 = sum((pll.chi2(p,profile_rho=profile_rho) for pll in self.plls))
        return chi2

    def dof(self):
        return sum((pll.dof() for pll in self.plls))

    def set_profile_rho(self,profile_rho=True):
        for pll in self.plls:
            pll.set_profile_rho(profile_rho)

    def __len__(self):
        return len(self.plls)

    def fit(self,x0=0,rho0=0.80,profile_rho=True):
        """ Quick and dirty fit..."""
        self.set_profile_rho(profile_rho)
        # quick brute force search to allow for shifts of a few bins
        logl = self.loglikelihood
        dom = np.linspace(-0.01,0.01,21) + x0
        if profile_rho:
            cod = [logl([x]) for x in dom]
            x0 = dom[np.argmin(cod)]
            p0 = fmin(logl,[x0],disp=0,xtol=1e-8)
        else:
            cod = [logl([x,rho0]) for x in dom]
            x0 = dom[np.argmin(cod)]
            p0 = fmin(logl,[x0,rho0],disp=0)
        dom = p0[0] + np.linspace(-1e-5,1e-5,3)
        if profile_rho:
            cod = [self.loglikelihood([x]) for x in dom]
        else:
            cod = [self.loglikelihood((x,p0[1])) for x in dom]
        err = (0.5*np.polyfit(dom,cod,2)[0])**-0.5
        if profile_rho:
            p0 = np.asarray([p0[0],-1])
        return p0,err

    def get_individual_fits(self,x0=0.,rho0=0.80,profile_rho=True):
        plls = self.plls
        ntote = sum((len(pll) for pll in plls))
        offsets = np.empty(ntote)
        errs = np.empty_like(offsets)
        corrs = np.empty_like(offsets)
        f1s = np.empty_like(offsets)
        t1s = np.empty_like(offsets)
        delta_phis = np.empty_like(offsets)
        overlaps = np.empty_like(offsets)
        indices = np.empty(len(plls))
        counter = 0
        x0_arg = x0
        for ipll,pll in enumerate(self.plls):
            for inner_pll in pll.plls:
                x0,err = inner_pll.fit(x0=x0_arg,rho0=rho0,
                    profile_rho=profile_rho)
                offsets[counter] = x0[0]
                corrs[counter] = x0[1]
                errs[counter] = err
                f1s[counter] = inner_pll.sp1.f0
                t1s[counter] = pll.sps1.t0
                delta_phis[counter] = inner_pll._delta_phi
                overlaps[counter] = inner_pll._overlap
                counter += 1
            indices[ipll] = counter

        # TODO ???
        # need to figure out the factor of 2!
        #errs *= 2**-0.5
        em2 = errs**-2
        mask = ~np.isnan(errs)

        # sanity check on correlation
        mask &= np.abs(corrs) < 2

        # iterate on "pulls"
        if mask.sum() > 1:
            mean = np.average(offsets[mask],weights=em2[mask])
        else:
            mean = np.average(offsets)
        pulls = (offsets-mean)/errs

        # compute mean with outiers removed
        max_pull = norm.isf(0.1/ntote)
        mask &= (np.abs(pulls) < max_pull)
        if mask.sum() > 1:
            mean = np.average(offsets[mask],weights=em2[mask])
            err = em2[mask].sum()**-0.5
        else:
            mean = np.average(offsets)
            err = np.inf

        print 'Masking out %d outliers of %d samples.'%(
            (~mask).sum(),len(mask))

        return f1s,t1s,offsets,errs,pulls,corrs,delta_phis,overlaps,mean,err,mask#,indices

    def subint_target_fit(self,guess):
        rvals = np.empty((len(self.plls),2))
        for i in xrange(len(self.plls)):
            rvals[i] = self.plls[i].fit(x0=guess)
            guess = np.average(rvals[:i+1,0],weights=rvals[:i+1,1]**-2)
        return rvals

class Delay(object):
    """ Encapsulate the offset between two Observation instances.
    
    Record information about the two observations used, as well as 
    compute the delay and its uncertainty.

    This implementation is based on the assumption that high resolution
    profiles will be compared, viz. subints and original channels.  Thus,
    there should be few worries about profile evolution and DM delays,
    and the only free parameters are the overall correlation and the
    overall shift between the two Observations.

    NB -- since this operates on "original" data, it is CRUCIAL to apply
    relevant corrections, e.g. the two-bin delay, correct metadata, etc.
    The assumption is that this has been done via the Observation objects,
    and when the profiles are loaded, we run the psrsh script that the
    Observation objects provides.

    """
    def __init__(self,obs1,obs2,dofit=True,profile_rho=True,use_new=True):

        assert(obs1.basename != obs2.basename)
        if use_new:
            profile_rho = False

        # put backends in preferred order
        self.obs1,self.obs2 = self._preferred_order(obs1,obs2)
        b1,b2 = check_nbin(self.obs1.fname,self.obs2.fname)
        if b1 is None:
            raise ValueError('Bins are not commensurate.') 
        f1,f2 = check_nchan(self.obs1.fname,self.obs2.fname)

        # these are simply deques of SubintProfiles
        initial_offset = self._two_bin_correction()
        initial_offset = 0.
        initial_offset = -self._two_bin_correction()
        print 'initial offset: ',initial_offset
        self.AA1,self.BB1 = load_singles(self.obs1,bscrunch=b1,
            initial_offset=initial_offset,fscrunch=f1)
        self.AA2,self.BB2 = load_singles(self.obs2,bscrunch=b2,
            initial_offset=initial_offset,fscrunch=f2)

        # log likelihoods for two feeds
        self.pll_AA = ObservationPairwiseLogLikelihood(self.AA1,self.AA2,
            use_new=use_new)
        self.pll_BB = ObservationPairwiseLogLikelihood(self.BB1,self.BB2,
            use_new=use_new)

        self.folding_period = self.AA1[0].period

        # try to get initial estimate from previous measurements
        rnm_delay = self.get_rnm_delay()
        if rnm_delay is not None:
            delay = rnm_delay.delay*1e-9
            guess = delay/self.folding_period
        else:
            guess = 0
        self.guess = guess
        print 'Using guess = ',guess

        if not dofit:
            return

        # TODO -- get intrinsic offsets?
        self.subint_target_fit()
        AA = self.AA_stuff = self.pll_AA.get_individual_fits(
            x0=guess,profile_rho=profile_rho)
        BB = self.BB_stuff = self.pll_BB.get_individual_fits(
            x0=guess,profile_rho=profile_rho)
        self.AA_params = [AA[-3],np.average(AA[-4][AA[-1]])]
        self.BB_params = [BB[-3],np.average(BB[-4][BB[-1]])]
        self.AA_err = AA[-2]
        self.BB_err = BB[-2]
        self.AA_chi2 = self.BB_chi2 = 0
        self.AA_dof = self.BB_dof = 1
        #self.tfit = self.target_fit()

    def _two_bin_correction(self):
        """ Compute phase offset due to two-bin problem."""
        obs1,obs2 = self.obs1,self.obs2
        obs1_mjd = obs1.get_mjd()
        obs2_mjd = obs1.get_mjd()
        phase_offset = 0

        if ((obs1['backend'] == 'PDFB3') and 
            (obs1_mjd>54676) and (obs1_mjd<54947.96)):
            phase_offset += 2./obs1['nbin']
        elif ((obs1['backend'] == 'PDFB4') and 
            (obs1_mjd>54751.30) and (obs1_mjd<55319.18)):
            phase_offset += 2./obs1['nbin']

        if ((obs2['backend'] == 'PDFB3') and 
            (obs2_mjd>54676) and (obs2_mjd<54947.96)):
            phase_offset -= 2./obs2['nbin']
        elif ((obs2['backend'] == 'PDFB4') and 
            (obs2_mjd>54751.30) and (obs2_mjd<55319.18)):
            phase_offset -= 2./obs2['nbin']

        return phase_offset

    def _preferred_order(self,obs1,obs2,prefs=['PDFB3','CPSR2m']):
        be1 = obs1['backend']
        be2 = obs2['backend']
        for pref in prefs:
            if be1 == pref:
                return obs1,obs2
            if be2 == pref:
                return obs2,obs1
        return obs1,obs2

    def get_correlation(self):
        return 0.5*(self.AA_params[1]+self.BB_params[1])

    def get_phase_offset(self,use_individual_fits=True,use_tfit=False,
        min_overlap=0):
        """ Return delay in terms of phase."""
        if use_individual_fits:
            AAdelay = self.AA_params[0]
            BBdelay = self.BB_params[0]
            AAerror = self.AA_err
            BBerror = self.BB_err
        else:
            AAdelay = self.AA_fit_params[0]
            BBdelay = self.BB_fit_params[0]
            AAerror = self.AA_fit_err
            BBerror = self.BB_fit_err
        if use_tfit:
            mask = np.asarray(self.tfit_AA_overlaps) > min_overlap
            if not np.any(mask):
                return np.nan,np.nan
            aa = self.tfit_AA[mask]
            bb = self.tfit_BB[mask]
            AAdelay = np.average(aa[:,0],weights=aa[:,1]**-2)
            BBdelay = np.average(bb[:,0],weights=bb[:,1]**-2)
            AAerror = np.sum(aa[:,1]**-2)**-0.5
            BBerror = np.sum(bb[:,1]**-2)**-0.5
        weight = (1./AAerror**2 + 1./BBerror**2)
        delay = (AAdelay/AAerror**2 + BBdelay/BBerror**2) / weight
        try:
            error = weight**-0.5
        except ZeroDivisionError:
            return -1,-1
        return delay,error

    def get_offset(self,use_tfit=False,min_overlap=-0):
        """ Return delay, averaged over polarizations."""
        delay,error = self.get_phase_offset(use_tfit=use_tfit,min_overlap=min_overlap)
        return delay*self.folding_period,error*self.folding_period

    def get_chi2(self,divide=False):
        chi2,dof = self.AA_chi2 + self.BB_chi2,self.AA_dof + self.BB_dof
        if divide:
            return chi2/dof
        return chi2,dof

    def get_configs(self):
        return self.obs1['be:config'],self.obs2['be:config']

    def get_rnm_delay(self):
        """ If the observations have been processed with delay flags,
        determine which pairwise difference this Delay corresponds to.
        """

        df1,df2 = self.obs1.delay_flags,self.obs2.delay_flags
        if (len(df1) > 0) and (len(df2) > 0):
            k1,k2 = df1.keys(),df2.keys()
            # NB -- multiple delay flags possible... need to determine
            delay = 1e9*(df1[k1[0]]-df2[k2[0]])
            return RNM_Delay(k1[0],k2[0],delay)
        return None

    def __getstate__(self):
        """ Return a lightweight version without data."""
        state = self.__dict__.copy()
        keys = ['AA1','AA2','BB1','BB2','pll_AA','pll_BB']
        for key in keys:
            try:
                state.pop(key)
            except KeyError:
                pass
        return state

    def __cmp__(self,other):
        if other.obs1.fname < self.obs1.fname:
            return 1
        if other.obs1.fname > self.obs1.fname:
            return -1
        return 0

    def valid(self,outfrac=0.1):
        """ Reject the obervation if either of the polarizations exceed
        the number of outliers specified."""
        AA_mask = self.AA_stuff[-1]
        BB_mask = self.BB_stuff[-1]
        ok = (1-outfrac)*len(AA_mask)
        return (float(AA_mask.sum()) > ok) and (float(BB_mask.sum()) > ok)

    def target_fit(self,guess=None,get_err=True):
        if guess is None:
            guess = self.guess
        logl = lambda d: self.pll_AA.loglikelihood(d) + self.pll_BB.loglikelihood(d)
        p0 = fmin(logl,[guess],disp=0)
        if not get_err:
            return p0
        dom = p0[0] + np.linspace(-1e-7,1e-7,3)
        cod = [logl([x]) for x in dom]
        err = (2*np.polyfit(dom,cod,2)[0])**-0.5
        return p0,err

    def subint_target_fit(self,guess=None,get_err=True):
        if guess is None:
            guess = self.guess
        self.tfit_AA = self.pll_AA.subint_target_fit(guess=guess)
        self.tfit_BB = self.pll_BB.subint_target_fit(guess=guess)
        self.tfit_AA_epochs = [x.in_seconds() for x in self.pll_AA.epochs]
        self.tfit_BB_epochs = [x.in_seconds() for x in self.pll_BB.epochs]
        self.tfit_AA_overlaps = self.pll_AA.typical_overlaps
        self.tfit_BB_overlaps = self.pll_BB.typical_overlaps

if __name__ == '__main__':
    import os
    import cProfile
    import observation
    import database
    import util
    import band
    import flagging
    dpath = '/home/ker14a/research/analyses/delays_pipeline/data/'
    robs = [dpath+'r090811_091313.rf']
    sobs = [dpath+'s090811_091313.rf']
    #sobs = [dpath+'s090425_232705.rf']
    #tobs = [dpath+'t090425_232709.rf']
    #sobs = [dpath+'s100920_001617.rf']
    #tobs = [dpath+'t100920_001618.rf']
    sobs = [dpath+'s110731_154334.rf']
    tobs = [dpath+'t110731_154409.rf']
    all_obs = map(observation.Observation,sobs + tobs)
    #all_obs = map(observation.Observation,sobs + robs)
    db = database.Database()
    db.add_correction('corrections/fix_ppta_data.asc')
    db.add_correction('corrections/delays.asc')
    db.add_correction('corrections/twobin_delays.asc')
    flagging.apply_class_flags(db)
    flagging.apply_global_flags(db)
    map(db.add_obs,all_obs)
    idx = 0
    cProfile.runctx("delay = Delay(db.obs[idx],db.obs[idx+1],dofit=True,profile_rho=False,use_new=True)",
        globals(),locals(),'/tmp/profout.asc')

def make_single_correlated(nbin=1024,rho=0.80,shift=0,profile=None,
    s1=1,s2=1,overlap=1):
    """ Simulate two correlated realizations of white noise.

    Overlap governs the degree of common samples in the two time series.
    """
    mu = np.asarray([0,0])
    cov = np.asarray([[s1**2,rho*s1*s2],[rho*s1*s2,s2**2]])
    a,b = np.random.multivariate_normal(mu,cov,nbin).transpose()
    # if overlap is not 1, replace a fraction of the samples with
    # uncorrelated white noise
    if overlap > 1:
        raise ValueError('Cannot have overlap > 1!')
    if overlap < 1:
        nsamp = nbin*(1-overlap)
        a[:nsamp] = np.random.randn(nsamp)*s1
        b[:nsamp] = np.random.randn(nsamp)*s2
    if profile is not None:
        assert(len(profile)==nbin)
        a += profile
        b += profile
    if shift != 0:
        b = profiles.rotated(b,shift)
    return a,b

def make_foldmode_single_correlated(nbin=1024,rho=0.95,shift=0,
    s1=1,s2=1,overlap=1,samples_per_bin=3.141,nperiod=30):
    """ Simulate two correlated realizations of white noise.

    Here, some effort is made to resemble real fold mode data, where the 
    binning produces correlations between bins.

    Overlap governs the degree of common samples in the two time series.

    NB that in "real" data, there will be some decorrelation due to
    sampling the electric field at different times within the Nyquist
    sampling period; I believe this can be accurately encapsulated just
    with the additional white noise of rho < 1.

    Additionally, there will be a small component of extra white noise due
    to the actual delay causing the time intervals to differ.  However,
    this is small compared to a typical sub-integration and can probably
    be ignored.

    Finally, there is the question of clocking samples into bins.  The DFBs
    should always trigger such that their first sample corresponds to the
    leading edge of the bin, and this implementation only satisfies that
    for the "first" time series.  This may lead to some spurious effects
    when the number of samples/bin is small.
    """
    mu = np.asarray([0,0])
    cov = np.asarray([[s1**2,rho*s1*s2],[rho*s1*s2,s2**2]])
    #nperiod = int(30 * samples_per_bin)
    #nsamp = nperiod*nbin
    nsamp = int(round(nperiod*nbin*samples_per_bin))
    a,b = np.random.multivariate_normal(mu,cov,nsamp).transpose()

    raw_phase = np.arange(nsamp,dtype=float)*(1./(nbin*samples_per_bin))
    phase = np.mod(raw_phase,1)
    bins = np.linspace(0,1,nbin+1)
    #phase = np.mod(np.linspace(0,nperiod,nsamp+1)[:-1],1)
    counts = np.histogram(phase,bins=bins)[0]
    a = np.histogram(phase,weights=a,bins=bins)[0]
    a *= counts**-0.5
    #a *= (1./nperiod**0.5)
    #phase = np.mod(np.linspace(shift,nperiod+shift,nsamp+1)[:-1],1)
    # NB this isn't quite right, as DFB would always start on 0 phase
    phase = np.mod(raw_phase+shift,1)
    counts = np.histogram(phase,bins=bins)[0]
    b = np.histogram(phase,weights=b,bins=bins)[0]
    #b *= (1./nperiod**0.5)
    b *= counts**-0.5

    # if overlap is not 1, add the specific amount of white noise in
    # uncorrelated white noise
    if overlap < 1:
        a = np.random.randn(nbin)*(1-overlap) + overlap*a
        a *= ( (1-overlap)**2 + overlap**2 )**-0.5
        b = np.random.randn(nbin)*(1-overlap) + overlap*b
        b *= ( (1-overlap)**2 + overlap**2 )**-0.5
    return a,b

def test_foldmode_coeff(rho=0.9,shift=0.1,n=20,nbin=1024,do_rotate=False):
    """ This it to simulate profiles with a sub-bin offset."""
    rvals = np.empty([3,n])
    for i in xrange(n):
        a,b = make_foldmode_single_correlated(overlap=1.0,shift=shift/nbin,
            nbin=nbin,rho=rho,samples_per_bin=4.891)
        if do_rotate:
            b = profiles.rotated(b,shift/nbin)
        rvals[0,i] = np.corrcoef(a,b)[0,1]
        rvals[1,i] = np.corrcoef(a,np.roll(b,-1))[0,1]
        rvals[2,i] = np.corrcoef(a,np.roll(b,+1))[0,1]
    return rvals

def test_foldmode_coeffs(rho=0.9,n=20,nbin=1024,do_rotate=False):
    # NB periodic after 1 bin
    shifts = np.linspace(0,1,21)
    rvals = np.empty([len(shifts),3])
    for i in xrange(len(shifts)):
        rvals[i] = test_foldmode_coeff(do_rotate=do_rotate,
            rho=rho,n=n,shift=shifts[i],nbin=nbin).mean(axis=1)
    return shifts,rvals

def test_difference_coeff(rho=0.9,shift=0.1,n=20,nbin=1024):
    """ This is to test the covariance and correlation when subtracting."""
    rvals = np.empty([3,n])
    for i in xrange(n):
        a,b = make_foldmode_single_correlated(overlap=1.0,shift=shift/nbin,
            nbin=nbin,rho=rho,samples_per_bin=4.891)
        z = a-b
        rvals[0,i] = np.std(z)
        rvals[1,i] = np.std(z-np.roll(z,1))/2**0.5
        rvals[2,i] = np.corrcoef(z,np.roll(z,1))[0,1]
        #rvals[2,i] = np.corrcoef(z,np.roll(z,-1))[0,1]
    d = 1-shift
    s = shift
    #prediction = [rho*d**2/(s**2+d**2),rho*s**2/(s**2+d**2)]
    return rvals#,prediction

def test_difference_coeffs(rho=0.9,n=20,nbin=1024):
    shifts = np.linspace(0,1,21)
    rvals = np.empty([len(shifts),3])
    for i in xrange(len(shifts)):
        rvals[i] = test_difference_coeff(
            rho=rho,n=n,shift=shifts[i],nbin=nbin).mean(axis=1)
    return shifts,rvals

def test_whitenoise_average(n=20,nbin=1024,overlap=0.1):
    """ This is to simulate sub-integrations with partial overlap."""
    rvals = np.empty(n)
    scale = ((1-overlap)**2 + overlap**2)**0.5
    for i in xrange(n):
        a1 = np.random.randn(nbin)
        t1 = (overlap*a1 + (1-overlap)*np.random.randn(nbin))/scale
        t2 = (overlap*a1 + (1-overlap)*np.random.randn(nbin))/scale
        rvals[i] = np.corrcoef(t1,t2)[0,1]
    #pred = (1-shift)**2/(shift**2 + (1-shift)**2)
    pred = ((overlap)**2/(overlap**2 + (1-overlap)**2))
    #print pred
    return np.mean(rvals),pred

def test_whitenoise_averages(n=20,nbin=1024):
    shifts = np.linspace(0,1,21)
    rvals = np.empty([len(shifts),2])
    for i in xrange(len(rvals)):
        rvals[i] = test_whitenoise_average(n=n,nbin=nbin,overlap=shifts[i])
    return shifts,rvals

def bin_by_bin_cc(a,b):
    assert(len(a)==len(b))
    results = np.empty_like(a)
    for i in xrange(len(a)):
        t = np.roll(b,i)
        results[i] = np.std(a-t)
    return results

def test_sub_method():
    """ Preliminary code to test likelihood as difference of two
    correlated normal variables.
    """
    # tmp for calibrator
    nbin = 1024
    profile = np.zeros(nbin) + 10000
    #dom = np.linspace(-0.5,0.5,nbin)
    #profile += 2*np.exp(-0.5*(dom/0.01)**2)
    profile[384:1024-384] += 100
    a,b = make_single_correlated(rho=0.80,shift=1e-2,nbin=nbin,
        profile=profile,s1=1,s2=2)

    ffta = np.fft.fft(a)[1:]
    fftb = np.fft.fft(b)[1:]
    nbin = len(a)
    shift_arg = np.fft.fftfreq(nbin)[1:]*(2j*np.pi*nbin)
    fft_cross = (ffta.conj()*fftb).astype(np.complex64)

    XA_2 = np.sum(ffta.real**2) + np.sum(ffta.imag**2)
    XB_2 = np.sum(fftb.real**2) + np.sum(fftb.imag**2)
    #XA_2 = np.sum(ffta.real[:513]**2) + np.sum(ffta.imag[:513]**2)
    #XB_2 = np.sum(fftb.real[:513]**2) + np.sum(fftb.imag[:513]**2)

    def loglikelihood(p,get_resids=False):
        #shift,rho,sig1,sig2 = p
        shift,rho = p
        sig1 = 1
        sig2 = 2
        phase_term = np.exp(-shift_arg*shift)
        X12 = np.sum(np.real(fft_cross*phase_term))
        #z = np.sum(np.abs(ffta-fftb*phase_term)**2/nbin)
        z = (XA_2 + XB_2 - 2*X12)/nbin
        v = sig1**2 + sig2**2 - 2*sig1*sig2*rho
        #v = 2-2*rho
        if get_resids:
            return z/v
        return 0.5*(z/v + nbin*np.log(v))

    # this isn't quite there...
    from scipy.optimize import fmin
    #p = fmin(loglikelihood,[1e-2,0.95,1,1])
    p = fmin(loglikelihood,[1e-2,0.95])
    print p
    #p = fmin(loglikelihood,[1e-2,0.8,1,2.1])
    p = fmin(loglikelihood,[1e-2,0.8])
    print p
    

def high_res_cross_corr(p1,p2,xlo=-1e-3,xhi=1e-3,steps=1000):
    X1 = np.fft.fft(p1)
    X2 = np.fft.fft(p2)
    nbin = p1.shape[-1]
    #shift_arg = (np.fft.fftfreq(nbin)*(2j*np.pi*nbin))[:,np.newaxis,np.newaxis,np.newaxis]
    shift_arg = (np.fft.fftfreq(nbin)*(2j*np.pi*nbin))
    dom = np.linspace(xlo,xhi,steps)
    cod = np.empty(steps)
    for i in xrange(steps):
        #cod[i] = np.real(np.sum((X1*(X2*np.exp(shift_arg*dom[i])).conj())[...,1:]))
        cod[i] = np.real(np.sum((X1*(X2*np.exp(shift_arg*dom[i])).conj())[1:]))
    return dom,cod

# TODO -- simulate an actual binning

def make_test(rho=0.80,shift=1e-5,nchan=32,overlap=0.3):
    """ Generate nchan realizations of PairwiseLoglikelihoods."""
    freqs1 = freqs2 = np.linspace(0,100,nchan)
    bw1 = bw2 = freqs1[1]-freqs1[0]
    plls = deque()

    nbin = 1024
    is_cal = False
    profile = np.zeros(nbin)
    if is_cal:
        profile[384:1024-384] += 5

    for f1 in freqs1:
        overlap = 1
        a,b = make_foldmode_single_correlated(rho=rho,shift=shift,
            overlap=overlap,nbin=nbin)
        #a,b = make_single_correlated(rho=rho,shift=shift,
        #    overlap=overlap,nbin=nbin)
        sp1 = SingleProfile(a,f1,bw1,is_cal=is_cal)
        sp2 = SingleProfile(b,f1,bw1,is_cal=is_cal)
        plls.append(PairwiseLogLikelihood(sp1,sp2,overlap))

    return plls


def shift_study(x0=0,x1=2./1024,samples_per_bin=1.5,rho=1):
    """ Make a study of decorrelation as a function of shift."""
    dom = np.linspace(x0,x1,201)
    cod = np.empty_like(dom)
    for i in xrange(len(dom)):
        a,b = make_foldmode_single_correlated(shift=dom[i],rho=rho,
            samples_per_bin=samples_per_bin)
        #cod[i] = np.std(a-profiles.rotated(b,dom[i]))
        cod[i] = np.corrcoef(a,profiles.rotated(b,dom[i]))[0,1]
    return dom,cod

def shift_study2(x0=0,x1=0.1/1024,n=30,samples_per_bin=3.0,rho=1):
    """ Make a study of bias as a function of shift."""
    dom = np.linspace(x0,x1,n+1)
    results = np.empty([len(dom),2])
    for i in xrange(len(dom)):
        plls = make_test(rho=rho,shift=dom[i],nchan=16,overlap=1.0)
        results[i] = np.mean([x.fit(profile_rho=True,x0=dom[i])[0] for x in plls],axis=0)
    return dom,results
        

def analysis(delays,plot_base=0,use_tfit=False,min_overlap=0.):

    # TODO -- make inset plot;
    # TODO -- make "grand" plot showing all delays
    # some kind of sensible labelling / display

    #delays = [x for x in delays if x.obs1['name']=='J1022+1001']
    delays = [x for x in delays if (('CPSR' not in x.obs1['backend']) and ('CPSR' not in x.obs2['backend']))]

    #print 'Valid mask %d --> '%len(delays),
    #delays = [d for d in delays if d.valid()]
    #print '%d'%len(delays)
    
    if not use_tfit:
        d,de = (np.asarray([x.get_offset() for x in delays])*1e9).transpose()
        corrs = np.asarray([x.get_correlation() for x in delays])
        chi2s = np.asarray([x.get_chi2(divide=True) for x in delays])

        corr_mask = corrs > 0.2
        #corr_mask = np.asarray([True]*len(chi2s))
        chi2_mask = chi2s < 1.5
        err_mask = de < 500
        print '%d / %d overlaps have low correlation.'%(
            (~corr_mask).sum(),len(corr_mask))
        print '%d / %d overlaps have poor goodness of fit.'%(
            (~chi2_mask).sum(),len(chi2_mask))

        mask = corr_mask & chi2_mask & err_mask
    else:
        #d = np.asarray([x.tfit[0][0]*x.folding_period for x in delays])*1e9
        #de = np.asarray([x.tfit[1]*x.folding_period for x in delays])*1e9
        d,de = (np.asarray([x.get_offset(use_tfit=True) for x in delays])*1e9).transpose()
        mask = np.ones_like(d,dtype=bool)
        

    delays = np.asarray(delays)[mask]
    d = d[mask]
    de = de[mask]
    bands = np.asarray([str(band.Band(x.obs1)) for x in delays])
    mjds = np.asarray([x.obs1.get_mjd() for x in delays])

    # find unique delay flags
    rnm_delays = np.asarray([delay.get_rnm_delay() for delay in delays])
    unique_rnm_delays = set(filter(lambda x: x is not None,rnm_delays))

    pl.figure(100+plot_base); pl.clf()
    ax_all = pl.axes([0.14,0.10,0.83,0.90])

    total = 0
    for iurd,urd in enumerate(unique_rnm_delays):
        #m = mask & (rnm_delays==urd)
        mask = rnm_delays==urd
        #print urd
        #for x in delays[mask]:
        #    print x.obs1.basename,x.obs2.basename 
        print '%d observations for %s/%s'%(mask.sum(),urd.f1,urd.f2)

        pl.figure(1+iurd+plot_base); pl.clf()
        ax_hi = pl.axes([0.14,0.46,0.83,0.48])
        ax_lo = pl.axes([0.14,0.10,0.83,0.32])
        colors = ['green','blue','red','red',]
        for b,c in zip(['20CM','10CM','40CM','50CM'],colors):
            m = mask & (bands==b)
            if m.sum() == 0:
                continue
            total += m.sum()
            ax_hi.errorbar(mjds[m],d[m],yerr=de[m],ls=' ',marker='o',color=c,mec='None')
            ax_lo.errorbar(mjds[m],d[m],yerr=de[m],ls=' ',marker='o',color=c,mec='None')
            ax_all.errorbar(mjds[m],d[m],yerr=de[m],ls=' ',marker='o',color=c,mec='None')
            ax_all.plot([urd.mjd_start,urd.mjd_stop],[urd.delay]*2,color=c)
        sig = profiles.std_mad(d[mask])
        pulls = (d-np.median(d[mask]))/sig
        if mask.sum() > 1:
            mask = mask & (np.abs(pulls) < 5)
        for iax,ax in enumerate([ax_hi,ax_lo]):
            ax.axhline(urd.delay,color='red')
            ymin = urd.delay-urd.delay_error
            ymax = urd.delay+urd.delay_error
            ax.axhspan(ymin,ymax,color='red',alpha=0.2)
            ax.set_ylabel('Difference (ns)',size='x-large')
            if mask.sum() == 0:
                continue
            wmean = np.average(d[mask],weights=de[mask]**-2)
            werr = np.sum(de[mask]**-2)**-0.5
            if iax==0:
                print '%25s - %25s'%(urd.f1,urd.f2)
                print 'Notional: %.2f +/- %.2f'%(urd.delay,urd.delay_error)
                print 'Observed: %.2f +/- %.2f'%(wmean,werr)
            ax.axhline(wmean,color='k')
            #ax.axhline(wmean-werr,color='k',ls='--')
            #ax.axhline(wmean+werr,color='k',ls='--')
            ax.axhspan(wmean-werr,wmean+werr,color='k',alpha=0.2)
        #ax_hi.axis([mjds.min()-20,mjds.max()+20,urd.delay-5000,urd.delay+5000])
        try:
            ymin = max(wmean-3000,d[mask].min()-30)
            ymax = min(wmean+3000,d[mask].max()+30)
            #ax_hi.axis([mjds.min()-20,mjds.max()+20,wmean-5000,wmean+5000])
            ax_hi.axis([mjds.min()-20,mjds.max()+20,ymin,ymax])
        except ValueError:
            pass
        try:
            #ax_lo.axis([mjds[mask].min()-20,mjds[mask].max()+20,urd.delay-5*sig,urd.delay+5*sig])
            pass
        except ValueError:
            pass
        ax_lo.set_xlabel('MJD',size='x-large')
        ax_hi.set_title('%s - %s'%(urd.f1,urd.f2))
        ax_all.axis([mjds.min()-20,mjds.max()+20,-20500,20500])
    print 'Total: %d'%total

def make_delays(jname,procdb,combo=None,pairs_only=False,cals_too=False,
    cals_only=False,combo_is_regexp=False):
    """ Compute a delay for each observation in the procdb.
    
    If specified (cals_too), additionally compute the delays for the
    associated calibrator files.  Or (cals_only), only use those files.
    """
    procobs = procdb.get_obs(jname,combo=combo,filter_valid=True,
        combo_is_regexp=combo_is_regexp)
    all_matches = dict()
    unique_matches = defaultdict(list)
    for p in procobs:
        matches = procdb.get_overlaps(p)
        if len(matches) == 0:
            continue
        for match in matches:
            # check reverse lookup
            try:
                if all_matches[match.obs.basename] == p.obs.basename:
                    # already processed this with other obs. first
                    continue
            except KeyError:
                unique_matches[p.obs.basename].append(match.obs.basename)
                all_matches[p.obs.basename] = match.obs.basename
                all_matches[match.obs.basename] = p.obs.basename

    # at the conclusion of above snippets, unique_matches contains a list
    # of all of the unique pairs of overlapping observations, while
    # all_matches, an ad hoc construct, contains both arrangements of the
    # pair

    # below a kluge to focus on DFBs for now
    new_matches = dict()
    ok = lambda key: not (key.startswith('w') or key.startswith('m') or key.startswith('n') or key.startswith('p') or key.startswith('b'))
    for key in unique_matches:
        if not ok(key):
            continue
        vals = [v for v in unique_matches[key] if ok(v)]
        if len(vals) == 0:
            continue
        new_matches[key] = vals

    # re-create procobs list from filtered obs list
    matches = sorted(new_matches.keys())
    procobs = deque()
    for ikey,key in enumerate(matches):
        pobs1 = procdb.get_by_basename(key)
        for obs2 in new_matches[key]:
            pobs2 = procdb.get_by_basename(obs2)
            procobs.append((pobs1,pobs2))

    # now, develop a list of unique calibrators
    cal_pairs = deque()
    all_matches = dict()
    if cals_too or cals_only:
        for pair in procobs:
            for d1 in pair[0].cal.get_diodes():
                for d2 in pair[1].cal.get_diodes():
                    if d1.overlap(d2) > 0:
                        try:
                            if all_matches[d1.basename] == d2.basename:
                                continue
                        except KeyError:
                            d1.delay_flags.update(pair[0].obs.delay_flags)
                            d2.delay_flags.update(pair[1].obs.delay_flags)
                            cal_pairs.append((d1,d2))
                            all_matches[d1.basename] = d2.basename
                            all_matches[d2.basename] = d1.basename

    if pairs_only:
        if cals_only:
            return cal_pairs
        obs_pairs = [[x[0].obs,x[1].obs] for x in procobs]
        if cals_too:
            obs_pairs.extend(cal_pairs)
        return obs_pairs

    # below deprecated
    delays = deque()
    for pair in procobs:
        pobs1,pobs2 = procob
        try:
            delay = Delay(pair[0].obs,pair[1].obs)
            delays.append(delay)
        except Exception as e:
            print 'Warning! Encountered the following exception:'
            print e
    return delays



def explore_individual_fits(delay,fignum=1):
    
    f1s,t1s,offs,errs,pulls,corrs,delta_phis,overlaps,mean,err,mask = delay.AA_stuff
    f1s_b,t1s_b,offs_b,errs_b,pulls_b,corrs_b,delta_phis_b,overlaps_b,mean_b,err_b,mask_b = delay.BB_stuff
    # NB f1s and t1s are "over covered" due to multiple channels per sub
    rnm_delay = delay.get_rnm_delay()
    all_f1s = np.append(f1s,f1s_b)
    all_t1s = np.append(t1s,t1s_b)
    all_pulls = np.append(pulls,pulls_b)
    all_corrs = np.append(corrs,corrs_b)
    all_overlaps = np.append(overlaps,overlaps_b)

    pl.figure(fignum); pl.clf()
    pl.plot(f1s,pulls,ls=' ',marker='.',label='AA')
    pl.plot(f1s_b,pulls_b,ls=' ',marker='.',label='BB')
    pl.xlabel('Frequency (MHz)',size='large')
    pl.ylabel('Pulls',size='large')
    if rnm_delay is not None:
        pl.title('%s_%s'%(rnm_delay.f1,rnm_delay.f2))

    pl.figure(fignum+1); pl.clf()
    pl.plot(f1s,corrs,ls=' ',marker='.',label='AA')
    pl.plot(f1s_b,corrs_b,ls=' ',marker='.',label='BB')
    pl.xlabel('Frequency (MHz)',size='large')
    pl.ylabel('Correlation Coefficient',size='large')
    pl.legend(loc='lower right',numpoints=1)
    if rnm_delay is not None:
        pl.title('%s_%s'%(rnm_delay.f1,rnm_delay.f2))

    pl.figure(fignum+2); pl.clf()
    pl.plot(overlaps,corrs,ls=' ',marker='.')
    pl.axis([0,1,pl.axis()[2],pl.axis()[3]])
    pl.xlabel('Predicted Overlap',size='large')
    pl.ylabel('Correlation Coefficient',size='large')
    if rnm_delay is not None:
        pl.title('%s_%s'%(rnm_delay.f1,rnm_delay.f2))

    pl.figure(fignum+3); pl.clf()
    pl.hist(pulls,
        bins=np.linspace(-5,5,41),histtype='step',normed=True,
        color='blue');
    pl.hist(pulls_b,
        bins=np.linspace(-5,5,41),histtype='step',normed=True,
        color='green');
    pl.hist(pulls/np.std(pulls),
        bins=np.linspace(-5,5,41),histtype='step',normed=True,ls='dashed',
        color='blue')
    pl.hist(pulls_b/np.std(pulls_b),
        bins=np.linspace(-5,5,41),histtype='step',normed=True,ls='dashed',
        color='green')
    from scipy.stats import norm
    dom = np.linspace(-5,5,1001)
    pl.plot(dom,norm.pdf(dom),lw=2,color='red')
    pl.xlabel('Pulls')

    pl.figure(fignum+4); pl.clf()
    #pl.hist(corrs/overlaps,histtype='step',normed=True,bins=20);
    #pl.hist(corrs_b/overlaps_b,histtype='step',normed=True,bins=20);
    pl.hist(corrs,histtype='step',normed=True,bins=20);
    pl.hist(corrs_b,histtype='step',normed=True,bins=20);
    pl.axvline(1,color='red',lw=2)
    #pl.hist(all_overlaps,histtype='step',normed=True,bins=20);
    pl.xlabel('Correlation Coefficient')

    pl.figure(fignum+5); pl.clf()
    pl.plot(t1s-t1s.min(),corrs,ls=' ',marker='o')
    pl.plot(t1s_b-t1s_b.min(),corrs_b,ls=' ',marker='o')
    pl.xlabel('Elapsed Time (s)',size='large')
    pl.ylabel('Correlation Coefficient',size='large')
    if rnm_delay is not None:
        pl.title('%s_%s'%(rnm_delay.f1,rnm_delay.f2))
    unique_t1s = sorted(set(t1s))
    for i in xrange(len(unique_t1s)):
        ts = t1s[t1s==unique_t1s[i]] 
        o = overlaps[t1s==unique_t1s[i]]
        pl.plot(np.mean(ts-t1s.min()),np.mean(o),color='red',marker='*',ms=10)

    pl.figure(fignum+6); pl.clf()
    pl.xlabel('Elapsed Time (s)',size='large')
    pl.ylabel('Mean Offset (ns)',size='large')
    if rnm_delay is not None:
        pl.title('%s_%s'%(rnm_delay.f1,rnm_delay.f2))
    if hasattr(delay,'tfit_AA_epochs'):
        ts = np.asarray(delay.tfit_AA_epochs) - delay.tfit_AA_epochs[0]
        ts += 0.5*(ts[1]-ts[0])
        scale = delay.folding_period*1e9
        aa = delay.tfit_AA*scale
        pl.errorbar(ts,aa[:,0],yerr=aa[:,1],color='blue',marker='s',ms=10,ls=' ')
        bb = delay.tfit_BB*scale
        pl.errorbar(ts,bb[:,0],yerr=bb[:,1],color='green',marker='s',ms=10,ls=' ')
    else:
        unique_t1s = sorted(set(t1s))
        tot_a = deque()
        tot_b = deque()
        tote_a = deque()
        tote_b = deque()
        for i in xrange(len(unique_t1s)):
            mask = t1s==unique_t1s[i]
            myoffs = offs[mask]
            myerrs = errs[mask]
            tot_a.append(np.average(myoffs,weights=myerrs**-2))
            tote_a.append(np.sum(myerrs**-2)**-0.5)
            myoffs = offs_b[mask]
            myerrs = errs_b[mask]
            tot_b.append(np.average(myoffs,weights=myerrs**-2))
            tote_b.append(np.sum(myerrs**-2)**-0.5)
        tot_a = np.asarray(tot_a)*delay.folding_period*1e9
        tote_a = np.asarray(tote_a)*delay.folding_period*1e9
        tot_b = np.asarray(tot_b)*delay.folding_period*1e9
        tote_b = np.asarray(tote_b)*delay.folding_period*1e9
        dom = np.asarray(unique_t1s)
        dt = 0.5*(dom[1]-dom[0])
        dom -= (dom[0] - dt)
        pl.errorbar(dom,tot_a,yerr=tote_a,color='blue',marker='o',ms=10,ls=' ')
        pl.errorbar(dom,tot_b,yerr=tote_b,color='green',marker='o',ms=10,ls=' ')

    pl.figure(fignum+7); pl.clf()
    scale = delay.folding_period*1e9
    pl.plot(offs*scale,corrs,ls=' ',marker='o')
    pl.plot(offs_b*scale,corrs_b,ls=' ',marker='o')
    pl.xlabel('Offset (ns)',size='large')
    pl.ylabel('Correlation Coefficient',size='large')
    pl.axis([pl.axis()[0],pl.axis()[1],pl.axis()[2],1.05 if pl.axis()[3]==1 else pl.axis()[3]])
    if rnm_delay is not None:
        pl.title('%s_%s'%(rnm_delay.f1,rnm_delay.f2))

def filter_delay(d):
    """ An adhoc method for getting rid of observations whose deficiencies
    can corrupt delay measurement.
    """
    bad_flags = ['pdfb4_phase_lock','pdfb4_reset_bug','pdfb3_phase_lock',
        'dfb1_phase_lock','pdfb3_commissioning','pdfb3_commissioning',
        'pdfb2_comissioning']
    # do the quarter band flags actually work?
    bad_flags += ['dfb2_quarter_band','dfb3_quarter_band','catastrophic_rfi','bad_config']
    for flag in d.obs1.get_toa_flags() + d.obs2.get_toa_flags():
        for bad_flag in bad_flags:
            if bad_flag in flag:
                return False
    return True

def plot_valid_delays():
    pl.figure(1); pl.clf()
    delays = RNM_Delay.get_valid_delays()
    for delay in delays:
        pl.plot([delay.mjd_start,delay.mjd_stop],[delay.delay]*2)
        

def plot_specific(delays,f1='dfb3_J0437_55319_56160',f2='pdfb4_55319_56055_cals',fignum=20,use_tfit=True,min_overlap=0):
    """ Plot time offsets as a function of "pulsar number" and of time.

    The idea is to show pulsar-dependence of particular delay measurements.
    Within each pulsar-specific segment, the points are time-ordered.
    """
    print '%s / %s'%(f1,f2)
    specific = [d for d in delays if d.get_rnm_delay() is not None and d.get_rnm_delay().f1==f1 and d.get_rnm_delay().f2==f2]
    if len(specific) == 0:
        print 'Reversing configurations...'
        f1,f2 = f2,f1
        specific = [d for d in delays if d.get_rnm_delay() is not None and d.get_rnm_delay().f1==f1 and d.get_rnm_delay().f2==f2]

    toffs,terrs = np.asarray([x.get_offset(use_tfit=use_tfit,min_overlap=min_overlap) for x in specific]).transpose() * 1e9
    mask1 = ~np.isnan(toffs)
    print 'nanmask: %d/%d'%(np.sum(mask1),len(mask1))
    specific = [x for x,y in zip(specific,mask1) if y]
    toffs = toffs[mask1]
    terrs = terrs[mask1]

    be1 = np.asarray([x.obs1['be:config'] for x in specific])
    be2 = np.asarray([x.obs2['be:config'] for x in specific])
    periods = np.asarray([x.folding_period for x in specific])
    phoffs,pherrs = np.asarray([x.get_phase_offset(use_tfit=use_tfit,min_overlap=min_overlap) for x in specific]).transpose() * 1e3
    tbc = np.asarray([x._two_bin_correction()*x.folding_period for x in specific])*1e9
    phtbc = np.asarray([x._two_bin_correction() for x in specific]) * 1e3
    delta_phi = np.asarray([np.mean(x.AA_stuff[6])*x.folding_period for x in specific])*1e9+tbc
    jnames = np.asarray([x.obs1['name'] for x in specific])
    mjds = np.asarray([x.obs1.get_mjd() for x in specific])
    unique_names = sorted(set(jnames))
    mad_delays = deque()
    specific_sort = np.empty(len(specific),dtype=int)
    cmask = np.asarray([check_subint_consistency(d,use_tfit=use_tfit) for d in specific])
    print 'cmask: %d/%d'%(np.sum(cmask),len(cmask))

    pl.figure(fignum); pl.clf()
    pl.figure(fignum+1); pl.clf()
    def s20(s,n=20):
        return ' '*(n-len(s))+s
    def s10(s):
        return s + ' '*(11-len(s))
    from plotting import get_tableau20
    colors = get_tableau20()
    for iname,name in enumerate(unique_names):
        mask = jnames==name
        b1 = util.most_common(be1[mask])
        b2 = util.most_common(be2[mask])
        print '%s be1=%s be2=%s'%(s10(name),s20(b1),s20(b2))
        a = np.argsort(mjds[mask])
        specific_sort[mask] = np.arange(len(specific))[mask][a]
        #if name=='J1909-3744':
            #mb1 = be1[mask][a]; mb2 = be2[mask][a]
            #for i in xrange(len(mb1)):
                #print '%s be1=%s be2=%s'%(name,s20(mb1[i]),s20(mb2[i]))
        pl.figure(fignum)
        color=colors[iname%len(colors)]
        cm = cmask[mask][a]
        pl.errorbar(np.arange(len(mask))[mask][cm],(toffs[mask][a])[cm],
            yerr=(terrs[mask][a])[cm],marker='.',color=color)
        pl.errorbar(np.arange(len(mask))[mask][~cm],(toffs[mask][a])[~cm],
            yerr=(terrs[mask][a])[~cm],marker='x',color=color,ls=' ')
        #pl.plot(np.arange(len(mask))[mask],toffs[mask][a]-tbc[mask][a],marker='.')
        #pl.plot(np.arange(len(mask))[mask],phoffs[mask][a]+phtbc[mask][a],marker='.')
        #pl.plot(np.arange(len(mask))[mask],phoffs[mask][a],marker='.')
        pl.axhline((np.asarray(specific)[mask][0]).get_rnm_delay().delay,
            color='k')
        pl.figure(fignum+1)
        pl.plot(np.arange(len(mask))[mask],tbc[mask][a],marker='.',
            color=color)
        #pl.plot(np.arange(len(mask))[mask],phtbc[mask][a],marker='.')
        #pl.plot(np.arange(len(mask))[mask],delta_phi[mask][a],marker='.')
        mad_delays.append([
            name,np.median(toffs[mask]),np.median(periods[mask]),
            dms[name],
            int(b1.split('_')[1]),int(b2.split('_')[1])])

    pl.figure(fignum)
    pl.ylabel('Difference (ns)')
    pl.title('%s - %s'%(f1,f2))
    pl.figure(fignum+1)
    pl.ylabel('Difference (ns)')
    pl.title('%s - %s'%(f1,f2))
    return list(np.asarray(specific)[specific_sort]),mad_delays

def plot_specific2(delays,be1s=['pdfb2_1024_256_1024'],be2s=['pdfb4_1024_256_1024'],fignum=22,use_tfit=True,min_overlap=0,show_legend=True,show_title=True):
    """ Plot all realizations of two configurations.

    Highlight different pulsars.
    """
    specific = []
    combos = []
    for ibe1 in xrange(len(be1s)):
        for ibe2 in xrange(len(be2s)):
            be1,be2 = be1s[ibe1],be2s[ibe2]
            spec = [d for d in delays if d.obs1['be:config']==be1 and d.obs2['be:config']==be2]
            if len(spec) == 0:
                #print 'Reversing configurations...'
                be1,be2 = be2,be1
                spec = [d for d in delays if d.obs1['be:config']==be1 and d.obs2['be:config']==be2]
            if len(spec) > 0:
                specific.extend(spec)
                combos.append([be1,be2])
    be1 = be1s[0]; be2 = be2s[0]
    print len(specific)

    toffs = np.asarray([x.get_offset(use_tfit=use_tfit,min_overlap=min_overlap)[0] for x in specific]) * 1e9
    mask1 = ~np.isnan(toffs)
    specific = [x for x,y in zip(specific,mask1) if y]
    toffs=toffs[mask1]

    periods = np.asarray([x.folding_period for x in specific])
    phoffs = np.asarray([x.get_phase_offset(use_tfit=use_tfit,min_overlap=min_overlap)[0] for x in specific]) * 1e3
    tbc = np.asarray([x._two_bin_correction()*x.folding_period for x in specific])*1e9
    phtbc = np.asarray([x._two_bin_correction() for x in specific]) * 1e3
    delta_phi = np.asarray([np.mean(x.AA_stuff[6])*x.folding_period for x in specific])*1e9+tbc
    jnames = np.asarray([x.obs1['name'] for x in specific])
    mjds = np.asarray([x.obs1.get_mjd() for x in specific])
    rnms = [x.get_rnm_delay() for x in specific]
    rnms = np.asarray([x.delay if x is not None else 0 for x in rnms])
    unique_names = sorted(set(jnames))
    mad_delays = deque()
    specific_sort = np.empty(len(specific),dtype=int)
    cmask = np.asarray([check_subint_consistency(d,use_tfit=use_tfit) for d in specific])

    from plotting import get_tableau20
    colors = get_tableau20()
    pl.figure(fignum); pl.clf()
    def s20(s):
        return ' '*(20-len(s))+s
    for iname,name in enumerate(unique_names):
        mask = jnames==name
        #print '%s be1=%s be2=%s'%(name,s20(b1),s20(b2))
        a = np.argsort(mjds[mask])
        cm = cmask[mask][a]
        specific_sort[mask] = np.arange(len(specific))[mask][a]
        color = colors[iname]
        pl.plot(mjds[mask][a][cm],toffs[mask][a][cm],marker='.',
            color=color,ls='-',label=name)
        pl.plot(mjds[mask][a][~cm],toffs[mask][a][~cm],marker='x',
            color=color,ls='')
        pl.plot(mjds[mask][a],rnms[mask][a],marker='o',
            color=color,ls='')

    pl.axvline(54947.97,color='k')
    pl.axvline(55319.18,color='k')
    pl.axvline(56055,color='k')
    pl.axvline(56110,color='k')
    pl.axvline(56160.18,color='k')

    if show_legend:
        pl.legend(numpoints=1,loc='upper left',ncol=2,prop={'size':'small'})
    pl.ylabel('Difference (ns)')
    pl.xlabel('MJD')
    if show_title:
        title_string = '\n'.join(['%s-%s'%(x[0],x[1]) for x in combos])
        #pl.title('%s - %s'%(be1,be2))
        pl.title(title_string,size='small')
    return specific

def good_plots(delays,combo=4,use_tfit=False,min_overlap=0,fignum=20):
    if combo == 0:
        f1='dfb3_J0437_55319_56160';f2='pdfb4_55319_56055_cals'
    elif combo == 1:
        f1='pdfb3_256MHz_1024ch';f2='pdfb4_256MHz_1024ch'
    elif combo == 2:
        f1='pdfb3_256MHz_1024ch';f2='pdfb2_256MHz_1024_ch'
    elif combo == 3:
        f1='pdfb3_256MHz_2048ch';f2='pdfb2_256MHz_2048_ch'
    elif combo == 4:
        f1='pdfb4_256MHz_1024ch';f2='pdfb2_256MHz_1024_ch'
    elif combo == 5:
        f1='pdfb2_1024_MHz';f2='pdfb4.*_1024_[1,2]...'
    elif combo == 6:
        f1='pdfb3_1024_MHz';f2='pdfb4.*_1024_[1,2]...'
    elif combo == 7:
        f1='dfb3_J0437_55319_56160';f2='pdfb4_56055_56110_cals'
    elif combo == 8:
        f1='dfb3_J0437_55319_56160';f2='pdfb4_56110_56160_cals'
    elif combo == 9:
        f1='dfb3_J0437_56160_60000';f2='pdfb4_56160_60000_cals'
    elif combo == 10:
        f1='pdfb2_1024_MHz';f2='pdfb4_55319_56055_cals'
    else:
        f1='dfb3_J0437_55319_56160';f2='pdfb4_56055_56110_cals'
    return plot_specific(delays,f1=f1,f2=f2,use_tfit=use_tfit,
        min_overlap=min_overlap,fignum=fignum)

def comp_backends(delays,be1='PDFB3',be2='PDFB4',show_legend=True,fignum=1):
    from plotting import get_tableau20
    colors = get_tableau20()
    markers = ['x','^','*']
    accepted = deque()
    for delay in delays:
        obs1,obs2 = delay.obs1,delay.obs2
        if (obs1['backend']==be1 and obs2['backend']==be2) or (obs2['backend']==be1 and obs1['backend']==be2):
            accepted.append(delay)
    mjds = np.asarray([d.obs1['mjd'] for d in accepted])
    bes1 = np.asarray([d.obs1['be:config'] for d in accepted])
    ubes1 = set(bes1)
    bes2 = np.asarray([d.obs2['be:config'] for d in accepted])
    ubes2 = set(bes2)
    a = np.argsort(mjds)
    mjds = mjds[a]
    bes1 = bes1[a]
    bes2 = bes2[a]
    vals = np.asarray([d.get_offset()[0] for d in accepted])[a]
    rnms = np.zeros_like(vals)
    for i in xrange(len(accepted)):
        q = accepted[i].get_rnm_delay()
        if q is not None:
            rnms[i] = q.delay
    rnms = rnms[a]
    pl.figure(fignum); pl.clf()
    counter = 0
    counts = []
    for ube1 in ubes1:
        for ube2 in ubes2:
            mask = (bes1 == ube1) & (bes2 == ube2)
            counts.append([ube1,ube2,mask.sum()])
    for idx in np.argsort([x[2] for x in counts])[::-1]:
        ube1,ube2,count = counts[idx]
        mask = (bes1 == ube1) & (bes2 == ube2)
        if not np.any(mask):
            continue
        def s14(s):
            return s + ' '*(14-len(s))
        u1 = '_'.join(ube1.split('_')[1:])
        u2 = '_'.join(ube2.split('_')[1:])
        if u1.split('_')[1] == '64':
            continue
        #if u1.split('_')[1] == '1024':
        #    continue
        #if u1 != u2:
        #    continue
        print counter,count,ube1,ube2
        color = colors[counter%len(colors)]
        marker = markers[counter/len(colors)]
        counter += 1
        label = '%s + %s'%(u1,u2)
        pl.plot(mjds[mask],vals[mask]*1e9,ls=' ',marker=marker,
            color=color,label=label)
        pl.plot(mjds[mask],rnms[mask],ls=' ',marker='o',
            color=color)
    if show_legend:
        pl.legend(numpoints=1,ncol=1,fontsize='x-small',loc='lower right')
    pl.axvline(54947.97,color='k')
    pl.axvline(55319.18,color='k')
    pl.axvline(56160.18,color='k')
    #pl.axis([54700,56900,-1300,250])

# TODO -- check out 
#stuff = offsets.good_plots(delays,combo=0)
#offsets.explore_individiual_fits(stuff[0][24])
# is this observation actually bad, or a case of a skipped correlator cycle?
def check_subint_consistency(self,use_tfit=True,thresh=2):
    """ Check that the estimates from individual subints give the same
    results as those for the total.  In cases where phase lock is a
    problem, these values will drift.
    """
    if use_tfit and hasattr(self,'tfit_AA'):
        scale = self.folding_period*1e9
        tot_a = self.tfit_AA[:,0]*scale
        tote_a = self.tfit_AA[:,1]*scale
    else:
        f1s,t1s,offs,errs,pulls,corrs,delta_phis,overlaps,mean,err,mask = self.AA_stuff
        f1s_b,t1s_b,offs_b,errs_b,pulls_b,corrs_b,delta_phis_b,overlaps_b,mean_b,err_b,mask_b = self.BB_stuff
        unique_t1s = sorted(set(t1s))
        tot_a = deque()
        tote_a = deque()
        for i in xrange(len(unique_t1s)):
            mask = t1s==unique_t1s[i]
            myoffs = np.append(offs[mask],offs_b[mask])
            myerrs = np.append(errs[mask],errs_b[mask])
            tot_a.append(np.average(myoffs,weights=myerrs**-2))
            tote_a.append(np.sum(myerrs**-2)**-0.5)
        tot_a = np.asarray(tot_a)*self.folding_period*1e9
        tote_a = np.asarray(tote_a)*self.folding_period*1e9
    m1 = np.average(tot_a,weights=tote_a**-2)
    pulls = (tot_a-m1)/tote_a
    if np.std(pulls) > thresh:
        return False
    return True

def make_a(n=1024,c=0.1):
    a = np.eye(n)
    for i in xrange(n-1):
        a[i,i+1] = a[i+1,i] = c
    a[n-1,0] = a[0,n-1] = c
    return a

class CorrEigen(object):
    """ Generate eigenvalues and eigenvectors for the nearest-neighbour
    pulse profiles case.

    Note that the eigenvectors depend only very loosely on the matrix
    parameters and so can be largely precomputed.
    """

    evecs = dict()
    eval_cache = dict()

    def __init__(self,n=1024):
        self.make(n)

    def make(self,n):

        dom = np.arange(0,n,dtype=float)
        thetas = (2*np.pi/n)*dom

        evecs = np.empty((n,n))
        k = (0.5*n)**-0.5
        for i in xrange(1,n/2):
            x = dom*thetas[i]
            evecs[:,i] = k*np.cos(x)
            evecs[:,-i] = -k*np.sin(x)
        evecs[:,0] = n**-0.5
        evecs[:,n/2] = n**-0.5*np.cos(dom*np.pi)

        CorrEigen.evecs[n] = np.matrix(evecs)
        CorrEigen.eval_cache[n] = np.cos(thetas)

    def get_evecs(self,n=1024):
        if n not in CorrEigen.evecs.keys():
            self.make(n)
        return CorrEigen.evecs[n]

    def get_evals(self,b,c,n=1024):
        if n not in CorrEigen.evecs.keys():
            self.make(n)
        return b + (2*c)*self.eval_cache[n]

class CorrLikelihood(object):
    
    ce = None
    
    def __init__(self,a,b):

        n = len(a)
        self.a = a
        self.b = b
        self.ce = CorrEigen(n=n)
        self.evecs = self.ce.get_evecs(n)

        self._cache = dict()

        # populate the cache with zero shift

    def get_vals(self,d,idx=None):
        if idx is None:
            idx = int(round(d))
            #idx = int(d)
        if idx not in self._cache.keys():
            # pre-multiply data by eigenvectors
            z = self.a-np.roll(self.b,-idx)
            x =  np.ravel(self.evecs*np.matrix(z).T)
            # self.vals will be multiplied by eigenvalues
            self._cache[idx] = x*x
        return idx,self._cache[idx]

    def loglikelihood(self,p,rho=1,force_idx=None):
        d = p[0]
        if len(p) > 1:
            rho = p[1]

        idx,vals = self.get_vals(d,idx=force_idx)

        d -= idx
        d = abs(d)

        # NB -- since we are swapping data at 0.5 increments, the diagonal
        # covariance decreases through delta = 0.5, then increases again
        # through to the profiles being aligned again and the diagonal
        # covariance matrix; this symmetry is taken care of by subtracting
        # the index above.

        # diagonal of covariance matrix of z
        b = 2*(1-rho*(1-d))
        c = -rho*d
        evals = self.ce.get_evals(b,c,n=len(vals))

        # determinant of the covariance matrix
        det = np.sum(np.log(evals))

        # "chi^2" term
        chi2 = np.sum(vals/evals)

        return 0.5*(det+chi2)

    
def test_eigvals(a,b=1):

    n = a.shape[0]
    c = a[0,1]

    #evals,evecs = np.linalg.eigh(a)
    evecs = np.empty((n,n))
    evals = np.empty(n)
    dom = np.arange(0,n,dtype=float)

    thetas = (2*np.pi/n)*dom
    evals = b + (2*c)*np.cos(thetas)


    k = (0.5*n)**-0.5
    for i in xrange(1,n/2):
        x = dom*thetas[i]
        evecs[:,i] = k*np.cos(x)
        evecs[:,-i] = -k*np.sin(x)


    """
    for i in xrange(n):
        if (i == 0) or (i == n/2):
            k = (n)**-0.5
        else:
            k = (0.5*n)**-0.5
        #if i%2 == 0:
        if i <= n/2:
            evecs[:,i] = k*np.cos(dom*th)
        else:
            evecs[:,i] = -k*np.sin(dom*th)
            #evecs[:,i] = k*np.cos(dom*th)
    """
    return a,evals,evecs

    lam = np.eye(n)
    lam[np.diag_indices(n)] = evals
    lam = np.matrix(lam)

    # evecs*(lam*evecs.T) = a
            
    return a,evals,np.matrix(evecs),lam

def get_cov_matrix(nbin=128,rho=0.9,n=100,shift=0.1):
    cov_matrix = np.zeros((nbin,nbin))
    variance = np.empty(n)
    for i in xrange(n):
        a,b = make_foldmode_single_correlated(shift=shift/nbin,
            rho=rho,samples_per_bin=4.5,nbin=nbin)
        z = a-b
        variance[i] = np.std(z)
        cov_matrix += np.outer(z,z)
    return cov_matrix / n, np.mean(variance)

def test_cov_matrix():
    shifts = np.linspace(0,1,11)
    rvals = np.empty((shifts.shape[0],2))
    for i in xrange(len(shifts)):
        m,v = get_cov_matrix(shift=shifts[i])
        m1 = np.mean(np.diagonal(m,offset=0))
        m2 = np.mean(np.diagonal(m,offset=1))
        rvals[i] = [m1,m2]
    return shifts,rvals

def fit_foldmode(a,b,x0,rho0,fit_rho=False,get_ll=False):
    #p = fmin(foldmode_loglikelihood,[x0,rho0],args=(a,b))
    ll = CorrLikelihood(a,b)
    if fit_rho:
        p = fmin(ll.loglikelihood,[x0,rho0],disp=0)
    else:
        p = fmin(ll.loglikelihood,[x0],args=(rho0,),disp=0)
    if get_ll:
        return p,ll
    return p

def foldmode_loglikelihood_slow(p,a,b,rho=1,get_c=False):
    """ Construct the covariance matrix."""
    n = len(a)
    c = np.zeros((2*n,2*n))
    d = p[0]
    # first, roll the input as necessary
    roll_idx = round(d*n)
    if roll_idx != 0:
        b = np.roll(b,-int(roll_idx)).copy()
    d -= roll_idx/n
    if d < 0:
        a,b = b,a
        d = -d
    if len(p) > 1:
        rho = p[1]
    overlap = (1-d*n) # fraction of phase bin; 1 for complete alignment
    #denom = 1./((1-overlap)**2 + overlap**2)
    #rho_11 = rho*(1-overlap)**2*denom
    #rho_12 = rho*(overlap)**2*denom
    rho_11 = overlap*rho
    rho_12 = (1-overlap)*rho

    for i in xrange(0,2*n-1,2):
        c[i,i] = 1
        c[i,i+1] = rho_11
        c[i,i-1] = rho_12
    for i in xrange(1,2*n-1,2):
        c[i,i] = 1
        c[i,i+1] = rho_12
        c[i,i-1] = rho_11
    c[2*n-1,2*n-1] = 1
    c[2*n-1,2*n-2] = rho_11
    # handle periodic conditions
    c[0,2*n-1] = rho_12
    c[2*n-1,0] = rho_12

    if get_c:
        return c

    # form likelihood
    v = np.empty(2*n)
    v[::2] = b
    v[1::2] = a

    from scipy.linalg import cholesky,solve_triangular
    #L = cholesky(c)
    L = np.linalg.cholesky(c).transpose().copy()
    det = np.sum(np.log(np.diag(L)))
    L_i = solve_triangular(L,np.eye(L.shape[0]),overwrite_b=True)
    t = np.asarray(v*np.matrix(L_i))[0]

    # NB the factors of 2 cancel from the L = sqrt(C) and the sqrt(det(C))
    return det + 0.5*np.inner(t,t)

    #return c
    #ci = np.linalg.inv(c)
    #return ci,v
    y = np.linalg.solve(c,v)
    #print np.log(d)
    return np.sum(v*y)
    #return y,v,d
    #d = np.linalg.det(c)
    return 0.5*(np.log(d) + np.sum(v*y))

def fit_foldmode_slow(a,b,x0,rho0):
    #p = fmin(foldmode_loglikelihood,[x0,rho0],args=(a,b))
    p = fmin(foldmode_loglikelihood_slow,[x0],args=(a,b,rho0),ftol=0.1)
    return p

def foldmode_loglikelihood_censored(p,a,b,rho=1,overlap=1):
    """ Construct the covariance matrix ignoring boundary conditions by
    truncating the final entries."""
    n = len(a)
    d = p[0]
    # first, roll the input as necessary
    roll_idx = round(d*n)
    if roll_idx != 0:
        b = np.roll(b,-int(roll_idx)).copy()
    d -= roll_idx/n
    if d < 0:
        a,b = b,a
        d = -d
    if len(p) > 1:
        rho = p[1]
    if len(p) > 2:
        s1,s2 = p[2],p[3]
    else:
        s1,s2 = 1.,1.
    if len(p) > 4:
        m1,m2 = p[4],p[5]
    else:
        m1,m2 = 0.,0.

    # subint overlap
    o = overlap
    rho_eff = rho*(o**2/(o**2+(1-o)**2))
    #print overlap,rho,rho_eff

    bin_overlap = (1-d*n) # fraction of phase bin; 1 for complete alignment
    #denom = 1./((1-overlap)**2 + overlap**2)
    #rho_11 = rho*(1-overlap)**2*denom
    #rho_12 = rho*(overlap)**2*denom
    rho_11 = bin_overlap*rho_eff
    rho_12 = (1-bin_overlap)*rho_eff

    # form a banded matrix (lower format)
    t = np.empty((2,2*(n-1)))
    t[0,0::2] = s1
    t[0,1::2] = s2
    s = (s1*s2)**0.5
    t[1,0:-1:2] = rho_11*s
    t[1,1:-1:2] = rho_12*s
    
    # form likelihood
    v = np.empty(2*(n-1))
    v[::2] = b[:-1]-m2
    v[1::2] = a[:-1]-m1

    #Lt = cholesky_banded(t,check_finite=False,lower=True)
    Lt = cholesky_banded(t,lower=True)
    det = np.sum(np.log(Lt[0]))
    #q = solve_banded((1,0),Lt,v,check_finite=False,overwrite_b=True)
    q = solve_banded((1,0),Lt,v,overwrite_b=True)

    # NB the factors of 2 cancel from the L = sqrt(C) and the sqrt(det(C))
    return det + 0.5*np.inner(q,q)

def foldmode_loglikelihood_censored_careful(p,a,b,rho=1,overlap=1):
    """ Construct the covariance matrix ignoring boundary conditions by
    truncating the final entries.  This version avoids the data swapping
    of the main version, by using a five-banded covariance matrix.
    
    The results come out the same (as they should!) but kept as a sanity
    check.
    """
    n = len(a)
    d = p[0]
    """
    # first, roll the input as necessary
    roll_idx = round(d*n)
    if roll_idx != 0:
        b = np.roll(b,-int(roll_idx)).copy()
    d -= roll_idx/n
    if d < 0:
        a,b = b,a
        d = -d
    """
    if len(p) > 1:
        rho = p[1]
    if len(p) > 2:
        s1,s2 = p[2],p[3]
    else:
        s1,s2 = 1.,1.
    if len(p) > 4:
        m1,m2 = p[4],p[5]
    else:
        m1,m2 = 0.,0.

    # subint overlap
    o = overlap
    rho_eff = rho*(o**2/(o**2+(1-o)**2))
    #print overlap,rho,rho_eff

    bin_overlap = (1-abs(d)*n) # fraction of phase bin; 1 for alignment
    #denom = 1./((1-overlap)**2 + overlap**2)
    #rho_11 = rho*(1-overlap)**2*denom
    #rho_12 = rho*(overlap)**2*denom
    rho_11 = bin_overlap*rho_eff
    rho_12 = (1-bin_overlap)*rho_eff

    # form a banded matrix (lower format)
    t = np.empty((4,2*(n-1)))
    t[0,0::2] = s1
    t[0,1::2] = s2
    s = (s1*s2)**0.5
    t[1,0:-1:2] = rho_11*s
    if d > 0:
        t[1,1:-1:2] = rho_12*s
        t[2:,:] = 0.
    else:
        t[1,1:-1:2] = 0.
        t[2:,:] = 0.
        t[3,0:-3:2] = rho_12*s

    # form likelihood
    v = np.empty(2*(n-1))
    v[::2] = b[:-1]-m2
    v[1::2] = a[:-1]-m1

    #Lt = cholesky_banded(t,check_finite=False,lower=True)
    Lt = cholesky_banded(t,lower=True)
    det = np.sum(np.log(Lt[0]))
    #q = solve_banded((1,0),Lt,v,check_finite=False,overwrite_b=True)
    q = solve_banded((3,0),Lt,v,overwrite_b=True)

    # NB the factors of 2 cancel from the L = sqrt(C) and the sqrt(det(C))
    return det + 0.5*np.inner(q,q)

def fit_foldmode_censored(a,b,x0,rho0,fit_rho=False,fit_sig=False,
        fit_mean=False,get_ll=False,overlap=1.0):
    logl = foldmode_loglikelihood_censored
    if fit_rho:
        if fit_sig:
            if fit_mean:
                p = fmin(logl,[x0,rho0,1.,1.,0.,0.],args=(a,b,1,overlap),
                        disp=0)
            else:
                p = fmin(logl,[x0,rho0,1.,1.],args=(a,b,1,overlap),disp=0)
        else:
            p = fmin(logl,[x0,rho0],args=(a,b,1,overlap),disp=0)
    else:
        p = fmin(logl,[x0],args=(a,b,rho0,overlap),disp=0)
    if get_ll:
        return p,ll
    return p

def test_overlap(rho0=0.9,nbin=1024,shift=-0.3):
    shift = shift/nbin
    #dom = np.linspace(-2.1,2.1,41)/nbin
    dom = np.arange(10,dtype=float)[1:]/10
    n = len(dom)
    cod = np.zeros((len(dom),n,2))
    for i in xrange(len(dom)):
        print dom[i]
        for j in xrange(n):
            a,b = make_foldmode_single_correlated(nbin=nbin,
                overlap=dom[i],shift=shift,rho=rho0)
            cod[i,j] = fit_foldmode_censored(a,b,shift,rho0,fit_rho=True,
                fit_sig=False,fit_mean=False,overlap=dom[i])

    return dom,cod

def test_foldmode(rho0=0.9,nbin=1024,n=10,overlap=0.7,fit_nuisance=False,
    fine_grid=False):
    if fine_grid:
        dom = np.linspace(-0.1,0.1,41)/nbin
        nbin *= 2
    else:
        dom = np.linspace(-2.1,2.1,41)/nbin
    cod1 = np.zeros((len(dom),n,6 if fit_nuisance else 2))
    err1 = np.zeros((len(dom),n))
    cod2 = np.zeros((len(dom),n))
    cod3 = np.zeros((len(dom),n,6 if fit_nuisance else 2))
    import time
    for i in xrange(len(dom)):
        t1 = time.time()
        print dom[i]*nbin
        for j in xrange(n):
            a,b = make_foldmode_single_correlated(nbin=nbin,
                overlap=overlap,shift=dom[i],rho=rho0)
            sp1 = SingleProfile(a,1369,0.5)
            sp2 = SingleProfile(b,1369,0.5)
            pll = PairwiseLogLikelihood(sp1,sp2,overlap)
            #pll2 = PairwiseLogLikelihood2(sp1,sp2,overlap)
            pll3 = PairwiseLogLikelihood2(sp1,sp2,overlap)
            #p,r = fit_foldmode(a,b,dom[i]*nbin,rho0,fit_rho=True)
            cod3[i,j] = fit_foldmode_censored(a,b,dom[i],rho0,fit_rho=True,
                fit_sig=fit_nuisance,fit_mean=fit_nuisance,overlap=overlap)
            #cod3[i] += fit_foldmode(a,b,dom[i]*nbin,rho0,fit_rho=True)
            #cod1[i] += pll3.fit(x0=0.,rho0=rho0,profile_rho=False)[0]
            x0 = pll.fit(x0=0.,rho0=rho0)[0][0]
            cod2[i,j] = x0
            #cod1[i] += pll3.fit(x0=dom[i],rho0=rho0,profile_rho=False)[0]
            #cod1[i,j] = pll3.fit(x0=0,rho0=rho0,profile_rho=False)[0]
            #cod1[i,j],err1[i,j] = pll3.fit(x0=0.,rho0=rho0,
            #    profile_rho=False,fit_nuisance=fit_nuisance,get_err=True,
            #    do_coarse=True,focus=False)
            cod1[i,j],err1[i,j] = pll3.fit(x0=dom[i],rho0=rho0,
                profile_rho=False,fit_nuisance=fit_nuisance,get_err=True,
                #do_coarse=not fine_grid,focus=fine_grid,no_grid=fine_grid)
                do_coarse=True,focus=False,no_grid=False)
            #cod1[i] += pll3.fit(x0=0.,rho0=rho0,profile_rho=False)[0]
        t2 = time.time()
        print 'Time = %.2f'%(t2-t1)

    return dom,cod1,cod2,cod3,err1

def grid_loglikelihood(logl,xvals,yvals):
    """ Make an image of log likelihood as function of delta and rho."""
    rvals = np.empty((len(xvals),len(yvals)))
    for i,ival in enumerate(xvals):
        for j,jval in enumerate(yvals):
            rvals[i,j] = logl([ival,jval])
    return rvals

def load_delays(jnames=None,exclude_0437=False,override_path=None):
    ppath = 'delay_pickles' if override_path is None else override_path
    if jnames is None:
        os.system('ls %s > /tmp/pnames.asc'%ppath)
        jnames = map(str.strip,file('/tmp/pnames.asc').readlines())
    jnames = sorted(jnames)
    delays = deque()
    for jname in jnames:
        if exclude_0437 and (jname=='J0437-4715'):
            continue
        fnames = glob.glob('%s/%s/*pickle'%(ppath,jname))
        pickles = deque()
        for fname in fnames:
            try:
                pickles.append(cPickle.load(file(fname)))
            except EOFError:
                print 'Failed to load %s.'%fname
        delays.extend(filter(filter_delay,pickles))
    return delays

