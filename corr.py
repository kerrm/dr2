"""
Implement classes for testing and correcting observations..

$Header: /nfs/slac/g/glast/ground/cvs/users/kerrm/tpipe/timing.py,v 1.14 2014/01/21 06:46:59 kerrm Exp $

author: Matthew Kerr <matthew.kerr@gmail.com>
"""

# classes for defining operations that "fix" things

from collections import deque
from itertools import imap
import operator
import re
import pyfits

import flagging
import alias
import zapping
from util import local_file,readlines

operators = {
    '==':operator.eq,
    '!=':operator.ne,
    '>':operator.gt,
    '<':operator.lt,
    '>=':operator.ge,
    '<=':operator.le,
    '+=':operator.add,
    '-=':operator.sub,
}

class Criterion(object):
    """ Encapsulate a criterion for a cut to be applied."""

    operators = ['==','!=','<=','<','>=','>']

    def __init__(self):
        pass

    def __call__(self,obsn):
        pass

class Action(object):
    """ Encapsulate an action on an observation."""

    operators = ['+=','-=','=']

    def init(self):
        self.verbosity = 1
    
    def __init__(self):
        self.init()

    def __call__(self,obsn,*args):
        pass

class Correction(object):

    def __init__(self,desc,criteria,actions):
        self.description = desc
        self.criteria = criteria
        self.actions = actions

    def __str__(self):
        s = '\n'.join([self.description,
            '\tCriteria','\n\t\t'.join(imap(str,self.criteria)),
            '\tActions','\n\t\t'.join(imap(str,self.actions))])
        return s


    def __call__(self,obsn):
        """ Apply correction if appropriate."""
        #if not obsn.valid():
            #return False
        for criterion in self.criteria:
            if not criterion(obsn):
                return False
        obsn.comment(self.description)
        for action in self.actions:
            action(obsn,self.description)
        return True

class check_keyword(Criterion):
    """ Check that a header keyword is a particular value."""

    def __str__(self):
        return 'Checking that %s %s %s.'%(self.k,self.comp_symbol,self.v)

    def __init__(self,keyword,value,comp='=='):
        self.k = keyword
        self.v = value
        if not hasattr(self.k,'__iter__'):
            self.k = [self.k]
        if not hasattr(self.v,'__iter__'):
            self.v = [self.v]
        if len(self.k) > 1:
            if len(self.v) > 1:
                raise ValueError('Multiple keywords imply one comparison value!')
        if len(self.v) > 1:
            if len(self.k) > 1:
                raise ValueError('Multiple comparison values imply one keyword!')
        self.comp = operators[comp]
        self.comp_symbol = comp

    def __call__(self,obsn):
        """ Check possible key/val pairs against observation's value.

        If multiple values are provided, accept a match if any match the
        observation value.

        Special cases:
            -- bandwidth is often negative (lower sideband) but we generally
                only care if the magnitudes are equal
        """
        for k in self.k:
            try:
                obsn_val = obsn[k]
            except KeyError:
                return False
            # it's unclear if we actually want to ignore the sideband
            #if obsn._key(k)==obsn.key('bandwidth'):
                ## ignore sideband
                #obsn_val = abs(obsn_val)
            t = type(obsn_val)
            try:
                for v in imap(t,self.v):
                    if self.comp(obsn_val,v):
                        return True
            except TypeError:
                # catch case of "None"
                return False
        return False

class check_keyword_re(Criterion):
    """ Check that a header keyword is a particular value."""

    def __str__(self):
        return 'Checking that %s matches pattern %s.'%(self.k,self.re.pattern)

    def __init__(self,keyword,regexp):
        self.k = keyword
        self.re = re.compile(regexp)

    def __call__(self,obsn):
        obsn_val = obsn[self.k]
        if type(obsn_val) is not type(''):
            raise ValueError('Can only use regular expressions with strings.')
        return self.re.match(obsn_val) is not None

class check_mjd(Criterion):
    """ Determine if an observation falls within an MJD range."""

    def __init__(self,args):
        self.mjd_0 = args[1]
        self.mjd_1 = args[2] 

    def __call__(self,obsn):
        t0,t1 = self.mjd_0,self.mjd_1
        #print 'Checking that obsn is beteen MJD %.2f and %.2f.'%(t0,t1)
        obs_mjd = obsn.get_mjd()
        #print 'Observation has %.2f.'%(obs_mjd)
        if (obs_mjd >= t0) and (obs_mjd < t1):
            return True
        return False

class check_extension(Criterion):
    """ Check the extension on a file."""

    def __init__(self,ext):
        if ext[0] != '.':
            self.ext = '.' + ext
        else:
            self.ext = ext

    def __call__(self,obs):
        return obs.ext==self.ext

class check_filename(Criterion):
    """ Check that filename is in a list of filenames."""

    def __init__(self,filelist):
        self.fnames = readlines(local_file(filelist))

    def __call__(self,obsn):
        return obsn.basename in self.fnames

class change_keyword(Action):
    """ Update a keyword entry in a PSRFITS header."""

    def __str__(self):
        return 'Changing %s %s %s.'%(self.k,self.assgn,self.v)

    def __init__(self,keyword,value,assgn='='):
        self.init()
        self.k = keyword
        self.v = value
        self.assgn = assgn

    def __call__(self,obsn,*args):
        try:
            t = type(obsn[self.k])
        except KeyError:
            if self.assgn != '=':
                raise ValueError('Cannot modify missing value %s!'%self.k)
            else:
                print 'Warning! Value for %s missing.  Attempting to guess!'%self.k
            if (self.k=='rcvr:basis') or (self.k=='FD_POLN'):
                t = str
            elif ('FD_' in self.k) or ('rcvr' in self.k):
                t = int
            elif self.k=='BE_DELAY':
                t = float
            else:
                raise KeyError('Could not determine type of %s.'%self.k)
        if self.assgn=='=':
            obsn[self.k] = t(self.v)
        elif self.assgn=='+=':
            obsn[self.k] = old_val + t(self.v)
        elif self.assgn=='-=':
            obsn[self.k] = old_val - t(self.v)
        else:
            raise ValueError('assgn not recognized')

class fix_cpsr2(Action):

    def __call__(self,obsn,*args): 
        # (1) if necessary add a "J" to the source name
        n = obsn['name']
        if n[0] != 'J':
            # do a crude check that it's actually a jname
            if len(n) >= 5:
                sign = n.strip()[4]
                if sign=='-' or sign=='+':
                    obsn['name'] = 'J'+obsn['name']
            else:
                print 'Found a curious name: %s'%(n)
        # (2) put in the correct receiver name
        freq = obsn['frequency']
        if (freq >= 300) and (freq < 800):
            obsn['receiver'] = '1050CM'
        elif (freq > 2900) and (freq < 3700):
            obsn['receiver'] = '1050CM'
        elif obsn['receiver'] == 'GALILEO':
            # a few observations taken with this receiver
            pass
        else:
            # NB fixed transpose error from fix.C 53328<-->53334
            hoh_dates = [[52980,53250],[53328,53334],
                         [53835,53837],[54065,54228]]
            mjd = obsn.get_mjd()
            f = lambda x: (mjd >= x[0]) and (mjd <= x[1])
            hoh = sum(imap(f,hoh_dates)) > 0
            obsn['receiver'] = 'H-OH' if hoh else 'MULTI'
            if (freq > 1300) and (freq < 1360):
                obsn['backend'] = 'CPSR2m'
            else:
                obsn['backend'] = 'CPSR2n'

class fix_obs_kw(Action):

    # NB -- I don't think we actually need this.  Let PSRCHIVE do.

    def __call__(self,obsn,*args):
        f = pyfits.open(obsn.fname,memmap=True)
        try:
            if obsn['OBSFREQ'] == 0:
                obsn['OBSFREQ'] = f['HISTORY'].data.field('CTR_FREQ')[0]
            if obsn['OBSNCHAN'] == 0:
                obsn['OBSNCHAN'] = f['HISTORY'].data.field('NCHAN')[0]
            if obsn['OBSBW'] == 0:
                ch_bw = f['HISTORY'].data.field('CHAN_BW')[0]
                # by FITS definition, this must be positive
                obsn['OBSBW'] = abs(ch_bw*obsn['nchan'])
        except IndexError:
            print 'Obs %s has no CTR_FREQ etc.'%obsn.basename
        f.close()

class flip_sideband(Action):
    """ Change the sense of the frequency channels.

    (1) Alter the order of the subint frequency channels.
    (2) Update the appropriate header keywords.
    """
    
    def __call__(self,obsn,*args):
        """ This is a little subtle.  We write out the psrsh script command
            to flip the sideband, but we also need to update the data
            loaded into memory with the header WITHOUT generating additional
            psrsh.

            For now, don't worry about DAT_FREQS, because the data aren't
            actually loaded in the Observation objects.
        """
        obsn.set('bandwidth',-obsn['bandwidth'])
        obsn.command('fix freq sideband')

class shift_centre_freq(Action):
    """ Change the labels of the frequency channels and OBSFREQ keyword.
    """
    def __init__(self,delta):
        self.init()
        self.delta = delta

    def __call__(self,obsn,*args):
        """ Both emit the psrsh to use and update the Observation object
        in memory.
        """
        obsn.set('frequency',obsn['frequency']+float(self.delta))
        obsn.command('fix freq offset %s'%self.delta)

class change_type(Action):
    """ Change an observation type.
    
    This requires a special translation from the FITS header values to the
    internal language of PSRFITS.
    """

    def __init__(self,*args):
        self.init()
        self.to_type = t = args[0]
        if t == 'PSR' or t == 'LEVPSR':
            self.to_psrfits_type = 'Pulsar'
        elif t == 'CAL' or t == 'LEVCAL':
            self.to_psrfits_type = 'PolnCal'
        elif t == 'FOF':
            self.to_psrfits_type = 'FluxCalOff'
        elif t == 'FON':
            self.to_psrfits_type = 'FluxCalOn'
        else:
            raise IndexError('Could not parse type %s.'%t)

    def __call__(self,obsn,*args):
        obsn.set('obs_mode',self.to_type)
        obsn.command('edit type=%s'%self.to_psrfits_type)

class fix_dfb1_offsets(Action):
    """ Apply a correction to STT_OFFS depending on number of channels.

        DEPRECATED in favor of delays table.
    """

    def __call__(self,obsn,*args):

        raise NotImplementedError('DEPRECATED!')

        if obsn['OBS_MODE'] != 'PSR':
            return
        # use bandpass channels as proxy for "original" no. of channels
        nchan = obsn['nchan']
        # taken from Jonathan Khoo's fix.C/fix_ppta_data
        corrs = {
            32:   0.0000012969,
            128:  0.0000035547,
            512:  0.0000122813,
            2048: 0.0000468829
        }
        try:
            delta_stt_offs = corrs[nchan]
        except KeyError:
            print 'Unrecognized number of channels %d.'%nchan
            return
        stt_offs = obsn['ext:stt_offs']
        obsn['ext:stt_offs'] = stt_offs - delta_stt_offs

class flag_data(Action):
    """ Simply apply flag to data via Observation/add_data_flag.
    
    This action is invoked by the special @flag notation."""

    def __init__(self,flag):
        self.init()
        self.flag_object = flagging.flag_factory(flag)

    def __call__(self,obsn,*args):
        obsn.add_data_flag(self.flag_object)
        if self.verbosity > 0:
            print '\033[91m',
            print '%s flag applied to %s.'%(self.flag_object.flag(),
                obsn.basename)
            print '\033[0m',

class fix_delay(Action):
    
    def __init__(self,rhs,sep):
        """ Process the right hand side of the delay statement."""
        self.init()
        self.op = operators[sep]
        self.tbin = False
        if 'ns' in rhs:
            # nanoseconds
            self.delay = 1e-9*float(rhs.split('ns')[0])
        elif 'us' in rhs:
            # microseconds
            self.delay = 1e-6*float(rhs.split('us')[0])
        elif 'ms' in rhs:
            self.delay = 1e-3*float(rhs.split('ms')[0])
        elif 's' in rhs:
            # is this stringent enough?
            self.delay = float(rhs.split('s')[0])
        elif 'tbin' in rhs:
            self.tbin = True
            self.delay = int(rhs.split('tbin')[0])
        else:
            raise ValueError('Did not understand delay %s %s.'%(sep,rhs))
        # this can be overriden to put delay somewhere else
        self.key = 'STT_OFFS'

    def _get_delay(self,obsn):
        if self.tbin:
            # handle files with TBIN = *
            try:
                delay = self.delay * float(obsn['tbin'])
            except ValueError:
                print 'Could not correct delay for %s!'%obsn.basename
                obsn.add_data_flag('no_dly')
                delay = 0.
        else:
            delay = self.delay
        return delay

    def __call__(self,obsn,*args):
        mode = obsn['OBS_MODE']
        if mode != 'PSR':
            print 'Called fix_delay on observation of type %s!'%(mode)
            return
        obsn[self.key] = self.op(obsn[self.key],self._get_delay(obsn))

class fix_alias_name(Action):

    def __call__(self,obsn,*args):
        # handle alias names
        preferred_name = alias.preferred_name(obsn['name'])
        if obsn['name'] != preferred_name:
            obsn['name'] = preferred_name

class fix_delay_toa(fix_delay):
    """ Handle delays at the TOA level with flags."""

    def __init__(self,rhs,sep):
        super(fix_delay_toa,self).__init__(rhs,sep)

    def __call__(self,obsn,*args):
        """ Don't emit psrsh, but update attributes in the observation."""
        mode = obsn['OBS_MODE']
        if mode != 'PSR':
            print 'Called fix_delay_toa on observation of type %s!'%(mode)
            return
        # determine flag to add -- NB now only write/overwrite flags
        flag = args[0].split()[1]
        obsn.delay_flags[flag] = self.op(0.,self._get_delay(obsn))

class fix_mb_cable_swap(Action):
    """ Generate psrsh to fix the data collected when A <--> B.

    TODO -- for now just flag as bad.
    """
    def __call__(self,obsn,*args):
        #obsn.command('fix feed_swap')
        obsn.comment('MB cable swap')
        obsn.add_data_flag('bad_config')

class fix_dfb3_quarter_band(Action):
    """ Zap the last nchan/4 channels.
    """
    def  __call__(self,obsn,*args):
        nchan = obsn['nchan']
        obsn.command('zap chan %d-%d'%(nchan-nchan/4-1,nchan-1))
        #TODO -- add a data quality flag?

class zap_dfb4_first_subints(Action):
    """ Zap the first sub-int of PDFB4 observations before MJD 55319.

    The timestamping of the first sub-integration is apparently off, such
    that if one installs a new ephemeris, its phase is shifted relative
    to the remaining ones.  For now, just get rid of it.
    """

    def  __call__(self,obsn,*args):
        print 'Zapping PDFB4 first sub-int for %s.'%obsn.basename
        obsn.comment('Zap first sub-int for DFB4 before 55319 due to incorrect timestamp.')
        obsn.command('zap subint 0')

class fix_early(Action):
    """A general catch-all for fixing data from pre-PPTA instruments,
    including early filterbanks, the multi-beam filterbank, and FPTM I/II.

    NB -- these are based off of a local version of the files that have
    already been processed to some extent.  If a re-processing of the raw
    data becomes available, they should be re-written.
    """

    def __call__(self,obsn,*args):

        freq = obsn.get_centre_frequency()
        mjd = obsn.get_mjd()
        bw = abs(obsn['bandwidth'])
        
        # this will generally be a filterbank file, i.e. collected in
        # "search mode" and later folded with an ephemeris.  All of the
        # early filterbanks have a 1-bit digitiser
        isfb = obsn.basename.startswith('f') or obsn.basename.startswith('g') or obsn.basename.startswith('p')
        isfptm = obsn.basename.startswith('c') or obsn.basename.startswith('d')
        if not (isfb or isfptm):
            return

        if ('PSR' in obsn['backend']):
            return
        if ('DFB' in obsn['backend']):
            return
        if ('CPSR' in obsn['backend']):
            return

        backend_ok = False

        # Handle some early 20cm data as special case
        if (mjd < 49609) and (freq == 1520):
            obsn.comment('Early 20cm data with old frontend and 2x64x5MHz analog filterbank.')
            obsn['backend'] = 'FB320-5000'
            obsn['receiver'] = '20CMFET'
            return
        
        if isfptm:
            if (mjd < 49920):
                obsn.comment('FPTMI a single IF system before July 1995.')
                obsn['backend'] = 'FPTM-I'
            else:
                obsn.comment('FPTMII a 2-IF (2x128 MHz) system from July 1995.')
                obsn['backend'] = 'FPTM-II'
                backend_ok = True

        # 70CM data was taken with both a filterbank and FPTM system
        if abs(freq - 436) < 12:# and (mjd < 49543):
            obsn.comment('Data taken with 70CM system.')
            obsn['receiver'] = '70CM'
            if (bw == 32 and isfb):
                    obsn.comment('Used a 2x256x0.125MHz filterbank.')
                    obsn['backend'] = 'FB32-0125'
                    backend_ok = True

        if abs(freq - 660) < 12:
            obsn.comment('Data taken with old 50CM system.')
            obsn['receiver'] = '50CM'
            if (bw == 32 and isfb):
                obsn.comment('Used a 2x128x0.250MHz filterbank.')
                obsn['backend'] = 'FB32-0250'
                backend_ok = True

        if (abs(freq - 685) < 12) and (mjd > 52944):
            obsn.comment('50cm data taken with co-axial 1050CM system.')
            obsn['receiver'] = '1050CM'
            
        # handle general 20cm case, deciding between multi-beam or H-OH
        if (freq > 1200) and (freq < 1720):

            if isfb:
                if bw==320:
                    obsn.comment('Used a 2x64x5MHz analog filterbank.')
                    obsn['backend'] = 'FB320-5000'
                    backend_ok = True
                elif (bw==288 or bw==576):
                    obsn['backend'] = 'AFB'
                    backend_ok = True
                elif bw==256:
                    obsn.comment('Used a 2x256x0.5MHz analog filterbank.')
                    obsn['backend'] = 'FB256-0500'
                    backend_ok = True
                elif bw==128:
                    obsn.comment('Used a 2x128x1.0MHz analog filterbank.')
                    obsn['backend'] = 'FB128-1000'
                    backend_ok = True

            if mjd < 50465:
                obsn['receiver'] = 'H-OH'
            elif mjd < 50849:
                obsn['receiver'] = 'MULTI'
            elif mjd < 51220:
                obsn['receiver'] = 'H-OH'
            elif mjd < 52959:
            #elif mjd < 52980:
                obsn['receiver'] = 'MULTI'
            #elif mjd < 53256:
            elif mjd < 53250:
                obsn['receiver'] = 'H-OH'
            #elif mjd < 54091:
            elif mjd < 54065:
                obsn['receiver'] = 'MULTI'
            elif mjd < 54228:
                obsn['receiver'] = 'H-OH'
            else:
                obsn['receiver'] = 'MULTI'

        if not backend_ok:
            obsn.comment('Generic filterbank.')
            obsn['backend'] = 'FB'

def apply_global_corrections(db):
    """ Corrections that should be applied to all observations."""
    db.add_correction(local_file('corrections/fix_ppta_data.asc'))

