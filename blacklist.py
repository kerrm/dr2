"""
Handle exclusion of observations on a per-file basis.

$Header: /nfs/slac/g/glast/ground/cvs/users/kerrm/tpipe/timing.py,v 1.14 2014/01/21 06:46:59 kerrm Exp $

author: Matthew Kerr <matthew.kerr@gmail.com>
"""

import re
from os.path import basename

from util import local_file

# NB -- this is largely deprecated, handled by flagging.py now.

dfb_name = re.compile(r'^[a-z][0-9]{6}_[0-9]{6}')

def read_file_re(fname):
    """ Read a file and only accept lines matching the DFB regexp."""
    return map(str.strip,
            filter(lambda line: dfb_name.match(line) is not None,
              file(fname).readlines()))


class ASCIIBlacklist(object):
    """ Convert a flat file into a black list (by filename).
    """ 
    def __init__(self,blacklist):
        self.fnames = set(
            [x.split('.')[0] for x in read_file_re(blacklist)])

    def flag_database(self,database):
        flags = 0
        obs = database.get_valid()
        basenames = map(lambda ob: ob.basename.split('.')[0],obs)
        for bn,ob in zip(basenames,obs):
            if bn in self.fnames:
                flags += 1
                ob.add_data_flag('blacklisted',comment=bn)
        print 'Flagged %d observations.'%flags
        return flags

global_blacklists = map(ASCIIBlacklist,[
    local_file('obslists/badobs.asc')
])

def apply_global_blacklists(db):
    for bl in global_blacklists:
        bl.flag_database(db)
    return db

